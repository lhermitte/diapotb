#!/bin/bash

export OTB_APPLICATION_PATH=${install_directory}/lib:$OTB_APPLICATION_PATH
export LD_LIBRARY_PATH=${install_directory}/lib/:$LD_LIBRARY_PATH
export PYTHONPATH=${install_directory}/python_src/:$PYTHONPATH
export DIAPOTB_INSTALL=${install_directory}
export OTB_LOGGER_LEVEL=WARNING
