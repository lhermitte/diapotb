set(DOCUMENTATION "OTB module for SAR processing in Diapason.")

# OTB_module() defines the module dependencies in SARTimeDomain
# SARTimeDomain depends on OTBCommon and OTBApplicationEngine
# The testing module in ExternalTemplate depends on OTBTestKernel
# and OTBCommandLine

# define the dependencies of the include module and the tests
otb_module(DiapOTBModule
  DEPENDS
    OTBCommon
    OTBBoost
    OTBApplicationEngine
    OTBConvolution
    OTBMetadata
  TEST_DEPENDS
    OTBTestKernel
    OTBCommandLine
  DESCRIPTION
    "${DOCUMENTATION}"
)
