/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARCompensatedComplexFromInterferogramImageFilter_h
#define otbSARCompensatedComplexFromInterferogramImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "itkSimpleFastMutexLock.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"

#include "otbImageKeywordlist.h"


namespace otb
{
/** \class SARCompensatedComplexFromInterferogramImageFilter 
 * \brief Creates an temporary image (to estimate conjugate multiplication) from interferogram. 
 * 
 * This filter built the conjugate multiplication between a master and a slave image with only the
 * interferogram as input.
 *
 * The input and output are vector images with real part, imag part and modulus of 
 * conjugate multiplication.
 *
 * \ingroup DiapOTBModule
 */

  template <typename TImageIn, typename TImageOut> 
  class ITK_EXPORT SARCompensatedComplexFromInterferogramImageFilter :
    public itk::ImageToImageFilter<TImageIn,TImageOut>
{
public:

  // Standard class typedefs
  typedef SARCompensatedComplexFromInterferogramImageFilter                    Self;
  typedef itk::ImageToImageFilter<TImageIn,TImageOut>    Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;
  typedef itk::SmartPointer<const Self>                  ConstPointer;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(SARCompensatedComplexFromInterferogramImageFilter,ImageToImageFilter);

  /** Typedef to image input type :  otb::VectorImage (Interferogram)  */
  typedef TImageIn                                  ImageInType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageInType::Pointer             ImageInPointer;
  typedef typename ImageInType::ConstPointer        ImageInConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageInType::RegionType          ImageInRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageInType::PixelType           ImageInPixelType;
  typedef typename ImageInType::PointType           ImageInPointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageInType::IndexType           ImageInIndexType;
  typedef typename ImageInType::IndexValueType      ImageInIndexValueType;
  typedef typename ImageInType::SizeType            ImageInSizeType;
  typedef typename ImageInType::SizeValueType       ImageInSizeValueType;
  typedef typename ImageInType::SpacingType         ImageInSpacingType;
  typedef typename ImageInType::SpacingValueType    ImageInSpacingValueType;
  
  /** Typedef to image output type : otb::VectorImage  */
  typedef TImageOut                                  ImageOutType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageOutType::Pointer             ImageOutPointer;
  typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageOutType::RegionType          ImageOutRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageOutType::PixelType           ImageOutPixelType;
  typedef typename ImageOutType::PointType           ImageOutPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageOutType::IndexType           ImageOutIndexType;
  typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
  typedef typename ImageOutType::SizeType            ImageOutSizeType;
  typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
  typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
  typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;


  // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;


  // Typedef for iterators
  typedef itk::ImageScanlineConstIterator< ImageInType > InputIterator;
  typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;



protected:
  // Constructor
  SARCompensatedComplexFromInterferogramImageFilter();

  // Destructor
  virtual ~SARCompensatedComplexFromInterferogramImageFilter() ITK_OVERRIDE;

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
  /** SARCompensatedComplexFromInterferogramImageFilter produces an image which are into output geometry. The differences between 
   * output and input images can be the size of images and the dimensions. 
   * As such, SARCompensatedComplexFromInterferogramImageFilter needs to provide an implementation for 
   * GenerateOutputInformation() in order to inform the pipeline execution model. 
   */ 
  virtual void GenerateOutputInformation() ITK_OVERRIDE;

  /** SARCompensatedComplexFromInterferogramImageFilter needs a input requested region for all inputs.
   * As such, SARCompensatedComplexFromInterferogramImageFilter needs to provide an implementation for 
   * GenerateInputRequestedRegion() in order to inform the pipeline execution model. 
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  //virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;


  /** 
   * SARCompensatedComplexFromInterferogramImageFilterr can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData() routine
   * which is called for each processing thread. The main output image data is
   * allocated automatically by the superclass prior to calling
   * ThreadedGenerateData().  ThreadedGenerateData can only write to the
   * portion of the output image specified by the parameter
   * "outputRegionForThread"
   *
   * \sa ImageToImageFilter::ThreadedGenerateData(),
   *     ImageToImageFilter::GenerateData() */
  virtual void ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread, 
				    itk::ThreadIdType threadId) ITK_OVERRIDE;
  

 private:
  SARCompensatedComplexFromInterferogramImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 

  // SAR Dimensions
  int m_nbLinesSAR;
  int m_nbColSAR;

  

};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARCompensatedComplexFromInterferogramImageFilter.txx"
#endif



#endif
