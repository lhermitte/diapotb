/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARGroupedByOrthoImageFilter_txx
#define otbSARGroupedByOrthoImageFilter_txx

#include "otbSARGroupedByOrthoImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"
#include "itkContinuousIndex.h"

#include "otbConfigurationManager.h"

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "ossim/base/ossimGpt.h"
#include "ossim/base/ossimEcefPoint.h"
#pragma GCC diagnostic pop
#else
#include "ossim/base/ossimGpt.h"
#include "ossim/base/ossimEcefPoint.h"
#endif

#include <cmath>
#include <algorithm>


namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImageVector, class TImageDEM, class TImageOut> 
  SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >::SARGroupedByOrthoImageFilter()
    :  m_SarSensorModelAdapter(ITK_NULLPTR), m_geoidEmg96(NULL), m_DEMPtr(ITK_NULLPTR), m_Gain(1.), 
       m_Margin(1000)
  {
    // Inputs required 
    this->SetNumberOfRequiredInputs(2);
 
    DEMHandlerPointerType DEMHandler = DEMHandler::Instance();
    
    m_geoidEmg96 = 0;
    const std::string isEmg96 =  "egm96";

    if (!(ConfigurationManager::GetGeoidFile().empty()))
      {
	// DEMHandler instance to specify the geoid file
	DEMHandler->OpenGeoidFile(ConfigurationManager::GetGeoidFile()); 

	// If Geoid by default (emg96) instanciate directly a ossimGeoidEgm96 (increase performance)
	std::size_t found = ConfigurationManager::GetGeoidFile().find(isEmg96);
	if (found!=std::string::npos)
	  {
	    m_geoidEmg96 = new ossimGeoidEgm96(ossimFilename(ConfigurationManager::GetGeoidFile()));
	  }
      }

    m_Mutex = new MutexType();    
  }
    
  /** 
   * Destructor
   */
  template <class TImageVector, class TImageDEM, class TImageOut> 
  SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >::~SARGroupedByOrthoImageFilter()
  {
    if (m_geoidEmg96)
      {
	delete m_geoidEmg96;
	m_geoidEmg96 = 0;
      }

    delete m_Mutex;
    m_Mutex = 0;

  }

  /**
   * Print
   */
  template<class TImageVector, class TImageDEM, class TImageOut>
  void
  SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);
  }

  /**
   * Set Master Image
   */ 
  template<class TImageVector, class TImageDEM, class TImageOut>
  void
 SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >
  ::SetCartesianMeanInput(const ImageVectorType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(0, const_cast<ImageVectorType *>(image));
  }

  /**
   * Set Coregistrated Slave Image
   */ 
  template<class TImageVector, class TImageDEM, class TImageOut>
  void
 SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >
  ::SetCompensatedComplexInput(const ImageVectorType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(1, const_cast<ImageVectorType *>(image));
  }

  /**
   * Get Master Image
   */ 
  template<class TImageVector, class TImageDEM, class TImageOut>
  const typename SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >::ImageVectorType *
  SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >
  ::GetCartesianMeanInput() const
  {
    if (this->GetNumberOfInputs()<1)
      {
	return 0;
      }
    return static_cast<const ImageVectorType *>(this->itk::ProcessObject::GetInput(0));
  }

  /**
   * Get Coregistrated Slave Image
   */ 
  template<class TImageVector, class TImageDEM, class TImageOut>
  const typename SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >::ImageVectorType *
  SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >
  ::GetCompensatedComplexInput() const
  {
    if (this->GetNumberOfInputs()<2)
      {
	return 0;
      }
    return static_cast<const ImageVectorType *>(this->itk::ProcessObject::GetInput(1));
  }

/**
 * Set Sar Image keyWordList
 */ 
template<class TImageVector, class TImageDEM, class TImageOut>
void
SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >
::SetSARImageKeyWorList(ImageKeywordlist sarImageKWL)
{
  m_SarImageKwl = sarImageKWL;
}

/**
 * Set DEM Pointer
 */ 
template<class TImageVector, class TImageDEM, class TImageOut>
void
SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >
::SetDEMImagePtr(ImageDEMPointer demPtr)
{
  m_DEMPtr = demPtr;
}

  /**
   * Method GenerateOutputInformaton()
   **/
  template<class TImageVector, class TImageDEM, class TImageOut>
  void
  SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >
  ::GenerateOutputInformation()
  {
    // Call superclass implementation
    Superclass::GenerateOutputInformation();

    // Get pointer to output
    ImageOutPointer outputPtr = this->GetOutput();
  
    /////////////////////// Main Output : interferogram (into Ground geometry) ///////////////////////
    // Vector Image  :
    // At Least 4 Components :  
    //                _ amplitude
    //                _ phase
    //                _ coherency
    //                _ occurrence
    outputPtr->SetNumberOfComponentsPerPixel(4);

    // The output is defined with the DEM Image
    // Origin, Spacing and Size (Ground geometry)
    ImageOutPointType outOrigin;
    outOrigin = m_DEMPtr->GetOrigin();
    
    // Define Output Largest Region
    ImageOutRegionType outputLargestPossibleRegion = m_DEMPtr->GetLargestPossibleRegion();
    outputLargestPossibleRegion.SetSize(m_DEMPtr->GetLargestPossibleRegion().GetSize());
    outputPtr->SetLargestPossibleRegion(outputLargestPossibleRegion);
    outputPtr->SetOrigin(outOrigin);
    outputPtr->SetSignedSpacing(m_DEMPtr->GetSignedSpacing());

    // Projection Ref
    outputPtr->SetProjectionRef(m_DEMPtr->GetProjectionRef());
    
    // Create and Initilaze the SarSensorModelAdapter
    m_SarSensorModelAdapter = SarSensorModelAdapter::New();
    bool loadOk = m_SarSensorModelAdapter->LoadState(m_SarImageKwl);
    
    if(!loadOk || !m_SarSensorModelAdapter->IsValidSensorModel())
      {
	itkExceptionMacro(<<"SAR image does not contain a valid SAR sensor model.");
      }

    // Add Bands meaning into keyWordList
    ImageKeywordlist outputKWL;
    outputKWL.AddKey("support_data.band.Amplitude", std::to_string(0));
    outputKWL.AddKey("support_data.band.Phase", std::to_string(1));
    outputKWL.AddKey("support_data.band.Coherency", std::to_string(2));

    // Set new keyword list to output image
    outputPtr->SetImageKeywordList(outputKWL);    

    m_nbLinesSAR = this->GetCompensatedComplexInput()->GetLargestPossibleRegion().GetSize()[1];
    m_nbColSAR = this->GetCompensatedComplexInput()->GetLargestPossibleRegion().GetSize()[0];
  }

/** 
* Method OutputREgionToInputRegion
*/
template<class TImageVector, class TImageDEM, class TImageOut>
typename SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >::ImageVectorRegionType 
SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >
::OutputRegionToInputRegion(const ImageOutRegionType& outputRegion, 
			    bool & intoInputImage) const
{
  // Compute the input requested region (size and start index)
  // Use the image transformations to insure an input requested region
  ImageOutSizeType  outputRequestedRegionSize = outputRegion.GetSize();
  ImageOutIndexType outputRequestedRegionIndex = outputRegion.GetIndex();
  
  // Here we are breaking traditional pipeline steps because we need to access the input field data
  // so as to compute the input image requested region
  // The direct localisation (SAR to DEM) is performed in order to determine the DEM region (input) 
  // represented by the SAR region (output)
  
  int nbColSAR = m_nbColSAR;
  int nbLinesSAR = m_nbLinesSAR;   

  // Margin to apply on direct localisation for the four sides.
  int margin = m_Margin;
  
  // Indice for the SAR bloc (determined by the Pipeline)
  ImageOutIndexType id[4] ;
  id[0][0] = outputRequestedRegionIndex[0];
  id[0][1] = outputRequestedRegionIndex[1];
  id[1][0] = outputRequestedRegionIndex[0];
  id[1][1] = outputRequestedRegionIndex[1] + outputRequestedRegionSize[1];
  id[2][0] = outputRequestedRegionIndex[0] + outputRequestedRegionSize[0];
  id[2][1] = outputRequestedRegionIndex[1];
  id[3][0] = outputRequestedRegionIndex[0] + outputRequestedRegionSize[0];
  id[3][1] = outputRequestedRegionIndex[1] + outputRequestedRegionSize[1];
  
  Point2DType pixelSAR_Into_MNT_LatLon(0);
  Point2DType pixelSAR(0);
  itk::ContinuousIndex<double,2> pixelSAR_Into_MNT;
  
  Point3DType demGeoPoint;
  Point2DType demLatLonPoint;
  Point2DType col_row(0);
  Point2DType y_z(0);
  ossimGpt gptPt;

  // Initialie the first and last DEM indice
  int firstL, firstC, lastL, lastC; 
  
  firstL = nbLinesSAR;
  lastL = 0;
  firstC = nbColSAR;
  lastC = 0;
  intoInputImage = true;

  
  // For each side of the output region
  for (int i = 0; i < 4; i++) 
    {
      // Transform index to Lat/Lon Point
      m_DEMPtr->TransformIndexToPhysicalPoint(id[i], demLatLonPoint);
      demGeoPoint[0] = demLatLonPoint[0];
      demGeoPoint[1] = demLatLonPoint[1];
      
      // Get elevation from earth geoid thanks to DEMHandler or ossimgeoidEmg96
      double h = 0;
      if (m_geoidEmg96)
	{
	  gptPt.lon =  demLatLonPoint[0];
	  gptPt.lat =  demLatLonPoint[1];
	  h = m_geoidEmg96->offsetFromEllipsoid(gptPt);
	}
      demGeoPoint[2] = h;

      // Call the method of sarSensorModelAdapter
      m_SarSensorModelAdapter->WorldToLineSampleYZ(demGeoPoint, col_row, y_z);
      
      // Assigne the limits
      if (firstL > col_row[1])
	{
	  firstL = col_row[1];
	}
      
      if (lastL < col_row[1])
	{
	  lastL = col_row[1];
	}

      if (firstC > col_row[0])
	{
	  firstC = col_row[0];
	}
      
      if (lastC < col_row[0])
	{
	  lastC = col_row[0];
	}

    }

  // Apply the marge
  firstL -= margin;
  firstC -= margin;
  lastL +=  margin;
  lastC +=  margin;

  // Check the limits
  if (firstC < 0)
    {
      firstC = 0;
    } 
  if (firstL < 0)
    {
      firstL = 0;
    } 
  if (lastC > nbColSAR-1)
    {
      lastC = nbColSAR-1;
    } 
  if (lastL > nbLinesSAR-1)
    {
      lastL = nbLinesSAR-1;
    } 


  // Check if into input Image
   
  if (firstC > nbColSAR-1 || firstL > nbLinesSAR-1)
    {
      intoInputImage = false; 
    } 
  if (lastC <= 0 || lastL <= 0)
    {
      intoInputImage = false; 
    } 


  // Transform sides to region
  ImageVectorIndexType inputRequestedRegionIndex; 
  ImageVectorRegionType inputRequestedRegion = outputRegion;
  if (intoInputImage)
    {
      inputRequestedRegionIndex[0] = static_cast<ImageVectorIndexValueType>(firstC);
      inputRequestedRegionIndex[1] = static_cast<ImageVectorIndexValueType>(firstL);
      ImageVectorSizeType inputRequestedRegionSize;
      inputRequestedRegionSize[0] = static_cast<ImageVectorIndexValueType>(lastC - firstC);
      inputRequestedRegionSize[1] = static_cast<ImageVectorIndexValueType>(lastL - firstL);  

      inputRequestedRegion.SetIndex(inputRequestedRegionIndex);
      inputRequestedRegion.SetSize(inputRequestedRegionSize);
    }
   
  return inputRequestedRegion;  
}

  /** 
   * Method GenerateInputRequestedRegion
   */
  template<class TImageVector, class TImageDEM, class TImageOut>
  void
  SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >
  ::GenerateInputRequestedRegion()
  {
    // call the superclass' implementation of this method
    Superclass::GenerateInputRequestedRegion();
    
    // Get Output requested region
    ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();

    // Input Region with OutputToInputRegion
    bool isIntoInputImages;
    ImageVectorRegionType inputRequestedRegion = OutputRegionToInputRegion(outputRequestedRegion, 
									   isIntoInputImages);
    ImageVectorPointer  cartesianMeanPtr = const_cast< ImageVectorType * >( this->GetCartesianMeanInput() );
    ImageVectorPointer  compensatedComplexPtr = const_cast< ImageVectorType * >( this->GetCompensatedComplexInput() );
    
    //if (isIntoInputImages)
    // {
	cartesianMeanPtr->SetRequestedRegion(inputRequestedRegion);
	compensatedComplexPtr->SetRequestedRegion(inputRequestedRegion);
    //   }
    // else
    //   {
    // 	itkExceptionMacro(<<"DEM and SAR geometry do not match.");
    //   }

  }
  

/**
   * Method GenerateData
   */
  template<class TImageVector, class TImageDEM, class TImageOut>
  void
  SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >
  ::GenerateData()
  {
    // Allocate outputs
    this->AllocateOutputs();

    // Get Output Requested region
    ImageOutRegionType outputRegion = this->GetOutput()->GetRequestedRegion();
    
    // Compute corresponding input region
    bool isIntoInputImage = true;
    ImageVectorRegionType inputRegion = OutputRegionToInputRegion(outputRegion, isIntoInputImage);


    if (isIntoInputImage)
      {
	// Allocation for temporay arrays
	unsigned int regionSize = outputRegion.GetSize()[0] * outputRegion.GetSize()[1];
	m_tmpArray_real = new double[regionSize];
	m_tmpArray_imag = new double[regionSize];
	m_tmpArray_mod = new double[regionSize];
	m_tmpArray_count = new double[regionSize];
	for (unsigned int i = 0; i < regionSize; i++) 
	  {
	    m_tmpArray_real[i] = static_cast<double>(0);
	    m_tmpArray_imag[i] = static_cast<double>(0);
	    m_tmpArray_mod[i] = static_cast<double>(0);
	    m_tmpArray_count[i] = static_cast<double>(0);
	  }

	// Set up the multithreaded processing
	ThreadStruct str;
	str.Filter = this;

	// Setting up multithreader
	this->GetMultiThreader()->SetNumberOfThreads(this->GetNumberOfThreads());
	this->GetMultiThreader()->SetSingleMethod(this->ThreaderCallback, &str);

	// multithread the execution
	this->GetMultiThreader()->SingleMethodExecute();
    

	// Loop on output
	ImageOutPixelType pixelOutput;
	pixelOutput.Reserve(4);
	int index_intoArray = 0;
    
	// Iterator on output (Ground geometry)
	OutputIterator OutIt(this->GetOutput(), outputRegion);
	OutIt.GoToBegin();

	// For each line
	while ( !OutIt.IsAtEnd())
	  {
	    OutIt.GoToBeginOfLine();
	
	    // For each colunm
	    while (!OutIt.IsAtEndOfLine())
	      {
		if (m_tmpArray_count[index_intoArray] > 0)
		  {
		    double mod_Acc = sqrt(m_tmpArray_real[index_intoArray]*
					  m_tmpArray_real[index_intoArray] + 
					  m_tmpArray_imag[index_intoArray]*
					  m_tmpArray_imag[index_intoArray]);

		    // Amplitude
		    pixelOutput[0] = m_Gain * 
		      sqrt((m_tmpArray_mod[index_intoArray]/(m_tmpArray_count[index_intoArray])));
		
		    // Phase
		    pixelOutput[1] = std::atan2(m_tmpArray_imag[index_intoArray], 
						m_tmpArray_real[index_intoArray]);
		
		    // Mod 2*Pi
		    pixelOutput[1] =  pixelOutput[1]-(2*M_PI)*floor(pixelOutput[1]/(2*M_PI));
		
		    // Coherency
		    pixelOutput[2] = mod_Acc / m_tmpArray_mod[index_intoArray] ;
		
		    // Occurrences 		
		    pixelOutput[3] = m_tmpArray_count[index_intoArray];
		  }
		else
		  {
		    pixelOutput[0] = 0;
		    pixelOutput[1] = 0;
		    pixelOutput[2] = 0;
		    pixelOutput[3] = 0;
		  }
	
		// Assigne Main output (Groun geometry) 
		OutIt.Set(pixelOutput);
  	   	
		// Next colunm
		++OutIt;
		++index_intoArray;
	      }
	    
	    // Next line
	    OutIt.NextLine();
	  }

	// Free Memory
	delete  m_tmpArray_real;
	m_tmpArray_real = 0;
	delete  m_tmpArray_imag;
	m_tmpArray_imag = 0;
	delete  m_tmpArray_mod;
	m_tmpArray_mod = 0;
	delete  m_tmpArray_count;
	m_tmpArray_count = 0;
      }
  }

  
  // Callback routine used by the threading library. This routine just calls
  // the ThreadedGenerateData method after setting the correct region for this
  // thread.
  template<class TImageVector, class TImageDEM, class TImageOut>
  ITK_THREAD_RETURN_TYPE
  SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >
  ::ThreaderCallback(void *arg)
  {
    ThreadStruct *str;
    int           threadId, threadCount;
   
    threadId = ((itk::MultiThreader::ThreadInfoStruct *) (arg))->ThreadID;
    threadCount = ((itk::MultiThreader::ThreadInfoStruct *) (arg))->NumberOfThreads;
    str = (ThreadStruct *) (((itk::MultiThreader::ThreadInfoStruct *) (arg))->UserData);

    // Get filter elts (temporary arrays and vector of input regions)
    double * tmpArray_real;
    double * tmpArray_imag;
    double * tmpArray_mod;
    double * tmpArray_count;
    str->Filter->GetArrays(tmpArray_real, tmpArray_imag, tmpArray_mod, tmpArray_count);

    // Get inputRegion for current thread (Split the input region to get smaller ones : one per thread)
    SplitterPointer splitter = SplitterType::New();
    ImageVectorRegionType inputRegionForThread = splitter->GetSplit(threadId, threadCount, 
								    str->Filter->GetInput()->GetRequestedRegion());
    

    str->Filter->ThreadedGenerateData(inputRegionForThread, 
				      str->Filter->GetOutput()->GetRequestedRegion(),
				      tmpArray_real, tmpArray_imag,
				      tmpArray_mod, tmpArray_count,
				      threadId);

    return ITK_THREAD_RETURN_VALUE;
  }
  

    /**
  * Method ThreadedGenerateData
  */
  template<class TImageVector, class TImageDEM, class TImageOut>
  void
  SARGroupedByOrthoImageFilter< TImageVector, TImageDEM, TImageOut >
  ::ThreadedGenerateData(const ImageVectorRegionType& inputRegion, 
			 const ImageOutRegionType& outputRegion,
			 double * tmpArray_real, double * tmpArray_imag,
			 double * tmpArray_mod, double * tmpArray_count,
			 itk::ThreadIdType itkNotUsed(threadId))
  {
    
    Point2DType demLonLatPoint;
    itk::ContinuousIndex<double,2> pixel_Into_DEM_index;
    ImageOutIndexType index;

    // Iterators on inputs (SAR geometry)
    InputIterator CartMeanIt(this->GetCartesianMeanInput(), inputRegion);
    InputIterator CompComplexIt(this->GetCompensatedComplexInput(), inputRegion);
	
    CartMeanIt.GoToBegin();
    CompComplexIt.GoToBegin();
	
    // Scan the input Region
    // for each line
    while ( !CartMeanIt.IsAtEnd() && !CompComplexIt.IsAtEnd())
      {
	CartMeanIt.GoToBeginOfLine();
	CompComplexIt.GoToBeginOfLine();

	// For each column
	while (!CartMeanIt.IsAtEndOfLine() && !CompComplexIt.IsAtEndOfLine())
	  {
	    // Get elt from cartesian mean image (X, Y and Z) and create an ECEF point with its
	    ossimEcefPoint cartPoint(CartMeanIt.Get()[0], CartMeanIt.Get()[1], CartMeanIt.Get()[2]);
		
	    // Conversion into World point
	    ossimGpt worldPoint(cartPoint);
		
	    demLonLatPoint[0] = worldPoint.lon;
	    demLonLatPoint[1] = worldPoint.lat;
	
	    // Get Nearest index into DEM/Ground Geometry
	    // Transform into continuous index 
	    m_DEMPtr->TransformPhysicalPointToContinuousIndex(demLonLatPoint, 
							      pixel_Into_DEM_index);
      
	    // Integer index 
	    index [0] = static_cast<int>(pixel_Into_DEM_index[0]);	
	    index [1] = static_cast<int>(pixel_Into_DEM_index[1]);
            
	    // Check if index into outputRegion
	    if (index[0] >= outputRegion.GetIndex()[0] && 
		index[0] < (outputRegion.GetIndex()[0] + 
                (int) outputRegion.GetSize()[0]) &&
		index[1] >= outputRegion.GetIndex()[1] && 
		index[1] < (outputRegion.GetIndex()[1] + 
              (int) outputRegion.GetSize()[1]))
	      {
		// Accumulations for CompensatedComplex for current ground index
		int index_intoArray = static_cast<int>((index[0]-outputRegion.GetIndex()[0]) + 
						       (index[1] - outputRegion.GetIndex()[1]) * 
						       outputRegion.GetSize()[0]);
		   
		// Mutex to protect class arguments (m_*Arrays)
		m_Mutex->Lock();
		tmpArray_real[index_intoArray] += CompComplexIt.Get()[0];
		tmpArray_imag[index_intoArray] += CompComplexIt.Get()[1];
		tmpArray_mod[index_intoArray] += CompComplexIt.Get()[2];
		++tmpArray_count[index_intoArray]; 
		m_Mutex->Unlock();
	      }
	    ++ CartMeanIt;
	    ++ CompComplexIt;
		  
	  }
		  
	// Next line
	CartMeanIt.NextLine();
	CompComplexIt.NextLine();
      } // End scan inputs
	
  }
 
  
} /*namespace otb*/

#endif
