/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARCompensatedComplexImageFilter_txx
#define otbSARCompensatedComplexImageFilter_txx

#include "otbSARCompensatedComplexImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"
#include "itkContinuousIndex.h"

#include <cmath>
#include <algorithm>


namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImageSAR, class TImagePhase, class TImageOut> 
  SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >::SARCompensatedComplexImageFilter()
    :  m_nbLinesSAR(0), m_nbColSAR(0)
  {
    // Inputs required and/or needed
    this->SetNumberOfRequiredInputs(2);
    this->SetNumberOfIndexedInputs(3);
  }
    
  /** 
   * Destructor
   */
  template <class TImageSAR, class TImagePhase, class TImageOut> 
  SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >::~SARCompensatedComplexImageFilter()
  {
  }

  /**
   * Print
   */
  template<class TImageSAR, class TImagePhase, class TImageOut>
  void
  SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);
  }

  /**
   * Set Master Image
   */ 
  template<class TImageSAR, class TImagePhase, class TImageOut>
  void
 SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >
  ::SetMasterInput(const ImageSARType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(0, const_cast<ImageSARType *>(image));
  }

  /**
   * Set Coregistrated Slave Image
   */ 
  template<class TImageSAR, class TImagePhase, class TImageOut>
  void
 SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >
  ::SetSlaveInput(const ImageSARType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(1, const_cast<ImageSARType *>(image));
  }

  /**
   * Set Topographic phase
   */ 
  template<class TImageSAR, class TImagePhase, class TImageOut>
  void
  SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >
  ::SetTopographicPhaseInput(const ImagePhaseType* phaseTopoImage)
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(2, const_cast<ImagePhaseType *>(phaseTopoImage));
  }

  /**
   * Get Master Image
   */ 
  template<class TImageSAR, class TImagePhase, class TImageOut>
  const typename SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >::ImageSARType *
  SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >
  ::GetMasterInput() const
  {
    if (this->GetNumberOfInputs()<1)
      {
	return 0;
      }
    return static_cast<const ImageSARType *>(this->itk::ProcessObject::GetInput(0));
  }

  /**
   * Get Coregistrated Slave Image
   */ 
  template<class TImageSAR, class TImagePhase, class TImageOut>
  const typename SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >::ImageSARType *
  SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >
  ::GetSlaveInput() const
  {
    if (this->GetNumberOfInputs()<2)
      {
	return 0;
      }
    return static_cast<const ImageSARType *>(this->itk::ProcessObject::GetInput(1));
  }

  /**
   * Get Phase Topographic
   */ 
  template<class TImageSAR, class TImagePhase, class TImageOut>
  const typename SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >::ImagePhaseType *
  SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >
  ::GetTopographicPhaseInput() const
  {
    if (this->GetNumberOfInputs()<3)
      {
	return 0;
      }
    return static_cast<const ImagePhaseType *>(this->itk::ProcessObject::GetInput(2));
  }

  /**
   * Method GenerateOutputInformaton()
   **/
  template<class TImageSAR, class TImagePhase, class TImageOut>
  void
  SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >
  ::GenerateOutputInformation()
  {
    // Call superclass implementation
    Superclass::GenerateOutputInformation();

    // Get pointers to the inputs and output
    ImageSARConstPointer masterPtr = this->GetMasterInput();
    ImageSARConstPointer slavePtr = this->GetSlaveInput();
    ImageOutPointer outputPtr = this->GetOutput();

    // KeyWordList
    ImageKeywordlist masterKWL = masterPtr->GetImageKeywordlist();

    // Master SAR Dimensions
    m_nbLinesSAR = this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[1];
    m_nbColSAR = this->GetMasterInput()->GetLargestPossibleRegion().GetSize()[0];
  
    /////////////////////// Main Output : conjugate multiplication (into SAR geometry) ///////////////////////
    // Vector Image  :
    // At Least 3 Components :  
    //                _ real part
    //                _ imag part
    //                _ modulus
    outputPtr->SetNumberOfComponentsPerPixel(3);

    // The output is defined with the Master SAR Image
    // Origin, Spacing and Size (SAR master geometry)
    ImageOutPointType outOrigin;
    outOrigin = masterPtr->GetOrigin();
    ImageOutSpacingType outSP;
    outSP = masterPtr->GetSpacing();

    // Define Output Largest Region
    ImageOutRegionType outputLargestPossibleRegion = masterPtr->GetLargestPossibleRegion();

    outputPtr->SetLargestPossibleRegion(outputLargestPossibleRegion);
    outputPtr->SetOrigin(outOrigin);
    outputPtr->SetSpacing(outSP);

    // Add ML factors and bands meaning into keyWordList
    ImageKeywordlist outputKWL = masterKWL;
    outputKWL.AddKey("support_data.band.Amplitude", std::to_string(0));
    outputKWL.AddKey("support_data.band.Phase", std::to_string(1));
    outputKWL.AddKey("support_data.band.Coherency", std::to_string(2));

    // Set new keyword list to output image
    outputPtr->SetImageKeywordList(outputKWL);    

    ///////// Checks (with input topographic phase keywordlists/metadata) /////////////
    // Check ML Factors (must be 1 to have the same geometry than SAR images)
     if (this->GetTopographicPhaseInput() != nullptr)
       {
	 // Get ImagKeyWordList
	 ImageKeywordlist topoKWL = this->GetTopographicPhaseInput()->GetImageKeywordlist();
	 
	 if (topoKWL.HasKey("support_data.ml_ran") && topoKWL.HasKey("support_data.ml_azi"))
	   {
	     // Get Master ML Factors
	     unsigned int topo_MLRan = atoi(topoKWL.GetMetadataByKey("support_data.ml_ran").c_str());
	     unsigned int topo_MLAzi = atoi(topoKWL.GetMetadataByKey("support_data.ml_azi").c_str());
	
	     if ((topo_MLRan != 1) || (topo_MLAzi != 1))
	       {
		 itkExceptionMacro(<<"Error, ML Factor for topographic phase are different than 1.");
	       }
	   }
       }
  }



  /** 
   * Method GenerateInputRequestedRegion
   */
  template<class TImageSAR, class TImagePhase, class TImageOut>
  void
  SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >
  ::GenerateInputRequestedRegion()
  {
    // call the superclass' implementation of this method
    Superclass::GenerateInputRequestedRegion();
    
    // Get Output requested region
    ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();

    // Input Region = Output region
    ImageSARPointer  masterPtr = const_cast< ImageSARType * >( this->GetMasterInput() );
    ImageSARPointer  slavePtr = const_cast< ImageSARType * >( this->GetSlaveInput() );
    
    masterPtr->SetRequestedRegion(outputRequestedRegion);
    slavePtr->SetRequestedRegion(outputRequestedRegion);

    // Region into topographic phase image (if needed) = Output region
    if (this->GetTopographicPhaseInput() != nullptr)
      {
	ImagePhasePointer  phasePtr = const_cast< ImagePhaseType * >( this->GetTopographicPhaseInput() );
	phasePtr->SetRequestedRegion(outputRequestedRegion);
      }
  }
  

  /**
   * Method ThreadedGenerateData
   */
  template<class TImageSAR, class TImagePhase, class TImageOut>
  void
  SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >
  ::ThreadedGenerateData(const ImageOutRegionType & outputRegionForThread,
			 itk::ThreadIdType threadId)
  {
    if (this->GetTopographicPhaseInput() == nullptr)
      {
	// Process for line
	this->ThreadedGenerateDataWithoutTopographicPhase(outputRegionForThread,threadId);
      } 
    else
      {
	// Process for column
	this->ThreadedGenerateDataWithTopographicPhase(outputRegionForThread,threadId);
      } 
  }

  /**
   * Method ThreadedGenerateDataWithoutTopographicPhase
   */
  template<class TImageSAR, class TImagePhase, class TImageOut>
  void
  SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >
  ::ThreadedGenerateDataWithoutTopographicPhase(const ImageOutRegionType & outputRegionForThread,
						itk::ThreadIdType threadId)
  {
    //  Define/declare an iterator that will walk the input regions for this
    // thread. 
    InputSARIterator  inMasterIt(this->GetMasterInput(), outputRegionForThread);
    InputSARIterator  inSlaveIt(this->GetSlaveInput(), outputRegionForThread);

    // Define/declare an iterator that will walk the output region for this
    // thread.
    OutputIterator outIt(this->GetOutput(), outputRegionForThread);

    // Support progress methods/callbacks
    itk::ProgressReporter progress( this, threadId, outputRegionForThread.GetNumberOfPixels() );

    inMasterIt.GoToBegin();
    inSlaveIt.GoToBegin();
    outIt.GoToBegin();
    
    // Output Pixel (VectorImage Pixel)
    ImageOutPixelType outPixel;
    outPixel.Reserve(3);

    // For each Line
    while ( !inMasterIt.IsAtEnd() && !inSlaveIt.IsAtEnd() && !outIt.IsAtEnd())
      {
	inMasterIt.GoToBeginOfLine();
	inSlaveIt.GoToBeginOfLine();
	outIt.GoToBeginOfLine();
	
	    // For each column
	    while (!inMasterIt.IsAtEndOfLine() && !inSlaveIt.IsAtEndOfLine() && !outIt.IsAtEndOfLine())
	      {
		// Conjugate multiplication (master * conj(slave))
		double real_interfero = (inMasterIt.Get().real()*inSlaveIt.Get().real() + 
					 inMasterIt.Get().imag()*inSlaveIt.Get().imag());

		double imag_interfero = (inMasterIt.Get().imag()*inSlaveIt.Get().real() - 
					 inMasterIt.Get().real()*inSlaveIt.Get().imag());

		///////////// Output assignations ///////////////
		outPixel[0] = real_interfero;

			
		outPixel[1] = imag_interfero;
			
			
		outPixel[2] = sqrt((real_interfero*real_interfero) + 
							 (imag_interfero*imag_interfero));

		
		outIt.Set(outPixel);
	    
		// Next colunm inputs
		++inMasterIt;
		++inSlaveIt;
		// Next colunm output
		++outIt;
	      }
	  
	    // Next line intputs
	    inMasterIt.NextLine();
	    inSlaveIt.NextLine();
	    // Next line output
	    outIt.NextLine();
      }
  }

   /**
   * Method ThreadedGenerateDataWithtTopographicPhase
   */
  template<class TImageSAR, class TImagePhase, class TImageOut>
  void
  SARCompensatedComplexImageFilter< TImageSAR, TImagePhase, TImageOut >
  ::ThreadedGenerateDataWithTopographicPhase(const ImageOutRegionType & outputRegionForThread,
						itk::ThreadIdType threadId)
  {
    //  Define/declare an iterator that will walk the input regions for this
    // thread. 
    InputSARIterator  inMasterIt(this->GetMasterInput(), outputRegionForThread);
    InputSARIterator  inSlaveIt(this->GetSlaveInput(), outputRegionForThread);

    // Define/declare an iterator that will walk the output region for this
    // thread.
    OutputIterator outIt(this->GetOutput(), outputRegionForThread);

    // Topographic phase
    InputPhaseIterator inTopoPhaseIt(this->GetTopographicPhaseInput(), outputRegionForThread);

    // Support progress methods/callbacks
    itk::ProgressReporter progress( this, threadId, outputRegionForThread.GetNumberOfPixels() );

    inMasterIt.GoToBegin();
    inSlaveIt.GoToBegin();
    inTopoPhaseIt.GoToBegin();
    outIt.GoToBegin();
    
    // Output Pixel (VectorImage Pixel)
    ImageOutPixelType outPixel;
    outPixel.Reserve(3);

    // For each Line
    while ( !inMasterIt.IsAtEnd() && !inSlaveIt.IsAtEnd() && !outIt.IsAtEnd())
      {
	inMasterIt.GoToBeginOfLine();
	inSlaveIt.GoToBeginOfLine();
	inTopoPhaseIt.GoToBeginOfLine();
	outIt.GoToBeginOfLine();
	
	    // For each column
	    while (!inMasterIt.IsAtEndOfLine() && !inSlaveIt.IsAtEndOfLine() && !outIt.IsAtEndOfLine())
	      {
		// Check isData (grom topographic phase)
		if (inTopoPhaseIt.Get()[1] != 0)
		  {			
		    // Complex Raw interferogram (master * conj(slave))
		    double real_RawInterfero = (inMasterIt.Get().real()*inSlaveIt.Get().real() + 
						inMasterIt.Get().imag()*inSlaveIt.Get().imag());

		    double imag_RawInterfero = (inMasterIt.Get().imag()*inSlaveIt.Get().real() - 
						inMasterIt.Get().real()*inSlaveIt.Get().imag());

		    ///////////// Topographoc phase as complex number ////////////////
		    double topoPhase = inTopoPhaseIt.Get()[0];
				
		    double complexTopoPhase_Re = std::cos(topoPhase);
		    double complexTopoPhase_Im = std::sin(topoPhase);

	
		    // Multiply the conj(complexTopoPhase) with complex raw interferogram
		    double real_interfero = (real_RawInterfero * complexTopoPhase_Re + 
					     imag_RawInterfero * complexTopoPhase_Im);
		    double imag_interfero = (imag_RawInterfero * complexTopoPhase_Re - 
					     real_RawInterfero * complexTopoPhase_Im);

		    ///////////// Output assignations ///////////////
		    outPixel[0] = real_interfero;

			
		    outPixel[1] = imag_interfero;
			
			
		    outPixel[2] = sqrt((real_interfero*real_interfero) + 
				       (imag_interfero*imag_interfero));
		  }
		else
		  {
		    outPixel[0] = 0;
		    outPixel[1] = 0;
		    outPixel[2] = 0;
		    outPixel[3] = 0;
		  }
	
		outIt.Set(outPixel);
	    
		// Next colunm inputs
		++inMasterIt;
		++inSlaveIt;
		++inTopoPhaseIt;
		// Next colunm output
		++outIt;
	      }
	  
	    // Next line intputs
	    inMasterIt.NextLine();
	    inSlaveIt.NextLine();
	    inTopoPhaseIt.NextLine();
	    // Next line output
	    outIt.NextLine();
      }
  }


  } /*namespace otb*/

#endif
