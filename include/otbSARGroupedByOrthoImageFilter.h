/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARGroupedByOrthoImageFilter_h
#define otbSARGroupedByOrthoImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"
#include "itkSimpleFastMutexLock.h"
#include "itkImageRegionSplitter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"

#include "otbImageKeywordlist.h"
#include "otbSarSensorModelAdapter.h"


#if defined(__GNUC__) || defined(__clang__)
# pragma GCC diagnostic push
#   pragma GCC diagnostic ignored "-Woverloaded-virtual"
#if defined(__GNUC__) && (__GNUC__ > 5)
#pragma GCC diagnostic ignored "-Wnonnull-compare"
#endif
#   include "ossim/base/ossimGeoidEgm96.h"
# pragma GCC diagnostic pop
#else
#   include "ossim/base/ossimGeoidEgm96.h"
#endif


namespace otb
{
/** \class SARGroupedByOrthoImageFilter 
 * \brief Creates an interferogram into ground geometry between two SAR images. 
 * 
 * This filter built the Ortho interferogram between a master and a slave image.
 *
 * The output is a vector image with amplitude, phase coherency adn occurrences.
 *
 * \ingroup DiapOTBModule
 */

  template <typename TImageVector, typename TImageDEM, typename TImageOut> 
  class ITK_EXPORT SARGroupedByOrthoImageFilter :
    public itk::ImageToImageFilter<TImageVector,TImageOut>
{
public:

  // Standard class typedefs
  typedef SARGroupedByOrthoImageFilter                    Self;
  typedef itk::ImageToImageFilter<TImageVector,TImageOut>    Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;
  typedef itk::SmartPointer<const Self>                  ConstPointer;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(SARGroupedByOrthoImageFilter,ImageToImageFilter);

  /** Typedef to vector image input type : otb::VectorImage  */
  typedef TImageVector                                  ImageVectorType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageVectorType::Pointer             ImageVectorPointer;
  typedef typename ImageVectorType::ConstPointer        ImageVectorConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageVectorType::RegionType          ImageVectorRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageVectorType::PixelType           ImageVectorPixelType;
  typedef typename ImageVectorType::PointType           ImageVectorPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageVectorType::IndexType           ImageVectorIndexType;
  typedef typename ImageVectorType::IndexValueType      ImageVectorIndexValueType;
  typedef typename ImageVectorType::SizeType            ImageVectorSizeType;
  typedef typename ImageVectorType::SizeValueType       ImageVectorSizeValueType;
  typedef typename ImageVectorType::SpacingType         ImageVectorSpacingType;
  typedef typename ImageVectorType::SpacingValueType    ImageVectorSpacingValueType;
  
  /** Typedef to image output type : otb::VectorImage  */
  typedef TImageOut                                  ImageOutType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageOutType::Pointer             ImageOutPointer;
  typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageOutType::RegionType          ImageOutRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageOutType::PixelType           ImageOutPixelType;
  typedef typename ImageOutType::PointType           ImageOutPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageOutType::IndexType           ImageOutIndexType;
  typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
  typedef typename ImageOutType::SizeType            ImageOutSizeType;
  typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
  typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
  typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;

  /** Typedef to dem type : otb::Image  */
  typedef TImageDEM                                  ImageDEMType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageDEMType::Pointer             ImageDEMPointer;
  typedef typename ImageDEMType::ConstPointer        ImageDEMConstPointer;
  typedef typename ImageDEMType::SpacingType         ImageDEMSpacingType;
  typedef typename ImageDEMType::SpacingValueType    ImageDEMSpacingValueType;

  // DEMHandler
  typedef otb::DEMHandler DEMHandlerType;
  typedef typename DEMHandlerType::Pointer DEMHandlerPointerType;

  
  // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;
  typedef double  ValueType;

  typedef itk::SimpleFastMutexLock                MutexType;
  typedef typename itk::ImageRegionSplitter<2>    SplitterType; // Dimension = 2 for inputs
  typedef typename SplitterType::Pointer          SplitterPointer;

  // Typedef for iterators
  typedef itk::ImageScanlineConstIterator< ImageVectorType > InputIterator;
  typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;

  
    
  // Setter/Getter for inputs (Cartesian mean and GroupedByOrtho) 
  /** Connect one of the operands for interferometry : Master */
  void SetCartesianMeanInput( const ImageVectorType* image);

  /** Connect one of the operands for interferometry : Coregistrated Slave */
  void SetCompensatedComplexInput(const ImageVectorType* image);

  /** Get the inputs */
  const ImageVectorType* GetCartesianMeanInput() const;
  const ImageVectorType* GetCompensatedComplexInput() const;

  
  // Setter for metadata
  void SetSARImageKeyWorList(ImageKeywordlist sarImageKWL);
  void SetDEMImagePtr(ImageDEMPointer demPtr);

    
  // Setter/Getter (Macro)
  itkSetMacro(Margin, unsigned int);
  itkGetConstMacro(Margin, unsigned int);

  // Getter for arrays called inot callback function 
  void GetArrays(double * &tmpArray_real, double * &tmpArray_imag, double * &tmpArray_mod,
		 double * &tmpArray_count)
  {
    // Assign Ptrs (=> needs reference)
    tmpArray_real = m_tmpArray_real;
    tmpArray_imag = m_tmpArray_imag;
    tmpArray_mod = m_tmpArray_mod;
    tmpArray_count = m_tmpArray_count;
  }

protected:
  // Constructor
  SARGroupedByOrthoImageFilter();

  // Destructor
  virtual ~SARGroupedByOrthoImageFilter() ITK_OVERRIDE;

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
  /** SARGroupedByOrthoImageFilter produces an image which are into output geometry. The differences between 
   * output and input images can be the size of images and the dimensions. 
   * As such, SARGroupedByOrthoImageFilter needs to provide an implementation for 
   * GenerateOutputInformation() in order to inform the pipeline execution model. 
   */ 
  virtual void GenerateOutputInformation() ITK_OVERRIDE;

  /** SARGroupedByOrthoImageFilter needs a input requested region that corresponds to our output requested 
   * region (SAR geometry for inputs and Ground for output).
   * As such, SARGroupedByOrthoImageFilter needs to provide an implementation for 
   * GenerateInputRequestedRegion() in order to inform the pipeline execution model. 
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /**
   * OutputRegionToInputRegion returns the input SAR region 
   */
  ImageVectorRegionType OutputRegionToInputRegion(const ImageOutRegionType& outputRegion,
						  bool & intoInputImage) const;


  /** 
   * SARGroupedByOrthoImageFilter can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData() routine
   * which is called for each processing thread. The main output image data is
   * allocated automatically by the superclass prior to calling
   * ThreadedGenerateData(). The implementation of multithreading is overrided here with the used of 
   * MultiThreader directly */  
  using Superclass::ThreadedGenerateData;
  /** inputRegion and outputRegion represent the input/output region for the current streaming. 
      Temporary arrays are shared between all threads.  */
  void ThreadedGenerateData(const ImageVectorRegionType& inputRegion, 
  			    const ImageOutRegionType& outputRegion,
  			    double * tmpArray_real, double * tmpArray_imag,
  			    double * tmpArray_mod, double * tmpArray_count,
  			    itk::ThreadIdType threadId);

  
  /** Static function used as a "callback" by the MultiThreader.  The threading
  * library will call this routine for each thread, which will delegate the
  * control to ThreadedGenerateData(). */
  static ITK_THREAD_RETURN_TYPE ThreaderCallback(void *arg);

  /** Internal structure used for passing image data into the threading library */
  struct ThreadStruct
  {
    Pointer Filter;
  };

  /** 
   * \sa ImageToImageFilter::ThreadedGenerateData(),
   *     ImageToImageFilter::GenerateData() */
  void GenerateData() override;


 private:
  SARGroupedByOrthoImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 

  // Instance of SarSensorModelAdapter
  SarSensorModelAdapter::Pointer m_SarSensorModelAdapter; 

  // SAR Image KeyWorldList
  ImageKeywordlist m_SarImageKwl;

  ossimGeoidEgm96 * m_geoidEmg96;
  
  // DEM
  ImageDEMConstPointer m_DEMPtr;

  float m_Gain;

  // Margin (to enlarge SAR scan)
  unsigned int m_Margin;

  int m_nbLinesSAR;
  int m_nbColSAR;

  MutexType * m_Mutex;

  // Temporary arrays
  double * m_tmpArray_real;
  double * m_tmpArray_imag;
  double * m_tmpArray_mod;
  double * m_tmpArray_count;
};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARGroupedByOrthoImageFilter.txx"
#endif



#endif
