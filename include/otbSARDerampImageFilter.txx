/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARDerampImageFilter_txx
#define otbSARDerampImageFilter_txx

#include "otbSARDerampImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"

#include "otbSarSensorModelAdapter.h"

#include <cmath>
#include <algorithm>

#if defined(USE_BOOST_TIME)
using boost::posix_time::microseconds;
using boost::posix_time::seconds;
#else
using ossimplugins::time::microseconds;
using ossimplugins::time::seconds;
#endif

namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImage> 
  SARDerampImageFilter< TImage >::SARDerampImageFilter()
    :  m_DerampMode(true),  m_ShiftMode(false), m_FirstAziTime(0), m_FirstRangeTime(0), m_MidAziTime(0), 
       m_VSatAtMidAziTime(0), m_Ks(0), m_AziTimeInt(0),m_RangeSamplingRate(0), m_FM_C0(0), m_FM_C1(0), 
       m_FM_C2(0), m_FM_Tau0(0), m_DCF_C0(0), m_DCF_C1(0), m_DCF_C2(0), m_DCF_Tau0(0), m_RefTime0(0), 
       m_FirstEstimation(true)
  {
    // Inputs required and/or needed
    this->SetNumberOfRequiredInputs(1);
    this->SetNumberOfIndexedInputs(2);
  }
    
  /** 
   * Destructor
   */
  template <class TImage> 
  SARDerampImageFilter< TImage >::~SARDerampImageFilter()
  {
   
  }

  /**
   * Print
   */
  template<class TImage>
  void
  SARDerampImageFilter< TImage >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);

    os << indent << "Mode for deramp filter, if true then Deramp Mode else Reramp mode : " << m_DerampMode 
       << std::endl;
  }

   /**
   * Set Image
   */ 
  template<class TImage>
  void
 SARDerampImageFilter< TImage >
  ::SetImageInput(const ImageType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(0, const_cast<ImageType *>(image));
  }

  /**
   * Set Grid
   */ 
  template<class TImage>
  void
 SARDerampImageFilter< TImage >
  ::SetGridInput(const GridType* grid)
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(1, const_cast<GridType *>(grid));
  }

  /**
   * Get mage
   */ 
  template<class TImage>
  const typename SARDerampImageFilter< TImage >::ImageType *
  SARDerampImageFilter< TImage >
  ::GetImageInput() const
  {
    if (this->GetNumberOfInputs()<1)
      {
	return 0;
      }
    return static_cast<const ImageType *>(this->itk::ProcessObject::GetInput(0));
  }

  /**
   * Get Grid
   */ 
  template<class TImage>
  const typename SARDerampImageFilter< TImage >::GridType *
  SARDerampImageFilter< TImage >
  ::GetGridInput() const
  {
    if (this->GetNumberOfInputs()<2)
      {
	return 0;
      }
    return static_cast<const GridType *>(this->itk::ProcessObject::GetInput(1));
  }



  template<class TImage>
  void
  SARDerampImageFilter< TImage >
  ::getAllCoefs(ImageKeywordlist  const& kwl, std::vector<FMRateRecordType> & FMRateRecords)
  {
    char fmRatePrefix_[1024];
    std::size_t nbLists(0);
    
    nbLists = std::stoi(kwl.GetMetadataByKey("azimuthFmRate.azi_fm_rate_coef_nb_list"));

    std::string FM_PREFIX = "azimuthFmRate.azi_fm_rate_coef_list";
    
    for (std::size_t listId=0; listId!= nbLists ; ++listId) 
      {
	const int pos = sprintf(fmRatePrefix_, "%s%zu.", FM_PREFIX.c_str(), (listId+1));
	assert(pos > 0 && (unsigned long) pos < sizeof(fmRatePrefix_));
	const std::string FMPrefix(fmRatePrefix_, pos);

	FMRateRecordType fmRateRecord;
	fmRateRecord.azimuthFMRateTime = ossimplugins::time::toModifiedJulianDate(kwl.GetMetadataByKey(FMPrefix+ 
										      "azi_fm_rate_coef_time"));
	fmRateRecord.coef0FMRate = std::stod(kwl.GetMetadataByKey(FMPrefix + "1.azi_fm_rate_coef"));
	fmRateRecord.coef1FMRate = std::stod(kwl.GetMetadataByKey(FMPrefix + "2.azi_fm_rate_coef"));
	fmRateRecord.coef2FMRate = std::stod(kwl.GetMetadataByKey(FMPrefix + "3.azi_fm_rate_coef"));
	fmRateRecord.tau0FMRate = std::stod(kwl.GetMetadataByKey(FMPrefix + "slant_range_time"));

	FMRateRecords.push_back(fmRateRecord);
      }
  }

  template<class TImage>
  void
  SARDerampImageFilter< TImage >
  ::getAllCoefs(ImageKeywordlist  const& kwl, std::vector<DCFRecordType> & DCFRecords)
  {
     char dcfPrefix_[1024];
    std::size_t nbLists(0);
    
    nbLists = std::stoi(kwl.GetMetadataByKey("dopplerCentroid.dop_coef_nb_list"));

    std::string DCF_PREFIX = "dopplerCentroid.dop_coef_list";
    
    for (std::size_t listId=0; listId!= nbLists ; ++listId) 
      {
	const int pos = sprintf(dcfPrefix_, "%s%zu.", DCF_PREFIX.c_str(), (listId+1));
	assert(pos > 0 && (unsigned long) pos < sizeof(dcfPrefix_));
	const std::string DCFPrefix(dcfPrefix_, pos);

	DCFRecordType dcfRecord;
	dcfRecord.azimuthDCFTime = ossimplugins::time::toModifiedJulianDate(kwl.GetMetadataByKey(DCFPrefix + 
									      "dop_coef_time"));
	dcfRecord.coef0DCF = std::stod(kwl.GetMetadataByKey(DCFPrefix + "1.dop_coef"));
	dcfRecord.coef1DCF = std::stod(kwl.GetMetadataByKey(DCFPrefix + "2.dop_coef"));
	dcfRecord.coef2DCF = std::stod(kwl.GetMetadataByKey(DCFPrefix + "3.dop_coef"));
	dcfRecord.tau0DCF = std::stod(kwl.GetMetadataByKey(DCFPrefix + "slant_range_time"));

	 DCFRecords.push_back(dcfRecord);
      }
  }

  /**
   * Method selectFMRateCoef
   */
  template<class TImage>
  bool 
  SARDerampImageFilter< TImage >
  ::selectFMRateCoef()
  {
    // Get input
    ImagePointer inputPtr = const_cast< ImageType * >( this->GetImageInput() );

     // Retrieve all polynomials
    std::vector<FMRateRecordType> FMRateRecords;

    ImageKeywordlist inputKWL;
    if (m_ShiftMode)
	  {
	    if (this->GetGridInput() != nullptr)
	      {
		inputKWL = m_SarKWL;
	      }
	    else
	      {
		itkExceptionMacro(<<"Missing Image Keywordlist for shift mode");
	      }
	  }
    else
      {
	inputKWL = inputPtr->GetImageKeywordlist();
      }
    
    this->getAllCoefs(inputKWL, FMRateRecords);

    DurationType diffAziTimeMin(0);
    unsigned int polySelected = 0;
    
    // Select one polynomial (with m_MidAziTime)
    for (unsigned int i = 0; i < FMRateRecords.size(); i++)
      {
	// Difference between midAziTime and aziTime of the current polynomial
	DurationType diffAziTime(0);
	if (m_MidAziTime > FMRateRecords[i].azimuthFMRateTime)
	  {
	    diffAziTime = DurationType(m_MidAziTime - FMRateRecords[i].azimuthFMRateTime);
	  }
	else
	  {
	    diffAziTime = DurationType(FMRateRecords[i].azimuthFMRateTime - m_MidAziTime);
	  }
	
	if (diffAziTime < diffAziTimeMin || i == 0)
	  {
	    diffAziTimeMin = diffAziTime;
	    polySelected = i;
	  }
      }

    // Assign m_FM_C0, m_FM_C1, m_FM_C2, m_FM_Tau0
    m_FM_C0 = FMRateRecords[polySelected].coef0FMRate;
    m_FM_C1 = FMRateRecords[polySelected].coef1FMRate;
    m_FM_C2 = FMRateRecords[polySelected].coef2FMRate;
    m_FM_Tau0 = FMRateRecords[polySelected].tau0FMRate;
      
    return true;
  }

 /**
   * Method selectDCFCoef
   */
  template<class TImage>
  bool 
  SARDerampImageFilter< TImage >
  ::selectDCFCoef()
  {
     // Get input
    ImagePointer inputPtr = const_cast< ImageType * >( this->GetImageInput() );

     // Retrieve all polynomials
    std::vector<DCFRecordType> DCFRecords;
    
    ImageKeywordlist inputKWL;
    if (m_ShiftMode)
	  {
	    if (this->GetGridInput() != nullptr)
	      {
		inputKWL = m_SarKWL;
	      }
	    else
	      {
		itkExceptionMacro(<<"Missing Image Keywordlist for shift mode");
	      }
	  }
    else
      {
	inputKWL = inputPtr->GetImageKeywordlist();
      }
    
    this->getAllCoefs(inputKWL, DCFRecords);

    DurationType diffAziTimeMin(0);
    unsigned int polySelected = 0;
    
    // Select one polynomial (with m_MidAziTime)
    for (unsigned int i = 0; i < DCFRecords.size(); i++)
      {
	// Difference between midAziTime and aziTime of the current polynomial
	DurationType diffAziTime(0);
	if (m_MidAziTime > DCFRecords[i].azimuthDCFTime)
	  {
	    diffAziTime = DurationType(m_MidAziTime - DCFRecords[i].azimuthDCFTime);
	  }
	else
	  {
	    diffAziTime = DurationType(DCFRecords[i].azimuthDCFTime - m_MidAziTime);
	  }
	
	if (diffAziTime < diffAziTimeMin || i == 0)
	  {
	    diffAziTimeMin = diffAziTime;
	    polySelected = i;
	  }
      }

    // Assign m_FM_C0, m_FM_C1, m_FM_C2, m_FM_Tau0
    m_DCF_C0 = DCFRecords[polySelected].coef0DCF;
    m_DCF_C1 = DCFRecords[polySelected].coef1DCF;
    m_DCF_C2 = DCFRecords[polySelected].coef2DCF;
    m_DCF_Tau0 = DCFRecords[polySelected].tau0DCF;

    return true;
  }

  /**
   * Apply FM Rate coefficients Method
   */
  template<class TImage>
  long double
  SARDerampImageFilter<TImage>
  ::applyFMRateCoefs(double index_sample)
  {
    double slant_range_time = m_FirstRangeTime + (index_sample / m_RangeSamplingRate);
    
    return (m_FM_C0 + m_FM_C1*(slant_range_time - m_FM_Tau0) + m_FM_C2*(slant_range_time - m_FM_Tau0)*
	    (slant_range_time - m_FM_Tau0));
  }

  /**
   * Apply doppler centroid Frequency coefficients Method
   */
  template<class TImage>
  long double
  SARDerampImageFilter<TImage>
  ::applyDCFCoefs(double index_sample)
  {
    double slant_range_time = m_FirstRangeTime + (index_sample / m_RangeSamplingRate);
    
    return (m_DCF_C0 + m_DCF_C1*(slant_range_time - m_DCF_Tau0) + m_DCF_C2*(slant_range_time - m_DCF_Tau0)*
	    (slant_range_time - m_DCF_Tau0));
  }

  /**
   * Get shifts into input grod with the current index
   */
  template<class TImage>
  void
  SARDerampImageFilter<TImage>
  ::getShifts(ImageIndexType index, double & shift_ran, double & shift_azi)
  {
    // Get the index of current tile into grid to retrive the shifts 
    // Output Geo = Master Geo = (Grid geo / GridStep) 
    int Cgrid =  std::round( index[0] / m_GridStep[0]); 
    int Lgrid =  std::round( index[1] / m_GridStep[1]); 
	    
    Cgrid = std::min (std::max (Cgrid, 0), 
		      static_cast<int>(this->GetGridInput()->GetLargestPossibleRegion().GetSize()[0])-1); 
    Lgrid = std::min (std::max (Lgrid, 0), 
		      static_cast<int>(this->GetGridInput()->GetLargestPossibleRegion().GetSize()[1])-1); 
	    
    GridIndexType gridIndex;
    gridIndex[0] = Cgrid;	    
    gridIndex[1] = Lgrid;
	    
    // Shifts 
    shift_ran = this->GetGridInput()->GetPixel(gridIndex)[0];
    shift_azi = this->GetGridInput()->GetPixel(gridIndex)[1];
  }

    /** 
   * Method GenerateInputRequestedRegion
   */
  template<class TImage>
  void
  SARDerampImageFilter< TImage >
  ::GenerateInputRequestedRegion()
  {
    // call the superclass' implementation of this method
    Superclass::GenerateInputRequestedRegion();
    
    // Get Output requested region
    ImageRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();

     ///////////// For Input image same region  /////////////
    ImagePointer  inputPtr = const_cast< ImageType * >( this->GetImageInput() );
    inputPtr->SetRequestedRegion(outputRequestedRegion);

    if (m_ShiftMode)
      {
	if (this->GetGridInput() != nullptr)
	  {	    
	    ///////////// Find the region into Shift Grid ////////////
	    GridRegionType gridRequestedRegion = OutputRegionToInputGridRegion(outputRequestedRegion);
	    GridPointer  gridPtr = const_cast< GridType * >( this->GetGridInput() );
	    gridPtr->SetRequestedRegion(gridRequestedRegion);
	  }
	else
	  {
	    itkExceptionMacro(<<"Missing Input Grid for shift mode");
	  }  
      }
  }
  

  /** 
   * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
   */
  template<class TImage>
  typename SARDerampImageFilter< TImage >::GridRegionType 
  SARDerampImageFilter< TImage >
  ::OutputRegionToInputGridRegion(const ImageRegionType& outputRegion) const
  {
    // Compute the input requested region (size and start index)
    // Use the image transformations to insure an input grid requested region
    // that will provide the proper range
    const ImageSizeType & outputRequestedRegionSize = outputRegion.GetSize();
    ImageIndexType outputRequestedRegionIndex = outputRegion.GetIndex();
        
    // Define the index and size for grid requested region. The input grid is on output/master geometry with
    // m_GridStep as factor
    GridIndexType indexGrid;
    indexGrid[0] = static_cast<GridIndexValueType>(outputRequestedRegionIndex[0]/m_GridStep[0]) - 1; 
    indexGrid[1] = static_cast<GridIndexValueType>(outputRequestedRegionIndex[1]/m_GridStep[1]) - 1;

    GridSizeType sizeGrid;
    sizeGrid[0] = static_cast<GridSizeValueType>(outputRequestedRegionSize[0]/m_GridStep[0]) + 3; 
    sizeGrid[1] = static_cast<GridSizeValueType>(outputRequestedRegionSize[1]/m_GridStep[1]) + 3;

    // Check Index and Size
    if (indexGrid[0] < this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[0])
      {
	indexGrid[0] = this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[0];
      }
    if (indexGrid[1] < this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[1])
      {
	indexGrid[1] = this->GetGridInput()->GetLargestPossibleRegion().GetIndex()[1];
      }
    if ((sizeGrid[0] + indexGrid[0]) > 
	this->GetGridInput()->GetLargestPossibleRegion().GetSize()[0])
      {
	sizeGrid[0] = this->GetGridInput()->GetLargestPossibleRegion().GetSize()[0] - 
	  indexGrid[0];
      }
    if ((sizeGrid[1] + indexGrid[1]) > 
	this->GetGridInput()->GetLargestPossibleRegion().GetSize()[1])
      {
	sizeGrid[1] = this->GetGridInput()->GetLargestPossibleRegion().GetSize()[1] - 
	  indexGrid[1];
      }
    
    // Transform into a region1
    GridRegionType gridRequestedRegion = outputRegion;
    gridRequestedRegion.SetIndex(indexGrid);
    gridRequestedRegion.SetSize(sizeGrid);

    return gridRequestedRegion;
  }


  /**
   * Method ThreadedGenerateData
   */
  template<class TImage>
  void
  SARDerampImageFilter<TImage>
  ::BeforeThreadedGenerateData()
  {
    // Estimates general parameters for the current burst, if m_FirstEstimation == true
    if (m_FirstEstimation)
      {
	m_FirstEstimation = false;

	// Get input
	ImagePointer inputPtr = const_cast< ImageType * >( this->GetImageInput() );
	// Choose KeyWordList
	ImageKeywordlist inputKWL;
	if (m_ShiftMode)
	  {
	    if (this->GetGridInput() != nullptr)
	      {
		inputKWL = m_SarKWL;
	      }
	    else
	      {
		itkExceptionMacro(<<"Missing Image Keywordlist for shift mode");
	      }
	  }
	else
	  {
	    inputKWL = inputPtr->GetImageKeywordlist();
	  }

	// Check version of header/kwl (at least 3)
	int headerVersion = std::stoi(inputKWL.GetMetadataByKey("header.version"));

	if (headerVersion < 3)
	  {
	    itkExceptionMacro(<<"Header version is inferior to 3. Please Upgrade your geom file");
	  }

	// Get some metadata
	double aziSteeringRate = std::stod(inputKWL.GetMetadataByKey("support_data.azimuth_steering_rate"));
	// Conversion to radians per seconds
	aziSteeringRate *= (M_PI/180); 

	m_FirstAziTime = ossimplugins::time::toModifiedJulianDate((inputKWL.GetMetadataByKey("support_data.first_line_time")));
	m_FirstRangeTime = std::stod(inputKWL.GetMetadataByKey("support_data.slant_range_to_first_pixel"));

	m_AziTimeInt = std::stod(inputKWL.GetMetadataByKey("support_data.line_time_interval"));
	m_RangeSamplingRate = std::stod(inputKWL.GetMetadataByKey("support_data.range_sampling_rate"));

	double radarFrequency = std::stod(inputKWL.GetMetadataByKey("support_data.radar_frequency"));
	
	int nbLineBurst = std::stod(inputKWL.GetMetadataByKey("support_data.geom.bursts.number_lines_per_burst"));

	int nbSampleBurst = std::stod(inputKWL.GetMetadataByKey("support_data.geom.bursts.number_samples_per_burst"));
	// Estimation m_Ks
	m_LineAtMidBurst = nbLineBurst/2.;
	m_MidAziTime = m_FirstAziTime + seconds(m_AziTimeInt * m_LineAtMidBurst);
	
	// Try to create a SarSensorModelAdapter
	SarSensorModelAdapter::Pointer sarSensorModel = SarSensorModelAdapter::New();
	bool loadOk = sarSensorModel->LoadState(inputKWL);

	if(!loadOk || !sarSensorModel->IsValidSensorModel())
	  itkExceptionMacro(<<"Input image does not contain a valid SAR sensor model.");

	Point3DType satpos, satvel;
 
	bool lineToSatPosAndVelOk = sarSensorModel->LineToSatPositionAndVelocity(m_LineAtMidBurst, satpos, satvel);

	if (!lineToSatPosAndVelOk)
	  itkExceptionMacro(<<"Failed to estimate satellite position and velocity.");
  
	m_VSatAtMidAziTime = std::sqrt(satvel[0]*satvel[0] + satvel[1]*satvel[1] + satvel[2]*satvel[2]);

	m_Ks = (2*m_VSatAtMidAziTime/C) * radarFrequency * aziSteeringRate;
	
	DurationType diffTime = m_MidAziTime - m_FirstAziTime;

	m_MidRanTime = m_FirstRangeTime + (nbSampleBurst / (2*m_RangeSamplingRate));

	// Polynomial selection (FM Rate and Doppler Centroid Frequency)
	this->selectFMRateCoef();
	this->selectDCFCoef();

	// Estimate Reference time at first sample
	m_RefTime0 = - (this->applyDCFCoefs(0) / this->applyFMRateCoefs(0));

	m_RefTimeMid =  - (this->applyDCFCoefs(nbSampleBurst / 2) / this->applyFMRateCoefs(nbSampleBurst / 2));

      }
  }
   

  /**
   * Method ThreadedGenerateData
   */
  template<class TImage>
  void
  SARDerampImageFilter<TImage>
  ::ThreadedGenerateData(const ImageRegionType & outputRegionForThread,
			 itk::ThreadIdType /*threadId*/)
  {
    // Compute corresponding input region for master and slave cartesian mean
    ImageRegionType inputRegionForThread = outputRegionForThread;

    // Iterator on output
    OutputIterator OutIt(this->GetOutput(), outputRegionForThread);
    OutIt.GoToBegin();

    // Iterator on input 
    InputIterator  InIt(this->GetImageInput(), inputRegionForThread);
    InIt.GoToBegin();

    // For each line
    while (!OutIt.IsAtEnd() && !InIt.IsAtEnd())
      {
	OutIt.GoToBeginOfLine();
	InIt.GoToBeginOfLine();


	// For each colunm
	while (!OutIt.IsAtEndOfLine() && !InIt.IsAtEndOfLine()) 
	  {
	    // Output index
	    ImageIndexType index = OutIt.GetIndex();

	    double indC = static_cast<double>(index[0]);
	    double indL = static_cast<double>(index[1]);

	    // If shift mode, apply shifts on current index
	    if (m_ShiftMode)
	      {
		double shift_ran, shift_azi;
		this->getShifts(index, shift_ran, shift_azi);

		indC += shift_ran; 
		indL += shift_azi; 
	      }

	    // Zero Doppler azimuth Time
	    double aziTime = m_AziTimeInt*(indL - m_LineAtMidBurst); // zero-Doppler azimuth time centered in the middle of the burst

	    // Reference Time
	    double refDur = - (this->applyDCFCoefs(indC) / this->applyFMRateCoefs(indC)) - m_RefTime0;
	    // Kt
	    double Kt = (this->applyFMRateCoefs(indC) * m_Ks) / (this->applyFMRateCoefs(indC) - m_Ks);

	    double diffTime = aziTime - refDur;
	    
	    // Phi
	    double Phi = - M_PI * Kt * (diffTime)*(diffTime);

	    // Deramping or Reramping 
	    if (!m_DerampMode)
	      {
		Phi *= -1;
	      }

	    ImagePixelType outPixel; // Complex 
	    outPixel.real((InIt.Get().real() * std::cos(Phi)) - (InIt.Get().imag() * std::sin(Phi)));
	    outPixel.imag((InIt.Get().real() * std::sin(Phi)) + (InIt.Get().imag() * std::cos(Phi)));
	    
	    //////////// Assign Output ////////////
	    OutIt.Set(outPixel);

	    // Increment iterators
	    ++OutIt;
	    ++InIt;
	  } // End colunms (ouput)

	// Next Line
	OutIt.NextLine();
	InIt.NextLine();
      } // End lines (ouput)
  }


} /*namespace otb*/

#endif
