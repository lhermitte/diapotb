/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARCompensatedComplexImageFilter_h
#define otbSARCompensatedComplexImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "itkSimpleFastMutexLock.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"

#include "otbImageKeywordlist.h"


namespace otb
{
/** \class SARCompensatedComplexImageFilter 
 * \brief Creates an temporary image (to estimate conjugate multiplication) between two images. 
 * 
 * This filter built the conjugate multiplication between a master and a slave image.
 * A topographic phase can be used to create this temporary image.
 *
 * The output is a vector image with real part, imag part and modulus of conjugate multiplication.
 *
 * \ingroup DiapOTBModule
 */

  template <typename TImageSAR, typename TImagePhase, typename TImageOut> 
  class ITK_EXPORT SARCompensatedComplexImageFilter :
    public itk::ImageToImageFilter<TImageSAR,TImageOut>
{
public:

  // Standard class typedefs
  typedef SARCompensatedComplexImageFilter                    Self;
  typedef itk::ImageToImageFilter<TImageSAR,TImageOut>    Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;
  typedef itk::SmartPointer<const Self>                  ConstPointer;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(SARCompensatedComplexImageFilter,ImageToImageFilter);

  /** Typedef to image input type (master and slave reech images) :  otb::Image (Complex)  */
  typedef TImageSAR                                  ImageSARType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageSARType::Pointer             ImageSARPointer;
  typedef typename ImageSARType::ConstPointer        ImageSARConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageSARType::RegionType          ImageSARRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageSARType::PixelType           ImageSARPixelType;
  typedef typename ImageSARType::PointType           ImageSARPointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageSARType::IndexType           ImageSARIndexType;
  typedef typename ImageSARType::IndexValueType      ImageSARIndexValueType;
  typedef typename ImageSARType::SizeType            ImageSARSizeType;
  typedef typename ImageSARType::SizeValueType       ImageSARSizeValueType;
  typedef typename ImageSARType::SpacingType         ImageSARSpacingType;
  typedef typename ImageSARType::SpacingValueType    ImageSARSpacingValueType;
  
  /** Typedef to image output type : otb::VectorImage  */
  typedef TImageOut                                  ImageOutType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageOutType::Pointer             ImageOutPointer;
  typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageOutType::RegionType          ImageOutRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageOutType::PixelType           ImageOutPixelType;
  typedef typename ImageOutType::PointType           ImageOutPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageOutType::IndexType           ImageOutIndexType;
  typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
  typedef typename ImageOutType::SizeType            ImageOutSizeType;
  typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
  typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
  typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;


  // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  // Typedef for topographic Phase
  typedef TImagePhase                                      ImagePhaseType;
  typedef typename ImagePhaseType::Pointer                 ImagePhasePointer;
  typedef typename ImagePhaseType::ConstPointer            ImagePhaseConstPointer;
  typedef typename ImagePhaseType::RegionType              ImagePhaseRegionType;
  typedef typename ImagePhaseType::IndexType               ImagePhaseIndexType;
  typedef typename ImagePhaseType::IndexValueType          ImagePhaseIndexValueType;
  typedef typename ImagePhaseType::SizeType                ImagePhaseSizeType;
  typedef typename ImagePhaseType::SizeValueType           ImagePhaseSizeValueType;

  // Typedef for iterators
  typedef itk::ImageScanlineConstIterator< ImageSARType > InputSARIterator;
  typedef itk::ImageScanlineConstIterator< ImagePhaseType > InputPhaseIterator;
  typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;

  typedef itk::SimpleFastMutexLock         MutexType;

    
  // Setter/Getter for inputs (SAR images and potentially topographic phase) 
  /** Connect one of the operands for interferometry : Master */
  void SetMasterInput( const ImageSARType* image);

  /** Connect one of the operands for interferometry : Coregistrated Slave */
  void SetSlaveInput(const ImageSARType* image);

  /** Connect one of the operands for interferometry : Coregistrated Slave */
  void SetTopographicPhaseInput(const ImagePhaseType* imagePhaseTopo);

  /** Get the inputs */
  const ImageSARType* GetMasterInput() const;
  const ImageSARType * GetSlaveInput() const;
  const ImagePhaseType * GetTopographicPhaseInput() const;


protected:
  // Constructor
  SARCompensatedComplexImageFilter();

  // Destructor
  virtual ~SARCompensatedComplexImageFilter() ITK_OVERRIDE;

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;
  
  /** SARCompensatedComplexImageFilter produces an image which are into output geometry. The differences between 
   * output and input images can be the size of images and the dimensions. 
   * As such, SARCompensatedComplexImageFilter needs to provide an implementation for 
   * GenerateOutputInformation() in order to inform the pipeline execution model. 
   */ 
  virtual void GenerateOutputInformation() ITK_OVERRIDE;

  /** SARCompensatedComplexImageFilter needs a input requested region for all inputs.
   * As such, SARCompensatedComplexImageFilter needs to provide an implementation for 
   * GenerateInputRequestedRegion() in order to inform the pipeline execution model. 
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;


  /** 
   * SARCompensatedComplexImageFilterr can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData() routine
   * which is called for each processing thread. The main output image data is
   * allocated automatically by the superclass prior to calling
   * ThreadedGenerateData().  ThreadedGenerateData can only write to the
   * portion of the output image specified by the parameter
   * "outputRegionForThread"
   *
   * \sa ImageToImageFilter::ThreadedGenerateData(),
   *     ImageToImageFilter::GenerateData() */
  virtual void ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread, 
				    itk::ThreadIdType threadId) ITK_OVERRIDE;
  
  virtual void ThreadedGenerateDataWithoutTopographicPhase(const ImageOutRegionType& outputRegionForThread, 
							   itk::ThreadIdType threadId);
  virtual void ThreadedGenerateDataWithTopographicPhase(const ImageOutRegionType& outputRegionForThread, 
							itk::ThreadIdType threadId);


 private:
  SARCompensatedComplexImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 

  // SAR Dimensions
  int m_nbLinesSAR;
  int m_nbColSAR;

  

};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARCompensatedComplexImageFilter.txx"
#endif



#endif
