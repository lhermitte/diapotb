/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbTilesAnalysisWithStepImageFilter_h
#define otbTilesAnalysisWithStepImageFilter_h


#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "itkSimpleFastMutexLock.h"
#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"

#include "otbTilesAnalysisImageFilter.h"

#include "otbImageKeywordlist.h"
#include "otbVectorImage.h"

namespace otb
{
/** \class TilesAnalysisWithStepImageFilter 
 * \brief Creates/Analyses tiles fixed by a given size and an input step. 
 * 
 * This filter :
 * _ creates output tiles with given size and step
 * _ select input requested region with built tiles, a given margin and possibly an input grid of deformations.
 * _ scan all tiles to apply a choosen fonction
 * _ assign output with accumulations
 * 
 * This filter can be used for phase filtering.
 *
 * Limitations : This filter works only with a streaming by strips
 * \ingroup DiapOTBModule
 */

  template <typename TImageIn,  typename TImageOut, typename TFunction> 
class ITK_EXPORT TilesAnalysisWithStepImageFilter :
    public otb::TilesAnalysisImageFilter<TImageIn,TImageOut, TFunction>
{
public:

  // Standard class typedefs
  typedef TilesAnalysisWithStepImageFilter                             Self;
  typedef otb::TilesAnalysisImageFilter<TImageIn,TImageOut, TFunction> Superclass;
  typedef itk::SmartPointer<Self>                                      Pointer;
  typedef itk::SmartPointer<const Self>                                ConstPointer;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(TilesAnalysisWithStepImageFilter,ImageToImageFilter);

  /** Typedef to image input type :  otb::Image (Complex or real)  */
  typedef TImageIn                                  ImageInType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageInType::Pointer             ImageInPointer;
  typedef typename ImageInType::ConstPointer        ImageInConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageInType::RegionType          ImageInRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageInType::PixelType           ImageInPixelType;
  typedef typename ImageInType::PointType           ImageInPointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageInType::IndexType           ImageInIndexType;
  typedef typename ImageInType::IndexValueType      ImageInIndexValueType;
  typedef typename ImageInType::SizeType            ImageInSizeType;
  typedef typename ImageInType::SizeValueType       ImageInSizeValueType;
  typedef typename ImageInType::SpacingType         ImageInSpacingType;
  typedef typename ImageInType::SpacingValueType    ImageInSpacingValueType;
  
  /** Typedef to image output type : otb::Image (Complex or real)  */
  typedef TImageOut                                  ImageOutType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageOutType::Pointer             ImageOutPointer;
  typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageOutType::RegionType          ImageOutRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageOutType::PixelType           ImageOutPixelType;
  typedef typename ImageOutType::PointType           ImageOutPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageOutType::IndexType           ImageOutIndexType;
  typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
  typedef typename ImageOutType::SizeType            ImageOutSizeType;
  typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
  typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
  typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;

  // Define Point2DType and Point3DType
  using Point2DType = itk::Point<double,2>;
  using Point3DType = itk::Point<double,3>;

  typedef itk::SimpleFastMutexLock                MutexType;

  // Typedef for iterators
  typedef itk::ImageScanlineConstIterator< ImageInType > InputIterator;
  typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;

  // Typedef for Functor
  typedef TFunction                                  FunctionType;
  typedef typename FunctionType::Pointer             FunctionPointer;

  // Setter
  itkSetMacro(Step, unsigned int);
  // Getter
  itkGetMacro(Step, unsigned int);

protected:
  // Constructor
  TilesAnalysisWithStepImageFilter();

  // Destructor
  ~TilesAnalysisWithStepImageFilter();

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const override;
  
  /** TilesAnalysisWithStepImageFilter produces an image which are into output geometry. The differences between 
   * output and input images can be the size of images and the dimensions. 
   * As such, TilesAnalysisWithStepImageFilter needs to provide an implementation for 
   * GenerateOutputInformation() in order to inform the pipeline execution model. 
   */ 
  void GenerateOutputInformation() override;

  
  /** TilesAnalysisWithStepImageFilter needs a input requested region that corresponds to the margin and shifts 
   * into the requested region of our output requested region. The output requested region needs to be modified
   * to construct as wanted tiles with input size and step.
   * As such, TilesAnalysesImageFilter needs to provide an implementation for 
   * GenerateInputRequestedRegion() in order to inform the pipeline execution model. 
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  void GenerateInputRequestedRegion() override;
 
  using Superclass::ThreadedGenerateData;
  /** startIndex and stopIndex represent the indices of tiles to process in thread threadId and index for the
      first tile */
  void ThreadedGenerateData(unsigned int startTileId, unsigned int stopTileId, 
			    unsigned int firstTileIndexL, unsigned int firstTileIndexC,
			    itk::ThreadIdType threadId);

  void BeforeThreadedGenerateData() override;

  void AfterThreadedGenerateData() override;

 /** Static function used as a "callback" by the MultiThreader.  The threading
  * library will call this routine for each thread, which will delegate the
  * control to ThreadedGenerateData(). */
  static ITK_THREAD_RETURN_TYPE ThreaderCallback(void *arg);

  /** Internal structure used for passing image data into the threading library */
  struct ThreadStruct
  {
    Pointer Filter;
  };

  /** 
   * \sa ImageToImageFilter::ThreadedGenerateData(),
   *     ImageToImageFilter::GenerateData() */
  void GenerateData() override;


 private:
  TilesAnalysisWithStepImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 
  

  // Step to scan input image
  unsigned int m_Step;

  MutexType * m_Mutex;

  // Store previous/effective index and size of output requested region and expect new ones
  // Used into GenerateInputRequestedRegion to define output requested region
  ImageOutIndexType m_expectedIndex;
  ImageOutSizeType m_expectedSize;
  ImageOutIndexType m_saveIndex;
  ImageOutSizeType m_saveSize;

  // Store previous output region with it size : to initialize correctly current output region
  ImageOutSizeType m_oldSize;
  ImageOutPixelType * m_saveOutputBuffer;

};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbTilesAnalysisWithStepImageFilter.txx"
#endif



#endif
