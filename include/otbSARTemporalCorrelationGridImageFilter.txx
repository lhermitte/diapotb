/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARTemporalCorrelationGridImageFilter_txx
#define otbSARTemporalCorrelationGridImageFilter_txx

#include "otbSARTemporalCorrelationGridImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkProgressReporter.h"
#include "itkNumericTraitsPointPixel.h"
#include "itkContinuousIndex.h"

#include "otbDEMHandler.h"

#include "itkFFTWCommon.h"
#include "itkFFTWGlobalConfiguration.h"

#include <cmath>


namespace otb
{
  /** 
   * Constructor with default initialization
   */
  template <class TImageIn, class TImageOut> 
  SARTemporalCorrelationGridImageFilter< TImageIn ,TImageOut >::SARTemporalCorrelationGridImageFilter()
  {
    this->SetNumberOfRequiredInputs(2);
 
    // Default window sizes
    m_WinAround_ShiftRan = 10;
    m_WinAround_ShiftAzi = 20;
    m_WinCor_ran = 10;
    m_WinCor_azi = 10;    
 
    // Default rough shits (0)
    m_RoughShift_ran = 0;
    m_RoughShift_azi = 0;

    // Grid Step
    m_GridStep.Fill(1);
    m_GridStep_ML.Fill(1);

    m_CorrelationThreshold = 0.3;

    m_FirstLCOffset = true;

    m_MLran = 3;
    m_MLazi = 3;
  }

  /**
   * Set Master Image
   */ 
  template <class TImageIn, class TImageOut> 
  void
  SARTemporalCorrelationGridImageFilter< TImageIn ,TImageOut >
  ::SetMasterInput(const ImageInType* image )
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(0, const_cast<ImageInType *>(image));
  }

  /**
   * Set Slave Image
   */ 
  template <class TImageIn, class TImageOut> 
  void
  SARTemporalCorrelationGridImageFilter< TImageIn ,TImageOut >
  ::SetSlaveInput(const ImageInType* image)
  {
    // Process object is not const-correct so the const casting is required.
    this->SetNthInput(1, const_cast<ImageInType *>(image));
  }

  /**
   * Get Master Image
   */ 
  template <class TImageIn, class TImageOut> 
  const typename SARTemporalCorrelationGridImageFilter< TImageIn ,TImageOut>::ImageInType *
  SARTemporalCorrelationGridImageFilter< TImageIn ,TImageOut >
  ::GetMasterInput() const
  {
    if (this->GetNumberOfInputs()<1)
      {
	return 0;
      }
    return static_cast<const ImageInType *>(this->itk::ProcessObject::GetInput(0));
  }

  /**
   * Get Slave Image
   */ 
  template <class TImageIn, class TImageOut> 
  const typename SARTemporalCorrelationGridImageFilter< TImageIn ,TImageOut>::ImageInType *
  SARTemporalCorrelationGridImageFilter< TImageIn ,TImageOut >
  ::GetSlaveInput() const
  {
    if (this->GetNumberOfInputs()<2)
      {
	return 0;
      }
    return static_cast<const ImageInType *>(this->itk::ProcessObject::GetInput(1));
  }

  /**
   * Print
   */
  template<class TImageIn, class TImageOut>
  void
  SARTemporalCorrelationGridImageFilter< TImageIn, TImageOut >
  ::PrintSelf(std::ostream & os, itk::Indent indent) const
  {
    Superclass::PrintSelf(os, indent);
    
    os << indent << "ML range factor : " << m_MLran << std::endl;
    os << indent << "ML azimut factor : " << m_MLazi << std::endl;
    os << indent << "Step for the grid : " << m_GridStep[0] << ", " << m_GridStep[1] << std::endl;
  }

  /**
   * Method GenerateOutputInformaton()
   **/
  template<class TImageIn, class TImageOut>
  void
  SARTemporalCorrelationGridImageFilter< TImageIn, TImageOut >
  ::GenerateOutputInformation()
  {    
    // Call superclass implementation
    Superclass::GenerateOutputInformation();

    // Get pointers to the input and output
    ImageInType * masterPtr  = const_cast<ImageInType * >(this->GetMasterInput());
    ImageInType * slavePtr  = const_cast<ImageInType * >(this->GetSlaveInput());
    ImageOutPointer outputPtr = this->GetOutput();
  
    // Check GridSteps (must be a multiple of ML Factors)
    if (m_GridStep[0] % m_MLran)
      {
	itkExceptionMacro(<<"GridSteps range mot a multiple of MLRan.");
      }
    if (m_GridStep[1] % m_MLazi)
      {
	itkExceptionMacro(<<"GridSteps azimut mot a multiple of MLAzi.");
      }

    m_GridStep_ML[0] = m_GridStep[0]/m_MLran;
    m_GridStep_ML[1] = m_GridStep[1]/m_MLazi;

    ///////// Checks (with input keywordlists/metadata) /////////////
    // Check ML Factors (inputs of this filter with Master and Slave metadata)
    // Get Master and Slave Keywordlist
    ImageKeywordlist masterKWL = masterPtr->GetImageKeywordlist();
    ImageKeywordlist slaveKWL = slavePtr->GetImageKeywordlist();

    if (masterKWL.HasKey("support_data.ml_ran") && masterKWL.HasKey("support_data.ml_azi"))
      {
	// Get Master ML Factors
	unsigned int master_MLRan = atoi(masterKWL.GetMetadataByKey("support_data.ml_ran").c_str());
	unsigned int master_MLAzi = atoi(masterKWL.GetMetadataByKey("support_data.ml_azi").c_str());

	if (slaveKWL.HasKey("support_data.ml_ran") && slaveKWL.HasKey("support_data.ml_azi"))
	  {
	    // Get Slave ML Factors
	    unsigned int slave_MLRan = atoi(slaveKWL.GetMetadataByKey("support_data.ml_ran").c_str());
	    unsigned int slave_MLAzi = atoi(slaveKWL.GetMetadataByKey("support_data.ml_azi").c_str());

	    if ((slave_MLRan != master_MLRan) || (slave_MLAzi != master_MLAzi))
	      {
		itkExceptionMacro(<<"ML Factor betwwen master and slave are different.");
	      }
	  }
	
	if ((master_MLRan != m_MLran) || (master_MLAzi != m_MLazi))
	  {
	    itkExceptionMacro(<<"ML Factor betwwen master and inputs of this application are different.");
	  }
      }
    else
      {
	if (slaveKWL.HasKey("support_data.ml_ran") && slaveKWL.HasKey("support_data.ml_azi"))
	  {
	    // Get Slave ML Factors
	    unsigned int slave_MLRan = atoi(slaveKWL.GetMetadataByKey("support_data.ml_ran").c_str());
	    unsigned int slave_MLAzi = atoi(slaveKWL.GetMetadataByKey("support_data.ml_azi").c_str());
	    
	    if ((slave_MLRan != m_MLran) || (slave_MLAzi != m_MLazi))
	      {
		itkExceptionMacro(<<"ML Factor betwwen slave and inputs of this application are different.");
	      }
	  }
      }

    // Set the number of Components for the Output VectorImage
    // 3 Components :  
    //                _ range shift between Master and Slave
    //                _ azimut shift between Master and Slave
    //                _ correlation rate
    outputPtr->SetNumberOfComponentsPerPixel(3);

    // Update size and spacing according to grid step
    ImageOutRegionType largestRegion  = static_cast<ImageOutRegionType>(masterPtr->GetLargestPossibleRegion());
    ImageOutSizeType outputSize = static_cast<ImageOutSizeType>(largestRegion.GetSize());
    ImageOutSpacingType outputSpacing = static_cast<ImageOutSpacingType>(masterPtr->GetSpacing());
    ImageOutPointType outputOrigin = static_cast<ImageOutPointType>(masterPtr->GetOrigin());

    // First/Last Colunm and row with an offet
    int firstC_master = 0;
    int firstL_master = 0;
    if (m_FirstLCOffset)
      {
	int abs_RoughShift_ran = std::abs(m_RoughShift_ran);
	int abs_RoughShift_azi = std::abs(m_RoughShift_azi);
	
	int firstC = std::max (1 + m_WinCor_ran, 1 + m_WinCor_ran - abs_RoughShift_ran + m_WinAround_ShiftRan);
	int firstL = std::max (1 + m_WinCor_azi, 1 + m_WinCor_azi - abs_RoughShift_azi + m_WinAround_ShiftAzi);
	int lastC = std::min(slavePtr->GetLargestPossibleRegion().GetSize()[0] - abs_RoughShift_ran - 
			     m_WinAround_ShiftRan - m_WinCor_ran, 
			     masterPtr->GetLargestPossibleRegion().GetSize()[0]- m_WinCor_ran);
	int lastL = std::min(slavePtr->GetLargestPossibleRegion().GetSize()[1] - abs_RoughShift_azi - 
			     m_WinAround_ShiftAzi- m_WinCor_azi, 
			     masterPtr->GetLargestPossibleRegion().GetSize()[1] - m_WinCor_azi);
    
	outputSize[0]= lastC - firstC + 1;
	outputSize[1] = lastL - firstL + 1;

	for(unsigned int dim = 0; dim < ImageOutType::ImageDimension; ++dim)
	  {
	    outputSize[dim] = (outputSize[dim] - outputSize[dim]%m_GridStep_ML[dim]) / m_GridStep_ML[dim];
	    outputSpacing[dim] *= m_GridStep_ML[dim];
	  }

	firstL_master   = firstL + (lastL - firstL + 1 - m_GridStep_ML[1] * (outputSize[1])) / 2;
	firstC_master = firstC + (lastC - firstC + 1 - m_GridStep_ML[0] * (outputSize[0])) / 2;
      }
    else
      {
	for(unsigned int dim = 0; dim < ImageOutType::ImageDimension; ++dim)
	  {
	    outputSize[dim] = (outputSize[dim] - outputSize[dim]%m_GridStep_ML[dim]) / m_GridStep_ML[dim];
	    outputSpacing[dim] *= m_GridStep_ML[dim];
	  }
      }

    
    
    // Set spacing
    outputPtr->SetSpacing(outputSpacing);

    // Set origin
    outputPtr->SetOrigin(outputOrigin);

    // Set largest region size
    largestRegion.SetSize(outputSize);
    outputPtr->SetLargestPossibleRegion(largestRegion);

    // Add GridSteps into KeyWordList
    ImageKeywordlist outputKWL;
    outputKWL.AddKey("support_data.gridstep.range", std::to_string(m_GridStep[0]));
    outputKWL.AddKey("support_data.gridstep.azimut", std::to_string(m_GridStep[1]));
    
    if (m_FirstLCOffset)
      {
	// Add first L/C into SLC geometry
	float firstC_SLC = (firstC_master)*m_MLran;  
	float firstL_SLC = (firstL_master)*m_MLazi;

	outputKWL.AddKey("support_data.grid.first_row", std::to_string(firstL_SLC));
	outputKWL.AddKey("support_data.grid.first_column", std::to_string(firstC_SLC));
      }

    outputPtr->SetImageKeywordList(outputKWL); 
  }

  /** 
   * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
   */
  template<class TImageIn, class TImageOut>
  void
  SARTemporalCorrelationGridImageFilter< TImageIn, TImageOut >
  ::OutputRegionToInputRegion(const ImageOutRegionType& outputRegion)
  {
    // Get pointers to inputs
    ImageInType * masterPtr = const_cast<ImageInType * >(GetMasterInput());
    ImageInType * slavePtr = const_cast<ImageInType * >(GetSlaveInput());

    ImageOutPointer outputPtr = this->GetOutput();
    
    int nbColSlave = slavePtr->GetLargestPossibleRegion().GetSize()[0];
    int nbLinesSlave = slavePtr->GetLargestPossibleRegion().GetSize()[1];
    
    /////////////////////// Requested region for master ////////////////////
    // Correlation grid is bonded to the master image => should represent with the 
    // m_Grid_step the output requested region
    // get a copy of the master requested region (should equal the output
    // requested region)
    ImageInRegionType masterRequestedRegion, slaveRequestedRegion;
    masterRequestedRegion = outputRegion;

    // Apply grid step
    ImageInSizeType masterRequestedSize = masterRequestedRegion.GetSize();
    ImageInIndexType masterRequestedIndex = masterRequestedRegion.GetIndex();
  
    for(unsigned int dim = 0; dim < ImageOutType::ImageDimension; ++dim)
      {
	masterRequestedSize [dim] *= m_GridStep_ML[dim];
	masterRequestedIndex[dim] *= m_GridStep_ML[dim];
      }
    
    int firstC_Grid = 0;
    int firstL_Grid = 0;
    int abs_RoughShift_ran = std::abs(m_RoughShift_ran);
    int abs_RoughShift_azi = std::abs(m_RoughShift_azi);
    
     if (m_FirstLCOffset)
       {
	 int firstC = std::max (1 + m_WinCor_ran, 1 + m_WinCor_ran - abs_RoughShift_ran + m_WinAround_ShiftRan);
	 int firstL = std::max (1 + m_WinCor_azi, 1 + m_WinCor_azi - abs_RoughShift_azi + m_WinAround_ShiftAzi);
	 int lastC = std::min(slavePtr->GetLargestPossibleRegion().GetSize()[0] - abs_RoughShift_ran - 
			      m_WinAround_ShiftRan - m_WinCor_ran, 
			      masterPtr->GetLargestPossibleRegion().GetSize()[0]- m_WinCor_ran);
	 int lastL = std::min(slavePtr->GetLargestPossibleRegion().GetSize()[1] - abs_RoughShift_azi - 
			      m_WinAround_ShiftAzi- m_WinCor_azi, 
			      masterPtr->GetLargestPossibleRegion().GetSize()[1] - m_WinCor_azi);
	 

	 firstC_Grid = firstC + (lastC - firstC + 1 - m_GridStep_ML[0] * 
				 (outputPtr->GetLargestPossibleRegion().GetSize()[0])) / 2;
	 firstL_Grid = firstL + (lastL - firstL + 1 - m_GridStep_ML[1] * 
				 (outputPtr->GetLargestPossibleRegion().GetSize()[1])) / 2;
       }
     
    // With correlation window and first row/colunm
    masterRequestedIndex[0] -= (m_WinCor_ran - firstC_Grid);
    masterRequestedIndex[1] -= (m_WinCor_azi - firstL_Grid);
    masterRequestedSize[0] += (2*m_WinCor_ran+1);
    masterRequestedSize[1] += (2*m_WinCor_azi+1);

    masterRequestedRegion.SetSize(masterRequestedSize);
    masterRequestedRegion.SetIndex(masterRequestedIndex);

    masterPtr->SetRequestedRegion(masterRequestedRegion);

     // Crop the fixed region at the fixed's largest possible region (or buffered)
     if ( masterRequestedRegion.Crop(masterPtr->GetLargestPossibleRegion()))
      {
	masterPtr->SetRequestedRegion( masterRequestedRegion );
      }
    else
      {
	// Couldn't crop the region (requested region is outside the largest
	// possible region).  Throw an exception.
	// store what we tried to request (prior to trying to crop)
	masterPtr->SetRequestedRegion( masterRequestedRegion );
	
	// Build an exception
	itk::InvalidRequestedRegionError e(__FILE__, __LINE__);
	std::ostringstream msg;
	msg << this->GetNameOfClass()
	    << "::GenerateInputRequestedRegion()";
	e.SetLocation(msg.str().c_str());
	e.SetDescription("Requested region is (at least partially) outside the largest possible region of image 1.");
	e.SetDataObject(masterPtr);
	throw e;
      }
    
    //////////////////// Requested region for slave ////////////////////
    int firstL, firstC, lastL, lastC; 
    // Transform back into slave region index space with an maximum shift between master and slave
    ImageInIndexType slaveIndex;
   
    // Find requested region
    ImageInSizeType slaveSize;

    // Set the max_shift (margin)
    firstL = masterRequestedIndex[1] - abs_RoughShift_azi - m_WinAround_ShiftAzi - m_WinCor_azi;
    firstC = masterRequestedIndex[0] - abs_RoughShift_ran - m_WinAround_ShiftRan - m_WinCor_ran;
    lastL = masterRequestedIndex[1] + masterRequestedSize[1] + abs_RoughShift_azi + m_WinAround_ShiftAzi + 
      m_WinCor_azi;
    lastC =  masterRequestedIndex[0] + masterRequestedSize[0] + abs_RoughShift_ran + m_WinAround_ShiftRan + m_WinCor_ran;

    // Check the limits
    if (firstC < 0)
      {
	firstC = 0;
      } 
    if (firstL < 0)
      {
	firstL = 0;
      } 
    if (lastC > nbColSlave-1)
      {
	lastC = nbColSlave-1;
      } 
    if (lastL > nbLinesSlave-1)
      {
	lastL = nbLinesSlave-1;
      } 

    
    slaveIndex[0] = static_cast<ImageInIndexValueType>(firstC);
    slaveIndex[1] = static_cast<ImageInIndexValueType>(firstL);
    slaveSize[0] = static_cast<ImageInIndexValueType>(lastC - firstC)+1;
    slaveSize[1] = static_cast<ImageInIndexValueType>(lastL - firstL)+1;

    slaveRequestedRegion.SetIndex(slaveIndex);
    slaveRequestedRegion.SetSize(slaveSize);

    // crop the moving region at the moving's largest possible region
    if ( slaveRequestedRegion.Crop(slavePtr->GetLargestPossibleRegion()))
      {
	slavePtr->SetRequestedRegion( slaveRequestedRegion );
      }
    else
      {
	// Couldn't crop the region (requested region is outside the largest
	// possible region). This case might happen so we do not throw any exception but
	// request a null region instead
	slaveSize.Fill(0);
	slaveRequestedRegion.SetSize(slaveSize);
	slaveIndex.Fill(0);
	slaveRequestedRegion.SetIndex(slaveIndex);
	
	// store what we tried to request (prior to trying to crop)
	slavePtr->SetRequestedRegion(slaveRequestedRegion);
      }
  }


  /** 
   * Method GenerateInputRequestedRegion
   */
  template<class TImageIn, class TImageOut>
  void
  SARTemporalCorrelationGridImageFilter< TImageIn, TImageOut >
  ::GenerateInputRequestedRegion()
  {
    // Call the superclass' implementation of this method
    Superclass::GenerateInputRequestedRegion();
  
    ImageOutRegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
    this->OutputRegionToInputRegion(outputRequestedRegion);
   }
 

 /**
   * Method BeforeThreadedGenerateData
   */
  template<class TImageIn, class TImageOut>
  void
  SARTemporalCorrelationGridImageFilter< TImageIn, TImageOut >
  ::ThreadedGenerateData(const ImageOutRegionType& outputRegionForThread,
			 itk::ThreadIdType threadId)
  {
    // Get pointers to inputs
    const ImageInType * masterPtr = this->GetMasterInput();
    const ImageInType * slavePtr = this->GetSlaveInput();

    ImageOutPointer outPtr = this->GetOutput();

    // Typedef for iterators
    typedef itk::ImageScanlineIterator< ImageOutType > OutputIterator;
    typedef itk::ImageRegionConstIterator<ImageInType> MasterSlaveIt;
    
    // Iterator on output (Grid geometry)
    OutputIterator OutIt(this->GetOutput(), outputRegionForThread);
    OutIt.GoToBegin();
    
    ImageOutPixelType pixelCorrelationGrid;
    pixelCorrelationGrid.Reserve(3);

    // Support progress methods/callbacks
    itk::ProgressReporter progress(this, threadId, this->GetOutput()->GetRequestedRegion().GetNumberOfPixels());

    int nbColSlave = slavePtr->GetLargestPossibleRegion().GetSize()[0];
    int nbLinesSlave = slavePtr->GetLargestPossibleRegion().GetSize()[1];

    // Array to store correlation rate for each pixel and each shift
    std::vector< std::vector<double> >  arrayRate (2*m_WinAround_ShiftAzi+1, std::vector<double>(2*m_WinAround_ShiftRan+1));
    float arrayPic[3][3];

    float dx, dy, VAL_MAX;
    int checkInterpolation;

    // Size for correlation estimation
    ImageInSizeType masterSize;
    masterSize[0] = 2*m_WinCor_ran+1;
    masterSize[1] = 2*m_WinCor_azi+1;

    ImageInSizeType slaveSize;
    slaveSize[0] = 2*m_WinCor_ran+1;
    slaveSize[1] = 2*m_WinCor_azi+1;

    // vector master and slave
    std::vector<double> master;
    std::vector<double> slave;

    // First L/C for the grid
    int firstC_Grid = 0;
    int firstL_Grid = 0;

    if (m_FirstLCOffset)
       {
	 int abs_RoughShift_ran = std::abs(m_RoughShift_ran);
	 int abs_RoughShift_azi = std::abs(m_RoughShift_azi);
	 
	 int firstC = std::max (1 + m_WinCor_ran, 1 + m_WinCor_ran - abs_RoughShift_ran + m_WinAround_ShiftRan);
	 int firstL = std::max (1 + m_WinCor_azi, 1 + m_WinCor_azi - abs_RoughShift_azi + m_WinAround_ShiftAzi);
	 int lastC = std::min(slavePtr->GetLargestPossibleRegion().GetSize()[0] - abs_RoughShift_ran - 
			      m_WinAround_ShiftRan - m_WinCor_ran, 
			      masterPtr->GetLargestPossibleRegion().GetSize()[0]- m_WinCor_ran);
	 int lastL = std::min(slavePtr->GetLargestPossibleRegion().GetSize()[1] - abs_RoughShift_azi - 
			      m_WinAround_ShiftAzi- m_WinCor_azi, 
			      masterPtr->GetLargestPossibleRegion().GetSize()[1] - m_WinCor_azi);
	 
	 firstC_Grid = firstC + (lastC - firstC + 1 - m_GridStep_ML[0] * 
				 (outPtr->GetLargestPossibleRegion().GetSize()[0])) / 2;
	 firstL_Grid = firstL + (lastL - firstL + 1 - m_GridStep_ML[1] * 
				 (outPtr->GetLargestPossibleRegion().GetSize()[1])) / 2;
       }

    // For each line of output 
    while ( !OutIt.IsAtEnd())
      {
	OutIt.GoToBeginOfLine();

	// For each colunm of the grid
       	while (!OutIt.IsAtEndOfLine()) 
	  {
	    // Get index
	    ImageInIndexType masterIndex;
	    
	    for(unsigned int dim = 0; dim < ImageInType::ImageDimension; ++dim)
	      {
		masterIndex[dim] = OutIt.GetIndex()[dim] * m_GridStep_ML[dim]; // Grid geometry
	      }

	    // To center correlation estimation and shift to the first L/C
	    masterIndex[0] -= (m_WinCor_ran-firstC_Grid);
	    masterIndex[1] -= (m_WinCor_azi-firstL_Grid);

	    // Build the master region (for correlation estimation)
	    ImageInRegionType masterCurrentRegion;
	    masterCurrentRegion.SetIndex(masterIndex);
	    masterCurrentRegion.SetSize(masterSize);
	    masterCurrentRegion.Crop(masterPtr->GetLargestPossibleRegion());
	    
	    // Iterators on master Ptr
	    MasterSlaveIt masterIt(masterPtr, masterCurrentRegion);
	    masterIt.GoToBegin();

	    
	    // Fill master patch
	    master.clear();
	    while( !masterIt.IsAtEnd())
	      {
		// Add to the master vector
		double value = masterIt.Get();
		master.push_back(value);

		++masterIt;
	      }

	    // Best integer shifts and correlation rate
	    int bestShift_ran = 0;
	    int bestShift_azi = 0;
	    double bestRate = 0;

	    // Loop on shift around rough ones
	    for (int l = -m_WinAround_ShiftAzi; l <= m_WinAround_ShiftAzi; l++)
	      {
		for (int c = -m_WinAround_ShiftRan; c <= m_WinAround_ShiftRan; c++)
		  {
		    // Find slave index for current shifts
		    ImageInIndexType slaveIndex;
		    slaveIndex[0] = masterIndex[0] + c + m_RoughShift_ran;
		    slaveIndex[1] = masterIndex[1] + l + m_RoughShift_azi;
		    //slaveIndex[0] = masterIndex[0];
		    //slaveIndex[1] = masterIndex[1];

		    // Check Index
		    if (slaveIndex[0] < 0)
		      {
			slaveIndex[0] = 0;
		      }
		    if (slaveIndex[1] < 0) 
		      {
			slaveIndex[1] = 0;
		      }
		    if (slaveIndex[0] > (nbColSlave-(2*m_WinCor_ran)))
		      {
			slaveIndex[0] = nbColSlave-2*m_WinCor_ran;
		      }
		    if (slaveIndex[1] > (nbLinesSlave-(2*m_WinCor_azi)))
		      {
			slaveIndex[1] = nbLinesSlave-2*m_WinCor_azi;
		      }

		    
		    // Build the slave region (for correlation estimation)
		    ImageInRegionType slaveCurrentRegion;
		    slaveCurrentRegion.SetIndex(slaveIndex);
		    slaveCurrentRegion.SetSize(slaveSize);
		    slaveCurrentRegion.Crop(slavePtr->GetLargestPossibleRegion());
	    
		    // Iterators on slave Ptr
		    MasterSlaveIt slaveIt(slavePtr, slaveCurrentRegion);
		    slaveIt.GoToBegin();
	    
		    // Fill slave patch
		    slave.clear();
		    while (!slaveIt.IsAtEnd())
		      {
			// Add to the slave vector
			double value = slaveIt.Get();
			slave.push_back(value);

			++slaveIt;
		      }
		    
		    //arrayRate[l][c] = Correlation(master, slave);

		    arrayRate[l + m_WinAround_ShiftAzi][c + m_WinAround_ShiftRan] = 
		      CorrelationDiapason(master, slave);

		    if (arrayRate[l+ m_WinAround_ShiftAzi][c+ m_WinAround_ShiftRan] > bestRate)
		      {
			bestRate = arrayRate[l + m_WinAround_ShiftAzi][c + m_WinAround_ShiftRan];
			bestShift_ran = c;
			bestShift_azi = l;
		      }
		  }
	      }


	    // Function PIC on the best shifts (interpolation)
	    if ((std::abs(bestShift_ran) < m_WinAround_ShiftRan) && 
		(std::abs(bestShift_azi) < m_WinAround_ShiftAzi) && 
		(bestRate >= m_CorrelationThreshold))
	      {
		// Built array_pic
		for (int k=-1;k<2;k++)
		  for (int  j=-1;j<2;j++)
		    arrayPic[k+1][j+1] = static_cast<float>(arrayRate[bestShift_azi + k + m_WinAround_ShiftAzi]
							    [bestShift_ran + j + m_WinAround_ShiftRan]);
		
		// Launch interpolation
		PIC(arrayPic, &dx, &dy, &VAL_MAX, &checkInterpolation);
		
		pixelCorrelationGrid[0] = m_RoughShift_ran + bestShift_ran + dx; 
		pixelCorrelationGrid[1] = m_RoughShift_azi + bestShift_azi + dy; 
		pixelCorrelationGrid[2] = VAL_MAX;

	      }
	    else 
	      {
		pixelCorrelationGrid[0] = m_RoughShift_ran + bestShift_ran; 
		pixelCorrelationGrid[1] = m_RoughShift_azi + bestShift_azi; 
		pixelCorrelationGrid[2] = 0;
	      }	
	    
	    
	    // Correlation into SLC geometry and not ML geometry
	    pixelCorrelationGrid[0] *= m_MLran;
	    pixelCorrelationGrid[1] *= m_MLazi;

	    // Assign Output
	    OutIt.Set(pixelCorrelationGrid);
	    progress.CompletedPixel();
	    
	    // Next Colunm for output
	    ++OutIt;
	  }
	
	// Next Line for output 
	OutIt.NextLine();
      }
  }


  /**
   * Method PIC
   */
  template<class TImageIn, class TImageOut>
  void
  SARTemporalCorrelationGridImageFilter< TImageIn, TImageOut >
  ::PIC(float TAB_PIC[3][3], float *DX, float *DY, float *VAL_MAX, int *CR) const
  {
    float		TAB_COS[3]= {-0.5,1.,-0.5};
    float		TAB_SIN[3]= {-0.8660254f,0.f,+0.8660254f};
    float		SXF,SYF,SF,N,A1,B1,SDXF,SDYF,A2,B2;
    float		AX, AY;
    int			i, j;
	
    // The method of least squares in cosinus
    *CR = 1;
    SF   = 0.;
    SXF  = 0.;
    SYF  = 0.;
  
    for (i=-1;i<2;i++)
      {
	for (j=-1;j<2;j++)
	  {
	    SF  = SF  + TAB_PIC[i+1][j+1];
	    SXF = SXF + TAB_PIC[i+1][j+1]*TAB_COS[j+1];		
	    SYF = SYF + TAB_PIC[i+1][j+1]*TAB_COS[i+1];	
	  }	
      }

    N = SF / 9.;
    A1 = SXF/ 4.5;
    B1 = SYF/ 4.5;
   
    // The method of least squares in sinus
    SDXF  = 0.; 
    SDYF  = 0.;
	  
    for (i=-1;i<2;i++)
      {
	for (j=-1;j<2;j++)
	  {
	    SDXF = SDXF + TAB_PIC[i+1][j+1]*TAB_SIN[j+1];		
	    SDYF = SDYF + TAB_PIC[i+1][j+1]*TAB_SIN[i+1];	
	  }	
      }

    A2 = SDXF/3.;        // 4*.75
    B2 = SDYF/3.;        // 4*.75 
             
    A2 = A2/1.5;
    B2 = B2/1.5; 
	
    // Max
    AX = std::sqrt (A1*A1+A2*A2);
    AY = std::sqrt (B1*B1+B2*B2);

    if (AX*AY == 0.)
      {                                                                                                        
      	*VAL_MAX = -1.;
        *DX = 0.; 
        *DY = 0.;               
        return;
      } 
    
    *DX = std::atan2 (A2,A1) /(2*M_PI/3.);
    *DY = std::atan2 (B2,B1) /(2*M_PI/3.);

    *VAL_MAX  = N + (AX + AY)* 0.5;
    *VAL_MAX  = std::max(*VAL_MAX,TAB_PIC[1][1]);
    *VAL_MAX  = std::min(static_cast<float>(1.),*VAL_MAX);
	                                                                                                  
    // Check values                                                                                             
    if ((fabs((double)*DX)>.75) || (fabs((double)*DY)>.75))
      {                                                                                   
      	*VAL_MAX = -3.;  
        *DX = 0.; 
        *DY = 0.;                  
      }
	
    return;                                                                                                                                
	
  }

  template<class TImageIn, class TImageOut>
  double
  SARTemporalCorrelationGridImageFilter< TImageIn, TImageOut >
  ::Correlation(const std::vector<double>& master, const std::vector<double>& slave) const
  {
    double meanSlave = 0;
    double meanMaster = 0;
    double correlationValue = 0;

    // Compute means
    for(unsigned int i = 0; i < master.size(); ++i)
      {
	meanSlave += slave[i];
	meanMaster += master[i];
      }
    meanSlave /= slave.size();
    meanMaster /= master.size();

    double crossProd = 0.;
    double squareSumMaster = 0.;
    double squareSumSlave = 0.;


    // Compute correlation
    for(unsigned int i = 0; i < master.size(); ++i)
      {
	crossProd += (slave[i]-meanSlave) * (master[i]-meanMaster);
	squareSumSlave += (slave[i]-meanSlave) * (slave[i]-meanSlave);
	squareSumMaster += (master[i]-meanMaster) * (master[i]-meanMaster);
      }

    correlationValue = std::abs(crossProd/(std::sqrt(squareSumSlave)*std::sqrt(squareSumMaster)));

    return correlationValue;
  }

   template<class TImageIn, class TImageOut>
  double
  SARTemporalCorrelationGridImageFilter< TImageIn, TImageOut >
  ::CorrelationDiapason(const std::vector<double>& master, const std::vector<double>& slave) const
  {
    double sumSlave = 0;
    double sumMaster = 0;
    double sumMasterSlave = 0;
    double sumSlaveSquare = 0;
    double sumMasterSquare = 0;

    double correlationValue = 0;

    // Compute sum, sum square and cross prod
    for(unsigned int i = 0; i < master.size(); ++i)
      {
	sumSlave += slave[i];
	sumMaster += master[i];
	sumMasterSlave += slave[i]*master[i];
	sumSlaveSquare += slave[i]*slave[i];
	sumMasterSquare += master[i]*master[i];
      }
    
    // Compute means
    double meanMaster = sumMaster/master.size();
    double meanSlave = sumSlave/slave.size();
    
    // Compute sigma and covariance
    double sigmaMaster = sumMasterSquare/master.size() - (meanMaster*meanMaster);
    double sigmaSlave = sumSlaveSquare/slave.size() - (meanSlave*meanSlave);
    double covariance = sumMasterSlave/master.size();
  


    if (sigmaMaster > 0 && sigmaSlave > 0)
      {
	correlationValue = (covariance - (meanMaster*meanSlave))/(std::sqrt(sigmaMaster*sigmaSlave));
      }

    return correlationValue;
  }
  


} /*namespace otb*/

#endif
