/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARUpdateMetadataImageFilter_h
#define otbSARUpdateMetadataImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkSmartPointer.h"
#include "itkPoint.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"

#include "otbImageKeywordlist.h"

namespace otb
{
/** \class SARUpdateMetadataImageFilter 
 * \brief Update some metadata after a correction chain. 
 * 
 * This filter updates some metadata (only metadata, input image remains the same).
 *
 * \ingroup DiapOTBModule
 */

  template <typename TImage> 
  class ITK_EXPORT SARUpdateMetadataImageFilter :
    public itk::ImageToImageFilter<TImage,TImage>
{
public:

  // Standard class typedefs
  typedef SARUpdateMetadataImageFilter                    Self;
  typedef itk::ImageToImageFilter<TImage,TImage>         Superclass;
  typedef itk::SmartPointer<Self>                        Pointer;
  typedef itk::SmartPointer<const Self>                  ConstPointer;

  /** Typedef to image input/output type  */
  typedef TImage                                  ImageType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageType::Pointer             ImagePointer;
  typedef typename ImageType::RegionType          ImageRegionType;

  // Method for creation through object factory
  itkNewMacro(Self);
  // Run-time type information
  itkTypeMacro(SARUpdateMetadataImageFilter,ImageToImageFilter);
  
   // Setter
  itkSetMacro(TimeFirstLine, std::string);
  itkSetMacro(SlantRange, double);

  // Getter
  itkGetMacro(TimeFirstLine, std::string);
  itkGetMacro(SlantRange, double);


protected:
  // Constructor
  SARUpdateMetadataImageFilter()
    {
      m_TimeFirstLine = "";
      m_SlantRange = 0.;
    };

  // Destructor
  virtual ~SARUpdateMetadataImageFilter() {};

  // Print
  void PrintSelf(std::ostream & os, itk::Indent indent) const ITK_OVERRIDE;

  /** SARUpdateMetadataImageFilter updates the imagekeywordlist for new output metadata.
   * As such, SARUpdateMetadataImageFilter needs to provide an implementation for 
   * GenerateOutputInformation() in order to inform the pipeline execution model. 
   */ 
  virtual void GenerateOutputInformation() ITK_OVERRIDE;

  void ThreadedGenerateData(const ImageRegionType& outputRegionForThread, itk::ThreadIdType threadId) 
    ITK_OVERRIDE;
   
 private:
  SARUpdateMetadataImageFilter(const Self&); // purposely not implemented
  void operator=(const Self &); // purposely not 

  // Precise metadata 
  std::string m_TimeFirstLine;
  double m_SlantRange;

};

} // End namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARUpdateMetadataImageFilter.txx"
#endif



#endif
