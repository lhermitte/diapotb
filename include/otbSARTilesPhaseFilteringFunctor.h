/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARTilesPhaseFilteringFunctor_h
#define otbSARTilesPhaseFilteringFunctor_h
#include <cassert>
#include <math.h>
#include <complex>

#include "itkVariableLengthVector.h"
#include "itkRGBPixel.h"
#include "itkRGBAPixel.h"
#include "itkObject.h"
#include "itkObjectFactory.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageScanlineIterator.h"
#include "itkImageRegionConstIterator.h"

#include "itkFFTWCommon.h"

namespace otb
{
namespace Function
{
/** \class SARTilesPhaseFilteringFunctor
* \brief PhaseFiltering functions (called into TilesAnalysis Filter).
*
* \ingroup DiapOTBModule
*
*/
template <class TImageIn, class TImageOut>
class SARTilesPhaseFilteringFunctor : public itk::Object
{
public:
  /** Standard class typedefs */
  typedef SARTilesPhaseFilteringFunctor        Self;
  typedef itk::Object                   Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Method for creation through the object factory */
  itkNewMacro(Self);

  /** Runtime information */
  itkTypeMacro(SARTilesPhaseFilteringFunctor, itk::Object);

  /** Typedef to image input type :  otb::Image (Complex)  */
  typedef TImageIn                                  ImageInType;
  /** Typedef to describe the inout image pointer type. */
  typedef typename ImageInType::Pointer             ImageInPointer;
  typedef typename ImageInType::ConstPointer        ImageInConstPointer;
  /** Typedef to describe the inout image region type. */
  typedef typename ImageInType::RegionType          ImageInRegionType;
  /** Typedef to describe the type of pixel and point for inout image. */
  typedef typename ImageInType::PixelType           ImageInPixelType;
  typedef typename ImageInType::PointType           ImageInPointType;
  /** Typedef to describe the image index, size types and spacing for inout image. */
  typedef typename ImageInType::IndexType           ImageInIndexType;
  typedef typename ImageInType::IndexValueType      ImageInIndexValueType;
  typedef typename ImageInType::SizeType            ImageInSizeType;
  typedef typename ImageInType::SizeValueType       ImageInSizeValueType;
  typedef typename ImageInType::SpacingType         ImageInSpacingType;
  typedef typename ImageInType::SpacingValueType    ImageInSpacingValueType;
  
  /** Typedef to image output type : otb::Image (Complex)  */
  typedef TImageOut                                  ImageOutType;
  /** Typedef to describe the output image pointer type. */
  typedef typename ImageOutType::Pointer             ImageOutPointer;
  typedef typename ImageOutType::ConstPointer        ImageOutConstPointer;
  /** Typedef to describe the output image region type. */
  typedef typename ImageOutType::RegionType          ImageOutRegionType;
  /** Typedef to describe the type of pixel and point for output image. */
  typedef typename ImageOutType::PixelType           ImageOutPixelType;
  typedef typename ImageOutType::PointType           ImageOutPointType;
  /** Typedef to describe the image index, size types and spacing for output image. */
  typedef typename ImageOutType::IndexType           ImageOutIndexType;
  typedef typename ImageOutType::IndexValueType      ImageOutIndexValueType;
  typedef typename ImageOutType::SizeType            ImageOutSizeType;
  typedef typename ImageOutType::SizeValueType       ImageOutSizeValueType;
  typedef typename ImageOutType::SpacingType         ImageOutSpacingType;
  typedef typename ImageOutType::SpacingValueType    ImageOutSpacingValueType;

  // Iterators
  typedef itk::ImageRegionConstIterator<ImageInType> InItType;
  typedef itk::ImageScanlineConstIterator< ImageInType > InputIterator;
  
  // ITK proxy to the fftw library
  typedef typename itk::fftw::Proxy<double>           FFTWProxyType;
  typedef typename FFTWProxyType::ComplexType         FFTProxyComplexType;
  typedef typename FFTWProxyType::PixelType           FFTProxyPixelType;
  typedef typename FFTWProxyType::PlanType            FFTProxyPlanType;
  
  // Setter
  itkSetMacro(SizeTiles, unsigned int);
  itkSetMacro(alpha, float);

  // Getter
  itkGetMacro(SizeTiles, unsigned int);
  itkGetMacro(alpha, float);


  /** Destructor */
  ~SARTilesPhaseFilteringFunctor() override {}
  
  /**
   * Method Initialize
   */
  void Initialize(int nbThreads=1) 
  {
    m_nbThreads = nbThreads;

    /////////////////////////////////////// Prepare FFT buffer /////////////////////////////////////
    // Allocate Tab
    m_FFTInTab = new FFTProxyComplexType*[nbThreads];
    m_FFTOutTab = new FFTProxyComplexType*[nbThreads];
    m_FFTInvInTab = new FFTProxyComplexType*[nbThreads];
    m_FFTInvOutTab = new FFTProxyComplexType*[nbThreads];
    
    
    m_FFTPlanTab = new FFTProxyPlanType[nbThreads];
    m_FFTInvPlanTab = new FFTProxyPlanType[nbThreads];
    
    // Memory allocation for each Thread
    for (int i = 0; i < nbThreads; i++) 
      {
	// For direct FFTs
	m_FFTInTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc((m_SizeTiles*m_SizeTiles) * 
									   sizeof(FFTProxyComplexType)));
	m_FFTOutTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc((m_SizeTiles*m_SizeTiles) * 
									   sizeof(FFTProxyComplexType)));
	
	
	// For inverse FFTs
	m_FFTInvInTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc(m_SizeTiles*m_SizeTiles * 
									   sizeof(FFTProxyComplexType)));
	m_FFTInvOutTab[i] = static_cast<FFTProxyComplexType*>(fftw_malloc(m_SizeTiles*m_SizeTiles * 
									       sizeof(FFTProxyComplexType)));

	// For Plans
	m_FFTPlanTab[i] = FFTWProxyType::Plan_dft_2d(m_SizeTiles,
							  m_SizeTiles,
							  m_FFTInTab[i],
							  m_FFTOutTab[i],
							  -1, // -1 = FFTW_FORWARD 
							  FFTW_ESTIMATE);
	
	m_FFTInvPlanTab[i] = FFTWProxyType::Plan_dft_2d(static_cast<int>(m_SizeTiles),
							     static_cast<int>(m_SizeTiles),
							     m_FFTInvInTab[i],
							     m_FFTInvOutTab[i],
							     +1,  // +1 = FFTW_BACKWARD
							     FFTW_ESTIMATE);
      }

  }
 
  
  /**
   * Method Terminate
   */
  void Terminate() 
  {
    /////////////////////////////////////// Free FFT buffer /////////////////////////////////////
    // Free Memory for each thread
    for (unsigned int i = 0; i < m_nbThreads; i++) 
      {
	// Destroy the FFT plans
	FFTWProxyType::DestroyPlan(m_FFTPlanTab[i]);
	FFTWProxyType::DestroyPlan(m_FFTInvPlanTab[i]);

	// Free fftw buffer
	fftw_free(m_FFTInTab[i]);
	fftw_free(m_FFTOutTab[i]);
	fftw_free(m_FFTInvInTab[i]);
	fftw_free(m_FFTInvOutTab[i]);
      }

    // Delete pointer arrays
    delete [] m_FFTInTab;
    delete [] m_FFTOutTab;
    delete [] m_FFTInvInTab;
    delete [] m_FFTInvOutTab;

    delete [] m_FFTPlanTab;
    delete [] m_FFTInvPlanTab;    
  }

  
  /**
   * Method Process
   */
  void Process(ImageInPointer inputPtr, ImageOutRegionType outputRegion, 
	       ImageOutPixelType * TilesResult, int threadId=0)
  {    
    // Check if the output Tile is cut 
    unsigned int lastInd = outputRegion.GetSize()[0]*outputRegion.GetSize()[1];
    if (lastInd < (m_SizeTiles*m_SizeTiles))
      {
	for (unsigned int i = 0; i < lastInd; i++)
	  {
	    TilesResult[i] = 0;
	  }
	return;
      }

    // Transpose outputRegion (current tile) into input image
    // Input (= output now)
    ImageInRegionType inputRegion = outputRegion;
    
    // Crop to be sure
    inputRegion.Crop(inputPtr->GetLargestPossibleRegion());



    ////// Estimate weights //////
    double * w = new double[m_SizeTiles];
    double ** wf = new double*[m_SizeTiles];
    for (unsigned int i = 0; i < m_SizeTiles; i++) 
      {
	wf[i] = new double[m_SizeTiles];
      }
    

    for (unsigned int i=0; i <= m_SizeTiles/2-1; i++) 
     {
       w[i] = 2.0*(i+1)/(float)m_SizeTiles;
       w[m_SizeTiles-i-1]=w[i];
       //w[i] = 1;
       //w[m_SizeTiles-i-1]=w[i];
     }

   for (unsigned int i=0; i < m_SizeTiles; i++)
     for (unsigned int j=0; j < m_SizeTiles; j++) wf[i][j] = w[i]*w[j];



   // Get Pointers for FFT and FFT-1
   FFTProxyComplexType * FFTIn = m_FFTInTab[threadId];
   FFTProxyComplexType * FFTOut = m_FFTOutTab[threadId];
   FFTProxyComplexType * FFTInvIn = m_FFTInvInTab[threadId]; ;
   FFTProxyComplexType * FFTInvOut = m_FFTInvOutTab[threadId];
   FFTProxyPlanType FFTPlan = m_FFTPlanTab[threadId]; 
   FFTProxyPlanType FFTInvPlan = m_FFTInvPlanTab[threadId];


   ////// Normalization (of complex/input) ///////
   // Iterator on input
   InItType InIt(inputPtr, inputRegion);
   InIt.GoToBegin();
   
   int compteur = 0;
   double norm;
   double real;
   double imag;

   while (!InIt.IsAtEnd())
     {
       real = InIt.Get().real();
       imag = InIt.Get().imag();
       norm = sqrt(real*real + imag*imag);
       
       if (norm != 0)
	 {
	   FFTIn[compteur][0] = real/norm; 
	   FFTIn[compteur][1] = imag/norm;
	 }
       else
	 {
	   FFTIn[compteur][0] = 0; 
	   FFTIn[compteur][1] = 0;
	 }
       ++compteur;
       ++InIt;

     }
   
   /////// FFT ////////
   // FFT with m_FFTInTab as input
   FFTWProxyType::Execute(FFTPlan);


   ////// Filtering (Goldstein) ////////
   double * bufAmp = new double[m_SizeTiles*m_SizeTiles]; 
   double peakAmp = 0;

   // Estimate amplitude 
   for (unsigned int k = 0; k < m_SizeTiles*m_SizeTiles; k++)
     {
       bufAmp[k] = sqrt(FFTOut[k][0] * FFTOut[k][0] + 
			FFTOut[k][1] * FFTOut[k][1]);

       if (peakAmp < bufAmp[k])
	 {
	   peakAmp = bufAmp[k];
	 }

       FFTInvIn[k][0] = 0;
       FFTInvIn[k][1] = 0;
     }

   if (peakAmp != 0)
     {
       // Goldstein filter
       for (unsigned int k = 0; k < m_SizeTiles*m_SizeTiles; k++)
	 {
	   FFTInvIn[k][0] = (FFTOut[k][0]*
				  pow(bufAmp[k]/peakAmp, m_alpha))/(m_SizeTiles);

	   FFTInvIn[k][1] = (FFTOut[k][1]*
				  pow(bufAmp[k]/peakAmp, m_alpha))/(m_SizeTiles);
	   
	 }
     }

   //////// FFT-1 ///////
   // FFT-1 with m_FFTInvInTab as input
   FFTWProxyType::Execute(FFTInvPlan);


   /////// Apply weight and assign output //////
   for (unsigned int k = 0; k < m_SizeTiles*m_SizeTiles; k++)
	 {
	   norm = sqrt(FFTInvOut[k][0] * FFTInvOut[k][0] + 
		       FFTInvOut[k][1] * FFTInvOut[k][1]);

	   // Euclidian division
	   unsigned int i1 = k/m_SizeTiles; // quotient
	   unsigned int i2 = k - i1*m_SizeTiles; // remainder

	   if (norm != 0)
	     {
	       //TilesResult[k].real( (FFTInvOut[k][0]*wf[i1][i2])/norm );  
	       //TilesResult[k].imag( (FFTInvOut[k][1]*wf[i1][i2])/norm );
	       TilesResult[k].real( (FFTInvOut[k][0]*wf[i1][i2]) );  
	       TilesResult[k].imag( (FFTInvOut[k][1]*wf[i1][i2]) );
	     }
	   else
	     {
	       TilesResult[k].real(0);  
	       TilesResult[k].imag(0);
	     }
	 }


   // Free Memory
   delete [] w;
   w = 0;
   for (unsigned int i = 0; i < m_SizeTiles; i++) 
     {
       delete [] wf[i];
       wf[i] = 0;
     }
   delete [] wf;
   wf = 0;
   
   delete [] bufAmp;
   bufAmp = 0;
 
 }

private :
  unsigned int m_SizeTiles;

  float m_alpha;

  // Number of threads
  unsigned int m_nbThreads;


  // FFT parameters
  // Range
  FFTProxyComplexType ** m_FFTInTab;
  FFTProxyComplexType ** m_FFTOutTab;
  FFTProxyComplexType ** m_FFTInvInTab;
  FFTProxyComplexType ** m_FFTInvOutTab;
  
  FFTProxyPlanType * m_FFTPlanTab; 
  FFTProxyPlanType * m_FFTInvPlanTab;
  
  };

}
}

#endif
