/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingMaximumMinimumImageFilter_txx
#define otbSARStreamingMaximumMinimumImageFilter_txx
#include "otbSARStreamingMaximumMinimumImageFilter.h"

#include "itkImageScanlineConstIterator.h"
#include "itkImageRegionIterator.h"
#include "itkProgressReporter.h"
#include "otbMacro.h"

#include <complex>

namespace otb
{

template<class TInputImage>
PersistentMaximumMinimumImageFilter<TInputImage>
::PersistentMaximumMinimumImageFilter()
  :m_ThreadMin(1),
   m_ThreadMax(1),
   m_ThreadIndexMaxC(0),
   m_ThreadIndexMaxL(0),
   m_ThreadIndexMinC(0),
   m_ThreadIndexMinL(0),
   m_IgnoreInfiniteValues(true),
   m_IgnoreUserDefinedValue(false)
{
  //this->itk::ProcessObject::SetNumberOfRequiredOutputs(3);

  // first output is a copy of the image, DataObject created by
  // superclass
  //
  // allocate the data objects for the outputs which are
  // just decorators around unsigned int types (for index)
  typename IndexObjectType::Pointer output1
    = static_cast<IndexObjectType*>(this->MakeOutput(3).GetPointer());
  this->itk::ProcessObject::SetNthOutput(1, output1.GetPointer());
   typename IndexObjectType::Pointer output2
    = static_cast<IndexObjectType*>(this->MakeOutput(3).GetPointer());
  this->itk::ProcessObject::SetNthOutput(2, output2.GetPointer());

  // For Min/Max
  typename ValueObjectType::Pointer output3
    = static_cast<ValueObjectType*>(this->MakeOutput(1).GetPointer());
  this->itk::ProcessObject::SetNthOutput(3, output3.GetPointer());
  typename ValueObjectType::Pointer output4
    = static_cast<ValueObjectType*>(this->MakeOutput(1).GetPointer());
  this->itk::ProcessObject::SetNthOutput(4, output4.GetPointer());
  
  // Initialisation
  IndexType indexInit;
  indexInit[0] = 0;
  indexInit[1] = 0;
  this->GetIndexOfMinOutput()->Set(indexInit);
  this->GetIndexOfMaxOutput()->Set(indexInit);
  this->GetMinOutput()->Set(10E20);
  this->GetMaxOutput()->Set(-10E20);

  // Initiate the infinite ignored pixel counters
  m_IgnoredInfinitePixelCount= std::vector<unsigned long>(this->GetNumberOfThreads(), 0);
  m_IgnoredUserPixelCount= std::vector<unsigned int>(this->GetNumberOfThreads(), 0);

  this->Reset();
}

template<class TInputImage>
typename itk::DataObject::Pointer
PersistentMaximumMinimumImageFilter<TInputImage>
::MakeOutput(DataObjectPointerArraySizeType output)
{
  switch (output)
    {
    case 0:
      return static_cast<itk::DataObject*>(LongObjectType::New().GetPointer());
      break;
    case 1:
      return static_cast<itk::DataObject*>(ValueObjectType::New().GetPointer());
      break;
    case 2:
      return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
      break;
    case 3:
      return static_cast<itk::DataObject*>(IndexObjectType::New().GetPointer());
      break;
    default:
      // might as well make an image
      return static_cast<itk::DataObject*>(TInputImage::New().GetPointer());
      break;
    }
}


template<class TInputImage>
typename PersistentMaximumMinimumImageFilter<TInputImage>::IndexObjectType*
PersistentMaximumMinimumImageFilter<TInputImage>
::GetIndexOfMinOutput()
{
  return static_cast<IndexObjectType*>(this->itk::ProcessObject::GetOutput(1));
}

template<class TInputImage>
const typename PersistentMaximumMinimumImageFilter<TInputImage>::IndexObjectType*
PersistentMaximumMinimumImageFilter<TInputImage>
::GetIndexOfMinOutput() const
{
  return static_cast<const IndexObjectType*>(this->itk::ProcessObject::GetOutput(1));
}

template<class TInputImage>
typename PersistentMaximumMinimumImageFilter<TInputImage>::IndexObjectType*
PersistentMaximumMinimumImageFilter<TInputImage>
::GetIndexOfMaxOutput()
{
  return static_cast<IndexObjectType*>(this->itk::ProcessObject::GetOutput(2));
}

template<class TInputImage>
const typename PersistentMaximumMinimumImageFilter<TInputImage>::IndexObjectType*
PersistentMaximumMinimumImageFilter<TInputImage>
::GetIndexOfMaxOutput() const
{
  return static_cast<const IndexObjectType*>(this->itk::ProcessObject::GetOutput(2));
}

template<class TInputImage>
typename PersistentMaximumMinimumImageFilter<TInputImage>::ValueObjectType*
PersistentMaximumMinimumImageFilter<TInputImage>
::GetMinOutput()
{
  return static_cast<ValueObjectType*>(this->itk::ProcessObject::GetOutput(3));
}

template<class TInputImage>
const typename PersistentMaximumMinimumImageFilter<TInputImage>::ValueObjectType*
PersistentMaximumMinimumImageFilter<TInputImage>
::GetMinOutput() const
{
  return static_cast<const ValueObjectType*>(this->itk::ProcessObject::GetOutput(3));
}


template<class TInputImage>
typename PersistentMaximumMinimumImageFilter<TInputImage>::ValueObjectType*
PersistentMaximumMinimumImageFilter<TInputImage>
::GetMaxOutput()
{
  return static_cast<ValueObjectType*>(this->itk::ProcessObject::GetOutput(4));
}

template<class TInputImage>
const typename PersistentMaximumMinimumImageFilter<TInputImage>::ValueObjectType*
PersistentMaximumMinimumImageFilter<TInputImage>
::GetMaxOutput() const
{
  return static_cast<const ValueObjectType*>(this->itk::ProcessObject::GetOutput(4));
}


template<class TInputImage>
void
PersistentMaximumMinimumImageFilter<TInputImage>
::GenerateOutputInformation()
{
  Superclass::GenerateOutputInformation();
  if (this->GetInput())
    {
      this->GetOutput()->CopyInformation(this->GetInput());
      this->GetOutput()->SetLargestPossibleRegion(this->GetInput()->GetLargestPossibleRegion());

      if (this->GetOutput()->GetRequestedRegion().GetNumberOfPixels() == 0)
	{
	  this->GetOutput()->SetRequestedRegion(this->GetOutput()->GetLargestPossibleRegion());
	}

    }
}
template<class TInputImage>
void
PersistentMaximumMinimumImageFilter<TInputImage>
::AllocateOutputs()
{
  // This is commented to prevent the streaming of the whole image for the first stream strip
  // It shall not cause any problem because the output image of this filter is not intended to be used.
  //InputImagePointer image = const_cast< TInputImage * >( this->GetInput() );
  //this->GraftOutput( image );
  // Nothing that needs to be allocated for the remaining outputs
}

template<class TInputImage>
void
PersistentMaximumMinimumImageFilter<TInputImage>
::Synthetize()
{
  int numberOfThreads = this->GetNumberOfThreads();
  
  // Init indexes and values
  IndexType minIndex, maxIndex;
  minIndex = this->GetIndexOfMin();
  maxIndex = this->GetIndexOfMax();;

  ValueType minValue, maxValue;
  minValue = this->GetMin();
  maxValue = this->GetMax();
  
  // For each threads, get information into our Thread arrays
  for (int i = 0; i < numberOfThreads; ++i)
    {
      // Conditions for min/max
      if (minValue > m_ThreadMin[i])
	{
	  minValue = m_ThreadMin[i];
	  minIndex[0] = m_ThreadIndexMinC[i];
	  minIndex[1] = m_ThreadIndexMinL[i];
	}
      if (maxValue < m_ThreadMax[i])
	{
	  maxValue = m_ThreadMax[i];
	  maxIndex[0] = m_ThreadIndexMaxC[i];
	  maxIndex[1] = m_ThreadIndexMaxL[i];
	}
    }
 

  // Set the outputs
  this->GetIndexOfMinOutput()->Set(minIndex);
  this->GetIndexOfMaxOutput()->Set(maxIndex);
  this->GetMinOutput()->Set(minValue);
  this->GetMaxOutput()->Set(maxValue);
}

template<class TInputImage>
void
PersistentMaximumMinimumImageFilter<TInputImage>
::Reset()
{
  int numberOfThreads = this->GetNumberOfThreads();

  // Resize the thread temporaries
  m_ThreadIndexMaxC.SetSize(numberOfThreads);
  m_ThreadIndexMaxL.SetSize(numberOfThreads);
  m_ThreadIndexMinC.SetSize(numberOfThreads);
  m_ThreadIndexMinL.SetSize(numberOfThreads);
  m_ThreadMax.SetSize(numberOfThreads);
  m_ThreadMin.SetSize(numberOfThreads);
  // Initialize the temporaries
  m_ThreadIndexMaxC.Fill(0);
  m_ThreadIndexMaxL.Fill(0);
  m_ThreadIndexMinC.Fill(0);
  m_ThreadIndexMinL.Fill(0);
  m_ThreadMin.Fill(10E20);
  m_ThreadMax.Fill(-10E20);

  if (m_IgnoreInfiniteValues)
    {
      m_IgnoredInfinitePixelCount= std::vector<unsigned long>(numberOfThreads, 0);
    }

  if (m_IgnoreUserDefinedValue)
    {
      m_IgnoredUserPixelCount= std::vector<unsigned int>(this->GetNumberOfThreads(), 0);
    }
}



/** 
 * Method OutputRegionToInputRegion for GenerateInputRequestedRegion
 */
template<class TInputImage >
typename PersistentMaximumMinimumImageFilter< TInputImage >::RegionType 
PersistentMaximumMinimumImageFilter< TInputImage >
::OutputRegionToInputRegion(const RegionType& outputRegion) const
{
  // Compute the input requested region (size and start index)
  // Use the image transformations to insure an input requested region
  // that will provide the proper range
  const SizeType & outputRequestedRegionSize = outputRegion.GetSize();
  const IndexType & outputRequestedRegionIndex = outputRegion.GetIndex();

  // Compute input index 
  IndexType  inputRequestedRegionIndex = outputRequestedRegionIndex;

  // Compute input size
  SizeType inputRequestedRegionSize = outputRequestedRegionSize; 
 
  // Input region
  RegionType inputRequestedRegion = outputRegion;
  inputRequestedRegion.SetIndex(inputRequestedRegionIndex);
  inputRequestedRegion.SetSize(inputRequestedRegionSize);

  return inputRequestedRegion;  
}
/** 
 * Method OutputRegionToInputRegion
 */
template<class TInputImage>
void
PersistentMaximumMinimumImageFilter< TInputImage >
::GenerateInputRequestedRegion()
{
  RegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
  RegionType inputRequestedRegion = OutputRegionToInputRegion(outputRequestedRegion);

  InputImagePointer  inputPtr = const_cast< ImageType * >( this->GetInput() );
  
  inputPtr->SetRequestedRegion(inputRequestedRegion);
}



template<class TInputImage>
void
PersistentMaximumMinimumImageFilter<TInputImage>
::ThreadedGenerateData(const RegionType& outputRegionForThread,
                       itk::ThreadIdType threadId)
{ 
  // Grab the input
  InputImagePointer inputPtr =  const_cast<TInputImage *>(this->GetInput(0));
  // Compute corresponding input region
  RegionType inputRegionForThread = OutputRegionToInputRegion(outputRegionForThread);

  // support progress methods/callbacks
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());

  //  Define/declare an iterator that will walk the input region for this
  // thread. 
  typedef itk::ImageScanlineConstIterator< ImageType > InputIterator;
  InputIterator  inIt(this->GetInput(), inputRegionForThread);
  
  inIt.GoToBegin();
  
  ValueType valueCurrent;
 
  // For each line 
  while ( !inIt.IsAtEnd())
  {
    inIt.GoToBeginOfLine();

    // For each column
    while (!inIt.IsAtEndOfLine())
    {
      //The input image value
      valueCurrent= inIt.Get();
            
      bool valueCurrentIsFinite = (vnl_math_isfinite(valueCurrent));
      
      // Assigne if finite
       if (valueCurrentIsFinite)
	 {
	   // Conditions for min/max
	   if (m_ThreadMin[threadId] > valueCurrent)
	     {
	       m_ThreadMin[threadId] = valueCurrent;
	       m_ThreadIndexMinC[threadId] = inIt.GetIndex()[0];
	       m_ThreadIndexMinL[threadId] = inIt.GetIndex()[1];
	     }
	   if (m_ThreadMax[threadId] < valueCurrent)
	     {
	       m_ThreadMax[threadId] = valueCurrent;
	       m_ThreadIndexMaxC[threadId] = inIt.GetIndex()[0];
	       m_ThreadIndexMaxL[threadId] = inIt.GetIndex()[1];
	     }
	 }
       
     
       // Next colunm
       ++inIt;
    }
     // Next Line
    inIt.NextLine();
    
  }

}

template <class TInputImage>
void
PersistentMaximumMinimumImageFilter<TInputImage>
::PrintSelf(std::ostream& os, itk::Indent indent) const
{
  Superclass::PrintSelf(os, indent);

  os << indent << "Max: "
     << static_cast<typename itk::NumericTraits<ValueType>::PrintType>(this->GetMax()) << std::endl;
  os << indent << "Min: "
     << static_cast<typename itk::NumericTraits<ValueType>::PrintType>(this->GetMin()) << std::endl;
  os << indent << "Index of Max: "      << this->GetIndexOfMax() << std::endl;
  os << indent << "Index of Min: "      << this->GetIndexOfMin() << std::endl;
}
} // end namespace otb
#endif
