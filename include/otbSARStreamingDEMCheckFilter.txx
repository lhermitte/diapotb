/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingDEMCheckFilter_txx
#define otbSARStreamingDEMCheckFilter_txx
#include "otbSARStreamingDEMCheckFilter.h"

#include "itkImageRegionIterator.h"
#include "itkImageScanlineConstIterator.h"
#include "itkProgressReporter.h"
#include "otbMacro.h"

#include <complex>

namespace otb
{

template<class TInputImage>
PersistentDEMCheckFilter<TInputImage>
::PersistentDEMCheckFilter()
  : m_Thread_Counter(0),
    m_UserIgnoredValue(-32768),
    m_SizeSARC(0),
    m_SizeSARL(0),
    m_OriginSARC(0),
    m_OriginSARL(0)
{
  // first output is a copy of the image, DataObject created by
  // superclass
  //
  // allocate the data objects for the outputs which are
  // just decorators around long int types
  typename LongObjectType::Pointer output
    = static_cast<LongObjectType*>(this->MakeOutput(3).GetPointer());
  this->itk::ProcessObject::SetNthOutput(1, output.GetPointer());
  
  this->GetCounter_Output()->Set(itk::NumericTraits<long int>::Zero);
  this->Reset();
}

template<class TInputImage>
typename itk::DataObject::Pointer
PersistentDEMCheckFilter<TInputImage>
::MakeOutput(DataObjectPointerArraySizeType output)
{
  switch (output)
    {
    case 0:
      return static_cast<itk::DataObject*>(TInputImage::New().GetPointer());
      break;
    case 1:
      return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
      break;
    case 2:
      return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
      break;
    case 3:
      return static_cast<itk::DataObject*>(LongObjectType::New().GetPointer());
      break;
    default:
      // might as well make an image
      return static_cast<itk::DataObject*>(TInputImage::New().GetPointer());
      break;
    }
}

template<class TInputImage>
typename PersistentDEMCheckFilter<TInputImage>::LongObjectType*
PersistentDEMCheckFilter<TInputImage>
::GetCounter_Output()
{
  return static_cast<LongObjectType*>(this->itk::ProcessObject::GetOutput(1));
}

template<class TInputImage>
const typename PersistentDEMCheckFilter<TInputImage>::LongObjectType*
PersistentDEMCheckFilter<TInputImage>
::GetCounter_Output() const
{
  return static_cast<const LongObjectType*>(this->itk::ProcessObject::GetOutput(1));
}


template<class TInputImage>
void
PersistentDEMCheckFilter<TInputImage>
::GenerateOutputInformation()
{
  Superclass::GenerateOutputInformation();
  if (this->GetInput())
    {
    this->GetOutput()->CopyInformation(this->GetInput());
    this->GetOutput()->SetLargestPossibleRegion(this->GetInput()->GetLargestPossibleRegion());

    if (this->GetOutput()->GetRequestedRegion().GetNumberOfPixels() == 0)
      {
      this->GetOutput()->SetRequestedRegion(this->GetOutput()->GetLargestPossibleRegion());
      }
    }
}
template<class TInputImage>
void
PersistentDEMCheckFilter<TInputImage>
::AllocateOutputs()
{
  // This is commented to prevent the streaming of the whole image for the first stream strip
  // It shall not cause any problem because the output image of this filter is not intended to be used.
  //InputImagePointer image = const_cast< TInputImage * >( this->GetInput() );
  //this->GraftOutput( image );
  // Nothing that needs to be allocated for the remaining outputs
}

template<class TInputImage>
void
PersistentDEMCheckFilter<TInputImage>
::Synthetize()
{
  int numberOfThreads = this->GetNumberOfThreads();
  
  // Assign the side and set the outputs (if m_Threads* != 0) 
  for (int i = 0; i < numberOfThreads; ++i)
    {
      if (m_Thread_Counter[i] !=  itk::NumericTraits<long int>::Zero)
	{
	  this->GetCounter_Output()->Set(m_Thread_Counter[i]);
	}
      
    } 

}

template<class TInputImage>
void
PersistentDEMCheckFilter<TInputImage>
::Reset()
{
  int numberOfThreads = this->GetNumberOfThreads();

  // Resize the thread temporaries
  m_Thread_Counter.SetSize(numberOfThreads);
   // Initialize the temporaries
  m_Thread_Counter.Fill(itk::NumericTraits<long int>::Zero);
  
}


/** 
 * Method GenerateInputRequestedRegion
 */
template<class TInputImage>
void
PersistentDEMCheckFilter< TInputImage >
::GenerateInputRequestedRegion()
{
  RegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
  RegionType inputRequestedRegion = outputRequestedRegion;

  InputImagePointer  inputPtr = const_cast< ImageType * >( this->GetInput() );
  
  inputPtr->SetRequestedRegion(inputRequestedRegion);
}



template<class TInputImage>
void
PersistentDEMCheckFilter<TInputImage>
::ThreadedGenerateData(const RegionType& outputRegionForThread,
                       itk::ThreadIdType threadId)
{
  /**
   * Grab the input
   */
  //InputImagePointer inputPtr =  const_cast<TInputImage *>(this->GetInput(0));
  // Compute corresponding input region (same as output here)
  RegionType inputRegionForThread = outputRegionForThread;

  // support progress methods/callbacks
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());

  //  Define/declare an iterator that will walk the input region for this
  // thread. 
  typedef itk::ImageScanlineConstIterator< ImageType > InputIterator;
  InputIterator  inIt(this->GetInput(), inputRegionForThread);
  
  inIt.GoToBegin();
   
  // For each line 
  while ( !inIt.IsAtEnd())
  {
    inIt.GoToBeginOfLine();
   
    // For each column
    while (!inIt.IsAtEndOfLine())
    {
      // Get the Projection for each dimensions (0 : range and 1 : azimut)
      double projC = inIt.Get()[0];
      double projL = inIt.Get()[1];

      // Check if the projection is inside SAR geometry 
      if (projC >= (m_OriginSARC) && projC < (m_OriginSARC + m_SizeSARC) &&
	  projL >= (m_OriginSARL) && projL < (m_OriginSARL + m_SizeSARL))
	{
	  ++m_Thread_Counter[threadId];
	}
             
       // Next colunm
       ++inIt;
    }

    // Next Line
    inIt.NextLine();
  }
}

template <class TImage>
void
PersistentDEMCheckFilter<TImage>
::PrintSelf(std::ostream& os, itk::Indent indent) const
{
  Superclass::PrintSelf(os, indent);
}
} // end namespace otb
#endif
