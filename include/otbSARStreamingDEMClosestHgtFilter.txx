/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingDEMClosestHgtFilter_txx
#define otbSARStreamingDEMClosestHgtFilter_txx
#include "otbSARStreamingDEMClosestHgtFilter.h"

#include "itkImageRegionIterator.h"
#include "itkImageScanlineConstIterator.h"
#include "itkProgressReporter.h"
#include "otbMacro.h"

#include <complex>

namespace otb
{

template<class TInputImage>
PersistentDEMClosestHgtFilter<TInputImage>
::PersistentDEMClosestHgtFilter() :
  m_UserIgnoredValue(-32768)
{
  
  // allocate the data objects for the outputs which are
  // just decorators around pixel types
  // First output : VectorHgt 
  typename PixelVectorObjectType::Pointer output1
      = static_cast<PixelVectorObjectType*>(this->MakeOutput(4).GetPointer());
  this->itk::ProcessObject::SetNthOutput(1, output1.GetPointer());

  // Second output : VectorId 
  typename IntVectorObjectType::Pointer output2
      = static_cast<IntVectorObjectType*>(this->MakeOutput(5).GetPointer());
  this->itk::ProcessObject::SetNthOutput(2, output2.GetPointer());

  // Second output : MapDuplicates 
  typename IntMapObjectType::Pointer output3
    = static_cast<IntMapObjectType*>(this->MakeOutput(6).GetPointer());
  this->itk::ProcessObject::SetNthOutput(3, output3.GetPointer());

  
  int numberOfThreads = this->GetNumberOfThreads();
  
  // Fill the Thread Vector with new pointers 
  for (int i = 0; i < numberOfThreads; ++i)
    {
      m_Thread_Hgt.push_back(new std::vector<PixelType>());
      m_Thread_Id.push_back(new std::vector<int>());
    }
    
  this->GetVectorHgt_Output()->Set(new std::vector<PixelType>());
  this->GetVectorId_Output()->Set(new std::vector<int>());
  this->GetMapDuplicates_Output()->Set(new std::map<int, int>());

  // Allocate local vector for required index to extract from the input DEM
  m_indexCL = new std::vector<IndexType>();


  this->Reset();
}

template<class TInputImage>
typename itk::DataObject::Pointer
PersistentDEMClosestHgtFilter<TInputImage>
::MakeOutput(DataObjectPointerArraySizeType output)
{
  switch (output)
    {
    case 0:
      return static_cast<itk::DataObject*>(TInputImage::New().GetPointer());
      break;
    case 1:
      return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
      break;
    case 2:
      return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
      break;
    case 4:
      return static_cast<itk::DataObject*>(PixelVectorObjectType::New().GetPointer());
      break;
    case 5:
      return static_cast<itk::DataObject*>(IntVectorObjectType::New().GetPointer());
      break;
    case 6:
      return static_cast<itk::DataObject*>(IntMapObjectType::New().GetPointer());
      break;
    default:
      // might as well make an image
      return static_cast<itk::DataObject*>(TInputImage::New().GetPointer());
      break;
    }
}

template<class TInputImage>
typename PersistentDEMClosestHgtFilter<TInputImage>::PixelVectorObjectType* 
PersistentDEMClosestHgtFilter<TInputImage>
::GetVectorHgt_Output()
{
  return static_cast<PixelVectorObjectType*>(this->itk::ProcessObject::GetOutput(1));
}

template<class TInputImage>
const typename PersistentDEMClosestHgtFilter<TInputImage>::PixelVectorObjectType* 
PersistentDEMClosestHgtFilter<TInputImage>
::GetVectorHgt_Output() const
{
  return static_cast<const PixelVectorObjectType*>(this->itk::ProcessObject::GetOutput(1));
}

template<class TInputImage>
const typename PersistentDEMClosestHgtFilter<TInputImage>::IntVectorObjectType* 
PersistentDEMClosestHgtFilter<TInputImage>
::GetVectorId_Output() const
{
  return static_cast<const IntVectorObjectType*>(this->itk::ProcessObject::GetOutput(2));
}

template<class TInputImage>
typename PersistentDEMClosestHgtFilter<TInputImage>::IntVectorObjectType* 
PersistentDEMClosestHgtFilter<TInputImage>
::GetVectorId_Output()
{
  return static_cast<IntVectorObjectType*>(this->itk::ProcessObject::GetOutput(2));
}


template<class TInputImage>
typename PersistentDEMClosestHgtFilter<TInputImage>::IntMapObjectType* 
PersistentDEMClosestHgtFilter<TInputImage>
::GetMapDuplicates_Output()
{
  return static_cast<IntMapObjectType*>(this->itk::ProcessObject::GetOutput(3));
}

template<class TInputImage>
const typename PersistentDEMClosestHgtFilter<TInputImage>::IntMapObjectType *
PersistentDEMClosestHgtFilter<TInputImage>
::GetMapDuplicates_Output() const
{
  return static_cast<const IntMapObjectType*>(this->itk::ProcessObject::GetOutput(3));
}

template<class TInputImage>
void
PersistentDEMClosestHgtFilter<TInputImage>
::GenerateOutputInformation()
{
  Superclass::GenerateOutputInformation();
  if (this->GetInput())
    {
    this->GetOutput()->CopyInformation(this->GetInput());
    this->GetOutput()->SetLargestPossibleRegion(this->GetInput()->GetLargestPossibleRegion());

    if (this->GetOutput()->GetRequestedRegion().GetNumberOfPixels() == 0)
      {
      this->GetOutput()->SetRequestedRegion(this->GetOutput()->GetLargestPossibleRegion());
      }
    }
}
template<class TInputImage>
void
PersistentDEMClosestHgtFilter<TInputImage>
::AllocateOutputs()
{
  // This is commented to prevent the streaming of the whole image for the first stream strip
  // It shall not cause any problem because the output image of this filter is not intended to be used.
  //InputImagePointer image = const_cast< TInputImage * >( this->GetInput() );
  //this->GraftOutput( image );
  // Nothing that needs to be allocated for the remaining outputs
}

// Set for the input vector of Lon/Lat
template<class TInputImage>
void 
PersistentDEMClosestHgtFilter<TInputImage>
::SetVectorLonLat(std::vector<double *> * vectorLonLat)
{
  m_VectorLonLat = vectorLonLat;
  
  Point2DType pixel_Into_DEM_LonLat(0);
  itk::ContinuousIndex<double,2> pixel_Into_DEM_index;
  IndexType index;
  
  // Fill index vectors according to input lon/lat
  for (unsigned int i = 0; i < m_VectorLonLat->size(); i++) 
    {
      pixel_Into_DEM_LonLat[0] = m_VectorLonLat->at(i)[0]; // lon 
      pixel_Into_DEM_LonLat[1] = m_VectorLonLat->at(i)[1]; // lat
      
      // Transform into continuous index 
      this->GetInput()->TransformPhysicalPointToContinuousIndex(pixel_Into_DEM_LonLat, 
								pixel_Into_DEM_index);
      
      // Integer index into index vectors
      index [0] = static_cast<IndexValueType>(pixel_Into_DEM_index[0]);	
      index [1] = static_cast<IndexValueType>(pixel_Into_DEM_index[1]);
            
      // Find if duplicates
      typedef typename std::vector<IndexType>::iterator itIndexType;
      itIndexType it = std::find(m_indexCL->begin(), m_indexCL->end(), 
				 index);
      
      std::map<int,int>::iterator itMap = this->GetMapDuplicates()->begin();
      if (it !=  m_indexCL->end())
	{
	  this->GetMapDuplicates()->insert(itMap, std::pair<int, int>((int) i, (int) std::distance(m_indexCL->begin(), it))); 
	}
      
      m_indexCL->push_back(index);
      
    }
}

template<class TInputImage>
void
PersistentDEMClosestHgtFilter<TInputImage>
::Synthetize()
{
  int numberOfThreads = this->GetNumberOfThreads();
  
  // Fill the Output Vector with selected shifts 
  for (int i = 0; i < numberOfThreads; ++i)
    {
      if (!m_Thread_Hgt[i]->empty())
	{
	  for (long unsigned int j = 0; j < m_Thread_Hgt[i]->size(); j++)
	    {
	      this->GetVectorHgt()->push_back(m_Thread_Hgt[i]->at(j)); // Ref ?? No value
	      this->GetVectorId()->push_back(m_Thread_Id[i]->at(j)); // Ref ?? No value
	    }
	}
    }
}

template<class TInputImage>
void
PersistentDEMClosestHgtFilter<TInputImage>
::Reset()
{
  int numberOfThreads = this->GetNumberOfThreads();

  // Initialize the temporaries
  for (int i = 0; i < numberOfThreads; i++) 
    {
      m_Thread_Hgt[i]->clear();
      m_Thread_Id[i]->clear();
    }
}


/** 
 * Method GenerateInputRequestedRegion
 */
template<class TInputImage>
void
PersistentDEMClosestHgtFilter< TInputImage >
::GenerateInputRequestedRegion()
{
  RegionType outputRequestedRegion = this->GetOutput()->GetRequestedRegion();
  RegionType inputRequestedRegion = outputRequestedRegion;

  InputImagePointer  inputPtr = const_cast< ImageType * >( this->GetInput() );
  
  inputPtr->SetRequestedRegion(inputRequestedRegion);
}



template<class TInputImage>
void
PersistentDEMClosestHgtFilter<TInputImage>
::ThreadedGenerateData(const RegionType& outputRegionForThread,
                       itk::ThreadIdType threadId)
{
  /**
   * Grab the input
   */
  //InputImagePointer inputPtr =  const_cast<TInputImage *>(this->GetInput(0));
  // Compute corresponding input region (same as output here)
  RegionType inputRegionForThread = outputRegionForThread;

  // support progress methods/callbacks
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels());

  //  Define/declare an iterator that will walk the input region for this
  // thread. 
  typedef itk::ImageScanlineConstIterator< ImageType > InputIterator;
  InputIterator  inIt(this->GetInput(), inputRegionForThread);
  
  inIt.GoToBegin();
  
  // For each line 
  while ( !inIt.IsAtEnd())
  {
    inIt.GoToBeginOfLine();
   
    // For each column
    while (!inIt.IsAtEndOfLine())
    {
      // Get the Index
      IndexType index_current = inIt.GetIndex();

      // Check if the current index is required
      typedef typename std::vector<IndexType>::iterator itIndexType;
      itIndexType itLC = std::find(m_indexCL->begin(), m_indexCL->end(), 
				   index_current);

      // If required => save the value of hgt and the index into indL and indC vectors
      if (itLC !=  m_indexCL->end())
	{
	  m_Thread_Hgt[threadId]->push_back(inIt.Get());
	  
	  // Same point/distance from itC or itL 
	  // ToDo handle duplication (same integer index) ! 
	  int indexIntoVectors = std::distance(m_indexCL->begin(), itLC); 

	  m_Thread_Id[threadId]->push_back(indexIntoVectors); 
	}
             
       // Next colunm
       ++inIt;
    }

    // Next Line
    inIt.NextLine();
  }
}

template <class TImage>
void
PersistentDEMClosestHgtFilter<TImage>
::PrintSelf(std::ostream& os, itk::Indent indent) const
{
  Superclass::PrintSelf(os, indent);

}
} // end namespace otb
#endif
