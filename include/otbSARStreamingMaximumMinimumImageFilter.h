/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef otbSARStreamingMaximumMinimumImageFilter_h
#define otbSARStreamingMaximumMinimumImageFilter_h

#include "otbPersistentImageFilter.h"
#include "itkNumericTraits.h"
#include "itkArray.h"
#include "itkSimpleDataObjectDecorator.h"
#include "otbPersistentFilterStreamingDecorator.h"

#include <complex>
#include <cmath>

namespace otb
{

/** \class PersistentMaximumMinimumImageFilter
 * \brief Estimate some values for an input image (min/max and corresponding indexes).
 * 
 * This filter persists its temporary data. It means that if you Update it n times on n different
 * requested regions, the output statistics will be the statitics of the whole set of n regions.
 *
 * To reset the temporary data, one should call the Reset() function.
 *
 * To get the wanted values once the regions have been processed via the pipeline, use the Synthetize() method.
 *
 * \ingroup DiapOTBModule
 */
template<class TInputImage>
class ITK_EXPORT PersistentMaximumMinimumImageFilter :
  public PersistentImageFilter<TInputImage, TInputImage>
{
public:
  /** Standard Self typedef */
  typedef PersistentMaximumMinimumImageFilter                 Self;
  typedef PersistentImageFilter<TInputImage, TInputImage> Superclass;
  typedef itk::SmartPointer<Self>                         Pointer;
  typedef itk::SmartPointer<const Self>                   ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(PersistentMaximumMinimumImageFilter, PersistentImageFilter);

  /** Image related typedefs. For Image Input*/
  typedef TInputImage                   ImageType;
  typedef typename TInputImage::Pointer InputImagePointer;

  typedef typename TInputImage::RegionType   RegionType;
  typedef typename TInputImage::SizeType     SizeType;
  typedef typename TInputImage::IndexType    IndexType;
  typedef typename TInputImage::PixelType    PixelType;
  typedef double                             ValueType;
  typedef typename ImageType::IndexValueType IndexValueType;
  typedef typename ImageType::SizeValueType  SizeValueType;

  itkStaticConstMacro(InputImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Image related typedefs. */
  itkStaticConstMacro(ImageDimension, unsigned int,
                      TInputImage::ImageDimension);

  /** Type to use for computations. */
  //  typedef typename itk::NumericTraits<PixelType>::RealType RealType;
  typedef typename itk::NumericTraits<double>::RealType RealType;
 
  /** Smart Pointer type to a DataObject. */
  typedef typename itk::DataObject::Pointer DataObjectPointer;
  typedef itk::ProcessObject::DataObjectPointerArraySizeType DataObjectPointerArraySizeType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>        RealObjectType;
  typedef itk::SimpleDataObjectDecorator<long>            LongObjectType;
  typedef itk::SimpleDataObjectDecorator<unsigned int>    UintObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType>       PixelObjectType;
  typedef itk::SimpleDataObjectDecorator<ValueType>       ValueObjectType;
  typedef itk::SimpleDataObjectDecorator<IndexType>       IndexObjectType;

    /** Return the number of valid points contained into the input grid. */
  IndexType GetIndexOfMin() const
  {
    return this->GetIndexOfMinOutput()->Get();
  }
  IndexObjectType* GetIndexOfMinOutput();
  const IndexObjectType* GetIndexOfMinOutput() const;

  IndexType GetIndexOfMax() const
  {
    return this->GetIndexOfMaxOutput()->Get();
  }
  IndexObjectType* GetIndexOfMaxOutput();
  const IndexObjectType* GetIndexOfMaxOutput() const;

  /** Return the Minimum. */
  ValueType GetMin() const
  {
    return this->GetMinOutput()->Get();
  }
  ValueObjectType* GetMinOutput();
  const ValueObjectType* GetMinOutput() const;

  /** Return the Maximum. */
  ValueType GetMax() const
  {
    return this->GetMaxOutput()->Get();
  }
  ValueObjectType* GetMaxOutput();
  const ValueObjectType* GetMaxOutput() const;

  /** Make a DataObject of the correct type to be used as the specified
   * output. */
  DataObjectPointer MakeOutput(DataObjectPointerArraySizeType idx) ITK_OVERRIDE;
  using Superclass::MakeOutput;

  /**
   * Synthetize and Reset function called by our PersistentFilterStreamingDecorator
   */
  void Synthetize(void) ITK_OVERRIDE;
  void Reset(void) ITK_OVERRIDE;

  itkSetMacro(IgnoreInfiniteValues, bool);
  itkGetMacro(IgnoreInfiniteValues, bool);

  itkSetMacro(IgnoreUserDefinedValue, bool);
  itkGetMacro(IgnoreUserDefinedValue, bool);

  itkSetMacro(UserIgnoredValue, RealType);
  itkGetMacro(UserIgnoredValue, RealType);

protected:
  PersistentMaximumMinimumImageFilter();
  ~PersistentMaximumMinimumImageFilter() ITK_OVERRIDE {}
  void PrintSelf(std::ostream& os, itk::Indent indent) const ITK_OVERRIDE;

  
  /** PersistentMaximumMinimumImageFilter needs a larger input requested region than the output
   * requested region.  As such, SARQuadraticAveragingImageFilter needs to provide an
   * implementation for GenerateInputRequestedRegion() in order to inform the
   * pipeline execution model.
   * \sa ProcessObject::GenerateInputRequestedRegion() */
  virtual void GenerateInputRequestedRegion() ITK_OVERRIDE;

  /**
   * OutputRegionToInputRegion returns the input region 
   */
  RegionType OutputRegionToInputRegion(const RegionType& outputRegion) const;

  /** Multi-thread version GenerateData. */
  void  ThreadedGenerateData(const RegionType&
                             outputRegionForThread,
                             itk::ThreadIdType threadId) ITK_OVERRIDE;

  /** Pass the input through unmodified. Do this by Grafting in the
   *  AllocateOutputs method.
   */
  void AllocateOutputs() ITK_OVERRIDE;
  void GenerateOutputInformation() ITK_OVERRIDE;

private:
  PersistentMaximumMinimumImageFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

  itk::Array<ValueType>       m_ThreadMin;
  itk::Array<ValueType>       m_ThreadMax;
  itk::Array<unsigned int>    m_ThreadIndexMaxC;
  itk::Array<unsigned int>    m_ThreadIndexMaxL;
  itk::Array<unsigned int>    m_ThreadIndexMinC;
  itk::Array<unsigned int>    m_ThreadIndexMinL;

;

  /* Ignored values */
  bool                        m_IgnoreInfiniteValues;
  bool                        m_IgnoreUserDefinedValue;
  RealType                    m_UserIgnoredValue;
  std::vector<unsigned long>  m_IgnoredInfinitePixelCount;
  std::vector<unsigned int>   m_IgnoredUserPixelCount;


}; // end of class PersistentMaximumMinimumImageFilter

/*===========================================================================*/

/** \class SARStreamingMaximumMinimumImageFilter
 * \brief This class streams the whole input image through the PersistentMaximumMinimumImageFilter.
 *
 * This way, it allows computing the first order global statistics of this image. It calls the
 * Reset() method of the PersistentMaximumMinimumImageFilter before streaming the image and the
 * Synthetize() method of the PersistentMaximumMinimumImageFilter after having streamed the image
 * to compute the statistics. The accessor on the results are wrapping the accessors of the
 * internal PersistentMaximumMinimumImageFilter.
 * By default infinite values are ignored, use IgnoreInfiniteValues accessor to consider
 * infinite values in the computation.
 *
 * This filter can be used as:
 * \code
 * typedef otb::StreamingMaximumMinimumImageFilter<ImageType> StatisticsType;
 * StatisticsType::Pointer statistics = StatisticsType::New();
 * statistics->SetInput(reader->GetOutput());
 * statistics->Update();
 * std::cout << statistics-> GetIndexOfMax() << std::endl;
 * std::cout << statistics-> GetMax() << std::endl;
 * \endcode
 *
 * \sa PersistentMaximumMinimumImageFilter
 * \sa PersistentImageFilter
 * \sa PersistentFilterStreamingDecorator
 * \sa StreamingImageVirtualWriter
 * \ingroup Streamed
 * \ingroup Multithreaded
 * \ingroup MathematicalStatisticsImageFilters
 *
 * \ingroup DiapOTBModule
 */

template<class TInputImage>
class ITK_EXPORT SARStreamingMaximumMinimumImageFilter :
  public PersistentFilterStreamingDecorator<PersistentMaximumMinimumImageFilter<TInputImage> >
{
public:
  /** Standard Self typedef */
  typedef SARStreamingMaximumMinimumImageFilter Self;
  typedef PersistentFilterStreamingDecorator
  <PersistentMaximumMinimumImageFilter<TInputImage> > Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Type macro */
  itkNewMacro(Self);

  /** Creation through object factory macro */
  itkTypeMacro(StreamingMaximumMinimumImageFilter, PersistentFilterStreamingDecorator);

  typedef typename Superclass::FilterType    StatFilterType;
  typedef typename StatFilterType::PixelType PixelType;
  typedef double ValueType;
  typedef typename StatFilterType::RealType  RealType;
  typedef TInputImage                        InputImageType;
  typedef typename TInputImage::IndexType    IndexType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>  RealObjectType;
  typedef itk::SimpleDataObjectDecorator<long>      LongObjectType;
  typedef itk::SimpleDataObjectDecorator<PixelType> PixelObjectType;
  typedef itk::SimpleDataObjectDecorator<ValueType> ValueObjectType;
  typedef itk::SimpleDataObjectDecorator<IndexType> IndexObjectType;

  using Superclass::SetInput;
  void SetInput(InputImageType * input)
  {
    this->GetFilter()->SetInput(input);
  }
  const InputImageType * GetInput()
  {
    return this->GetFilter()->GetInput();
  }

  IndexType GetIndexOfMax()
  {
    return this->GetFilter()->GetIndexOfMaxOutput()->Get();
  }

  IndexType GetIndexOfMin()
  {
    return this->GetFilter()->GetIndexOfMinOutput()->Get();
  }


  ValueType GetMax()
  {
    return this->GetFilter()->GetMaxOutput()->Get();
  }

  ValueType GetMin()
  {
    return this->GetFilter()->GetMinOutput()->Get();
  }


  otbSetObjectMemberMacro(Filter, IgnoreInfiniteValues, bool);
  otbGetObjectMemberMacro(Filter, IgnoreInfiniteValues, bool);

  otbSetObjectMemberMacro(Filter, IgnoreUserDefinedValue, bool);
  otbGetObjectMemberMacro(Filter, IgnoreUserDefinedValue, bool);

  otbSetObjectMemberMacro(Filter, UserIgnoredValue, RealType);
  otbGetObjectMemberMacro(Filter, UserIgnoredValue, RealType);

protected:
  /** Constructor */
  SARStreamingMaximumMinimumImageFilter() {};
  /** Destructor */
  ~SARStreamingMaximumMinimumImageFilter() ITK_OVERRIDE {}

private:
  SARStreamingMaximumMinimumImageFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented
};

} // end namespace otb

#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSARStreamingMaximumMinimumImageFilter.txx"
#endif

#endif
