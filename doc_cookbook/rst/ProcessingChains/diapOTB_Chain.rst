.. _diapOTB_Chain:

For S1 Stripmap mode and Cosmo
==============================

This workflow works on S1 StripMap and Cosmo data.

Description
-----------

This chain is divided into three parts descrided below :

- Pre-Processing Chain
- Metadata Correction Chain
- DIn-SAR Chain

Launch this chain
-----------------

A Python script called diapOTB.py has been developed and takes a configuration file (.json) as input. 
The configuration file has always the same organization :

.. image:: ../../Art/conf_chains/diapOTB_1_chain.png

Four sections compose the configuration file :

- Global :

  - **Master_Image_Path :** Path to the Master SAR Image. 
  - **Slave_Image_Path :** Path to the Master SAR Image
  - **DEM_Path :** Path to the DEM
  - **output_dir :** Output directory (all images or files created by DiapOTB will be stored inside this directory)


- Pre_Processing :

  - **doppler_file :** Output file to store Doppler0 result
  - **ML_range :** MultLook factor on range dimension
  - **ML_azimut :** MultLook factor on azimut dimension
  - **ML_gain :** Gain applied on MultiLooked images


- Metadata_Correction :

  - **fine_metadata_file :** Output file to corrected metadata
  - **activate :** Boolean to activate this chain (By default false)
  - **GridStep_range :** Step for the correlation grid on range dimension
  - **GridStep_azimut :** Step for the correlation grid on azimut dimension


- DIn_SAR :

  - **GridStep_range :** Step for the deformation grid on range dimension
  - **GridStep_azimut :** Step for the deformation grid on azimut dimensio
  - **GridStep_Threshold :** Threshold for the correlation grid (applied on correlation rate)
  - **Grid_Gap :** Maximum gap between DEM grid values and the mean value. If the difference betwwen a value of the DEM grid ans the mena is superior to this gap then the value is set to the mean. (Avoid incoherent shift)
  - **Interferogram_gain  :** Gain applied on amplitude band of output inteferegram.
  - **Interferogram_ortho  :** Boolean to activate the optionnal ouput : inteferogram into ground geometry (By default false)

The processing chain needs metadata to launch the applications. Thus the path to the input images must be into the native directory for each kind of products (i.e SAFE directory for Sentinel-1 products).
For that moment the Metadata_Correction is not used (activate always sets to false) and the ground geometry for interferogram does not provide satisfactory results.
