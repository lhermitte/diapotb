#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2017 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" 
    SAR_MultiSlc.py
    ===============

    Interferometry chain between multitemporal SAR images for S1 Strimap and Cosmo products. 
    One master image is fixed by users and other images are processed as slave image.
 
"""

__author__ = "Maxime Azzoni"
__version__ = "0.1"
__status__ = "Developpement"
__date__ = "05/02/2020"
__last_modified__ = "05/02/2020"

# ===============
# Import section
# ===============

import logging
import os
import argparse
import xml.etree.ElementTree as ET
import datetime
import gdal
import shutil
from shutil import copyfile
import h5py

from processings import Pre_Processing
from processings import Ground
from processings import DInSar
from processings import Post_Processing

import utils.DiapOTB_applications as diapOTBApp
from utils import func_utils

# ==================================== #
 #                Main                # 
  # ================================ #

if __name__ == "__main__":

    # ====== Check arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("configfile",
                        help="""input conguration file for the
                        application DiapOTB""")
    args = parser.parse_args()

    print(args.configfile)

    func_utils.init_logger()

    # ====== Read and Load the configuration file
    dataConfig = func_utils.load_configfile(args.configfile, mode="multi_S1")

    try :
        # ====== Get dictionaries
        dict_Global = dataConfig['Global']
        dict_PreProcessing = dataConfig['Pre_Processing']
        dict_Metadata_Correction = dataConfig['Metadata_Correction']
        dict_DInSAR = dataConfig['DIn_SAR']
        dict_PostProcessing = dataConfig['Post_Processing']

        # ====== Check extension (if .h5 => HDF5 file => Cosmo Sensor)
        master_ext = dict_Global['in']['Master_Image'].split(".")[-1:]

        # ====== Get elements from configuration file
        # ====== Global
        srtm_shapefile = dict_Global['in']['SRTM_Shapefile']
        hgts_path = dict_Global['in']['SRTM_Path']
        geoid_path = dict_Global['in']['Geoid']

        eof_Path = None
        if 'EOF_Path' in dict_Global['in']:
            eof_Path = dict_Global['in']['EOF_Path']

        output_dir = dict_Global['out']['Output_Path']

        if not os.path.exists(output_dir):
            print("The output directory does not exist and will be created")
            os.makedirs(output_dir)
        else :
            print("The output directory exists. Some files can be overwritten")

        iso_start, iso_end = func_utils.argDates_to_isoDates(dict_Global['in']['Start_Date'], dict_Global['in']['End_Date'])
        start_time = int(dict_Global['in']['Start_Date'])
        end_time = int(dict_Global['in']['End_Date'])
        master_Image_base = dict_Global['in']['Master_Image']
        master_Image = "" 
        pol = ""
        exclude = "-9999"
        if 'Exclude' in dict_Global['in']:
            exclude = dict_Global['in']['Exclude']
        relative_orbit = ""
        manifest = ""
        light_version = "yes"
        ram = str(4000)
        if 'parameter' in dict_Global: 
            if 'optram' in dict_Global['parameter']:
                ram =  str(dict_Global['parameter']['optram'])
                if int(ram) > 4000 : 
                    ram = str(4000)
            if 'clean' in dict_Global['parameter']:
                light_version = dict_Global['parameter']['clean']
        light_version = func_utils.str2bool(light_version)


        func_utils.check_ifExist(dict_Global['in']['Input_Path'])

        if master_ext[0] == "h5" : # Cosmo case
            master_Image = func_utils.get_imgFromDir(dict_Global['in']['Master_Image'], dict_Global['in']['Input_Path'])

            if not master_Image :
                print(master_Image_base + " not found into given input path " + \
                      dict_Global['in']['Input_Path'])
                print("Please check your input path")
                quit()
            else :
                correct = func_utils.check_image_pattern(master_Image_base, mode="Cosmo")
                if not correct : 
                    print("Master image " + master_Image_base + " does not respect naming conventions for Cosmo")
                    quit()

            master_date = master_Image_base.split("_")[8][:8]
            pol = master_Image_base.split("_")[5]

        else : #S1 SM case
            master_Image = func_utils.get_imgFromSAFE(dict_Global['in']['Master_Image'], dict_Global['in']['Input_Path'])

            if not master_Image :
                print(master_Image_base + " not found into given input path " + \
                      dict_Global['in']['Input_Path'])
                print("Please check your input path")
                quit()
            else :
                correct = func_utils.check_image_pattern(master_Image_base, mode="S1SM")
                if not correct : 
                    print("Master image " + master_Image_base + " does not respect naming conventions for S1SM")
                    quit()


            master_date = master_Image_base.split("-")[4].split("t")[0]
            pol = master_Image_base.split("-")[3]
            manifest = master_Image.split("measurement")[0]+"/manifest.safe"
            relative_orbit = func_utils.get_relative_orbit(manifest)

        satellite = "default"
        mode = "default"

        if 'sensor' in dict_Global:
            satellite = dict_Global['sensor']['satellite']
            mode = dict_Global['sensor']['mode']

        # ====== Pre_Processing
        rng = 3
        azi = 3
        if "ML_ran" in dict_PreProcessing['parameter']:
            rng = dict_PreProcessing['parameter'].get('ML_ran')
        if "ML_azi" in dict_PreProcessing['parameter']:
            azi = dict_PreProcessing['parameter'].get('ML_azi')
        ml_range = int(rng)
        ml_azimut = int(azi)
        ml_gain = dict_PreProcessing['parameter']['ML_gain']
        dop_file = dict_PreProcessing['out']['doppler_file']

        # ====== Metadata_Correction
        activateMetadataCorrection = dict_Metadata_Correction['parameter']['activate']
        ml_simu_range = ml_range
        ml_simu_azimut = ml_azimut
        ml_simu_gain = 1.
        ml_correlSimu_range = ml_range
        ml_correlSimu_azimut = ml_azimut
        correlSimu_gridstep_range = dict_Metadata_Correction['parameter']['GridStep_range']
        correlSimu_gridstep_azimut = dict_Metadata_Correction['parameter']['GridStep_azimut']
        fine_metadata_file = dict_Metadata_Correction['out']['fine_metadata_file']

        # ====== DIn_SAR
        roi = None
        if 'roi' in dict_DInSAR['parameter']:
            roi = dict_DInSAR['parameter']['roi']
        version_interferogram = dict_DInSAR['parameter']['Activate_Interferogram']
        geoGrid_gridstep_range = dict_DInSAR['parameter']['GridStep_range']
        geoGrid_gridstep_azimut = dict_DInSAR['parameter']['GridStep_azimut']
        geoGrid_threshold = dict_DInSAR['parameter']['Grid_Threshold']
        geoGrid_gap = dict_DInSAR['parameter']['Grid_Gap']
        ml_geoGrid_range = ml_range
        ml_geoGrid_azimut = ml_azimut
        gain_interfero = dict_DInSAR['parameter']['Interferogram_gain']

        ml_interf_range = ml_range
        ml_interf_azimut = ml_azimut

        if "Interferogram_mlran" in dict_DInSAR['parameter'] :
            ml_interf_range = int(dict_DInSAR['parameter']['Interferogram_mlran'])

        if "Interferogram_mlazi" in dict_DInSAR['parameter'] :
            ml_interf_azimut = int(dict_DInSAR['parameter']['Interferogram_mlazi'])

        # ====== Post_Processing
        spacingxy = "0.0001"
        if "Spacingxy" in dict_PostProcessing['parameter']:
            spacingxy = str(dict_PostProcessing['parameter']['Spacingxy'])

        ortho_interferogram = "false"
        if 'Activate_Ortho' in dict_PostProcessing['parameter']:
            ortho_interferogram = dict_PostProcessing['parameter']['Activate_Ortho']
        if roi:
            ortho_interferogram = "yes"
            print("ortho_interferogram", ortho_interferogram)

        activateFiltering = "false"
        if "Activate_Filtering" in dict_PostProcessing['parameter'] :
            activateFiltering = dict_PostProcessing['parameter']['Activate_Filtering']

        ml_interf_filt_range = 3
        if "Filtered_Interferogram_mlran" in dict_PostProcessing['parameter'] :
            ml_interf_filt_range = int(dict_PostProcessing['parameter']['Filtered_Interferogram_mlran'])

        ml_interf_filt_azimut = 3
        if "Filtered_Interferogram_mlazi" in dict_PostProcessing['parameter'] :
            ml_interf_filt_azimut = int(dict_PostProcessing['parameter']['Filtered_Interferogram_mlazi'])

        interf_filt_alpha = 0.7
        if "Filtering_parameter_alpha" in dict_PostProcessing['parameter'] :
            interf_filt_alpha = dict_PostProcessing['parameter']['Filtering_parameter_alpha']

    except KeyError as e :
        # Indicate a incohenrency between expected keys and input json file (jsson schemas can be used)
        response = "Exception Key error  : \n " \
                   "Error into keys correspondance : json file does not contain all required keys. \n " \
                   "You can check your input configuration file  with available schemas " \
                   "(into json_schemas repository) and by setting a environnement variable (DIAPOTB_INSTALL) " \
                   "to your installation before relaunching."

        print(response)

        print("For your information, the missing key is : " + str(e))
        quit()

    except Exception as e :
        print("Exception into input variable handlings : " + str(e))
        quit()


    if (geoGrid_threshold < 0) or (geoGrid_threshold > 1):
        func_utils.log(logging.CRITICAL, "Error, Wrong Threshold for fine deformation grid")
        geoGrid_threshold = 0.3

    # ====== Check if interferogram is activated for ortho interferogram
    if func_utils.str2bool(ortho_interferogram) and not func_utils.str2bool(version_interferogram):
        func_utils.log(logging.CRITICAL, "Error, Impossible to have ortho interferogram without interferogram\n"
        "If Ortho is required, please activate interferogram estimation in your json file")
        quit()

    # ====== Check if interferogram is activated for filtering
    if func_utils.str2bool(activateFiltering) and not func_utils.str2bool(version_interferogram):
        func_utils.log(logging.CRITICAL, "Error, Impossible to have filering without interferogram\n"
        "If Filtering is required, please activate interferogram estimation in your json file")
        quit()

    # ====== Check if images exist
    func_utils.check_ifExist(srtm_shapefile)
    func_utils.check_ifExist(hgts_path)
    func_utils.check_ifExist(master_Image)

    # Check eof path 
    if eof_Path :
        if not os.path.exists(eof_Path) :
            func_utils.log(logging.CRITICAL, "Error, {path} does not exist. Check its path.".format(path=eof_Path))
            quit()

    # ====== Check roi format (if roi)
    if roi :
        func_utils.check_roiFormat(roi)

    # ====== Create global folder with starting and ending dates + master date
    output_glob = "{}/output_{}_to_{}_m_{}".format(output_dir, start_time, 
                                                   end_time, master_date)
    if not os.path.exists(output_glob):
        os.makedirs(output_glob)

    # ====== Create Digital Elevation Model
    dem, target_dir = func_utils.build_virutal_raster(master_Image, start_time, end_time,
                                                      master_date, srtm_shapefile, hgts_path,
                                                      output_dir)
    print("\n Removing master_image_envelope shp files...\n")
    for i in os.listdir(target_dir):
        if i.startswith("master_envelope"):
            os.remove(target_dir + "/" + i)


    # Init file handler (all normaly print on std is redirected into info.log) 
    # To use previous print on std, use printOnStd
    func_utils.init_fileLog(output_glob)
    
    # Recap of input parameter into info.log
    func_utils.log(logging.INFO, "########### Input Parameters for the current execution ############## ")
    # Global
    func_utils.log(logging.INFO, " Global : ")
    func_utils.log(logging.INFO, "srtm_shapefile : {param}".format(param=srtm_shapefile))
    func_utils.log(logging.INFO, "hgts_path : {param}".format(param=hgts_path))
    func_utils.log(logging.INFO, "geoid_path : {param}".format(param=geoid_path))
    func_utils.log(logging.INFO, "Input_Path : {param}".format(param=dict_Global['in']['Input_Path']))
    func_utils.log(logging.INFO, "Start_Date : {param}".format(param=start_time))
    func_utils.log(logging.INFO, "End_Date : {param}".format(param=end_time))
    func_utils.log(logging.INFO, "Master_Image : {param}".format(param=master_Image_base))
    func_utils.log(logging.INFO, "Exclude : {param}".format(param=exclude))
    func_utils.log(logging.INFO, "clean : {param}".format(param=light_version))
    func_utils.log(logging.INFO, "optram : {param}".format(param=ram))
    func_utils.log(logging.INFO, "satellite : {param}".format(param=satellite))
    func_utils.log(logging.INFO, "mode : {param}".format(param=mode))

    # Pre_Processing
    func_utils.log(logging.INFO, " Pre_Processing : ")
    func_utils.log(logging.INFO, "ml_range : {param}".format(param=ml_range))
    func_utils.log(logging.INFO, "ml_azimut : {param}".format(param=ml_azimut))
    func_utils.log(logging.INFO, "ml_gain : {param}".format(param=ml_gain))

    # DIn_SAR
    func_utils.log(logging.INFO, " DIn_SAR : ")
    func_utils.log(logging.INFO, "geoGrid_gridstep_range : {param}".format(param=geoGrid_gridstep_range))
    func_utils.log(logging.INFO, "geoGrid_gridstep_azimut : {param}".format(param=geoGrid_gridstep_azimut))
    func_utils.log(logging.INFO, "geoGrid_threshold : {param}".format(param=geoGrid_threshold))
    func_utils.log(logging.INFO, "geoGrid_gap : {param}".format(param=geoGrid_gap))
    func_utils.log(logging.INFO, "ml_geoGrid_range : {param}".format(param=ml_geoGrid_range))
    func_utils.log(logging.INFO, "ml_geoGrid_azimut : {param}".format(param=ml_geoGrid_azimut))
    func_utils.log(logging.INFO, "gain_interfero : {param}".format(param=gain_interfero))
    func_utils.log(logging.INFO, "ml_interf_range : {param}".format(param=ml_interf_range))
    func_utils.log(logging.INFO, "ml_interf_azimut : {param}".format(param=ml_interf_azimut))
    func_utils.log(logging.INFO, "roi : {param}".format(param=roi))
    func_utils.log(logging.INFO, "Activate_Interferogram : {param}".format(param=version_interferogram))

    # Post_Processing
    func_utils.log(logging.INFO, " Post_Processing : ")
    func_utils.log(logging.INFO, "Activate_Ortho : {param}".format(param=ortho_interferogram))
    if func_utils.str2bool(ortho_interferogram):
        func_utils.log(logging.INFO, "spacingxy : {param}".format(param=spacingxy))
    func_utils.log(logging.INFO, "activateFiltering : {param}".format(param=activateFiltering))
    if func_utils.str2bool(activateFiltering):
        func_utils.log(logging.INFO, "ml_interf_filt_range : {param}".format(param=ml_interf_filt_range))
        func_utils.log(logging.INFO, "ml_interf_filt_azimut : {param}".format(param=ml_interf_filt_azimut))
        func_utils.log(logging.INFO, "interf_filt_alpha : {param}".format(param=interf_filt_alpha))

  


    # Retrieve some information about our master image
    dictKWLMaster = func_utils.getImageKWL(master_Image)
    number_col = int(dictKWLMaster['number_samples'])
    number_lines = int(dictKWLMaster['number_lines'])
    
    # Check input size (Master size) to protect memory. 
    if (number_col > 15000 or number_lines > 15000) :
        
        intermediate_mlran = int(number_col/15000) + 1
        intermediate_mlazi = int(number_lines/15000) + 1
        
        # Compare ml factor with intermediate
        if ((intermediate_mlazi*intermediate_mlran) > (ml_range*ml_azimut)) :
            func_utils.log(logging.CRITICAL, "ML Factors are not appropriate (too low) for these estimations. Please use other factors such as : {iran} x {iazi} \n".format(iran=intermediate_mlran, iazi=intermediate_mlazi))
    
            quit()


    
    # =============================
    # Get the elements from os.dirs
    # =============================

    # ====== Get the list of GTiff corresponding to dates and patterns
    tiff_list, throw_warning = func_utils.get_AllTiff(pol=pol, ext=master_ext[0], searchDir=dict_Global["in"]["Input_Path"])
    
    # Throw a warning
    if throw_warning :
        func_utils.log(logging.WARNING, "WARNING : At least one of selected images into your input path does not match the expected pattern"+ "\n")

 
    tiff_dates = func_utils.get_Tiff_WithDates(start_time, end_time, exclude, tiff_list, master_ext[0])
    tiff_dates = func_utils.avoidDuplicates(tiff_dates)

    if len(tiff_dates)-1 <= 0 :
        func_utils.log(logging.CRITICAL, "ERROR : None secondary images found, please check your input path and your selection (dates, exclude ...)"+ "\n")
        quit()

    if throw_warning :
        # Indicate reference and all secondary images
        func_utils.printOnStd("Reference and Secondary images : ")
        func_utils.printOnStd(tiff_dates)

    counter = 0
    # ====== For loop processing
    for i in (i for i in tiff_dates if i != master_Image_base):
        total_slaves = len(tiff_dates)-1
        slave_Image_base = i
        slave_Image = ""
        if not master_ext[0] == "h5" :
            slave_Image = func_utils.get_imgFromSAFE(slave_Image_base, searchDir=dict_Global["in"]["Input_Path"])
        else :
            slave_Image = func_utils.get_imgFromDir(slave_Image_base, searchDir=dict_Global["in"]["Input_Path"])
        slave_date = func_utils.get_Date(i, master_ext[0])
        counter += 1
        output_dir = output_glob + "/{}_m_{}_s".format(master_date, slave_date)
        if os.path.exists(output_dir):
            shutil.rmtree(output_dir)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        master_data_dir = output_glob + "/{}_master_directory".format(master_date)
        if not os.path.exists(master_data_dir):
            os.makedirs(master_data_dir)

        # ====== Check extension (if .h5 => HDF5 file => Cosmo Sensor)
        slave_ext = slave_Image.split(".")[-1:]

        func_utils.log(logging.INFO, "master_ext = " + master_ext[0] + "\n")
        func_utils.log(logging.INFO, "slave_ext = " + slave_ext[0] + "\n")

        if counter <= 1:
            if master_ext[0] == "h5":
                master_H5 = h5py.File(master_Image, 'r')
                lDataSet_master = list(master_H5.keys())

                if len(lDataSet_master) != 1:
                    func_utils.log(logging.CRITICAL, "Error, H5 input files does not contain the expected dataset \n")
                    quit()

                if lDataSet_master[0] != "S01":
                    func_utils.log(logging.CRITICAL, "Error, H5 input files does not contain the expected dataset \n")
                    quit()

                master_S01 = dict(master_H5['S01'])

                if not 'SBI' in master_S01:
                    func_utils.log(logging.CRITICAL, "Error, H5 input files does not contain the expected dataset \n")
                    quit()

                # Change the name of master and slave image to read directly the //S01/SBI
                master_Image = "HDF5:" + master_Image + "://S01/SBI"
                # Adapt satellite
                satellite = "cosmo"


        if slave_ext[0] == "h5":
            slave_H5 = h5py.File(slave_Image, 'r')
            lDataSet_slave = list(slave_H5.keys())

            if len(lDataSet_slave) != 1:
                func_utils.log(logging.CRITICAL, "Error, H5 input files does not contain the expected dataset \n")
                quit()

            if lDataSet_slave[0] != "S01":
                func_utils.log(logging.CRITICAL, "Error, H5 input files does not contain the expected dataset \n")
                quit()

            slave_S01 = dict(slave_H5['S01'])

            if not 'SBI' in slave_S01:
                func_utils.log(logging.CRITICAL, "Error, H5 input files does not contain the expected dataset \n")
                quit()

            slave_Image = "HDF5:" + slave_Image + "://S01/SBI"

        # Find eof files for each image if not cosmo
        # Then, create the "fine" geom (with precise orbits) 
        # Eventually, assign an extended filename if EOF file correspond to the image
        if satellite != "cosmo" :
            if eof_Path :
                list_ofEOF = func_utils.get_AllFilesWithExt(eof_Path, ".EOF")


                # master 
                if counter <= 1 :
                    start_master = dictKWLMaster['support_data.first_line_time']
                    end_master = dictKWLMaster['support_data.last_line_time']

                    # Get a eof file 
                    eof_file = func_utils.select_EofWithDate(start_master, end_master, list_ofEOF)

                    if (eof_file) :
                        # Create the new geom file into dedicated repository
                        extendedGeom_Path = os.path.join(master_data_dir, "extended_geom")
                        if not os.path.exists(extendedGeom_Path):
                            os.makedirs(extendedGeom_Path)

                        # Call SARMetadataCorrection
                        diapOTBApp.metadataCorrection(mode="orbits", insar=master_Image, indem=None, 
                                                      infineorbits=os.path.join(eof_Path, eof_file),
                                                      outPath=os.path.join(extendedGeom_Path, 
                                                                           "extended_master.geom"), 
                                                      ram=ram)


                        # Assign new geom file with extended filename
                        master_Image += "?geom=" + os.path.join(extendedGeom_Path, 
                                                                "extended_master.geom")


                # slave 
                dictKWLSlave = func_utils.getImageKWL(slave_Image)
                start_slave = dictKWLSlave['support_data.first_line_time']
                end_slave = dictKWLSlave['support_data.last_line_time']

                # Get a eof file 
                eof_file = func_utils.select_EofWithDate(start_slave, end_slave, list_ofEOF)

                if (eof_file) :
                    # Create the new geom file into dedicated repository
                    extendedGeom_Path = os.path.join(output_dir, "extended_geom")
                    if not os.path.exists(extendedGeom_Path):
                        os.makedirs(extendedGeom_Path)

                    # Call SARMetadataCorrection
                    diapOTBApp.metadataCorrection(mode="orbits", insar=slave_Image, indem=None, 
                                                  infineorbits=os.path.join(eof_Path, eof_file),
                                                  outPath=os.path.join(extendedGeom_Path, 
                                                                       "extended_slave.geom"),
                                                  ram=ram)

                    # Assign new geom file with extended filename
                    slave_Image += "?geom=" + os.path.join(extendedGeom_Path, 
                                                           "extended_slave.geom")



        func_utils.log(logging.INFO, "########### Input Images for the current execution ############## ")
        func_utils.log(logging.INFO, "Nb iteration : {param} with : ".format(param=counter))
        func_utils.log(logging.INFO, "master_Image : {param}".format(param=master_Image))
        func_utils.log(logging.INFO, "slave_Image : {param}".format(param=slave_Image))

        func_utils.printOnStd("\n Nb iteration : {param} with : \n".format(param=counter))
        func_utils.printOnStd("\n master_Image : {param} : \n".format(param=master_Image))
        func_utils.printOnStd("\n slave_Image : {param} : \n".format(param=slave_Image))

        # #################################################################### #
        # ###################### Pre Processing Chain ######################## #
        # #################################################################### #
        # === Convert master to .tif
        if counter <= 1:
            func_utils.printOnStd("\n Master Pre_Processing chain \n")
            func_utils.log(logging.INFO, "Master Pre_Processing Application")

            master_slc = ""
            if not master_ext[0] == "h5" and roi is None:
                master_slc = func_utils.getSlcMlNamming_fromProductName(master_Image_base, mode="S1SM") + ".tif"
          
                # === Here we convert master single band CInt16 to dual band FLOAT32
                # === To do so, OTB_ExtractRoi is much faster than OTB_DynamicConvert
                diapOTBApp.extractROI(master_Image, 
                                      os.path.join(master_data_dir, master_slc+"?&gdal:co:TILED=YES"),
                                      ram)

            if master_ext[0] == "h5":
                master_slc = func_utils.getSlcMlNamming_fromProductName(master_Image_base, mode="Cosmo") + ".tif"
                ds = gdal.Open(master_Image, gdal.GA_ReadOnly)
                ds = gdal.Translate(os.path.join(master_data_dir, master_slc), ds, format="GTiff",
                                    outputType=gdal.GDT_Float32, creationOptions=['TILED=YES'])

            # Master
            paramPreMaster = {}
            paramPreMaster['ml_range'] = ml_range
            paramPreMaster['ml_azimut'] = ml_azimut
            paramPreMaster['ml_gain'] = ml_gain
            paramPreMaster['dop_file'] = dop_file
            paramPreMaster['ram'] = ram

            Pre_Processing.extractToMultilook(master_Image, master_Image_base, paramPreMaster, "Others", master_data_dir)


        # === Slave
        func_utils.printOnStd("\n Slave Pre_Processing chain \n")
        func_utils.log(logging.INFO, "Slave Pre_Processing Application")

        paramPreSlave = {}
        paramPreSlave['ml_range'] = ml_range
        paramPreSlave['ml_azimut'] = ml_azimut
        paramPreSlave['ml_gain'] = ml_gain
        paramPreSlave['dop_file'] = dop_file
        paramPreSlave['ram'] = ram

        dop0Slave = Pre_Processing.extractToMultilook(slave_Image, slave_Image_base, paramPreSlave, "Others", output_dir)


        master_Image_ML = os.path.splitext(master_Image_base)[0] + "_ml" + str(ml_azimut) + str(ml_range) + ".tif"
        slave_Image_ML = os.path.splitext(slave_Image_base)[0] + "_ml" + str(ml_azimut) + str(ml_range) + ".tif"


        # #################################################################### #
        # ################### Metadata Correction Chain ###################### #
        # #################################################################### #
        if activateMetadataCorrection:
            ####### SARDEMToAmplitude Application (Simu_SAR step) ######
            if counter <= 1:
                # TO DO
                print("\n Metadata Correction Chain not available for now \n")
                #func_utils.printOnStd("Metadata Correction Chain not available for now")

                # paramMetadata = {}
                # paramMetadata['ml_range'] = ml_simu_range
                # paramMetadata['ml_azimut'] = ml_simu_azimut
                # paramMetadata['ml_gain'] = ml_simu_gain
                # paramMetadata['geoGrid_gridstep_range'] = correlSimu_gridstep_range
                # paramMetadata['geoGrid_gridstep_azimut'] = correlSimu_gridstep_azimut
                # paramMetadata['nodata'] = -32768
                # paramMetadata['fine_metadata_file'] = fine_metadata_file

                # Metadata_Correction.fineMetadata(master_Image, master_Image_base, dem, paramMetadata, master_data_dir)


        # ##################################################################### #
        # ######################## Ground Chain ############################### #
        # ##################################################################### #
        # Master
        if counter <= 1:
            func_utils.printOnStd("\n Master Ground chain \n")
            func_utils.log(logging.INFO, "Master Ground Application")

            paramGroundMaster = {}
            paramGroundMaster['nodata'] = -32768
            paramGroundMaster['withxyz'] = "true"
            paramGroundMaster['for_slave_Image'] = False
            paramGroundMaster['ram'] = ram

            Ground.demProjectionAndCartesianEstimation(master_Image, master_Image_base, dem, paramGroundMaster, 
                                                       "Others", master_data_dir)
        # Slave
        func_utils.printOnStd("\n Slave Ground chain \n")
        func_utils.log(logging.INFO, "Slave Ground Application")
        paramGroundSlave = {}
        paramGroundSlave['nodata'] = -32768
        paramGroundSlave['withxyz'] = "true"
        paramGroundSlave['for_slave_Image'] = True
        paramGroundSlave['ram'] = ram

        Ground.demProjectionAndCartesianEstimation(slave_Image, slave_Image_base, dem, paramGroundSlave, 
                                                   "Others", output_dir)

        demProj_Master = "demProj_Master.tif"
        master_cartesian_mean = "CartMeanMaster.tif"
        demProj_Slave = "demProj_Slave.tif"

        # ##################################################################### #
        # ####################### DIn_SAR Chain ############################### #
        # ##################################################################### #
        func_utils.printOnStd("\n DIn_SAR chain \n")
        func_utils.log(logging.INFO, "DIn_SAR chain")
        # Create param
        param = {}
        param['ml_azimut'] = ml_azimut
        param['ml_range'] = ml_range
        param['ml_geoGrid_azimut'] = ml_geoGrid_azimut
        param['ml_geoGrid_range'] = ml_geoGrid_range
        param['ml_interf_range'] = ml_interf_range  
        param['ml_interf_azimut'] = ml_interf_azimut
        param['geoGrid_gridstep_range'] = geoGrid_gridstep_range
        param['geoGrid_gridstep_azimut'] = geoGrid_gridstep_azimut
        param['geoGrid_threshold'] = geoGrid_threshold
        param['geoGrid_gap'] = geoGrid_gap
        param['doppler0'] = dop0Slave
        param['gain_interfero'] = gain_interfero
        param['advantage'] = "projection" # By default projection
        param['ram'] = ram
        if satellite == "cosmo" or satellite == "CSK":
            param['advantage'] = "correlation"    

        slave_Image_CoRe = ""
        if not master_ext[0] == "h5":
            slave_Image_CoRe = func_utils.getSlcMlNamming_fromProductName(slave_Image_base, mode="S1SM") + ".tif"
        if master_ext[0] == "h5":
            slave_Image_CoRe = func_utils.getSlcMlNamming_fromProductName(slave_Image_base, mode="Cosmo") + ".tif"

        param['slave_CoRe_Name'] = slave_Image_CoRe

        param['with_interferogram']  = version_interferogram

        list_of_Grids, list_of_Interferogram = DInSar.gridToInterferogram(dem, master_Image, master_Image_base, slave_Image, slave_Image_base, master_data_dir, output_dir, param, 'Others', output_dir)

        interferogram_path = ""
        if func_utils.str2bool(version_interferogram):
            interferogram_path = list_of_Interferogram[0]
        grid_path = list_of_Grids[0]

        # ==================================== #
         #        Post Processing Chain       # 
          #                                  #
           # ============================== #


        func_utils.printOnStd("\n Post Processing chain \n")
        func_utils.log(logging.INFO, "Post Processing chain")


        ### Names definition ###
        InterferoB123 = ""
        Interfero_Ortho = ""
        Interfero_roi  = ""
        Interfo_Filt = ""

        if not master_ext[0] == "h5":
            interf_base = func_utils.getInterfNamming_fromProductName(master_Image_base, slave_Image_base,
                                                                      mode="S1SM")
            InterferoB123  = interf_base + "_Interferogram.tif"
            Interfero_Ortho  = interf_base + "_Ortho-Interferogram.tif"
            Interfero_roi  = interf_base + "_ROI_Ortho-Interferogram.tif"
            Interfo_Filt = interf_base + "_Filtred-Interferogram.tif"

        if master_ext[0] == "h5":
            interf_base = func_utils.getInterfNamming_fromProductName(master_Image_base, slave_Image_base,
                                                                      mode="Cosmo")
            InterferoB123  = interf_base + "_Interferogram.tif"
            Interfero_Ortho  = interf_base + "_Ortho-Interferogram.tif"
            Interfero_roi  = interf_base + "_ROI_Ortho-Interferogram.tif"
            Interfo_Filt = interf_base + "_Filtred-Interferogram.tif"


        # Phase Filtering (if required)
        if func_utils.str2bool(activateFiltering):
            # Default paramater 
            step = 16
            sizetiles = 64

            paramPost = {}
            paramPost['slave_CoRe_Name'] = slave_Image_CoRe
            paramPost['ml_filt_interf_range'] = ml_interf_filt_range
            paramPost['ml_filt_interf_azimut'] = ml_interf_filt_azimut
            paramPost['geoGrid_gridstep_range'] = geoGrid_gridstep_range
            paramPost['geoGrid_gridstep_azimut'] = geoGrid_gridstep_azimut
            paramPost['ml_filt_interf_gain'] = gain_interfero
            paramPost['filt_alpha'] = interf_filt_alpha
            paramPost['ram'] = ram
            paramPost['filtered_Name'] = Interfo_Filt

            Post_Processing.filtering(master_Image, master_Image_base, slave_Image, 
                                      slave_Image_base, master_data_dir, output_dir, paramPost, 
                                      "Others", output_dir)

            

        # === Multilook on Coregistrated Slave
        func_utils.silentremove(output_dir, slave_Image_ML)
        slave_Image_ML_old = slave_Image_ML
        if not master_ext[0] == "h5" :
            slave_Image_ML = func_utils.getSlcMlNamming_fromProductName(slave_Image_base, mode="S1SM") + \
                             "_ml" + str(ml_azimut) + str(ml_range) + ".tif"
        else :
            slave_Image_ML = func_utils.getSlcMlNamming_fromProductName(slave_Image_base, mode="Cosmo") + \
                             "_ml" + str(ml_azimut) + str(ml_range) + ".tif"

        diapOTBApp.multilook(os.path.join(output_dir, slave_Image_CoRe), ml_range, ml_azimut, ml_gain, 
                             os.path.join(output_dir, slave_Image_ML), ram)


        

        ### ROI, Ortho and band_extract ###
        if func_utils.str2bool(ortho_interferogram):
            interf_ortho = "interferogram_ortho.tif"
            diapOTBApp.orthorectification(interferogram_path, spacingxy, 
                                          hgts_path, geoid_path, 
                                          os.path.join(output_dir, interf_ortho), ram)

            # Set WGS 84 as projection to ortho interferogram
            func_utils.add_WGSProjection(os.path.join(output_dir, interf_ortho))

            if not roi: 
                func_utils.extract_band123(os.path.join(output_dir, interf_ortho), os.path.join(output_dir, Interfero_Ortho))
                func_utils.extract_band123(interferogram_path, os.path.join(output_dir, InterferoB123))
            if roi : 
                interf_roi = "interferogram_roi.tif"
                func_utils.extract_roi(os.path.join(output_dir, interf_ortho), os.path.join(output_dir, interf_roi), roi)
                func_utils.extract_band123(os.path.join(output_dir, interf_roi), os.path.join(output_dir, Interfero_roi))
        if func_utils.str2bool(version_interferogram) and not func_utils.str2bool(ortho_interferogram) and roi is None:
            func_utils.extract_band123(interferogram_path, os.path.join(output_dir, InterferoB123))

         # ==================================== #
          #               Removing             # 
           #               files              #
            # ============================== #
        if func_utils.str2bool(version_interferogram):
            func_utils.silentremove(os.path.dirname(interferogram_path), os.path.basename(interferogram_path))
            
            os.rename(os.path.join(os.path.dirname(interferogram_path), 
                                   os.path.basename(interferogram_path).split(".")[0]+".geom"), 
                      os.path.join(output_dir, InterferoB123.split(".")[0]+".geom"))

        if func_utils.str2bool(ortho_interferogram):
            interferogram_ortho = "interferogram_ortho.tif"
            func_utils.silentremove(output_dir, interferogram_ortho)

            os.rename(os.path.join(output_dir, interferogram_ortho.split(".")[0]+".geom"), 
                      os.path.join(output_dir, Interfero_Ortho.split(".")[0]+".geom"))

        if roi: 
            func_utils.silentremove(output_dir, interf_roi)


        if light_version is True:
            func_utils.log(logging.INFO, "\n Removing files for light version \n")
            func_utils.silentremove(output_dir, dop_file)
            func_utils.silentremove(output_dir, demProj_Slave)
            func_utils.silentremove(output_dir, demProj_Slave.split(".")[0]+".geom")
            func_utils.silentremove(os.path.dirname(grid_path), os.path.basename(grid_path))
            func_utils.silentremove(os.path.dirname(grid_path), os.path.basename(grid_path).split(".")[0]+".geom")
            func_utils.silentremove(output_dir, slave_Image_ML_old.split(".")[0]+".geom")

        # Remove .tif.aux.xml files into output_dir
        aux_files = func_utils.get_AllFilesWithExt(output_dir, ".tif.aux.xml")
        for i_file in aux_files :
            if i_file == "filfPhaCoh.tif.aux.xml":
                filt_dir = os.path.join(output_dir, "filt")
                func_utils.silentremove(filt_dir, i_file)
            else :    
                func_utils.silentremove(output_dir, i_file)
            

                
    # After processing loop : rename and clean master_data_dir
    # Rename master_ML and geom file
    if not master_ext[0] == "h5" :
        master_new_ML = func_utils.getSlcMlNamming_fromProductName(master_Image_base, mode="S1SM") + \
                        "_ml" + str(ml_azimut) + str(ml_range) + ".tif"
        master_new_ML_geom = func_utils.getSlcMlNamming_fromProductName(master_Image_base, mode="S1SM") + \
                             "_ml" + str(ml_azimut) + str(ml_range) + ".geom"
    else :
        master_new_ML = func_utils.getSlcMlNamming_fromProductName(master_Image_base, mode="Cosmo") + \
                        "_ml" + str(ml_azimut) + str(ml_range) + ".tif"
        master_new_ML_geom = func_utils.getSlcMlNamming_fromProductName(master_Image_base, mode="Cosmo") + \
                             "_ml" + str(ml_azimut) + str(ml_range) + ".geom"

    os.rename(os.path.join(master_data_dir, master_Image_ML), 
              os.path.join(master_data_dir, master_new_ML))

    master_ML_geom = master_Image_ML.split(".")[0] + ".geom"
    os.rename(os.path.join(master_data_dir, master_ML_geom), 
              os.path.join(master_data_dir, master_new_ML_geom))

    if light_version is True:
        func_utils.log(logging.INFO, "\n Removing files for light version \n")
        if os.path.exists(os.path.join(master_data_dir, dop_file)):
            func_utils.silentremove(master_data_dir, dop_file)
        func_utils.silentremove(master_data_dir, demProj_Master)
        func_utils.silentremove(master_data_dir, demProj_Master.split(".")[0]+".geom")
        func_utils.silentremove(master_data_dir, master_cartesian_mean)
        func_utils.silentremove(master_data_dir, master_cartesian_mean.split(".")[0]+".geom")
        if master_ext[0] == "h5":
            func_utils.silentremove(master_data_dir, master_slc.split(".")[0]+".tif.aux.xml")
            CoreGeom = slave_Image_CoRe.split(".")[0]+".geom"
            MasterGeom = master_slc.split(".")[0]+".geom"
            copyfile(os.path.join(output_dir, CoreGeom), os.path.join(master_data_dir, MasterGeom))

