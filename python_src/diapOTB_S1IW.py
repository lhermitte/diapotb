#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2017 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

""" 
    diapOTB_S1IW.py
    ===============

    Interferometry chain between two SAR images (master/slave) for S1 IW products
 
"""

__author__ = "POC-THALES"
__version__ = "0.1"
__status__ = "Developpement"
__date__ = "11/12/2018"
__last_modified__ = "05/02/2020"

# Imports
import logging
import os
import sys
import argparse
import re

from processings import Pre_Processing
from processings import Ground
from processings import DInSar
from processings import Post_Processing

import utils.DiapOTB_applications as diapOTBApp
from utils import func_utils

# Main
if __name__ == "__main__":
    
    # Check arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("configfile", help="input conguration file for the application DiapOTB")
    args = parser.parse_args()
    print(args.configfile)

    func_utils.init_logger()
       
    dataConfig = func_utils.load_configfile(args.configfile, "S1_IW")

    try :
        # Get dictionaries
        dict_Global = dataConfig['Global']
        dict_PreProcessing = dataConfig['Pre_Processing']
        dict_Ground = dataConfig['Ground']
        dict_DInSAR = dataConfig['DIn_SAR']
        dict_PostProcessing = dataConfig['Post_Processing']


        # Get elements from dictionaries
        # Global
        master_Image = dict_Global['in']['Master_Image_Path']
        slave_Image =  dict_Global['in']['Slave_Image_Path']
        dem =  dict_Global['in']['DEM_Path']
        eof_Path = None
        if 'EOF_Path' in dict_Global['in']:
            eof_Path = dict_Global['in']['EOF_Path']

        output_dir = dict_Global['out']['output_dir']

        ram = str(4000)
        if 'parameter' in dict_Global: 
            if 'optram' in dict_Global['parameter']:
                ram =  str(dict_Global['parameter']['optram'])
                if int(ram) > 4000 : 
                    ram = 4000

        # Pre_Processing
        ml_range = dict_PreProcessing['parameter']['ML_range'] 
        ml_azimut = dict_PreProcessing['parameter']['ML_azimut']
        ml_gain = dict_PreProcessing['parameter']['ML_gain']
        dop_file = dict_PreProcessing['out']['doppler_file']

        # Ground

        # DIn_SAR
        geoGrid_gridstep_range = dict_DInSAR['parameter']['GridStep_range']
        geoGrid_gridstep_azimut = dict_DInSAR['parameter']['GridStep_azimut']
        geoGrid_threshold = dict_DInSAR['parameter']['Grid_Threshold']
        geoGrid_gap = dict_DInSAR['parameter']['Grid_Gap']
        ml_geoGrid_range = ml_range
        ml_geoGrid_azimut = ml_azimut
        gain_interfero = dict_DInSAR['parameter']['Interferogram_gain']
        # esd loop 
        esd_AutoMode = False # automatic mode to apply a threshold inside the esd loop
        esd_NbIter = 0

        if 'ESD_iter' in dict_DInSAR['parameter']:
            esd_NbIter = dict_DInSAR['parameter']['ESD_iter']
            if not isinstance(esd_NbIter, int) :
                esd_AutoMode = True
                esd_NbIter = 10 # 10 iterations maximum for automatic mode
        else :
            esd_AutoMode = True
            esd_NbIter = 10 # 10 iterations maximum for automatic mode


        # Post_Processing
        activateOrtho = "false"
        if "Activate_Ortho" in dict_PostProcessing['parameter'] :
            activateOrtho = dict_PostProcessing['parameter']['Activate_Ortho']

        spacingxy = "0.0001"
        if "spacingxy" in dict_PostProcessing['parameter'] :
            spacingxy = str(dict_PostProcessing['parameter']['Spacingxy'])

        activateFiltering = "false"
        if "Activate_Filtering" in dict_PostProcessing['parameter'] :
            activateFiltering = dict_PostProcessing['parameter']['Activate_Filtering']

        ml_interf_filt_range = 3
        if "Filtered_Interferogram_mlran" in dict_PostProcessing['parameter'] :
            ml_interf_filt_range = int(dict_PostProcessing['parameter']['Filtered_Interferogram_mlran'])

        ml_interf_filt_azimut = 3
        if "Filtered_Interferogram_mlazi" in dict_PostProcessing['parameter'] :
            ml_interf_filt_azimut = int(dict_PostProcessing['parameter']['Filtered_Interferogram_mlazi'])

        interf_filt_alpha = 0.7
        if "Filtering_parameter_alpha" in dict_PostProcessing['parameter'] :
            interf_filt_alpha = dict_PostProcessing['parameter']['Filtering_parameter_alpha']


    except KeyError as e :
        # Indicate a incohenrency between expected keys and input json file (jsson schemas can be used)
        response = "Exception Key error  : \n " \
                   "Error into keys correspondance : json file does not contain all required keys. \n " \
                   "You can check your input configuration file  with available schemas " \
                   "(into json_schemas repository) and by setting a environnement variable (DIAPOTB_INSTALL) " \
                   "to your installation before relaunching."

        print(response)

        print("For your information, the missing key is : " + str(e))
        quit()

    except Exception as e :
        print("Exception into input variable handlings : " + str(e))
        quit()


    # Check Threshold
    if (geoGrid_threshold < 0) or (geoGrid_threshold > 1) :
        func_utils.log(logging.CRITICAL, "Error, Wrong Threshold for fine deformation grid")
        geoGrid_threshold = 0.3

    # Check if images/dem exist
    if not os.path.exists(master_Image) :
        func_utils.log(logging.CRITICAL, "Error, {img} does not exist. Check its path.".format(img=master_Image))
        quit()
    if not os.path.exists(slave_Image) :
        func_utils.log(logging.CRITICAL, "Error, {img} does not exist. Check its path.".format(img=slave_Image))
        quit()
    if not os.path.exists(dem) :
        func_utils.log(logging.CRITICAL, "Error, {img} does not exist. Check its path.".format(img=dem))
        quit()
    if not os.path.exists(output_dir):
        print("The output directory does not exist and will be created")
        os.makedirs(output_dir)
    else :
        print("The output directory exists. Some files can be overwritten")

    # Check eof path 
    if eof_Path :
        if not os.path.exists(eof_Path) :
            func_utils.log(logging.CRITICAL, "Error, {path} does not exist. Check its path.".format(path=eof_Path))
            quit()
        
    
    # Init file handler (all normaly print on std is redirected into info.log) 
    # To use previous print on std, use printOnStd
    func_utils.init_fileLog(output_dir) 

    # Recap of input parameter into info.log
    func_utils.log(logging.INFO, "########### Input Parameters for the current execution ############## ")
    func_utils.log(logging.INFO, " Pre_Processing : ")
    func_utils.log(logging.INFO, "ml_range : {param}".format(param=ml_range))
    func_utils.log(logging.INFO, "ml_azimut : {param}".format(param=ml_azimut))
    func_utils.log(logging.INFO, "ml_gain : {param}".format(param=ml_gain))
    func_utils.log(logging.INFO, "dop_file : {param}".format(param=dop_file))

    # DIn_SAR
    func_utils.log(logging.INFO, " DIn_SAR : ")
    func_utils.log(logging.INFO, "geoGrid_gridstep_range : {param}".format(param=geoGrid_gridstep_range))
    func_utils.log(logging.INFO, "geoGrid_gridstep_azimut : {param}".format(param=geoGrid_gridstep_azimut))
    func_utils.log(logging.INFO, "geoGrid_threshold : {param}".format(param=geoGrid_threshold))
    func_utils.log(logging.INFO, "geoGrid_gap : {param}".format(param=geoGrid_gap))
    func_utils.log(logging.INFO, "ml_geoGrid_range : {param}".format(param=ml_geoGrid_range))
    func_utils.log(logging.INFO, "ml_geoGrid_azimut : {param}".format(param=ml_geoGrid_azimut))
    func_utils.log(logging.INFO, "gain_interfero : {param}".format(param=gain_interfero))
    func_utils.log(logging.INFO, "esd_AutoMode : {param}".format(param=esd_AutoMode))
    func_utils.log(logging.INFO, "esd_NbIter : {param}".format(param=esd_NbIter))

     # Post_Processing
    func_utils.log(logging.INFO, " Post_Processing : ")
    func_utils.log(logging.INFO, "activateOrtho : {param}".format(param=activateOrtho))
    if func_utils.str2bool(activateOrtho):
        func_utils.log(logging.INFO, "spacingxy : {param}".format(param=spacingxy))
    func_utils.log(logging.INFO, "activateFiltering : {param}".format(param=activateFiltering))
    if func_utils.str2bool(activateFiltering):
        func_utils.log(logging.INFO, "ml_interf_filt_range : {param}".format(param=ml_interf_filt_range))
        func_utils.log(logging.INFO, "ml_interf_filt_azimut : {param}".format(param=ml_interf_filt_azimut))
        func_utils.log(logging.INFO, "interf_filt_alpha : {param}".format(param=interf_filt_alpha))

    func_utils.log(logging.INFO, "########### Input Images for the current execution ############## ")
    func_utils.log(logging.INFO, "master_Image : {param}".format(param=master_Image))
    func_utils.log(logging.INFO, "slave_Image : {param}".format(param=slave_Image))
    func_utils.log(logging.INFO, "dem : {param}".format(param=dem))

    # check Burst index      
    master_Image_base = os.path.basename(master_Image)
    slave_Image_base = os.path.basename(slave_Image)
    
    # Retrieve some information about our input images
    dictKWLMaster = func_utils.getImageKWL(master_Image)
    dictKWLSlave = func_utils.getImageKWL(slave_Image)

    # Check header version
    if int(dictKWLMaster['header.version']) < 3 or int(dictKWLSlave['header.version']) < 3 :
        func_utils.log(logging.CRITICAL, "Error, Upgrade your geom file")
        quit()

    # Get information about DEM (spacing, size ..)
    dictDEMInformation = func_utils.getDEMInformation(dem)

    # Choose advantage for correlation or projection according to DEM resolution
    advantage = "projection" # By default projection
    
    if dictDEMInformation['estimatedGroundSpacingXDEM'] > 40. or dictDEMInformation['estimatedGroundSpacingYDEM'] > 40. :
        advantage = "correlation" # Correlation if resolution > 40 m
        func_utils.log(logging.WARNING, "Resolution of the input DEM is inferior to 40 meters : A correlation will be used to correct all deformation grids")
        

    # Check the index of bursts
    minNbBurst = min([int(dictKWLMaster['support_data.geom.bursts.number']), int(dictKWLSlave['support_data.geom.bursts.number'])])

    firstBurst = 0
    lastBurst = minNbBurst
    burstIndexOk = True

    try:
        if 'parameter' in dict_Global:
            if 'burst_index' in dict_Global['parameter']:
                burstList = dict_Global['parameter']['burst_index'].split('-');
                burstList = [int(i) for i in burstList]
            
                if len(burstList) == 2 :
                    firstBurst = min(burstList)
                    lastBurst = max(burstList)
    except Exception as err:
        func_utils.log(logging.CRITICAL, "Error, Wrong burst index")
        quit()
                
    if minNbBurst < firstBurst or minNbBurst < lastBurst or lastBurst < 0 or firstBurst < 0 :
        func_utils.log(logging.CRITICAL,"Error, Wrong burst index")
        quit()
   
    
    nbBurstSlave = int(dictKWLSlave['support_data.geom.bursts.number'])
    validBurstMaster, validBurstSlave = func_utils.selectBurst(dictKWLMaster, dictKWLSlave, firstBurst, lastBurst, nbBurstSlave)

  
    if len(validBurstMaster) == 0 or len(validBurstSlave) == 0 :
        func_utils.log(logging.CRITICAL, "Error, Wrong burst index (slave index does not match with master index)")
        quit()


    # Create directory for each burst
    for burstId in range(validBurstMaster[0], validBurstMaster[len(validBurstMaster)-1]+1):
        if not os.path.exists(os.path.join(output_dir, "burst" + str(burstId))):
            os.makedirs(os.path.join(output_dir, "burst" + str(burstId)))


    func_utils.printOnStd("\n Beginning of DiapOTB processing (S1 IW mode) \n")
    func_utils.log(logging.INFO, "############ Beginning of DiapOTB processing (S1 IW mode) ##############")

    # Find eof files for each image if not cosmo
    # Then, create the "fine" geom (with precise orbits) 
    # Eventually, assign an extended filename if EOF file correspond to the image
    if eof_Path :
        list_ofEOF = func_utils.get_AllFilesWithExt(eof_Path, ".EOF")


        # master 
        start_master = dictKWLMaster['support_data.first_line_time']
        end_master = dictKWLMaster['support_data.last_line_time']

        # Get a eof file 
        eof_file = func_utils.select_EofWithDate(start_master, end_master, list_ofEOF)

        if (eof_file) :
            # Create the new geom file into dedicated repository
            extendedGeom_Path = os.path.join(output_dir, "extended_geom")
            if not os.path.exists(extendedGeom_Path):
                os.makedirs(extendedGeom_Path)

            # Call SARMetadataCorrection
            diapOTBApp.metadataCorrection(mode="orbits", insar=master_Image, indem=None, 
                                          infineorbits=os.path.join(eof_Path, eof_file),
                                          outPath=os.path.join(extendedGeom_Path, 
                                                               "extended_master.geom"),
                                          ram=ram)


            # Assign new geom file with extended filename
            master_Image += "?geom=" + os.path.join(extendedGeom_Path, 
                                                    "extended_master.geom")


        # slave 
        dictKWLSlave = func_utils.getImageKWL(slave_Image)
        start_slave = dictKWLSlave['support_data.first_line_time']
        end_slave = dictKWLSlave['support_data.last_line_time']

        # Get a eof file 
        eof_file = func_utils.select_EofWithDate(start_slave, end_slave, list_ofEOF)

        if (eof_file) :
            # Create the new geom file into dedicated repository
            extendedGeom_Path = os.path.join(output_dir, "extended_geom")
            if not os.path.exists(extendedGeom_Path):
                os.makedirs(extendedGeom_Path)

            # Call SARMetadataCorrection
            diapOTBApp.metadataCorrection(mode="orbits", insar=slave_Image, indem=None, 
                                          infineorbits=os.path.join(eof_Path, eof_file),
                                          outPath=os.path.join(extendedGeom_Path, 
                                                               "extended_slave.geom"),
                                          ram=ram)

            # Assign new geom file with extended filename
            slave_Image += "?geom=" + os.path.join(extendedGeom_Path, 
                                                   "extended_slave.geom")

            

                        
    ####################### Pre Processing Chain ##########################
    # Master
    func_utils.printOnStd("\n Master Pre_Processing chain \n")
    func_utils.log(logging.INFO, "Master Pre_Processing Application")

    paramPreMaster = {}
    paramPreMaster['ml_range'] = ml_range
    paramPreMaster['ml_azimut'] = ml_azimut
    paramPreMaster['ml_gain'] = ml_gain
    paramPreMaster['dop_file'] = dop_file
    paramPreMaster['validBurstMaster'] = validBurstMaster
    paramPreMaster['ram'] = ram

    results_PreProM = Pre_Processing.extractToMultilook(master_Image, master_Image_base, paramPreMaster, "S1_IW",
                                                        output_dir)
    
    dop0Master = results_PreProM[0]
     
    # Slave
    func_utils.printOnStd("\n Slave Pre_Processing chain \n")
    func_utils.log(logging.INFO, "Slave Pre_Processing Application")
   
    paramPreSlave = {}
    paramPreSlave['ml_range'] = ml_range
    paramPreSlave['ml_azimut'] = ml_azimut
    paramPreSlave['ml_gain'] = ml_gain
    paramPreSlave['dop_file'] = dop_file
    paramPreSlave['validBurstMaster'] = validBurstMaster
    paramPreSlave['validBurstSlave'] = validBurstSlave
    paramPreSlave['ram'] = ram

    results_PreProS = Pre_Processing.extractToMultilook(slave_Image, slave_Image_base, paramPreSlave, "S1_IW", 
                                                       output_dir)
   
    dop0Slave = results_PreProS[0]

    ######################### Ground Chain #############################
    # Master
    func_utils.printOnStd("\n Master Ground chain \n")
    func_utils.log(logging.INFO, "Master Ground Application")
 
    paramGroundMaster = {}
    paramGroundMaster['nodata'] = -32768
    paramGroundMaster['withxyz'] = "true"
    paramGroundMaster['validBurstMaster'] = validBurstMaster
    paramGroundMaster['ram'] = ram

    Ground.demProjectionAndCartesianEstimation(master_Image, master_Image_base, dem, paramGroundMaster, 
                                               "S1_IW", output_dir)
    

    # Slave
    func_utils.printOnStd("\n Slave Ground chain \n")
    func_utils.log(logging.INFO, "Slave Ground Application")

    paramGroundSlave = {}
    paramGroundSlave['nodata'] = -32768
    paramGroundSlave['withxyz'] = "true"
    paramGroundSlave['validBurstMaster'] = validBurstMaster
    paramGroundSlave['validBurstSlave'] = validBurstSlave
    paramGroundSlave['ram'] = ram

    Ground.demProjectionAndCartesianEstimation(slave_Image, slave_Image_base, dem, paramGroundSlave, 
                                               "S1_IW", output_dir)


    ######################## DIn_SAR Chain #############################
    func_utils.printOnStd("\n DIn_SAR chain \n")
    func_utils.log(logging.INFO, "DIn_SAR chain")

    counter = 0
    list_of_Interferograms = []

    # Create param
    param = {}
    param['ml_azimut'] = ml_azimut
    param['ml_range'] = ml_range
    param['validBurstMaster'] = validBurstMaster
    param['validBurstSlave'] = validBurstSlave
    param['ml_geoGrid_azimut'] = ml_geoGrid_azimut
    param['ml_geoGrid_range'] = ml_geoGrid_range
    param['geoGrid_gridstep_range'] = geoGrid_gridstep_range
    param['geoGrid_gridstep_azimut'] = geoGrid_gridstep_azimut
    param['geoGrid_threshold'] = geoGrid_threshold
    param['geoGrid_gap'] = geoGrid_gap
    param['doppler0'] = dop0Slave
    param['gain_interfero'] = gain_interfero
    param['advantage'] = advantage
    param['esd_NbIter'] = esd_NbIter 
    param['esd_AutoMode']  = esd_AutoMode
    param['ram'] = ram

    DInSar.gridToInterferogram(dem, master_Image, master_Image_base, slave_Image, slave_Image_base, output_dir, output_dir, param, 'S1_IW', output_dir)


    ######################## Post_Processing Chain #############################
    func_utils.printOnStd("\n Post_Processing chain \n")
    func_utils.log(logging.INFO, "Post_Processing chain")

    # Phase Filtering (if required)
    if func_utils.str2bool(activateFiltering):
        # Default paramater 
        step = 16
        sizetiles = 64

        paramPost = {}
        paramPost['ml_filt_interf_range'] = ml_interf_filt_range
        paramPost['ml_filt_interf_azimut'] = ml_interf_filt_azimut
        paramPost['filt_alpha'] = interf_filt_alpha
        paramPost['ram'] = ram

        Post_Processing.filtering(master_Image, master_Image_base, slave_Image, slave_Image_base, output_dir, output_dir, paramPost, "S1_IW", output_dir)


    # Interferogram into Ortho geometry (if required)
    if func_utils.str2bool(activateOrtho) :
        interf_ortho = "interferogram_ortho.tif"
        interf = "interferogram_swath.tif"
        hgts_path = os.path.dirname(dem)
        diapOTBApp.orthorectification(os.path.join(output_dir, interf), spacingxy, 
                                      hgts_path, "", os.path.join(output_dir, interf_ortho), ram)

        # Set WGS 84 as projection to ortho interferogram
        func_utils.add_WGSProjection(os.path.join(output_dir, interf_ortho))
        
    func_utils.printOnStd("\n End of DiapOTB processing (S1 IW mode) \n")
    func_utils.log(logging.INFO, "############ End of DiapOTB processing  (S1 IW mode) ##############")
