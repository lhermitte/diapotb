#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2017 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" 
    SAR_MultiSlc_IW.py
    ==================

    Interferometry chain between multitemporal SAR images for S1 IW products. 
    One master image is fixed by users and other images are processed as slave image.
 
"""

__author__ = "Maxime Azzoni"
__version__ = "0.1"
__status__ = "Developpement"
__date__ = "05/02/2020"
__last_modified__ = "05/02/2020"

# ==================================== #
 #              Imports              # 
  # ================================ #

import logging
import os
import argparse
import datetime
import gdal
import tempfile
import shutil
import errno

from processings import Pre_Processing
from processings import Ground
from processings import DInSar
from processings import Post_Processing

import utils.DiapOTB_applications as diapOTBApp
from utils import func_utils

#import psutil
#mem_state = dict(psutil.virtual_memory()._asdict())


# ==================================== #
 #                Main                # 
  # ================================ #

if __name__ == "__main__":

    # ====== Check arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("configfile",
                        help="""input conguration file for the
                        application DiapOTB""")
    args = parser.parse_args()

    print(args.configfile)
    
    func_utils.init_logger()

    dataConfig = func_utils.load_configfile(args.configfile, "multi_SW")
                
    # ====================================
    # Get elements from configuration file
    # ====================================
    
    try :
        # ====== Get dictionaries
        dict_Global = dataConfig['Global']
        dict_PreProcessing = dataConfig['Pre_Processing']
        dict_Ground = dataConfig['Ground']
        dict_DInSAR = dataConfig['DIn_SAR']
        dict_PostProcessing = dataConfig['Post_Processing']

        # ====== Get elements from dictionaries
        # ====== Global
        srtm_shapefile = dict_Global['in']['SRTM_Shapefile']
        hgts_path = dict_Global['in']['SRTM_Path']
        geoid_path = dict_Global['in']['Geoid']

        eof_Path = None
        if 'EOF_Path' in dict_Global['in']:
            eof_Path = dict_Global['in']['EOF_Path']

        output_dir = dict_Global['out']['Output_Path']

        if not os.path.exists(output_dir):
            print("The output directory does not exist and will be created")
            os.makedirs(output_dir)
        else :
            print("The output directory exists. Some files can be overwritten")

        # ====== Set the variables names
        iso_start, iso_end = func_utils.argDates_to_isoDates(dict_Global['in']['Start_Date'], dict_Global['in']['End_Date'])
        start_time = int(dict_Global['in']['Start_Date'])
        end_time = int(dict_Global['in']['End_Date'])
        master_Image_base = dict_Global['in']['Master_Image']

        func_utils.check_ifExist(dict_Global['in']['Input_Path'])

        master_Image = func_utils.get_imgFromSAFE(dict_Global['in']['Master_Image'], dict_Global['in']['Input_Path'])

        if not master_Image :
            print(master_Image_base + " not found into given input path " + \
                  dict_Global['in']['Input_Path'])
            print("Please check your input path")
            quit()
        else :
            correct = func_utils.check_image_pattern(master_Image_base, mode="S1IW")
            if not correct : 
                print("Master image " + master_Image_base + " does not respect naming conventions for S1IW")
                quit()

        master_date = master_Image_base.split("-")[4].split("t")[0]
        pol = master_Image_base.split("-")[3]
        iw = master_Image_base.split("-")[1]
        burst_index = "0-8"
        ram = str(4000)
        light_version = "yes"
        tmpdir_into_outputdir = "no"
        if 'parameter' in dict_Global: 
            if 'optram' in dict_Global['parameter']:
                ram =  str(dict_Global['parameter']['optram'])
                if int(ram) > 4000 :
                    ram = str(4000)
            if 'clean' in dict_Global['parameter']:
                light_version = dict_Global['parameter']['clean']
            if 'burst_index' in dict_Global['parameter']:
                burst_index = dict_Global['parameter']['burst_index']
            if 'tmpdir_into_outputdir' in dict_Global['parameter']:
                tmpdir_into_outputdir = dict_Global['parameter']['tmpdir_into_outputdir']
        light_version = func_utils.str2bool(light_version)
        tmpdir_into_outputdir = func_utils.str2bool(tmpdir_into_outputdir)
        print(burst_index)###############################################################################
        exclude = "-9999"
        if 'Exclude' in dict_Global['in']:
            exclude = dict_Global['in']['Exclude']
        manifest = master_Image.split("measurement")[0]+"/manifest.safe"
        relative_orbit = func_utils.get_relative_orbit(manifest)

        # ====== Pre_Processing
        rng = 3
        azi = 3
        if "ML_ran" in dict_PreProcessing['parameter']:
            rng = dict_PreProcessing['parameter'].get('ML_ran')
        if "ML_azi" in dict_PreProcessing['parameter']:
            azi = dict_PreProcessing['parameter'].get('ML_azi')
        ml_range = int(rng)
        ml_azimut = int(azi)
        ml_gain = dict_PreProcessing['parameter']['ML_gain']
        dop_file = dict_PreProcessing['out']['doppler_file']

        # ====== Ground

        # ====== DIn_SAR
        version_interferogram = "true"
        if "Activate_Interferogram" in dict_DInSAR['parameter']:
            version_interferogram = dict_DInSAR['parameter']['Activate_Interferogram']

        roi = None
        if 'roi' in dict_DInSAR['parameter']:
            roi = dict_DInSAR['parameter']['roi']

        geoGrid_gridstep_range = dict_DInSAR['parameter']['GridStep_range']
        geoGrid_gridstep_azimut = dict_DInSAR['parameter']['GridStep_azimut']
        geoGrid_threshold = dict_DInSAR['parameter']['Grid_Threshold']
        geoGrid_gap = dict_DInSAR['parameter']['Grid_Gap']
        ml_geoGrid_range = int(rng)
        ml_geoGrid_azimut = int(azi)
        gain_interfero = dict_DInSAR['parameter']['Interferogram_gain']
        # esd loop 
        esd_AutoMode = False # automatic mode to apply a threshold inside the esd loop
        esd_NbIter = 0

        if 'ESD_iter' in dict_DInSAR['parameter']:
            esd_NbIter = dict_DInSAR['parameter']['ESD_iter']
            if not isinstance(esd_NbIter, int) :
                esd_AutoMode = True
                esd_NbIter = 10 # 10 iterations maximum for automatic mode
        else :
            esd_AutoMode = True
            esd_NbIter = 10 # 10 iterations maximum for automatic mode


        # ====== Post_Processing
        spacingxy = "0.0001"
        if "Spacingxy" in dict_PostProcessing['parameter']:
            spacingxy = str(dict_PostProcessing['parameter']['Spacingxy'])

        ortho_interferogram = "false"
        if 'Activate_Ortho' in dict_PostProcessing['parameter']:
            ortho_interferogram = dict_PostProcessing['parameter']['Activate_Ortho']
        if roi:
            ortho_interferogram = "yes"
            print("ortho_interferogram", ortho_interferogram)

        activateFiltering = "false"
        if "Activate_Filtering" in dict_PostProcessing['parameter'] :
            activateFiltering = dict_PostProcessing['parameter']['Activate_Filtering']

        ml_interf_filt_range = 3
        if "Filtered_Interferogram_mlran" in dict_PostProcessing['parameter'] :
            ml_interf_filt_range = int(dict_PostProcessing['parameter']['Filtered_Interferogram_mlran'])

        ml_interf_filt_azimut = 3
        if "Filtered_Interferogram_mlazi" in dict_PostProcessing['parameter'] :
            ml_interf_filt_azimut = int(dict_PostProcessing['parameter']['Filtered_Interferogram_mlazi'])

        interf_filt_alpha = 0.7
        if "Filtering_parameter_alpha" in dict_PostProcessing['parameter'] :
            interf_filt_alpha = dict_PostProcessing['parameter']['Filtering_parameter_alpha']

    except KeyError as e :
        # Indicate a incohenrency between expected keys and input json file (jsson schemas can be used)
        response = "Exception Key error  : \n " \
                   "Error into keys correspondance : json file does not contain all required keys. \n " \
                   "You can check your input configuration file  with available schemas " \
                   "(into json_schemas repository) and by setting a environnement variable (DIAPOTB_INSTALL) " \
                   "to your installation before relaunching."

        print(response)

        print("For your information, the missing key is : " + str(e))
        quit()

    except Exception as e :
        print("Exception into input variable handlings : " + str(e))
        quit()



    # ====== Check if interferogram is activated for ortho interferogram
    if func_utils.str2bool(ortho_interferogram) and not func_utils.str2bool(version_interferogram):
        func_utils.log(logging.CRITICAL, "Error, Impossible to have ortho interferogram without interferogram\n"
        "If Ortho is required, please activate interferogram estimation in your json file")
        quit()

    # ====== Check if interferogram is activated for filtering
    if func_utils.str2bool(activateFiltering) and not func_utils.str2bool(version_interferogram) :
        func_utils.log(logging.CRITICAL, "Error, Impossible to have filering without interferogram\n"
        "If Filtering is required, please activate interferogram estimation in your json file")
        quit()

    # ====== Check Threshold
    if (geoGrid_threshold < 0) or (geoGrid_threshold > 1) :
        func_utils.log(logging.CRITICAL, "Error, Wrong Threshold for fine deformation grid")
        geoGrid_threshold = 0.3
        
    # ====== Check if images exist
    func_utils.check_ifExist(srtm_shapefile)
    func_utils.check_ifExist(hgts_path)
    func_utils.check_ifExist(master_Image)
    
    # Check eof path 
    if eof_Path :
        if not os.path.exists(eof_Path) :
            func_utils.log(logging.CRITICAL, "Error, {path} does not exist. Check its path.".format(path=eof_Path))
            quit()

    # ====== Check roi format (if roi)
    if roi :
        func_utils.check_roiFormat(roi)
    
    # ====== Check regex for Burst index
    firstBurst, lastBurst, burstList = func_utils.check_burstIndex(burst_index)
    print("from check regex",firstBurst, lastBurst)###########################################################
    
    # ====== Create global folder with starting and ending dates + master date
    output_glob = "{}/output_{}_to_{}_m_{}".format(output_dir, start_time, 
                                                   end_time, master_date)
    if os.path.exists(output_glob):
        shutil.rmtree(output_glob)
    if not os.path.exists(output_glob):
        os.makedirs(output_glob)
        
    # ====== Create Digital Elevation Model
    dem, target_dir = func_utils.build_virutal_raster(master_Image, start_time, end_time,
                                                      master_date, srtm_shapefile, hgts_path,
                                                      output_dir)
    print("\n Removing master_image_envelope shp files...\n")
    for i in os.listdir(target_dir):
        if i.startswith("master_envelope"):
            os.remove(target_dir + "/" + i)

    
    # Init file handler (all normaly print on std is redirected into info.log) 
    # To use previous print on std, use printOnStd
    func_utils.init_fileLog(output_glob) 
    
    # Recap of input parameter into info.log
    func_utils.log(logging.INFO, "########### Input Parameters for the current execution ############## ")
    func_utils.log(logging.INFO, " Global : ")
    func_utils.log(logging.INFO, "Input_Path :  {param}".format(param=dict_Global['in']['Input_Path']))
    func_utils.log(logging.INFO, "Master_Image : {param}".format(param=master_Image_base))
    func_utils.log(logging.INFO, "Start_Date : {param}".format(param=start_time))
    func_utils.log(logging.INFO, "End_Date : {param}".format(param=end_time))
    func_utils.log(logging.INFO, "srtm_shapefile : {param}".format(param=srtm_shapefile))
    func_utils.log(logging.INFO, "hgts_path : {param}".format(param=hgts_path))
    func_utils.log(logging.INFO, "geoid_path : {param}".format(param=geoid_path))
    func_utils.log(logging.INFO, "optram : {param}".format(param=ram))
    func_utils.log(logging.INFO, "clean : {param}".format(param=light_version))
    func_utils.log(logging.INFO, "burst_index : {param}".format(param=burst_index))
    func_utils.log(logging.INFO, "Exclude : {param}".format(param=exclude))

    func_utils.log(logging.INFO, " Pre_Processing : ")
    func_utils.log(logging.INFO, "ml_range : {param}".format(param=ml_range))
    func_utils.log(logging.INFO, "ml_azimut : {param}".format(param=ml_azimut))
    func_utils.log(logging.INFO, "ml_gain : {param}".format(param=ml_gain))

    # DIn_SAR
    func_utils.log(logging.INFO, " DIn_SAR : ")
    func_utils.log(logging.INFO, "geoGrid_gridstep_range : {param}".format(param=geoGrid_gridstep_range))
    func_utils.log(logging.INFO, "geoGrid_gridstep_azimut : {param}".format(param=geoGrid_gridstep_azimut))
    func_utils.log(logging.INFO, "geoGrid_threshold : {param}".format(param=geoGrid_threshold))
    func_utils.log(logging.INFO, "geoGrid_gap : {param}".format(param=geoGrid_gap))
    func_utils.log(logging.INFO, "ml_geoGrid_range : {param}".format(param=ml_geoGrid_range))
    func_utils.log(logging.INFO, "ml_geoGrid_azimut : {param}".format(param=ml_geoGrid_azimut))
    func_utils.log(logging.INFO, "gain_interfero : {param}".format(param=gain_interfero))
    func_utils.log(logging.INFO, "Activate_Interferogram : {param}".format(param=version_interferogram))
    func_utils.log(logging.INFO, "roi : {param}".format(param=roi))
    func_utils.log(logging.INFO, "esd_NbIter : {param}".format(param=esd_NbIter))
    func_utils.log(logging.INFO, "esd_AutoMode : {param}".format(param=esd_AutoMode))    

    # Post_Processing
    func_utils.log(logging.INFO, " Post_Processing : ")
    func_utils.log(logging.INFO, "Activate_Ortho : {param}".format(param=ortho_interferogram))
    if func_utils.str2bool(ortho_interferogram):
        func_utils.log(logging.INFO, "spacingxy : {param}".format(param=spacingxy))
    func_utils.log(logging.INFO, "activateFiltering : {param}".format(param=activateFiltering))
    if func_utils.str2bool(activateFiltering):
        func_utils.log(logging.INFO, "ml_interf_filt_range : {param}".format(param=ml_interf_filt_range))
        func_utils.log(logging.INFO, "ml_interf_filt_azimut : {param}".format(param=ml_interf_filt_azimut))
        func_utils.log(logging.INFO, "interf_filt_alpha : {param}".format(param=interf_filt_alpha))


    # ============================
    # Get the elements from os.dirs
    # ============================

    # ====== Get the list of GTiff corresponding to dates and patterns
    tiff_list, throw_warning = func_utils.get_AllTiff(pol, iw, searchDir=dict_Global["in"]["Input_Path"])
    
    # Throw a warning
    if throw_warning :
        func_utils.log(logging.WARNING, "WARNING : At least one of selected images into your input path does not match the expected pattern"+ "\n")
    
    tiff_dates = func_utils.get_Tiff_WithDates(start_time, end_time, exclude, tiff_list)
    tiff_dates = func_utils.avoidDuplicates(tiff_dates)

    
    if len(tiff_dates)-1 <= 0 :
        func_utils.log(logging.CRITICAL, "ERROR : None secondary images found, please check your input path and your selection (dates, exclude ...)"+ "\n")
        quit()

    if throw_warning :
        # Indicate reference and all secondary images
        func_utils.printOnStd("Reference and Secondary images : ")
        func_utils.printOnStd(tiff_dates)


    counter = 0
    for i in (i for i in tiff_dates if i != master_Image_base):
        total_slaves = len(tiff_dates)-1
        slave_Image_base = i
        slave_Image = func_utils.get_imgFromSAFE(slave_Image_base, searchDir=dict_Global["in"]["Input_Path"])
        slave_date = func_utils.get_Date(i)
        counter += 1
        output_dir = output_glob + "/{}_m_{}_s".format(master_date, slave_date)
        if os.path.exists(output_dir):
            shutil.rmtree(output_dir)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        master_data_dir = output_glob + "/{}_master_directory".format(master_date)
        if not os.path.exists(master_data_dir):
            os.makedirs(master_data_dir)


        # ==============================================
        # Retrieving informations about master and slave
        # ==============================================    
        dictKWLMaster = func_utils.getImageKWL(master_Image)
        dictKWLSlave = func_utils.getImageKWL(slave_Image)


        # Find eof files for each image if not cosmo
        # Then, create the "fine" geom (with precise orbits) 
        # Eventually, assign an extended filename if EOF file correspond to the image
        if eof_Path :
            list_ofEOF = func_utils.get_AllFilesWithExt(eof_Path, ".EOF")


            # master 
            if counter <= 1 :
                start_master = dictKWLMaster['support_data.first_line_time']
                end_master = dictKWLMaster['support_data.last_line_time']

                # Get a eof file 
                eof_file = func_utils.select_EofWithDate(start_master, end_master, list_ofEOF)

                if (eof_file) :
                    # Create the new geom file into dedicated repository
                    extendedGeom_Path = os.path.join(master_data_dir, "extended_geom")
                    if not os.path.exists(extendedGeom_Path):
                        os.makedirs(extendedGeom_Path)

                    # Call SARMetadataCorrection
                    diapOTBApp.metadataCorrection(mode="orbits", insar=master_Image, indem=None, 
                                                  infineorbits=os.path.join(eof_Path, eof_file),
                                                  outPath=os.path.join(extendedGeom_Path, 
                                                                       "extended_master.geom"),
                                                  ram=ram)


                    # Assign new geom file with extended filename
                    master_Image += "?geom=" + os.path.join(extendedGeom_Path, 
                                                            "extended_master.geom")


            # slave 
            start_slave = dictKWLSlave['support_data.first_line_time']
            end_slave = dictKWLSlave['support_data.last_line_time']

            # Get a eof file 
            eof_file = func_utils.select_EofWithDate(start_slave, end_slave, list_ofEOF)

            if (eof_file) :
                # Create the new geom file into dedicated repository
                extendedGeom_Path = os.path.join(output_dir, "extended_geom")
                if not os.path.exists(extendedGeom_Path):
                    os.makedirs(extendedGeom_Path)

                # Call SARMetadataCorrection
                diapOTBApp.metadataCorrection(mode="orbits", insar=slave_Image, indem=None, 
                                              infineorbits=os.path.join(eof_Path, eof_file),
                                              outPath=os.path.join(extendedGeom_Path, 
                                              "extended_slave.geom"),
                                              ram=ram)

                # Assign new geom file with extended filename
                slave_Image += "?geom=" + os.path.join(extendedGeom_Path, 
                                                       "extended_slave.geom")




        func_utils.log(logging.INFO, "########### Input Images for the current execution ############## ")
        func_utils.log(logging.INFO, "Nb iteration : {param} with : ".format(param=counter))
        func_utils.log(logging.INFO, "master_Image : {param}".format(param=master_Image))
        func_utils.log(logging.INFO, "slave_Image : {param}".format(param=slave_Image))

        func_utils.printOnStd("\n Nb iteration : {param} with : \n".format(param=counter))
        func_utils.printOnStd("\n master_Image : {param} : \n".format(param=master_Image))
        func_utils.printOnStd("\n slave_Image : {param} : \n".format(param=slave_Image))


        # ===== Check header version
        if int(dictKWLMaster['header.version']) < 3 or int(dictKWLSlave['header.version']) < 3 :
            func_utils.log(logging.CRITICAL, "Error, Upgrade your geom file")
            quit()

        # ===== Check the index of bursts
        minNbBurst = min([int(dictKWLMaster['support_data.geom.bursts.number']),
                          int(dictKWLSlave['support_data.geom.bursts.number'])])

        if minNbBurst < firstBurst or minNbBurst < lastBurst:
            func_utils.log(logging.CRITICAL, "Error, Wrong burst index (superior to number of bursts)")
            quit()

        # ===== Selection of bursts
        keyBurst = "support_data.geom.bursts.burst[" + str(0) + "].azimuth_anx_time" 

        # ===== Check the index of bursts
        minNbBurst = min([int(dictKWLMaster['support_data.geom.bursts.number']), int(dictKWLSlave['support_data.geom.bursts.number'])])
        print("minNbBurst", minNbBurst)##############################################################################

        firstBurst = 0
        lastBurst = minNbBurst
        burstIndexOk = True
        print("from check the index", firstBurst, lastBurst)

        try:
            if len(burstList) == 2 :
                        firstBurst = min(burstList)
                        lastBurst = max(burstList)


        except Exception as err:
            func_utils.log(logging.CRITICAL, "Error, Wrong burst index")
            quit()

        if minNbBurst < firstBurst or minNbBurst < lastBurst or lastBurst < 0 or firstBurst < 0 :
            func_utils.log(logging.CRITICAL, "Error, Wrong burst index")
            quit()

        validBurstMaster = [] 
        validBurstSlave = []
        nbBurstSlave = int(dictKWLSlave['support_data.geom.bursts.number'])
        validBurstMaster, validBurstSlave = func_utils.selectBurst(dictKWLMaster, dictKWLSlave, firstBurst, lastBurst, nbBurstSlave)

        if len(validBurstMaster) == 0 or len(validBurstSlave) == 0 :
            func_utils.log(logging.CRITICAL, "Error, Wrong burst index (slave index does not match with master index)")
            quit()

        # ===== Update firstBurst and lastBurst with selected Burst for master image
        firstBurst = validBurstMaster[0]
        lastBurst = validBurstMaster[len(validBurstMaster)-1]
        # ===== Create directory for each burst
        temp_dir = ""
        if tmpdir_into_outputdir :
            temp_dir = tempfile.mkdtemp(dir=output_dir)
        else :
            temp_dir = tempfile.mkdtemp()

        for burstId in range(firstBurst, lastBurst+1):
            os.makedirs(os.path.join(temp_dir, "burst" + str(burstId)))

        # #################################################################### #
        # ###################### Pre Processing Chain ######################## #
        # #################################################################### #
        # Initialisation of doppler file 
        dopFile = open(os.path.join(output_dir, dop_file), "w")
        dopFile.close()

        # Master
        if counter <= 1:
            func_utils.printOnStd("\n Master Pre_Processing chain \n")
            func_utils.log(logging.INFO, "Master Pre_Processing Application")
            
            Master_temp_dir = ""
            if tmpdir_into_outputdir :
                Master_temp_dir = tempfile.mkdtemp(dir=master_data_dir)
            else :
                Master_temp_dir = tempfile.mkdtemp()
            
            for id_loop in range(0, len(validBurstMaster)):
                burstId = validBurstMaster[id_loop]
                os.makedirs(os.path.join(Master_temp_dir, "burst" + str(burstId)))

            paramPreMaster = {}
            paramPreMaster['ml_range'] = ml_range
            paramPreMaster['ml_azimut'] = ml_azimut
            paramPreMaster['ml_gain'] = ml_gain
            paramPreMaster['dop_file'] = dop_file
            paramPreMaster['validBurstMaster'] = validBurstMaster
            paramPreMaster['ram'] = ram

            dop0Master, BurstToConcatenateM, Deramp_BurstToConcatenateM = Pre_Processing.extractToMultilook(master_Image, master_Image_base, paramPreMaster, "S1_IW", Master_temp_dir)


            # ###### Concatenating and georeferencing bursts ###### #
            # Master SLC
            masterSlc = func_utils.getSlcMlNamming_fromProductName(master_Image_base, mode="S1IW") + ".tif"
            Con_masterSlc = os.path.join(master_data_dir, masterSlc)
            diapOTBApp.concatenate(BurstToConcatenateM, master_Image, firstBurst, 
                                   Con_masterSlc, ram)


            # Master deramp
            if light_version is not True:
                masterDeramp = os.path.splitext(master_Image_base)[0]+"_deramp.tif"
                Con_masterDeramp = os.path.join(master_data_dir, masterDeramp)
                diapOTBApp.concatenate(Deramp_BurstToConcatenateM, master_Image, firstBurst, 
                                       Con_masterDeramp, ram)

        # Slave
        func_utils.printOnStd("\n Slave Pre_Processing chain \n")
        func_utils.log(logging.INFO, "Slave Pre_Processing Application")

        paramPreSlave = {}
        paramPreSlave['ml_range'] = ml_range
        paramPreSlave['ml_azimut'] = ml_azimut
        paramPreSlave['ml_gain'] = ml_gain
        paramPreSlave['dop_file'] = dop_file
        paramPreSlave['validBurstMaster'] = validBurstMaster
        paramPreSlave['validBurstSlave'] = validBurstSlave
        paramPreSlave['ram'] = ram

        dop0Slave, BurstToConcatenateS, Deramp_BurstToConcatenateS = Pre_Processing.extractToMultilook(slave_Image, slave_Image_base, paramPreSlave, "S1_IW", temp_dir)

        # ###### Concatenating and georeferencing bursts ###### #
        # Slave Deramp
        if light_version is not True:
            Con_slaveDeramp = os.path.splitext(master_Image_base)[0] + "_deramp.tif"
            diapOTBApp.concatenate(Deramp_BurstToConcatenateS, slave_Image, firstBurst, Con_slaveDeramp)

        # ##################################################################### #
        # ######################## Ground Chain ############################### #
        # ##################################################################### #
        # Master
        func_utils.printOnStd("\n Master Ground chain \n")
        func_utils.log(logging.INFO, "Master Ground Application")
        if counter <=1:
            paramGroundMaster = {}
            paramGroundMaster['nodata'] = -32768
            paramGroundMaster['withxyz'] = "true"
            paramGroundMaster['validBurstMaster'] = validBurstMaster
            paramGroundMaster['ram'] = ram

            Ground.demProjectionAndCartesianEstimation(master_Image, master_Image_base, dem, paramGroundMaster, 
                                                       "S1_IW", Master_temp_dir)


        # Slave
        func_utils.printOnStd("\n Slave Ground chain \n")
        func_utils.log(logging.INFO, "Slave Ground Application")

        paramGroundSlave = {}
        paramGroundSlave['nodata'] = -32768
        paramGroundSlave['withxyz'] = "true"
        paramGroundSlave['validBurstMaster'] = validBurstMaster
        paramGroundSlave['validBurstSlave'] = validBurstSlave
        paramGroundSlave['ram'] = ram

        Ground.demProjectionAndCartesianEstimation(slave_Image, slave_Image_base, dem, paramGroundSlave, 
                                                   "S1_IW", temp_dir)


        # ##################################################################### #
        # ####################### DIn_SAR Chain ############################### #
        # ##################################################################### #
        func_utils.printOnStd("\n DIn_SAR chain \n")
        func_utils.log(logging.INFO, "DIn_SAR chain")

        # Create param
        param = {}
        param['ml_azimut'] = ml_azimut
        param['ml_range'] = ml_range
        param['validBurstMaster'] = validBurstMaster
        param['validBurstSlave'] = validBurstSlave
        param['ml_geoGrid_azimut'] = ml_geoGrid_azimut
        param['ml_geoGrid_range'] = ml_geoGrid_range
        param['geoGrid_gridstep_range'] = geoGrid_gridstep_range
        param['geoGrid_gridstep_azimut'] = geoGrid_gridstep_azimut
        param['geoGrid_threshold'] = geoGrid_threshold
        param['geoGrid_gap'] = geoGrid_gap
        param['doppler0'] = dop0Slave
        param['gain_interfero'] = gain_interfero
        param['advantage'] = "projection" # By default projection
        param['esd_NbIter'] = esd_NbIter
        param['esd_AutoMode']  = esd_AutoMode
        param['with_interferogram']  = version_interferogram
        param['with_concatenate']  = "no"
        param['ram'] = ram

        list_of_Grids, interfToConcatenate, slaveCorRerampToConcatenate, masterRerampToConcatenate = DInSar.gridToInterferogram(dem, master_Image, master_Image_base, slave_Image, slave_Image_base, Master_temp_dir, temp_dir, param, 'S1_IW', output_dir)

        # ###### Concatenating and georeferencing bursts ###### #
        # Slave Coregistrated Reramped
        slaveCorRE = func_utils.getSlcMlNamming_fromProductName(slave_Image_base, mode="S1IW") + ".tif"
        Con_slaveCorRE = os.path.join(output_dir, slaveCorRE)
        diapOTBApp.concatenate(slaveCorRerampToConcatenate, master_Image, firstBurst, 
                               Con_slaveCorRE, ram)

        masterRerampMl = os.path.splitext(master_Image_base)[0] + "_ml" + str(ml_azimut) + str(ml_range) + "Master.tif"
        Con_masterReramp = os.path.join(master_data_dir, masterRerampMl)
        diapOTBApp.concatenate(masterRerampToConcatenate, master_Image, firstBurst, 
                               Con_masterReramp, ram)


        # ==================================== #
         #        Post Processing Chain       # 
          #                                  #
           # ============================== #

        func_utils.printOnStd("\n Post Processing chain \n")
        func_utils.log(logging.INFO, "Post Processing chain")

        # Multilook on coregistred
        CoRe_ML = func_utils.getSlcMlNamming_fromProductName(slave_Image_base, mode="S1IW") + \
                  "_ml" + str(ml_azimut) + str(ml_range) + ".tif"
        diapOTBApp.multilook(os.path.join(output_dir, slaveCorRE), ml_range, ml_azimut, 
                             ml_gain, os.path.join(output_dir, CoRe_ML), ram)


        # A === Interferogram
        ### Names definition ###
        mib = master_Image_base
        sib = slave_Image_base

        if func_utils.str2bool(version_interferogram):
            Interferogram_Multiband = mib[:15]+"M"+mib[14:30]+"-"+mib[
                    47:54]+"S"+sib[14:23]+"-"+sib[47:53]+"-"+ str(relative_orbit)+"_interf.tif"
        
            interf_base = func_utils.getInterfNamming_fromProductName(master_Image_base, slave_Image_base,
                                                                      mode="S1IW")

            Interferogram_ = interf_base +"_Interferogram.tif"

            Interfo_Filt = interf_base + "_Filtred-Interferogram.tif"

            Con_Interf = os.path.join(output_dir, Interferogram_Multiband)
            diapOTBApp.concatenate(interfToConcatenate, master_Image, firstBurst, Con_Interf, ram)

            # B === Extraction of band 1,2,3
            ds = gdal.Open(os.path.join(output_dir, Interferogram_Multiband), gdal.GA_ReadOnly)
            ds = gdal.Translate(os.path.join(output_dir, Interferogram_), ds, bandList = ["1","2","3"])
            
            # Phase Filtering (if required)
            if func_utils.str2bool(activateFiltering):
                # Default paramater 
                step = 16
                sizetiles = 64

                paramPost = {}
                paramPost['ml_filt_interf_range'] = ml_interf_filt_range
                paramPost['ml_filt_interf_azimut'] = ml_interf_filt_azimut
                paramPost['filt_alpha'] = interf_filt_alpha
                paramPost['interf_Name'] = Con_Interf
                paramPost['ram'] = ram
                paramPost['filtered_Name'] = Interfo_Filt

                Post_Processing.filtering(master_Image, master_Image_base, slave_Image, 
                                          slave_Image_base, Master_temp_dir, temp_dir, paramPost, 
                                          "S1_IW", output_dir)

            os.rename(os.path.join(output_dir, Interferogram_Multiband.split(".")[0]+".geom"), 
                      os.path.join(output_dir, Interferogram_.split(".")[0]+".geom"))

        # C === Ortho Interferogram
        if func_utils.str2bool(version_interferogram) and func_utils.str2bool(ortho_interferogram):            
            Ortho_  = interf_base + "_Ortho-Interferogram.tif"
            Ortho_roi  = interf_base + "_ROI_Ortho-Interferogram.tif"

            diapOTBApp.orthorectification(os.path.join(output_dir, Interferogram_), 
                                          spacingxy, hgts_path, geoid_path, 
                                          os.path.join(output_dir, Ortho_), ram)

            # Set WGS 84 as projection to ortho interferogram
            func_utils.add_WGSProjection(os.path.join(output_dir, Ortho_))

            if roi:
                func_utils.extract_roi(os.path.join(output_dir, Ortho_), os.path.join(output_dir, Ortho_roi), roi)
                func_utils.silentremove(output_dir, Ortho_)


        # ==================================== #
         #               Remove               # 
          # ================================ #
        func_utils.silentremove(output_dir, dop_file)
        if func_utils.str2bool(version_interferogram):
            func_utils.silentremove(output_dir, Interferogram_Multiband)
        shutil.rmtree(temp_dir)


    # Multilook on master reramped
    Reramp_ML = func_utils.getSlcMlNamming_fromProductName(master_Image_base, mode="S1IW") + \
                "_ml" + str(ml_azimut) + str(ml_range) + ".tif"
    diapOTBApp.multilook(os.path.join(master_data_dir, masterRerampMl), ml_range, ml_azimut, 
                         ml_gain, os.path.join(master_data_dir, Reramp_ML), ram)

    func_utils.silentremove(master_data_dir, masterRerampMl)
    func_utils.silentremove(master_data_dir, masterRerampMl.split(".")[0]+".geom")
    shutil.rmtree(Master_temp_dir)
