#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" 
    generateConfigFile.py
    =====================

    Python script to generate configuration file (json format) with default parameters and user's paths
 
"""

import os
import sys
import re
import json
import readline, glob

import func_utils

# Util fct for autocompletion
def complete(text, state):
    """ 
    Function for autocompletion
    """
    return (glob.glob(text+'*')+[None])[state]

# Print with colors
def prRed(skk): print("\033[91m {}\033[00m" .format(skk)) 
def prGreen(skk): print("\033[92m {}\033[00m" .format(skk)) 
def prYellow(skk): print("\033[93m {}\033[00m" .format(skk)) 
def prLightPurple(skk): print("\033[94m {}\033[00m" .format(skk)) 
def prPurple(skk): print("\033[95m {}\033[00m" .format(skk)) 
def prCyan(skk): print("\033[96m {}\033[00m" .format(skk)) 
def prLightGray(skk): print("\033[97m {}\033[00m" .format(skk)) 
def prBlack(skk): print("\033[98m {}\033[00m" .format(skk)) 

# Input with colors
def genericInput(skk, color) :
    """ 
    Override input function for colors and exceptions
    """
    try:
        return input(color .format(skk)) 
    except KeyboardInterrupt :
        prRed("Generation Aborted, Ctrl-C detected")
        sys.exit(1)
    except :
        prRed("Generation Aborted")
        sys.exit(1)

def inRed(skk): return genericInput(skk, "\033[91m {}\033[00m")
def inGreen(skk): return genericInput(skk, "\033[92m {}\033[00m") 
def inYellow(skk): return genericInput(skk, "\033[93m {}\033[00m") 
def inLightPurple(skk): return genericInput(skk, "\033[94m {}\033[00m") 
def inPurple(skk): return genericInput(skk, "\033[95m {}\033[00m") 
def inCyan(skk): return genericInput(skk, "\033[96m {}\033[00m") 
def inLightGray(skk): return genericInput(skk, "\033[97m {}\033[00m") 
def inBlack(skk): return genericInput(skk, "\033[98m {}\033[00m") 


# Questions to user for SAR_MultiSlc* chains
def askForMultiSlc(dataConfig) :
    """ 
    Q&A for SAR_MultiSlc* chains
    Modify the dataConfig following user's awnsers 
    """

    # Select sensor if SAR_MultiSlc
    sensor = "S1IW"
    if (response == "SAR_MultiSlc") :
        sensor = inLightPurple("Please, select the wanted sensor S1SM " \
                       "(for Sentinel-1 StripMap mode) or Cosmo " \
                       "(for Cosmo-Skymed Spotligth and StriMap mode) : " )

        if sensor not in ["S1SM", "Cosmo"] :
            prRed("Unknown sensor, please choose between S1SM or Cosmo")
            quit()

    # SRTM_Shapefile
    SRTM_Shapefile = os.path.realpath(inPurple("Please, enter your path to srtm shp : " ))
    func_utils.check_ifExist(SRTM_Shapefile)

    # SRTM_Path
    SRTM_Path = os.path.realpath(inPurple("Please, enter your path to srtm hgt files : " ))
    func_utils.check_ifDir(SRTM_Path)

    # Input/Output Paths
    Input_Path = os.path.realpath(inPurple("Please, enter your path to input images : " ))
    func_utils.check_ifDir(Input_Path)

    Output_Path = os.path.realpath(inPurple("Where would you like to store the output results : " ))
    #func_utils.check_ifDir(os.path.dirname(Output_Path))
    func_utils.check_ifDir(Output_Path)

    # reference image (must be into Input_Path)
    reference_image = inPurple("Which image is your reference : " )
    func_utils.check_ifExist(reference_image)
    reference_image = os.path.basename(reference_image)

    if not func_utils.get_imgFromDir(reference_image, Input_Path) :
        prRed(reference_image + " not found into given input path " + \
              "Input_Path")
        prRed("Please check your input path")
        quit()
    else :
        correct = func_utils.check_image_pattern(reference_image, mode=sensor)
        if not correct : 
            prRed("Reference image " + reference_image + " does not respect naming conventions for the " \
                  "selected sensor")
            quit()

    # Geoid file
    res_geoid = inPurple("Would you like to add a geoid file (yes/no) : ")
    Geoid = None

    if res_geoid == "yes" :
        Geoid = os.path.realpath(inLightPurple("Please, enter your path to your geoid file : "))
        func_utils.check_ifExist(Geoid)
    else :
        Geoid = os.getenv('OTB_GEOID_FILE')
        if not Geoid :
            prRed("Undefined geoid (empty OTB_GEOID_FILE environnement variable)")
            inPurple("Please indicate a geoid file by setting path or with OTB_GEOID_FILE environnement variable")
            quit()
            
    for retry in range(1,6):
        # Start/End date for image selection (5 retries for this selection)
        res_date = inLightPurple("Would you like to specify a start and end date for image selection (yes/no) : ")

        # Dummy dates to select by default all images into Input_Path 
        start_date = "19000101"
        end_date = "29000101"
        pattern = "".join(['\d{8}'])
        if res_date == "yes" :
            start_date = inPurple("Please, indicate a start date with YYYYMMDD format : ")
            if not re.match(pattern, start_date) :
                prRed("start_date " + start_date + " does not respect the expected format YYYYMMDD")
                quit()

            end_date = inPurple("Please, indicate a end date with YYYYMMDD format : ")
            if not re.match(pattern, end_date) :
                prRed("end_date " + end_date + " does not respect the expected format YYYYMMDD")
                quit()

        # Indicate to user, all selected images with given dates, polarisation and input_path
        ext = "tiff"
        pol = ""
        iw = ""
        if sensor == "Cosmo" :
            ext = "h5"
            pol = reference_image.split("_")[5]
        else :
            pol = reference_image.split("-")[3]

        if sensor == "S1IW" :
            iw = reference_image.split("-")[1]
            ext = ""

        exclude = "-9999"
        tiff_list, throw_warning = func_utils.get_AllTiff(pol=pol, ext=ext, iw=iw, searchDir=Input_Path)
        tiff_dates = func_utils.get_Tiff_WithDates(int(start_date), int(end_date), exclude, tiff_list, ext)

        # Avoid duplicates 
        tiff_dates = func_utils.avoidDuplicates(tiff_dates)
        tiff_dates.remove(reference_image)

        prYellow("For your information, the selected images for processings will be : ")
        prYellow("As reference : " + reference_image)
        prYellow("As secondaries : " + str(tiff_dates))

        # Ask to continue if selection OK
        res_continue = inLightPurple("Do you agree to continue with this selection (yes/no/exit) : ")

        if res_continue == "exit" :
            prRed("You choose to exit, you can relaunch this script with new inputs")
            quit()

        if res_continue != "yes" :
            if retry < 5 :
                prRed("Previous selection does not fullfill your expectations, please select with different dates")
            else :
                prRed("Previous selection does not fullfill your expectations with too many retries.\n You can relaunch this script with new inputs")
                quit()
        else :
            break

    # EOF file
    EOF_Path = None
    if sensor != "Cosmo" :
        res_eof = inLightPurple("Would you like to indicate fine orbits (yes/no) : ")

        if res_eof == "yes" : 
            EOF_Path = os.path.realpath(inPurple("Please, enter your path to .EOF files : " ))
            func_utils.check_ifDir(EOF_Path)


    # Fill with user's response our generic fields for SAR_MultiSlc* chains
    dataConfig['Global']['in']['SRTM_Shapefile'] = SRTM_Shapefile
    dataConfig['Global']['in']['SRTM_Path'] = SRTM_Path
    dataConfig['Global']['in']['Input_Path'] = Input_Path
    dataConfig['Global']['in']['Master_Image'] = reference_image
    dataConfig['Global']['in']['Start_Date'] = start_date
    dataConfig['Global']['in']['End_Date'] = end_date
    if Geoid :
        dataConfig['Global']['in']['Geoid'] = Geoid
    else :
        del dataConfig['Global']['in']['Geoid']
    if EOF_Path :
        dataConfig['Global']['in']['EOF_Path'] = EOF_Path

    dataConfig['Global']['out']['Output_Path'] = Output_Path    


# Questions to user for diapOTB* chains
def askForDiapOTB(dataConfig) :
    """ 
    Q&A for diapOTB* chains
    Modify the dataConfig following user's awnsers 
    """

    # Select sensor if diapOTB
    sensor = "S1IW"
    if (response == "diapOTB") :
        sensor = inLightPurple("Please, select the wanted sensor S1SM " \
                       "(for Sentinel-1 StripMap mode) or Cosmo " \
                       "(for Cosmo-Skymed Spotligth and StriMap mode) : " )

        if sensor not in ["S1SM", "Cosmo"] :
            prRed("Unknown sensor, please choose between S1SM or Cosmo")
            quit()

        
    # reference image (path to image)
    reference_image = os.path.realpath(inPurple("Which image is your reference : " ))
    func_utils.check_ifExist(reference_image)
    reference_image_base = os.path.basename(reference_image)

    correct = func_utils.check_image_pattern(reference_image_base, mode=sensor)
    if not correct : 
        prRed("Reference image " + reference_image_base + " does not respect naming conventions for the " \
              "selected sensor")
        quit()

    # reference image (path to image)
    secondary_image = os.path.realpath(inPurple("Which image is secondary : " ))
    func_utils.check_ifExist(secondary_image)
    secondary_image_base = os.path.basename(secondary_image)

    correct = func_utils.check_image_pattern(secondary_image_base, mode=sensor)
    if not correct : 
        prRed("Reference image " + secondary_image_base + " does not respect naming conventions for the " \
              "selected sensor")
        quit()


    # DEM Path
    DEM_Path = os.path.realpath(inPurple("Please, enter your path to your DEM : "))
    func_utils.check_ifExist(DEM_Path)

    # Output Path
    Output_Path = os.path.realpath(inPurple("Where would you like to store the output results : " ))
    func_utils.check_ifDir(os.path.dirname(Output_Path))

    # EOF file
    EOF_Path = None
    if sensor != "Cosmo" :
        res_eof = inLightPurple("Would you like to indicate fine orbits (yes/no) : ")

        if res_eof == "yes" : 
            EOF_Path = os.path.realpath(inPurple("Please, enter your path to .EOF files : " ))
            func_utils.check_ifDir(EOF_Path)


    # Fill with user's response our generic fields for SAR_MultiSlc* chains
    dataConfig['Global']['in']['Master_Image_Path'] = reference_image
    dataConfig['Global']['in']['Slave_Image_Path'] = secondary_image
    dataConfig['Global']['in']['DEM_Path'] = DEM_Path
    if EOF_Path :
        dataConfig['Global']['in']['EOF_Path'] = EOF_Path

    dataConfig['Global']['out']['output_dir'] = Output_Path   



###################
###### Main #######
###################
if __name__ == "__main__":
    
    ######### Introduction prints #########
    prCyan("Welcome to DiapOTB remote module !")
    prCyan("You can generate configuration files for the four available processing chains : diapOTB," \
          "diapOTB_S1IW, SAR_MultiSlc and SAR_MultiSlc_IW")
    
    ######### Load the example for prepare the configuration file according to user's choice #########
    # First choice for user : the selected chain
    response = inLightGray("Please, choose your processing chain (diapOTB, diapOTB_S1IW, SAR_MultiSlc and " \
                     "SAR_MultiSlc_IW) : ")
    
    if response not in ['diapOTB', 'diapOTB_S1IW', 'SAR_MultiSlc', 'SAR_MultiSlc_IW'] : 
        prRed("Wrong chain, please choose between available chains")
        quit()
        
    
    # Load examples according to the selected chain => Init a dictionnary with default parameters
    current_path = os.path.dirname(os.path.realpath(__file__))
    ex_config_path = os.path.join(current_path, "../ex_config")
   
    dataConfig = {}
    if response == "diapOTB" :
        
        ex_confile = os.path.join(ex_config_path, "ex_config_diapOTB_Cosmo.json") 
        dataConfig = func_utils.load_configfile(ex_confile, "S1_SM")
    
    elif response == "diapOTB_S1IW" :

        ex_confile = os.path.join(ex_config_path, "ex_config_diapOTB_S1IW.json") 
        dataConfig = func_utils.load_configfile(ex_confile, "S1_IW")
    
    elif response == "SAR_MultiSlc" :

        ex_confile = os.path.join(ex_config_path, "ex_config_MultiSlc_CosmoS1SM.json")
        dataConfig = func_utils.load_configfile(ex_confile, "multi_S1")
    
    elif response == "SAR_MultiSlc_IW" :

        ex_confile = os.path.join(ex_config_path, "ex_config_MultiSlc_IW.json")
        dataConfig = func_utils.load_configfile(ex_confile, "multi_SW")



    ########## Prepare for questions : with autocompletion #########
    readline.set_completer_delims(' \t\n;')
    readline.parse_and_bind("tab: complete")
    readline.set_completer(complete)
    
    ########## Ask to user for generic fields #########
    # SAR_MultiSlc* chains
    if (response == "SAR_MultiSlc" or response == "SAR_MultiSlc_IW") : 
        try:
            askForMultiSlc(dataConfig)
        except :
            prRed("Generation Aborted")
            sys.exit(1)

    if (response == "diapOTB" or response == "diapOTB_S1IW") : 
        try : 
            askForDiapOTB(dataConfig)
        except :
            prRed("Generation Aborted")
            sys.exit(1)

    # Dump dataConfig with the new fields and the default parameters
    res_json = os.path.realpath(inLightGray("Where do you want store your configuration file : "))
    
    # if directory
    if os.path.isdir(res_json) :
        res_json_name = inLightGray("Please, enter a name for your configuration " \
                                    "file (with .json extension) : ")
        
        if os.path.exists(os.path.join(res_json, res_json_name)) :
            res_json_overwrite = inLightGray("Would you like to overwrite the file " + 
                                             res_json_name + " (yes/no) : ")
            
            if res_json_overwrite == "yes" :
                with open(os.path.join(res_json, res_json_name), 'w') as f:
                    json.dump(dataConfig, f, indent=2, sort_keys=False)
            else :
                prRed("Generation Aborted")
                quit()

        else :
            with open(os.path.join(res_json, res_json_name), 'w') as f:
                json.dump(dataConfig, f, indent=2, sort_keys=False)
                
    # If file (or wrong path)
    else :
        if os.path.isdir(os.path.dirname(res_json)) :
            
            if os.path.exists(res_json) :
                res_json_overwrite = inLightGray("Would you like to overwrite the file " + 
                                                 res_json + " (yes/no) : ")
            
                if res_json_overwrite == "yes":
                    with open(res_json, 'w') as f:
                        json.dump(dataConfig, f, indent=2, sort_keys=False)
                else :
                    prRed("Generation Aborted")
                    quit()
            else :
                with open(res_json, 'w') as f:
                    json.dump(dataConfig, f, indent=2, sort_keys=False)

        else :
            prRed("Wrong path for the configuration file, Generation Aborted")
            

    ######### Conclusion prints #########
    prGreen("The configuration file was generated !")
    prCyan("You can modify the parameters in it and launch the processing chain with this file as only argument")
    prCyan("You can find further information on https://gitlab.orfeo-toolbox.org/remote_modules/diapotb/-/wikis/")
        
        
