#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2017 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

""" 
    func_utils module
    ==================

    Pool of functions for logger, checks, image operations ...
 
"""


# Imports
import logging
import json
from jsonschema import validate
import os
import sys
import argparse
import re
import xml.etree.ElementTree as ET
import datetime
import time
import gdal
import osr
import ogr

import otbApplication as otb

# Streamer to our log file
class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """
    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level

    def write(self, buf):
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())

    def flush(self):
        for handler in self.logger.handlers:
            handler.flush()

# Global variables for logger and std_out
logger = logging.getLogger(__name__)
LogFormatter = logging.Formatter('%(filename)s :: %(levelname)s :: %(message)s')
s1 = StreamToLogger(logger, logging.INFO)
stdout_save = s1 

### Functions for logger ###
def init_logger():
    """
        Init logger with a stream handler.
    """
    logger.setLevel(logging.INFO)   

    # Create console handler with a high log level (warning level)
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.WARNING)

    # Add Handlers
    logger.addHandler(stream_handler)


def init_fileLog(output_dir):
    """
        Init logger with a file handler (info.log).
        the standard ouput is redirected into this file handler. The std was saved into stdout_save.
    """
    # File handler for the logger
    # Create file handler which logs even info messages (used as stdout redirection)
    file_handler = logging.FileHandler(os.path.join(output_dir, 'info.log'), 'a')
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(LogFormatter)

    # Add Handlers
    logger.addHandler(file_handler)

    # Redirect stdout and stderr to logger 
    stdout_saveWrite = sys.stdout.write # Save stdout.write to print some info into the console
    stdout_saveFlush = sys.stdout.flush # Save stdout.flush to print some info into the console
    sys.stdout.write = s1.write # Replace stdout.write by our StreamToLogger 
    sys.stdout.flush = s1.flush # Replace stdout.flush by our StreamToLogger
    #stdout_save = s1 # Different object
    stdout_save.write = stdout_saveWrite # Restore stdout.write into stdout_save
    stdout_save.flush = stdout_saveFlush # Restore stdout.write into stdout_save


def log(level, msg) :
    """
        Transfer the msg to our logger with the required level.
    """
    logger.log(level, msg)


def printOnStd(msg):
    """
        Transfer the msg to our stdout => real print on standard output.
    """
    # Print on stdout_save aka the original/true stdout
    print(msg, file=stdout_save)



### Functions for configFile (json) ###
def validate_json(json, schema):
    """
        Read and Load the configuration file
    """
    try:
        validate(json, schema)
    except Exception as valid_err:
        print("Invalid JSON: {}".format(valid_err))
        return False
    else:
        # Realise votre travail
        print("Valid JSON")
        return True

def load_configfile(configfile, mode="Others"):
    """
        Read and Load the configuration file (check this file according to a schmema)
    """
    # Empty dictionary
    dataConfig = {}

    # Read and Load the configuration file
    try:
        with open(configfile, 'r') as f:
            dataConfig = json.load(f)
            
    except Exception as err:
        logger.critical("Impossible to read or load JSON configuration file : {err}. Check its path and content.".format(err=configfile))
        quit()

    schema_json = "schema_S1SM.json"  
    if mode == "S1_IW":
        schema_json = "schema_S1IW.json"
    elif mode == "multi_S1":
        schema_json = "schema_MultiSlc.json"
    elif mode == "multi_SW":
        schema_json = "schema_MultiSlc_IW.json"

    # Load schema (into DiapOTB install)
    diapOTB_install = os.getenv('DIAPOTB_INSTALL')

    if diapOTB_install is not None and os.path.exists(diapOTB_install):
        schemas_path = os.path.join(diapOTB_install, "json_schemas")
        if os.path.exists(schemas_path):
            schema_file = os.path.join(schemas_path, schema_json)
    
            try:
                with open(schema_file, "r") as sch:
                    dataSchema = json.load(sch)
            except Exception as err:
                logger.critical("Impossible to read or load JSON configuration file : {err}. Check its path and content.".format(err=schema_S1SM))
                quit()

            # Check Json file
            jsonIsValid = validate_json(dataConfig, dataSchema)

            if not jsonIsValid :
                logger.critical("Error, provided config file does not match requirements")
                quit()
    
    return dataConfig



### Functions for metadata ###
def getImageKWL(Img):
    """
        Retrieve keyword list from an image thanks to ReadImageInfo
    """
    # Retrieve some information about our input images with ReadImageInfo application
    ReadImageInfo = otb.Registry.CreateApplication("ReadImageInfo")
    ReadImageInfo.SetParameterString("in", Img)
    ReadImageInfo.SetParameterString("keywordlist", "true")
    ReadImageInfo.Execute()
    
    keyword = ReadImageInfo.GetParameterString("keyword")
    keywordlist = ReadImageInfo.GetParameterString("keyword").split("\n")
    keywordlist = filter(None, keywordlist)
    dictKWL = { i.split(':')[0] : re.sub(r"[\n\t\s]*", "", "".join(i.split(':')[1:])) for i in keywordlist }
    
    return dictKWL


def getDEMInformation(dem):
    """
        Retrieve DEM information thanks to ReadImageInfo
    """
    # Get information about DEM (spacing, size ..)
    ReadDEMInfo = otb.Registry.CreateApplication("ReadImageInfo")
    ReadDEMInfo.SetParameterString("in", dem)
    ReadDEMInfo.SetParameterString("keywordlist", "true")
    ReadDEMInfo.Execute()

    dictDEMInformation = {}
    dictDEMInformation['spacingXDEM'] = ReadDEMInfo.GetParameterFloat("spacingx")
    dictDEMInformation['estimatedGroundSpacingXDEM'] = ReadDEMInfo.GetParameterFloat("estimatedgroundspacingx")
    dictDEMInformation['spacingYDEM'] = ReadDEMInfo.GetParameterFloat("spacingy")
    dictDEMInformation['estimatedGroundSpacingYDEM'] = ReadDEMInfo.GetParameterFloat("estimatedgroundspacingy")

    return dictDEMInformation
    

### Functions for image/file selection
def get_imgFromSAFE(arg, searchDir="."):
    """
        Retrive selected image from a SAFE directory
    """
    img = []
    for root, dirs, files in os.walk(searchDir):
        for i in (i for i in files if i == arg):
            img.append(os.path.join(root, i))
            img = str("".join(img))
            return img

def get_imgFromDir(arg, searchDir="."):
    """
        Retrive selected image from a directory (for Cosmo sensor)
    """
    img = []
    for root, dirs, files in os.walk(searchDir):
        for i in (i for i in files if i == arg):
            img.append(os.path.join(root, i))
            img = str("".join(img))
            return img

def check_image_pattern(img, mode="S1SM") :
    """
        Check pattern for current image. Must be cohetrent according to the sensor and mode
    """

    correct_pattern = False
    
    
    if mode == "S1SM" :
        # Mode S1 SM : mmm-bb-ttt-pp-yyyymmddthhmmss-yyyymmddthhmmss*
        # mmm : Mission identifier (s1a or s1b)
        # bb : Mode/Beam (s1-s6 for SM)
        # ttt : Product type (always slc here)
        # pp : Polarisations (2 letters : hh or vv or vh or hv)
        # yyyymmddthhmmss : Product start/stop date and times 
        # (14 digits representing the date and time separated by the character "t")
        # * : Others representations such as orbits number or images number ...
        pattern = "".join(["s1.", "-", '\w{1}', '\d', "-slc-", '\w{2}', "-", '\d{8}', "t", '\d{6}', "-", '\d{8}', "t", '\d{6}'])
        
        if re.match(pattern, img) :
            correct_pattern = True

    elif mode == "Cosmo":
        # Mode Cosmo : CSKS<i>_*_<YYYYMMDDhhmmss>_<YYYYMMDDhhmmss>
        # i : Identifier of the satellite (1, 2, 3 or 4)
        # YYYYMMDDhhmmss : Product start/stop date and times 
        # * : Others representations such as identifier for orbit direction or look side
        pattern = "".join(["CSKS", '\d'])
        
        if re.match(pattern, img) :
            pattern_dates = "".join(['\d{14}', "_", '\d{14}'])
            dates = re.findall(pattern_dates, img)
            
            if len(dates) == 1 :
                correct_pattern = True
        
    else :
        # Mode S1 IW : mmm-bb-ttt-pp-yyyymmddthhmmss-yyyymmddthhmmss*
        # mmm : Mission identifier (s1A or s1B)
        # bb : Mode/Beam (iw1-iw3 for IW)
        # ttt : Product type (always slc here)
        # pp : Polarisations (2 letters : hh or vv or vh or hv)
        # yyyymmddthhmmss : Product start/stop date and times 
        # (14 digits representing the date and time separated by the character "t")
        # * : Others representations such as orbits number or images number ...
        pattern = "".join(["s1.", "-", '\w{2}', '\d', "-slc-", '\w{2}', "-", '\d{8}', "t", '\d{6}', "-", '\d{8}', "t", '\d{6}'])
        
        if re.match(pattern, img) :
            correct_pattern = True

    return correct_pattern


def getSlcMlNamming_fromProductName(product_name, mode="S1SM") :
    """
        Get correct names (with conventions) for SLC and ML outputs (without extension)
    """

    # At the end, the name for slc and ml images must be : 
    # fullsensor_subwath_product_pol_UTCfirstdate with
    # fullsensor : S1A/B for S1 or CSK for Cosmo
    # subwath : for instance, s4 for S1SM and Cosmo or iw1 for S1 IW
    # product : SCS_U/B for Cosmo or SLC for S1
    # UTCdate : date with YYYYMMDDthhmmss format
    slcMlName = ""

    if mode == "Cosmo":
        # Mode Cosmo : CSKS<i>_SCS_U/B_Sk_*_pol_<YYYYMMDDhhmmss>_<YYYYMMDDhhmmss>
        # i : Identifier of the satellite (1, 2, 3 or 4)
        # Sk : Mode/Beam (s1-s6)
        # pol : Polarisations (HH or VV or VH or HV)
        # YYYYMMDDhhmmss : Product start/stop date and times 
        # * : Others representations such as identifier for orbit direction or look side
        productName_list = product_name.split("_")
        
        slcMlName = productName_list[0][:3] + "_" + productName_list[3] + "_" + \
                    productName_list[1] + productName_list[2] + "_" + productName_list[5] + \
                    "_" + productName_list[8][:8] + "t" + productName_list[8][8:]
        
    else :
        # Mode S1 IW : mmm-bb-ttt-pp-yyyymmddthhmmss-yyyymmddthhmmss*
        # mmm : Mission identifier (s1A or s1B)
        # bb : Mode/Beam (iw1-iw3 for IW or s1-s6)
        # ttt : Product type (always slc here)
        # pp : Polarisations (2 letters : hh or vv or vh or hv)
        # yyyymmddthhmmss : Product start/stop date and times 
        # (14 digits representing the date and time separated by the character "t")
        # * : Others representations such as orbits number or images number ...
        productName_list = product_name.split("-")
        
        slcMlName = productName_list[0] + "_" + productName_list[1] + "_" + \
                    productName_list[2] + "_" + productName_list[3] + "_" + \
                    productName_list[4]

    # Return output name without extension or ml factors
    return slcMlName.lower()



def getInterfNamming_fromProductName(productMaster_name, productSlave_name, mode="S1SM") :
    """
        Get correct names (with conventions) for interferogram outputs (without extension)
    """
    # At the end, the name for interf images must be : 
    # sensor_M_UTCfirstdatemaster_S_* UTCfirstdateslave with
    # sensor : S1 for S1 or CSK for Cosmo
    # UTCdate : date with YYYYMMDDthhmss format
    interfName = ""
        
    if mode == "Cosmo":
        # Mode Cosmo : CSKS<i>_SCS_U/B_Sk_*_pol_<YYYYMMDDhhmmss>_<YYYYMMDDhhmmss>
        # i : Identifier of the satellite (1, 2, 3 or 4)
        # Sk : Mode/Beam (s1-s6)
        # pol : Polarisations (HH or VV or VH or HV)
        # YYYYMMDDhhmmss : Product start/stop date and times 
        # * : Others representations such as identifier for orbit direction or look side
        productM_list = productMaster_name.split("_")
        productS_list = productSlave_name.split("_")
        
        interfName = productM_list[0][:3] + "_M_" +  \
                     productM_list[8][:8] + "t" + productM_list[8][8:] + "_S_" + \
                     productS_list[8][:8] + "t" + productS_list[8][8:]
        
    else :
        # Mode S1 IW : mmm-bb-ttt-pp-yyyymmddthhmmss-yyyymmddthhmmss*
        # mmm : Mission identifier (s1A or s1B)
        # bb : Mode/Beam (iw1-iw3 or s1-s6 for IW)
        # ttt : Product type (always slc here)
        # pp : Polarisations (2 letters : hh or vv or vh or hv)
        # yyyymmddthhmmss : Product start/stop date and times 
        # (14 digits representing the date and time separated by the character "t")
        # * : Others representations such as orbits number or images number ...
        productM_list = productMaster_name.split("-")
        productS_list = productSlave_name.split("-")
        
        interfName = productM_list[0][:2].upper() + "_M_" + \
                     productM_list[4]  + "_S_" + \
                     productS_list[4]

    # Return output name without extension or ml factors
    return interfName
    



def image_envelope(inTIF, outSHP):
    """ 
        This method returns a shapefile of an image
    """
    app = otb.Registry.CreateApplication("ImageEnvelope")
    app.SetParameterString("in", inTIF)
    app.SetParameterString("out", outSHP)
    app.ExecuteAndWriteOutput()
    return outSHP

def get_master_geometry(inSHP):
    """ 
        This method returns the geometry, of an input georeferenced
        shapefile
    """
    driver = ogr.GetDriverByName("ESRI Shapefile")
    mstr_ds = driver.Open(inSHP, 0)
    mstr_layer = mstr_ds.GetLayer()
    for master in mstr_layer:
        master.GetGeometryRef().Clone()
        return master.GetGeometryRef().Clone()

def check_srtm_coverage(inSHP_Geometry, SRTM):
    """ 
        This method checks and returns the SRTM tiles intersected
    """
    driver = ogr.GetDriverByName("ESRI Shapefile")
    srtm_ds = driver.Open(SRTM, 0)
    srtm_layer = srtm_ds.GetLayer()
    needed_srtm_tiles = {}
    srtm_tiles = []
    for srtm_tile in srtm_layer:
        srtm_footprint = srtm_tile.GetGeometryRef()
        intersection = inSHP_Geometry.Intersection(srtm_footprint)
        if intersection.GetArea() > 0:
            # coverage = intersection.GetArea()/area
            srtm_tiles.append(srtm_tile.GetField('FILE'))  # ,coverage))
    needed_srtm_tiles = srtm_tiles
    return needed_srtm_tiles


def add_WGSProjection(inTiff) :
    """ 
        Add a projection reference (WPS 84) to input tiff 
    """
    # Open input tiff
    ds = gdal.Open(inTiff, gdal.GA_Update)

    # First : Adapt Projetion for WGS84
    wkt = ds.GetProjection()

    # if no projection defined => projection by default (ESPG 4326)
    if not wkt :
        sr = osr.SpatialReference()
        sr.ImportFromEPSG(4326) # ESPG : 4326 = WGS84
        wkt = sr.ExportToWkt()

    # Set Projection and set to None to apply chgts
    ds.SetProjection(wkt)    
    ds = None 


def get_AllTiff(pol, iw="", ext="", searchDir="."):
    """
        Get all tiff from an input directory (check on pattern)
    """
    TiffList = []
    throw_warning = False

    # Mode S1 IW
    if not iw == "" :
        for root, dirs, files in os.walk(searchDir):
            for i in (i for i in files):  

                # Selection with extension (.tiff)
                if i.endswith(".tiff") :
    
                    # Check pattern
                    correct = check_image_pattern(i, "S1IW")
        
                    if correct :
                        # Selection with polarisation and subwath
                        if pol == i.split("-")[3]:
                            if iw == i.split("-")[1]:
                                TiffList.append(i)
                    else :
                        throw_warning = True
                                        
    else :
        # Mode Cosmo
        if ext == "h5":
            for root, dirs, files in os.walk(searchDir):
                for i in (i for i in files):
                    
                    # Selection with extension (.h5)
                    if i.endswith(".h5") :
                        
                        # Check pattern
                        correct = check_image_pattern(i, "Cosmo")
        
                        if correct :
                            # Selection with polarisation
                            if pol == i.split("_")[5]:
                                TiffList.append(i)
                        else :
                            throw_warning = True
                       
        # Mode S1 SM
        if not ext == "h5":
            for root, dirs, files in os.walk(searchDir):
                for i in (i for i in files):
                    
                    # Selection with extension (.tiff)
                    if i.endswith(".tiff") :
                        
                        # Check pattern
                        correct = check_image_pattern(i, "S1SM")
        
                        if correct :
                            # Selection with polarisation
                            if pol == i.split("-")[3]:
                                TiffList.append(i)
                        else :
                            throw_warning = True
                       

    return TiffList, throw_warning

def get_AllFilesWithExt(searchDir, ext) :
    """
        Get all into a search directory with a given extension
    """
    list_files = []
    for root, dirs, files in os.walk(searchDir):
        for i in (i for i in files): 
            if i.endswith(ext):
                list_files.append(i)
    
    return list_files


def get_Date(inTIF, ext=""):
    """
        Get all date from an input tiff
    """
    if ext == "h5":
        inTIF_date = inTIF.split("_")[8][:8]
        return inTIF_date
    if not ext == "h5":
        inTIF_date = inTIF.split("-")[4].split("t")[0]
        return inTIF_date


def get_Tiff_WithDates(start, end, exclude, TiffList, ext=""):
    """
        Get all tiff from an input list and between start and end date
    """
    date_list = []
    exclude = list(exclude)
    for i in TiffList:
        Date = get_Date(i, ext)
        if start <= int(Date) and end >= int(Date):
            if Date not in exclude:
                date_list.append(i)
    return date_list

def select_EofWithDate(start, end, eofList) :
    """
        Select into the input list, the file that correspond to dates
    """
    time_start = time.mktime(time.strptime(start,
                                          "%Y-%m-%dT%H%M%S.%f"))

    time_end = time.mktime(time.strptime(end,
                                        "%Y-%m-%dT%H%M%S.%f"))
    

    for i_eof in eofList:
        # Without extension
        i_eof = i_eof.split(".EOF")[0]

        start_eofDate = i_eof.split('_')[-2]
        start_eofDate = start_eofDate.split("V")[1]
        end_eofDate = i_eof.split('_')[-1] 

        # Save date format
        time_start_eofDate = time.mktime(time.strptime(start_eofDate,
                                                       "%Y%m%dT%H%M%S"))

        time_end_eofDate = time.mktime(time.strptime(end_eofDate,
                                                     "%Y%m%dT%H%M%S"))
    

        # Compare dates and return eof file if eof file contains the current image dates
        if time_start >= time_start_eofDate and time_end <= time_end_eofDate :
            return i_eof + ".EOF"

        
    # if none of files contains wanted dates : return None
    return None
        

def get_relative_orbit(manifest):
    """
        Get from manifest file, the orbit number
    """
    root=ET.parse(manifest)
    return int(root.find("metadataSection/metadataObject/metadataWrap/xmlData/{http://www.esa.int/safe/sentinel-1.0}orbitReference/{http://www.esa.int/safe/sentinel-1.0}relativeOrbitNumber").text)

def build_virutal_raster(master_Image, start_time, end_time, master_date,
                         srtm_shapefile, hgts_path, output_dir="."):
    """
        Build a vrt file corresponding to a dem from hgt (SRTM) files.
        The hgt file are contained into a global path : hgts_path
    """
    # create a vector envelope of the raster
    target_dir = os.path.dirname(master_Image)
    master_envelope = (target_dir + "/master_envelope.shp")
    master_envelope = image_envelope(master_Image, master_envelope)
    # Get master geometry
    master_footprint = get_master_geometry(master_envelope)
    # Create a virtual raster that will be used as DEM
    hgts = check_srtm_coverage(master_footprint, srtm_shapefile)
    hgts_tuple = []
    for hgt in hgts:
        hgts_tuple.append(os.path.join(hgts_path, hgt))

    print("\n Creating virtual raster from intersected hgt files...\n")
    dem = gdal.BuildVRT("{}/output_{}_to_{}_m_{}/dem_scene.vrt".format(
                        output_dir, start_time, end_time, master_date), hgts_tuple)
    dem = "{}/output_{}_to_{}_m_{}/dem_scene.vrt".format(output_dir, start_time, 
                                                         end_time, master_date)
    return dem, target_dir

def argDates_to_isoDates(start_date, end_date):
    """
        Date conversion
    """
    if start_date is not None:
        iso_start = datetime.datetime.strptime(start_date,
                                               "%Y%m%d").date()
        iso_end = datetime.datetime.strptime(end_date, "%Y%m%d").date()
        print("\n\n Selected dates: \n From {}, to {} \n".format(iso_start,
              iso_end))
    if not start_date:
        print("Start time is needeed.")
        quit()
    return iso_start, iso_end

### Image operations ###
def extract_roi(inp, out, roi):
    """
        Extracts ROI 
    """
    ds = gdal.Open(inp)
    ds = gdal.Translate(out, ds, projWin =roi)

    
def extract_band123(inp, out):
    """
        Extracts ROI from a vector image (extracts several bands, 3 in total)
    """
    ds = gdal.Open(inp, gdal.GA_ReadOnly)
    ds = gdal.Translate(out, ds, bandList=["1", "2", "3"], format="GTiff",
                        outputType=gdal.GDT_Float32, creationOptions=['TILED=YES'])
    return out


def silentremove(directory, filename):
    try:
        os.remove(os.path.join(directory, filename))
    except OSError as e:
        if e.errno != errno.ENOENT:
            raise
    except NameError as e:
        if e.errno != errno.ENOENT:
            raise

### Functions for some checks ###
def selectBurst(dictMaster, dictSlave, firstBurst, lastBurst, nbBurstSlave):
    """
        Selects from master bursts, the corresponding slave ones. 
        This selection uses a metadata parameter (azimuth_anx_time) to do so.
    """    
    key1Burst = "support_data.geom.bursts.burst["
    
    # Initialize the output lists (empty lists)
    validBurstMaster = [] 
    validBurstSlave = []
    
    # Loop on Master bursts
    for id_B in range(firstBurst, lastBurst+1):
        keyBurstMaster = key1Burst + str(id_B) + "].azimuth_anx_time"
        
        # Get the anx time for the current burst (into Master Image)
        anxMaster = float(dictMaster[keyBurstMaster])

        # Loop on slave bursts to find the closest anx time
        minDiff = 200
        id_B_save = id_B
        for id_B_slave in range(0, nbBurstSlave):
            keyBurstSlave = key1Burst + str(id_B_slave) + "].azimuth_anx_time"

            # Get anx time for slave burst
            anxSlave = float(dictSlave[keyBurstSlave])
            
            # Comparaison between master and slave
            diff = abs(anxMaster - anxSlave)
            
            if minDiff > diff :
                minDiff = diff
                id_B_save = id_B_slave

        # Check if difference between the anx time is valid (must be inferior to 1)
        if minDiff < 1. :
            # Fill lists with master Burst_id and the selected slave burst_id
            validBurstMaster.append(id_B)
            validBurstSlave.append(id_B_save)

    return validBurstMaster, validBurstSlave

def check_roiFormat(roi):
    """
        Check the roi format, must be 'ulx uly lrx lry' (ex: -roi 2.44115 48.96126 2.44176 48.95927)
    """
    regx_exp = re.compile("^([+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)\s){4}$")
    if not roi.endswith(' '):
        roi = roi + ' '

    res = regx_exp.match(roi)
    # If res = None => does not match with the correct format => quit the progrm
    if not res :
        print("Wrong format for roi paramater, must be 'ulx uly lrx lry' (ex: -roi 2.44115 48.96126 2.44176 48.95927)")
        quit()

def str2bool(v):
    """
        Conversion between a string to a boolean (several ways to say yes into json configuration file).
    """
    return v.lower() in ("yes", "true", "t", "1") 


def avoidDuplicates(inlist):
    """
        Remove duplicates into an input list.
    """
    outlist = list(dict.fromkeys(inlist))
    return outlist

def check_ifExist(fileOrPathOrImg):
    """
        Check if a file, path or image exists. If not quit the program.
    """
    if not os.path.exists(fileOrPathOrImg):
        print(fileOrPathOrImg + " does not exists")
        quit()


def check_ifDir(inDir):
    """
        Check if the input path exists and is a directory
    """
    if not os.path.isdir(inDir):
        print(inDir + " does not exists or is not a directory")
        quit()

def check_burstIndex(burst_index):
    """
        Check if the burst_index as string input is correctly sent
    """
    burstIndexOk = bool(re.match(r'^[\-0-9]+$', burst_index))
    firstBurst = 0
    lastBurst = 0
    if burstIndexOk:
        burstList = burst_index.split('-')
        burstList = [int(i) for i in burstList]
        if len(burstList) != 2:
            burstIndexOk = False
        else:
            firstBurst = min(burstList)
            lastBurst = max(burstList)
    if not burstIndexOk:
        print("Wrong Burst Index Format (for instance 0-5)")
        quit()
    return firstBurst, lastBurst, burstList

### Remove files ###
def silentremove(directory, filename):
    """
        Remove filename into directory
    """
    try:
        os.remove(os.path.join(directory, filename))
    except OSError as e:
        if e.errno != errno.ENOENT:
            raise
    except NameError as e:
        if e.errno != errno.ENOENT:
            raise
