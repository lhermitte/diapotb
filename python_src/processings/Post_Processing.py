#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2017 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""
    The ``Post_Processing`` chain
    =============================
 
    Use the module to launch post processigns like phase filtering.
 
    main function : filtering
    -----------------------------------
 
"""

import os
import sys
from functools import partial

import utils.DiapOTB_applications as diapOTBApp
from utils import func_utils

import otbApplication as otb


def filtering(master_Image, master_Image_base, slave_Image, slave_Image_base, master_dir, slave_dir, param, mode, output_dir):
    """
        Main function for Post processing
    
        This function applies several processing to provide the filtering result interferogram 
        (according to the mode)
    
        :param master_Image: Master image name (with path included) 
        :param master_Image_base: Master image name   
        :param slave_Image: Slave image name (with path included) 
        :param slave_Image_base: Slave image name
        :param master_dir: Output directory (to store result on master image only)
        :param slave_dir: Output directory (to store result on slave image only)
        :param param: Dictionary to retrieve all parameters used into this processing chain 
        :param mode: Selected mode according to input product : S1IW or other (for Cosmo and S1 StripMap products) 
        :param output_dir: Output directory 

        :type master_Image: string 
        :type master_Image_base: string
        :type slave_Image: string
        :type slave_Image_base: string
        :type master_dir: string
        :type slave_dir: string
        :type param: dict
        :type mode: string
        :type output_dir: string

        :return: filtered interferogram
        :rtype: string
    """
    if mode == 'S1_IW' :
        return filtering_S1IW(master_Image, master_Image_base, slave_Image, slave_Image_base, master_dir, slave_dir, param, output_dir)
    else :
        return filtering_Others(master_Image, master_Image_base, slave_Image, slave_Image_base, master_dir, slave_dir, param, output_dir)

    

def filtering_Others(master_Image, master_Image_base, slave_Image, slave_Image_base, master_dir, slave_dir, param, output_dir):
    """
        Main function for post processing and other than S1IW mode (S1SM and Cosmo)
    
        This function applies several processing to provide the result interferogram 
     
        :param master_Image: Master image name (with path included) 
        :param master_Image_base: Master image name   
        :param slave_Image: Slave image name (with path included) 
        :param slave_Image_base: Slave image name
        :param master_dir: Output directory (to store result on master image only)
        :param slave_dir: Output directory (to store result on slave image only)
        :param param: Dictionary to retrieve all parameters used into this processing chain 
        :param output_dir: Output directory 

        :type master_Image: string 
        :type master_Image_base: string
        :type slave_Image: string
        :type slave_Image_base: string
        :type master_dir: string
        :type slave_dir: string
        :type param: dict
        :type output_dir: string

        :return: 
        :rtype: None
    """    
    # Get elements into param dictionnaries 
    ml_azimut = 1
    ml_range = 1
    ml_gain = 1
    alpha = 0.7
    
    ml_filt_interf_range = ml_range
    if "ml_filt_interf_range" in param :
        ml_filt_interf_range = param['ml_filt_interf_range']
    
    ml_filt_interf_azimut = ml_azimut
    if "ml_filt_interf_azimut" in param :
        ml_filt_interf_azimut = param['ml_filt_interf_azimut']

    ml_filt_interf_gain = ml_gain
    if "ml_filt_interf_gain" in param :
        ml_filt_interf_gain = param['ml_filt_interf_gain']

    if "filt_alpha" in param :
        alpha = param['filt_alpha']

    ram = "4000"
    if "ram" in param :
        ram = param['ram']
    
    geoGrid_gridstep_range = param['geoGrid_gridstep_range']
    geoGrid_gridstep_azimut = param['geoGrid_gridstep_azimut']   
        

    # Filtering : Pipeline in memory (=> Execute and direct calls to applications)
    master_cartesian_mean = "CartMeanMaster.tif"
    fine_grid = "fineDeformationGrid.tif"

    topographicPhase = "Topo.tif"
    filt_dir = os.path.join(output_dir, "filt")

    if not os.path.exists(filt_dir):
        os.makedirs(filt_dir)
    
    # First SARTopographicPhase
    appTopoPhase = otb.Registry.CreateApplication("SARTopographicPhase")
    appTopoPhase.SetParameterString("insarslave", slave_Image)
    appTopoPhase.SetParameterString("ingrid",  os.path.join(slave_dir, fine_grid))
    appTopoPhase.SetParameterString("incartmeanmaster", 
                                    os.path.join(master_dir, master_cartesian_mean))
    appTopoPhase.SetParameterInt("mlran", 1)
    appTopoPhase.SetParameterInt("mlazi", 1)
    appTopoPhase.SetParameterInt("gridsteprange", geoGrid_gridstep_range)
    appTopoPhase.SetParameterInt("gridstepazimut", geoGrid_gridstep_azimut)
    appTopoPhase.SetParameterString("ram", ram)
    appTopoPhase.SetParameterString("out", os.path.join(filt_dir, topographicPhase))
    appTopoPhase.Execute()

    # Second CompensatedComplex
    slave_Image_CoRe = os.path.splitext(slave_Image_base)[0] + "_coregistrated.tif"
    if "slave_CoRe_Name" in param :
        slave_Image_CoRe = param['slave_CoRe_Name']
        slave_Image_CoRe += "?&gdal:co:TILED=YES"

    coRe_path = os.path.join(slave_dir, slave_Image_CoRe)

    complexImg = "CompensatedComplex.tif" 

    appComplex = otb.Registry.CreateApplication("SARCompensatedComplex")
    appComplex.SetParameterString("insarslave", coRe_path)
    appComplex.SetParameterString("insarmaster",  master_Image)
    appComplex.SetParameterInputImage("topographicphase", 
                                        appTopoPhase.GetParameterOutputImage("out"))
    appComplex.SetParameterString("ram", ram)
    appComplex.SetParameterString("out", os.path.join(filt_dir, complexImg))
    appComplex.Execute()

    # Third SARPhaseFiltering
    filt_phacoh = "filfPhaCoh.tif"
    filt_phacoh_path = os.path.join(filt_dir, filt_phacoh)
    appPhaFiltering = otb.Registry.CreateApplication("SARPhaseFiltering")
    appPhaFiltering.SetParameterInputImage("incomplex", appComplex.GetParameterOutputImage("out"))
    appPhaFiltering.SetParameterInt("mlran", ml_filt_interf_range)
    appPhaFiltering.SetParameterInt("mlazi", ml_filt_interf_azimut)
    appPhaFiltering.SetParameterInt("step", 16)
    appPhaFiltering.SetParameterInt("sizetiles", 64)
    appPhaFiltering.SetParameterFloat("alpha", alpha)
    appPhaFiltering.SetParameterString("out", filt_phacoh_path)
    appPhaFiltering.SetParameterString("ram", ram)
    appPhaFiltering.ExecuteAndWriteOutput()

    
    # Eventually SARAddBandInterfergoram
    filt_interferogram = "filtered_interferogram.tif"
    if "filtered_Name" in param :
        filt_interferogram = param['filtered_Name']
    appAddAmp = otb.Registry.CreateApplication("SARAddBandInterferogram")
    appAddAmp.SetParameterInputImage("incomplexamp", appComplex.GetParameterOutputImage("out"))
    appAddAmp.SetParameterString("ininterf", filt_phacoh_path)    
    appAddAmp.SetParameterInt("mlran", ml_filt_interf_range)
    appAddAmp.SetParameterInt("mlazi", ml_filt_interf_azimut)
    appAddAmp.SetParameterFloat("gain", ml_filt_interf_gain)
    appAddAmp.SetParameterString("out", os.path.join(output_dir, filt_interferogram))
    appAddAmp.SetParameterString("ram", ram)
    appAddAmp.ExecuteAndWriteOutput()
    


def filtering_S1IW(master_Image, master_Image_base, slave_Image, slave_Image_base, master_dir, slave_dir, param, output_dir):
    """
        Main function for Post processing and S1IW mode
    
        This function applies several processing to provide the result interferogram (with filtering)
    
        :param master_Image: Master image name (with path included) 
        :param master_Image_base: Master image name   
        :param slave_Image: Slave image name (with path included) 
        :param slave_Image_base: Slave image name
        :param master_dir: Output directory (to store result on master image only)
        :param slave_dir: Output directory (to store result on slave image only)
        :param param: Dictionary to retrieve all parameters used into this processing chain 
        :param output_dir: Output directory 

        :type master_Image: string 
        :type master_Image_base: string
        :type slave_Image: string
        :type slave_Image_base: string
        :type master_dir: string
        :type slave_dir: string
        :type param: dict
        :type output_dir: string

        :return: 
        :rtype: None
    """    
    # Get elements into param dictionnaries 
    ml_azimut = 1
    ml_range = 1
    ml_gain = 1
    alpha = 0.7
    
    ml_filt_interf_range = ml_range
    if "ml_filt_interf_range" in param :
        ml_filt_interf_range = param['ml_filt_interf_range']
    
    ml_filt_interf_azimut = ml_azimut
    if "ml_filt_interf_azimut" in param :
        ml_filt_interf_azimut = param['ml_filt_interf_azimut']
    
    ml_filt_interf_gain = ml_gain
    if "ml_filt_interf_gain" in param :
        ml_filt_interf_gain = param['ml_filt_interf_gain']

    if "filt_alpha" in param :
        alpha = param['filt_alpha']

    ram = "4000"
    if "ram" in param :
        ram = param['ram']

    # define partial function with previous parameters
    phaseFilteringApp = partial(diapOTBApp.phaseFiltering, withComplex="false", 
                                mlran=ml_filt_interf_range, mlazi=ml_filt_interf_azimut, step=16, 
                                sizetiles=64, alpha=alpha, ram=ram)

    addAmpApp = partial(diapOTBApp.addAmpInterferogram, withComplex="false", 
                        mlran=ml_filt_interf_range, mlazi=ml_filt_interf_azimut, 
                        gain=ml_filt_interf_gain, ram=ram)

    filt_dir = os.path.join(output_dir, "filt")

    if not os.path.exists(filt_dir):
        os.makedirs(filt_dir)

    # Use directly the interferogram (ML 1x1)
    interf_name = "interferogram_swath.tif"
    if "interf_Name" in param :
        interf_name = param['interf_Name']
      
    
    

    # SARPhaseFiltering
    filt_phacoh = "filfPhaCoh.tif"
    filt_phacoh_path = os.path.join(filt_dir, filt_phacoh)
    phaseFilteringApp(inForFiltering=os.path.join(output_dir, interf_name),
                      outPath=filt_phacoh_path)

    
    # SARAddBandInterfergoram
    filt_interferogram = "filtered_interferogram.tif"
    if "filtered_Name" in param :
        filt_interferogram = param['filtered_Name']
    addAmpApp(inForAmp=os.path.join(output_dir, interf_name), 
              inForPhaCoh=filt_phacoh_path,
              outPath=os.path.join(output_dir, filt_interferogram))
