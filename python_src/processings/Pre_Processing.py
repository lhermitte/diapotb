#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2005-2017 Centre National d'Etudes Spatiales (CNES)
#
# This file is part of Orfeo Toolbox
#
#     https://www.orfeo-toolbox.org/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""
    The ``Pre_Processing`` chain
    ============================
 
    Use the module to launch Pre_Processing chain.
 
    main function : extractToMultilook
    ----------------------------------

"""

import os
import sys
from functools import partial

import utils.DiapOTB_applications as diapOTBApp



def extractToMultilook(sar_Image, sar_Image_base, param, mode, output_dir):
    """
        Main function for Pre_Processing chain
    
        This function applies several processing according to the selected mode :
        For S1IW mode : For each burst, Burst Extraction + Deramping + Doppler0 estimation + MultiLook processing
        For other mode : Doppler0 estimation + MultiLook processing
    
        :param sar_Image: SAR image name (with path included) 
        :param sar_Image_base: SAR image name
        :param param: Dictionary to retrieve all parameters used into this processing chain 
        :param mode: Selected mode according to input product : S1IW or other (for Cosmo and S1 StripMap products) 
        :param output_dir: Output directory 

        :type sar_Image: string 
        :type sar_Image_base: string
        :type param: dict
        :type mode: string
        :type output_dir: string

        :return: Doppler0 values 
        :rtype: list 
    """
    if mode == 'S1_IW' :
        return extractToMultilook_S1IW(sar_Image, sar_Image_base, param, output_dir)
    else :
        return extractToMultilook_Others(sar_Image, sar_Image_base, param, output_dir)
    

def extractToMultilook_Others(sar_Image, sar_Image_base, param, output_dir):
    """
        Main function for Pre_Processing chain and other than S1IW mode (S1SM and Cosmo)
    
        This function applies several processing according to the selected mode :
         For other mode : Doppler0 estimation + MultiLook processing
    
        :param sar_Image: SAR image name (with path included) 
        :param sar_Image_base: SAR image name   
        :param param: Dictionary to retrieve all parameters used into this processing chain 
        :param output_dir: Output directory 

        :type sar_Image: string 
        :type sar_Image_base: string
        :type param: dict
        :type output_dir: string

        :return: Doppler0 value 
        :rtype: float 
    """    
    
    # Get elements into param dictionnaries 
    ml_azimut = param['ml_azimut']
    ml_range = param['ml_range']
    ml_gain = param['ml_gain']
    dop_file = param['dop_file']

    ram = "4000"
    if "ram" in param :
        ram = param['ram']

    # Define some applications functions with the previous parameters
    multilookApp = partial(diapOTBApp.multilook, mlran=ml_range, mlazi=ml_azimut, mlgain=ml_gain, 
                           ram=ram)
    
    # Create an empty list
    dop0 = 0

    ## SARDoppler Application ##
    print("\n Doppler Application \n")
    dop0 = diapOTBApp.doppler0(insar=sar_Image, 
                               outPath=os.path.join(output_dir, dop_file), ram=ram)
    
    ## SARMultiLook Application ##
    print("\n MultiLook Application \n")
    sar_Image_ML = os.path.splitext(sar_Image_base)[0] + "_ml" + str(ml_azimut) + str(ml_range) + ".tif"

    multilookApp(inImg=sar_Image, 
                 outPath=os.path.join(output_dir, sar_Image_ML))

    # return the value of Doppler0 values
    return dop0



def extractToMultilook_S1IW(sar_Image, sar_Image_base, param, output_dir):
    """
        Main function for Pre_Processing chain and S1IW mode
    
        This function applies several processing according to the selected mode :
        For S1IW mode : For each burst, Burst Extraction + Deramping + Doppler0 estimation + MultiLook processing
        For other mode : Doppler0 estimation + MultiLook processing
    
        :param sar_Image: SAR image name (with path included) 
        :param sar_Image_base: SAR image name   
        :param param: Dictionary to retrieve all parameters used into this processing chain 
        :param output_dir: Output directory 

        :type sar_Image: string 
        :type sar_Image_base: string
        :type param: dict
        :type output_dir: string

        :return: Doppler0 values 
        :rtype: list 
    """    
    
    for_slave_Image = False

    # Get elements into param dictionnaries 
    ml_azimut = param['ml_azimut']
    ml_range = param['ml_range']
    ml_gain = param['ml_gain']
    dop_file = param['dop_file']
    validBurstMaster = param['validBurstMaster']
    validBurstSlave = []
    if "validBurstSlave" in param :
        validBurstSlave = param['validBurstSlave']
        for_slave_Image = True
        
    ram = "4000"
    if "ram" in param :
        ram = param['ram']


    # Define some applications functions with the previous parameters
    multilookApp = partial(diapOTBApp.multilook, mlran=ml_range, mlazi=ml_azimut, mlgain=ml_gain, 
                           ram=ram)
    
    derampApp = partial(diapOTBApp.deramp, reramp="false", shift="false", ingrid="", inslave="", 
                        gridsteprange=0, gridstepazimut=0,ram=ram)

    # Create an empty lists
    dop0 = []
    bursts = []
    deramp_bursts = []
        
    # Loop on bursts
    for id_loop in range(0, len(validBurstMaster)):
    
        burstId_dir = validBurstMaster[id_loop]
        
        burstId = burstId_dir
        if for_slave_Image :
            burstId = validBurstSlave[id_loop]

        print("\n BurstId = " + str(burstId_dir) + "\n")

        burst_dir = os.path.join(output_dir, "burst" + str(burstId_dir))

        ## SARBurstExtraction Application ##
        print("\n Burst Extraction Application \n")
        burst = os.path.splitext(sar_Image_base)[0] + "_burst" + str(burstId) + ".tif"
        
        diapOTBApp.burstExtraction(sar_Image, burstId, os.path.join(burst_dir, burst), ram=ram)

        bursts.append(os.path.join(burst_dir, burst))

        ## SARDeramp Application ##
        print("\n Deramping Application \n")
        burstDeramp = os.path.splitext(sar_Image_base)[0] + "_burst" + str(burstId) + "_deramp" + ".tif"
        derampApp(inImg=os.path.join(burst_dir, burst), outPath=os.path.join(burst_dir, burstDeramp))
        
        deramp_bursts.append(os.path.join(burst_dir, burstDeramp))

        ## SARDoppler Application ##
        print("\n Doppler Application \n")
        dop0.append(diapOTBApp.doppler0(insar=os.path.join(burst_dir, burstDeramp), 
                                        outPath=os.path.join(output_dir, dop_file), ram=ram))

        ## SARMultiLook Application ##
        print("\n MultiLook Application \n")
        sar_Image_ML = os.path.splitext(sar_Image_base)[0] + "_burst" + str(burstId) + "_ml" + str(ml_azimut) + str(ml_range) + ".tif"
        
        multilookApp(inImg=os.path.join(burst_dir, burstDeramp), 
                     outPath=os.path.join(burst_dir, sar_Image_ML))

    # return the list of Doppler0 values
    return dop0, bursts, deramp_bursts
