/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdlib>
#include "itkMacro.h"
#include "otbImage.h"
#include "otbImageFileReader.h"
#include "otbSARStreamingCorrelationImageFilter.h"

#include <complex>

int otbSARStreamingCorrelationImageFilterTest(int argc, char * argv[])
{
  // Verify the number of parameters in the command line
  if (argc != 2)
    {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << " inputImageFile" << std::endl;
    return EXIT_FAILURE;
    }

  // Image type
  const unsigned int Dimension = 2;
  typedef std::complex<float>             PixelType;
  typedef otb::Image<PixelType, Dimension> ImageType;

  // Reader
  typedef otb::ImageFileReader<ImageType> ReaderType;
  ReaderType::Pointer reader = ReaderType::New();

  // Filter
  typedef otb::SARStreamingCorrelationImageFilter<ImageType> CorrelationType;
  CorrelationType::Pointer corFilter = CorrelationType::New();
  
  
  // Define pipeline
  reader->SetFileName(argv[1]);  
  corFilter->SetInput(reader->GetOutput());
 

  // Execute pipeline
  try
    {
      corFilter->Update();
      std::cout << corFilter->GetSum() << std::endl;
      std::cout << corFilter->GetCorrelation() << std::endl;
      std::cout << corFilter->GetDoppler() << std::endl;
    }
  catch (itk::ExceptionObject& err)
    {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}

