otb_module_test()

#${otb-module} will be the name of this module and will not need to be
#changed when this module is renamed.

set(DATA_TEST ${CMAKE_CURRENT_SOURCE_DIR}/../data)
set(OUTPUT_TEST ${CMAKE_BINARY_DIR}/Testing/Temporary)
set(NOTOL      0.0)

set(${otb-module}Tests
  otbDiapasonModuleTestDriver.cxx
  otbSARGainImageFilterTest.cxx
  otbSARQuadraticAveragingImageFilterTest.cxx
  otbSARStreamingCorrelationImageFilterTest.cxx
  otbSARDEMProjectionImageFilterTest.cxx
  otbSARAmplitudeEstimationImageFilterTest.cxx
  otbSARStreamingDEMInformationFilterTest.cxx
  otbSARDerampImageFilterTest.cxx
  otbSARDopplerCentroidFreqImageFilterTest.cxx
)

add_executable(otbDiapasonModuleTestDriver ${${otb-module}Tests})
target_link_libraries(otbDiapasonModuleTestDriver ${${otb-module}-Test_LIBRARIES})
otb_module_target_label(otbDiapasonModuleTestDriver)

add_executable(otbSARTilesAnalysisImageFilterMain otbSARTilesAnalysisImageFilterMain.cxx)
target_link_libraries(otbSARTilesAnalysisImageFilterMain ${${otb-module}-Test_LIBRARIES})
otb_module_target_label(otbSARTilesAnalysisImageFilterMain)

otb_add_test(NAME otbSARQuadraticAveragingImageFilterInAzimuthTest
  COMMAND otbDiapasonModuleTestDriver otbSARQuadraticAveragingImageFilterInAzimuthTest
  ${DATA_TEST}/s1a_s6_slc_0180416T195057_vv_cap_verde.tif
  ${OUTPUT_TEST}/otbSARQuadraticAveragingImageFilterInAzimuthTestOutput.tif	
 )


otb_add_test(NAME otbSARQuadraticAveragingImageFilterInRangeTest
  COMMAND otbDiapasonModuleTestDriver otbSARQuadraticAveragingImageFilterInRangeTest
  ${DATA_TEST}/s1a_s6_slc_0180416T195057_vv_cap_verde.tif
  ${OUTPUT_TEST}/otbSARQuadraticAveragingImageFilterInRangeTestOutput.tif	
 )


otb_add_test(NAME otbSARStreamingCorrelationImageFilterTest
  COMMAND otbDiapasonModuleTestDriver otbSARStreamingCorrelationImageFilterTest
  ${DATA_TEST}/s1a_s6_slc_0180416T195057_vv_cap_verde.tif
 )


otb_add_test(NAME otbSARDEMProjectionImageFilterTest
  COMMAND otbDiapasonModuleTestDriver otbSARDEMProjectionImageFilterTest
  ${DATA_TEST}/N14W025.hgt
  ${DATA_TEST}/s1a_s6_slc_0180416T195057_vv_cap_verde.tif
  ${OUTPUT_TEST}/otbSARDEMProjectionImageFilterTestOutput.tif
 )

set_tests_properties(otbSARDEMProjectionImageFilterTest PROPERTIES ENVIRONMENT OTB_GEOID_FILE=${DATA_TEST}/egm96.grd)

otb_add_test(NAME otbSARStreamingDEMInformationFilterTest
  COMMAND otbDiapasonModuleTestDriver otbSARStreamingDEMInformationFilterTest
  ${DATA_TEST}/N14W025.hgt
  ${DATA_TEST}/s1a_s6_slc_0180416T195057_vv_cap_verde.tif
 )

otb_add_test(NAME otbSARAmplitudeEstimationImageFilterTest
  COMMAND otbDiapasonModuleTestDriver otbSARAmplitudeEstimationImageFilterTest
  ${OUTPUT_TEST}/otbSARDEMProjectionImageFilterTestOutput.tif
  ${DATA_TEST}/N14W025.hgt
  ${DATA_TEST}/s1a_s6_slc_0180416T195057_vv_cap_verde.tif
  99.0213
  1
  -1
  ${OUTPUT_TEST}/otbSARAmplitudeEstimationImageFilterTestOutput.tif
 )

set_tests_properties(otbSARAmplitudeEstimationImageFilterTest PROPERTIES DEPENDS otbSARDEMProjectionImageFilterTest)


otb_add_test(NAME otbSARDerampFilterTest
  COMMAND otbDiapasonModuleTestDriver otbSARDerampFilterTest
  ${DATA_TEST}/s1a-iw1-slc-vv-20170123t060709-20170123t060734-014955-018698-004_burst4_extract.tiff
  ${OUTPUT_TEST}/otbSARDerampFilterTestOutput.tif
 )

otb_add_test(NAME otbSARDopplerCentroidFreqFilterTest
  COMMAND otbDiapasonModuleTestDriver otbSARDopplerCentroidFreqFilterTest
  ${DATA_TEST}/s1a-iw1-slc-vv-20170123t060709-20170123t060734-014955-018698-004_burst4_extract.tiff
  ${OUTPUT_TEST}/otbSARDopplerCentroidFreqFilterTestOutput.tif
 )


#test application
otb_test_application(NAME otbSARMultiLookTest
                    APP  SARMultiLook
                    OPTIONS -incomplex ${DATA_TEST}/s1a_s6_slc_0180416T195057_vv_cap_verde.tif
                            -out ${OUTPUT_TEST}/otbSARMultiLookTestOutput.tif
                            -mlran 3
                            -mlazi 3
			    -mlgain 0.1)

otb_test_application(NAME otbSARDoppler0Test
                    APP  SARDoppler0
                    OPTIONS -insar ${DATA_TEST}/s1a_s6_slc_0180416T195057_vv_cap_verde.tif
			    -outfile ${OUTPUT_TEST}/otbSARDoppler0TestOutput.txt)

otb_test_application(NAME otbSARDEMProjectionTest
                    APP  SARDEMProjection
                    OPTIONS -indem ${DATA_TEST}/N14W025.hgt
		            -insar ${DATA_TEST}/s1a_s6_slc_0180416T195057_vv_cap_verde.tif
			    -withxyz true
                            -out ${OUTPUT_TEST}/otbSARDEMProjectionTestOutput.tif)

set_tests_properties(otbSARDEMProjectionTest PROPERTIES ENVIRONMENT OTB_GEOID_FILE=${DATA_TEST}/egm96.grd)

otb_test_application(NAME otbSARAmplitudeEstimationTest
                    APP  SARAmplitudeEstimation
                    OPTIONS -indem ${DATA_TEST}/N14W025.hgt
		            -insar ${DATA_TEST}/s1a_s6_slc_0180416T195057_vv_cap_verde.tif
			    -indemproj ${OUTPUT_TEST}/otbSARDEMProjectionTestOutput.tif
			    -gain 99.0213
			    -directiondemc 1
			    -directiondeml -1
                            -out ${OUTPUT_TEST}/otbSARAmplitudeEstimationTestOutput.tif)

set_tests_properties(otbSARAmplitudeEstimationTest PROPERTIES DEPENDS otbSARDEMProjectionTest)

otb_test_application(NAME otbSARDEMToAmplitudeTest
                    APP  SARDEMToAmplitude
                    OPTIONS -indem ${DATA_TEST}/N14W025.hgt
		            -insar ${DATA_TEST}/s1a_s6_slc_0180416T195057_vv_cap_verde.tif
                            -out ${OUTPUT_TEST}/otbSARDEMToAmplitudeTestOutput.tif)

set_tests_properties(otbSARDEMToAmplitudeTest PROPERTIES ENVIRONMENT OTB_GEOID_FILE=${DATA_TEST}/egm96.grd)

otb_test_application(NAME otbSARCartesianMeanEstimationTest
                    APP  SARCartesianMeanEstimation
                    OPTIONS -indem ${DATA_TEST}/N14W025.hgt
		            -insar ${DATA_TEST}/s1a_s6_slc_0180416T195057_vv_cap_verde.tif
			    -indemproj ${OUTPUT_TEST}/otbSARDEMProjectionTestOutput.tif
			    -indirectiondemc 1
			    -indirectiondeml -1
                            -out ${OUTPUT_TEST}/otbSARCartesianMeanEstimationTestOutput.tif)

set_tests_properties(otbSARCartesianMeanEstimationTest PROPERTIES DEPENDS otbSARDEMProjectionTest)

otb_test_application(NAME otbSARDerampTest
                    APP  SARDeramp
                    OPTIONS -in ${DATA_TEST}/s1a-iw1-slc-vv-20170123t060709-20170123t060734-014955-018698-004_burst4_extract.tiff
		    -out ${OUTPUT_TEST}/otbSARDerampTestOutput.tif)
