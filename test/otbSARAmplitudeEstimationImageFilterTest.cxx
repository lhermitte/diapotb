/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdlib>
#include "itkMacro.h"
#include "otbImage.h"
#include "otbVectorImage.h"
#include "otbImageFileReader.h"
#include "otbImageFileWriter.h"
#include "otbSARDEMPolygonsAnalysisImageFilter.h"
#include "otbSARAmplitudeEstimationFunctor.h"

int otbSARAmplitudeEstimationImageFilterTest(int argc, char * argv[])
{
  // Verify the number of parameters in the command line
  if (argc != 8)
    {
      std::cerr << "Usage: " << std::endl;
      std::cerr << argv[0] << " inputVectorFile inputDEMFile inputSARImageFile inputGain intputDirectionC inputDirectionL outputImageFile" << std::endl;
      return EXIT_FAILURE;
    }


  // Image Types
  typedef otb::Image <float, 2>    ImageMNTInType;
  typedef otb::Image <std::complex<float>, 2>       ImageSARInType ;
  typedef otb::VectorImage <float , 2> ImageVectorType;
  typedef otb::Image <float, 2>       ImageOutType ;
  
  typedef typename ImageVectorType::PixelType   ImageInPixelType;
  typedef typename ImageOutType::PixelType      ImageOutPixelType;


  // Function Type
  typedef otb::Function::SARAmplitudeEstimationFunctor<ImageInPixelType, ImageOutPixelType> AmpFunctorType;

  // Readers and Writers Type
  typedef otb::ImageFileReader <ImageMNTInType > ReaderType ;
  typedef otb::ImageFileReader <ImageVectorType > ReaderVectorType ;
  typedef otb::ImageFileReader <ImageSARInType > ReaderComplexType ;
  typedef otb::ImageFileWriter <ImageOutType > WriterType ;

  // Instiante reader and writer
  ReaderType::Pointer reader_MNT = ReaderType::New();
  ReaderVectorType::Pointer reader_vector = ReaderVectorType::New();
  ReaderComplexType::Pointer reader_SAR = ReaderComplexType::New();
  WriterType::Pointer writer = WriterType::New();

  // Input MNT and SAR arguments
  reader_vector->SetFileName(argv[1]);
  reader_MNT->SetFileName(argv[2]);
  reader_SAR->SetFileName(argv[3]);

  // Writer argument
  writer->SetFileName(argv[7]);

  // Retrive DEM informations (values for cap_verde image from data repository)
  double gain = atof(argv[4]);
  int directionC = atoi(argv[5]);
  int directionL = atoi(argv[6]);;
 
  // Start the first pipelines (Read SAR image metedata and MNT metadata)
  ImageSARInType::Pointer SARPtr = reader_SAR->GetOutput();
  reader_SAR->UpdateOutputInformation();
  ImageMNTInType::Pointer MNTPtr = reader_MNT->GetOutput();
  reader_MNT->UpdateOutputInformation();

  // Functor 
  AmpFunctorType::Pointer functor =  AmpFunctorType::New(gain);
  
  // Instanciate the filter (use SARDEMPolygonsAnalysisImageFilter with SARAmplitudeEstimationFunctor
  // to estimate amplitude image)
  typedef otb::SARDEMPolygonsAnalysisImageFilter<ImageVectorType, ImageOutType, ImageMNTInType, ImageSARInType, AmpFunctorType> FilterType;
  FilterType::Pointer filter = FilterType::New();
  // Set elts
  filter->SetSARImageKeyWorList(SARPtr->GetImageKeywordlist());
  filter->SetSARImagePtr(SARPtr);
  filter->SetDEMImagePtr(MNTPtr);
  filter->SetDEMInformation(gain,directionC, directionL);
  filter->SetFunctorPtr(functor);
  filter->initializeMarginAndRSTransform();

  std::cout<<"Print AmplitudeEstimation filter information: "<< std::endl;
  std::cout << filter << std::endl;

  // Define pipeline
  filter->SetInput(reader_vector->GetOutput());
  writer->SetInput(filter->GetOutput());

  // Execute pipeline
  try
    {
      writer->Update();
    }
  catch (itk::ExceptionObject& err)
    {
      std::cerr << "ExceptionObject caught !" << std::endl;
      std::cerr << err << std::endl;
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}

