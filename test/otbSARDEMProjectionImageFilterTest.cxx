/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdlib>
#include "itkMacro.h"
#include "otbImage.h"
#include "otbVectorImage.h"
#include "otbImageFileReader.h"
#include "otbImageFileWriter.h"
#include "otbSARDEMProjectionImageFilter.h"

int otbSARDEMProjectionImageFilterTest(int argc, char * argv[])
{
  // Verify the number of parameters in the command line
  if (argc != 4)
    {
      std::cerr << "Usage: " << std::endl;
      std::cerr << argv[0] << " inputDEMFile inputSARImageFile outputImageFile" << std::endl;
      return EXIT_FAILURE;
    }


  // Image Types
  typedef otb::Image <float, 2>    ImageMNTInType;
  typedef otb::Image <std::complex<float>, 2>       ImageSARInType ;
  typedef otb::VectorImage <float , 2> ImageOutType;

  // Readers and Writers Type
  typedef otb::ImageFileReader <ImageMNTInType > ReaderType ;
  typedef otb::ImageFileReader <ImageSARInType > ReaderComplexType ;
  typedef otb::ImageFileWriter <ImageOutType > WriterType ;

  // Instiante reader and writer
  ReaderType::Pointer reader_MNT = ReaderType::New();
  ReaderComplexType::Pointer reader_SAR = ReaderComplexType::New();
  WriterType::Pointer writer = WriterType::New();

  // Input MNT and SAR arguments
  reader_MNT->SetFileName(argv[1]);
  reader_SAR->SetFileName(argv[2]);

  // Writer argument
  writer->SetFileName(argv[3]);

  // Start the first pipeline (Read SAR image metedata)
  ImageSARInType::Pointer SARPtr = reader_SAR->GetOutput();
  reader_SAR->UpdateOutputInformation();

  // Instanciate the filter
  typedef otb::SARDEMProjectionImageFilter<ImageMNTInType, ImageOutType> FilterType;
  FilterType::Pointer filter = FilterType::New();
  filter->SetSARImageKeyWorList(SARPtr->GetImageKeywordlist());

  std::cout<<"Print DEMProjection filter information: "<< std::endl;
  std::cout << filter << std::endl;

  // Define pipeline
  filter->SetInput(reader_MNT->GetOutput());
  writer->SetInput(filter->GetOutput());

  // Execute pipeline
  try
    {
      writer->Update();
    }
  catch (itk::ExceptionObject& err)
    {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}

