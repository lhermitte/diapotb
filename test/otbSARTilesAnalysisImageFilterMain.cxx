#include <cstdlib>
#include <complex>
#include "itkMacro.h"
#include "otbImage.h"
#include "otbVectorImage.h"
#include "otbImageFileReader.h"
#include "otbImageFileWriter.h"
#include "otbTilesAnalysisImageFilter.h"
#include "otbTilesExampleFunctor.h"
#include "otbSARTilesCoregistrationFunctor.h"
#include "otbSARStreamingGridInformationImageFilter.h"

int otbSARTilesAnalysisImageFilterTest(int argc, char * argv[])
{
  // Verify the number of parameters in the command line
  if (argc != 4 )
    {
      std::cerr << "Usage: " << std::endl;
      std::cerr << argv[0] << " inputMasterMLImage inputGrid inputSlaveMLImage " << std::endl;
      return EXIT_FAILURE;
    }


  // Image Types
  typedef otb::Image <std::complex<float> , 2>        ImageInType;
  typedef otb::Image <std::complex<float>, 2>      ImageOutType;
  typedef otb::VectorImage <float , 2> GridType;

  // Readers and Writers Type
  typedef otb::ImageFileReader <ImageInType >   ReaderType ;
  typedef otb::ImageFileReader <GridType >      ReaderGridType ;
  typedef otb::ImageFileWriter <ImageOutType >  WriterType ;

  // Instiante reader and writer
  ReaderType::Pointer reader = ReaderType::New();
  WriterType::Pointer writer = WriterType::New();
  ReaderGridType::Pointer reader_grid = ReaderGridType::New();

  // Input Image
  reader->SetFileName(argv[1]);
  reader_grid->SetFileName(argv[2]);
  
  // Writer argument
  writer->SetFileName(argv[3]);

  // Parameters
  unsigned int sizeTiles = 50;
  unsigned int margin = 7;
  unsigned int gridStep = 150;
  unsigned int sizeWindow = sizeTiles + 2*margin; 
  unsigned int nbRampes = 257;
  double dopAzi = 0.011851326955; // Doppler frequency for slave image


  // Choose the functor with a mode : 
  // mode = 0 => SARTilesExampleFunctor (just a copy of input image)
  // mode = 1 => SARTilesCoregistrationFunctor 
  int mode = 1;

  if (mode)
    {
      // First Pipeline : Read the whole grid
      reader_grid->UpdateOutputInformation();

      // Persitent Filter ;: GridInformation 
      typedef otb::SARStreamingGridInformationImageFilter<GridType> GridInformationFilterType;
      GridInformationFilterType::Pointer filterGridInfo = GridInformationFilterType::New();

      filterGridInfo->SetInput(reader_grid->GetOutput());
      filterGridInfo->SetEstimateMean(false);

      // Retrieve min/max into intput grid for each dimension
      filterGridInfo->Update();
      double minRan, minAzi, maxRan, maxAzi; 
      filterGridInfo->GetMinMax(minRan, minAzi, maxRan, maxAzi);
      
      std::cout << "minRan = " << minRan << std::endl;
      std::cout << "minAzi = " << minAzi << std::endl;
      std::cout << "maxRan = " << maxRan << std::endl;
      std::cout << "maxAzi = " << maxAzi << std::endl;

      double maxAbsRan = std::max(std::abs(minRan), std::abs(maxRan));
      double maxAbsAzi = std::max(std::abs(minAzi), std::abs(maxAzi));

      std::cout << "maxAbsRan = " << maxAbsRan << std::endl;
      std::cout << "maxAbsAzi = " << maxAbsAzi << std::endl;
      
      // Function Type : TilesCoRegistation
      typedef otb::Function::SARTilesCoregistrationFunctor<ImageInType, ImageOutType, GridType> TilesCoRegistrationFunctorType;
      TilesCoRegistrationFunctorType::Pointer functor =  TilesCoRegistrationFunctorType::New();
      functor->SetSizeTiles(sizeTiles);
      functor->SetMargin(margin);
      functor->SetSizeWindow(sizeWindow);
      functor->SetNbRampes(nbRampes);
      functor->SetDopAzi(dopAzi);
      functor->SetGridParameters(reader_grid->GetOutput(), gridStep, gridStep);

      // Instanciate the filter
      typedef otb::TilesAnalysisImageFilter<ImageInType, ImageOutType, TilesCoRegistrationFunctorType> FilterType;
      FilterType::Pointer filter = FilterType::New();
      filter->SetSizeTiles(sizeTiles);
      filter->SetMargin(margin);
      filter->SetConfigForCache(200); // 50 MB available to store tiles
      filter->SetGridParameters(maxAbsRan, maxAbsAzi, gridStep, gridStep);
      filter->SetFunctorPtr(functor);

      std::cout<<"Print Tiles Analysis filter information: "<< std::endl;
      std::cout << filter << std::endl;

      // Define the main Pipeline
      filter->SetImageInput(reader->GetOutput());
      filter->SetGridInput(reader_grid->GetOutput());
      writer->SetInput(filter->GetOutput());

      // Execute pipeline
      try
	{
	  writer->Update();
	}
      catch (itk::ExceptionObject& err)
	{
	  std::cerr << "ExceptionObject caught !" << std::endl;
	  std::cerr << err << std::endl;
	  return EXIT_FAILURE;
	}
    }
  else
    {
      // Function Type : TilesExample
      typedef otb::Function::TilesExampleFunctor<ImageInType, ImageOutType> TilesExFunctorType;
      TilesExFunctorType::Pointer functor =  TilesExFunctorType::New();

      // Instanciate the filter
      typedef otb::TilesAnalysisImageFilter<ImageInType, ImageOutType, TilesExFunctorType> FilterType;
      FilterType::Pointer filter = FilterType::New();
      filter->SetSizeTiles(sizeTiles);
      filter->SetMargin(margin);
      filter->SetConfigForCache(50); // 50 MB available to store tiles
      filter->SetFunctorPtr(functor);

      std::cout<<"Print Tiles Analysis filter information: "<< std::endl;
      std::cout << filter << std::endl;

      // Define the main Pipeline
      filter->SetImageInput(reader->GetOutput());
      writer->SetInput(filter->GetOutput());

      // Execute pipeline
      try
	{
	  writer->Update();
	}
      catch (itk::ExceptionObject& err)
	{
	  std::cerr << "ExceptionObject caught !" << std::endl;
	  std::cerr << err << std::endl;
	  return EXIT_FAILURE;
	}
    }

  return EXIT_SUCCESS;
}


int main(int argc, char * argv[])
{
  int result = otbSARTilesAnalysisImageFilterTest(argc,argv);
  return result;
}
