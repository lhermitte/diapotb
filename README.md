# General 

This is a module for the Diapason processing with OTB
The module must be used with OTB Orfeo Toolbox (https://www.orfeo-toolbox.org/). 
It is designed to work with OTB v7.0 (or higher).

This module can be built as a remote module or as a standalone module

# Getting Started 

DiapOTB is an OTB's official remote module. Its applications and processing chains are directly included inside OTB's package.

Thus, two main ways can be used to install DiapOTB :
* From OTB standalone package (the easy way)
* From scratch (source code)

## Installation

### Installation from OTB standalone package

This installation provides standalone binaries for several OS. You can choose the wanted binaries at https://www.orfeo-toolbox.org/download/

On all platforms, the OTB standalone package contains everything: applications for command line and graphical user interface, python bindings, Monteverdi and also official remote modules (with **DiapOTB**). This installation is extermely simplify by following given instructions : https://www.orfeo-toolbox.org/CookBook/Installation.html 

### Installation from scratch

To install DiapOTB from scratch (from source code), binaries and libraries have to be overrided inside the existing installation of OTB. This override involves installing a “personal” OTB.

Therefore, two main steps are required for a complete installation of DiapOTB form scratch :


**1. OTB installation** 

Find further information at https://www.orfeo-toolbox.org/CookBook/CompilingOTBFromSource.html

`
NB: A Superbuild is available for OTB installation to handle dependencies. With this build, dependencies are directly downloading inside a given repository (-DDOWNLOAD_LOCATION) before being installed.
`

* Clone the repository and choose a branch (usually develop by default)
```
git clone https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb.git
git checkout <develop>
```

* Create a build directory (choose a place)
```
% mkdir build
% cd build
```

* Setup environment for all dependencies and cmake


* Configure for a normal build

Cmake command makes configuration with an example given below (<install_OTB_directory> represents the selected repository for the OTB installation and <root_OTB> the root for the source code):
```
% CC=$GCCHOME/bin/gcc CXX=$GCCHOME/bin/g++ cmake <root_OTB> -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER:FILEPATH=$GCCHOME/bin/g++ -DCMAKE_C_COMPILER:FILEPATH=$GCCHOME/bin/gcc -DCMAKE_CXX_FLAGS="-std=c++14" -DCMAK
E_INSTALL_PREFIX=<install_directory> -DOTB_USE_QWT=ON -DOTB_USE_OPENGL=ON -DOTB_USE_GLEW=ON -DOTB_USE_GLUT=ON -DOTB_USE_QT=ON -DOTB_USE_MUPARSER=ON -DOTB_USE_MUPAR
SERX=ON -DOTB_WRAP_PYTHON=ON -DOTB_USE_LIBSVM=ON -DBUILD_TESTING=OFF -DOTB_I18N_MERGE_TS=OFF -DOTB_USE_CURL=OFF -DOTB_USE_GLFW=OFF -DOTB_USE_GSL=OFF -DOTB_USE_LIBKML=OFF -DOTB_USE_OPENCV=OFF -DOTB_USE_SHARK=OFF
-DOTB_USE_SPTW=OFF
```

`
NB: Configuration is sligthly different with superbuild. Please, find futher information at https://www.orfeo-toolbox.org/CookBook/CompilingOTBFromSource.html?highlight=superbuild
`


* Compile and install
```
% make
% make install
```

**2. DiapOTB installation**

* Clone the repository
```
% git clone
For OTB Gitlab : git clone https://gitlab.orfeo-toolbox.org/remote_modules/diapotb.git
```

* Go to source directory
```
% cd DiapOTB
```

* Create build directory
```
% mkdir build
% cd build
```

* Setup environment for all dependencies and cmake


* Setup OTB environment by setting the following variables (with x.x = OTB version) 
```
% export OTB_DIR=<install_OTB_directory>/lib/cmake/OTB-x.x 
% export LD_LIBRARY_PATH=<install_OTB_directory>/lib:$ LD_LIBRARY_PATH
```

* Configure, compile and install (<install_DiapOTB_directory> represents the selected repository for the OTB installation)
```
% CC=${GCCHOME}/bin/gcc CXX=${GCCHOME}/bin/g++ cmake -DOTB_BUILD_MODULE_AS_STANDALONE=ON -DCMAKE_INSTALL_PREFIX=<install_DiapOTB_directory> -DCMAKE_BUILD_TYPE=<Release or Debug> ..
% make
% make install
```

* Prepare execution by setting:
```
% export OTB_APPLICATION_PATH=<install_OTB_directory>/lib/otb/applications:<install_DiapOTB_directory>/lib
% export LD_LIBRARY_PATH=<install_OTB_directory>/lib/:<install_DiapOTB_directory>/lib/:$LD_LIBRARY_PATH
% export PYTHONPATH=<install_OTB_directory>/lib:<install_OTB_directory>/lib/otb/python:$PATH
% export PATH=<install_OTB_directory>/bin:$PATH
```

Other environment variables can be setted such as OTB_GEOID_FILE or ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS.




## Usage to launch one application (ex : SARMultiLook)

### C++ application

1. Load environment with 
* otbenv for the first kind of installation (standalone package)
* by setting the following variable for the installation from scratch
```
% export OTB_APPLICATION_PATH=<install_OTB_directory>/lib/otb/applications:<install_DiapOTB_directory>/lib
% export LD_LIBRARY_PATH=<install_OTB_directory>/lib/:<install_DiapOTB_directory>/lib/:$LD_LIBRARY_PATH
% export PYTHONPATH=<install_OTB_directory>/lib:<install_OTB_directory>/lib/otb/python:$PATH
% export PATH=<install_OTB_directory>/bin:$PATH
```

Other environment variables can be setted such as OTB_GEOID_FILE or ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS.

2. Get help
```
otbApplicationLauncherCommandLine SARMultiLook
```

3. Execute
```
otbApplicationLauncherCommandLine SARMultiLookApp -incomplex ./Data_In/sar/Reunion/S1A_S4_SLC__without_calibration.SAFE/measurement/s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002.tiff -out ./Data_Out/out_SAR_Diap/s1a-s4-ml-vv.tif -mlran 3 -mlazi 3 -mlgain 0.1
```

### Python

1. Load environment 
* otbenv for the first kind of installation (standalone package)
* by setting the following variable for the installation from scratch
```
% export OTB_APPLICATION_PATH=<install_OTB_directory>/lib/otb/applications:<install_DiapOTB_directory>/lib
% export LD_LIBRARY_PATH=<install_OTB_directory>/lib/:<install_DiapOTB_directory>/lib/:$LD_LIBRARY_PATH
% export PYTHONPATH=<install_OTB_directory>/lib:<install_OTB_directory>/lib/otb/python:$PATH
% export PATH=<install_OTB_directory>/bin:$PATH
```

Other environment variables can be setted such as OTB_GEOID_FILE or ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS.



2. Write a Python file
```
import otbApplication as otb
app = otb.Registry.CreateApplication("SARMultiLook")
app.SetParameterString("incomplex", "s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002.tiff")
app.SetParameterString("out", "output.tif")
app.SetParameterInt("mlran",3)
app.SetParameterInt("mlazi",3)
app.SetParameterFloat("mlgain", 0.1)
app.ExecuteAndWriteOutput()
```

3. Execute
```
python <script_python.py>
```

## Usage to launch DiapOTB processing

1. Configure Python (if needed)
Some packages are required to run DiapOTB's processing chains with :
* GDAL
* h5py
* jsonschema
* numpy

2. Load environment
* otbenv for the first kind of installation (standalone package)
* by setting the following variable for the installation from scratch
```
% export OTB_APPLICATION_PATH=<install_OTB_directory>/lib/otb/applications:<install_DiapOTB_directory>/lib
% export LD_LIBRARY_PATH=<install_OTB_directory>/lib/:<install_DiapOTB_directory>/lib/:$LD_LIBRARY_PATH
% export PYTHONPATH=<install_OTB_directory>/lib:<install_OTB_directory>/lib/otb/python:$PATH
% export PATH=<install_OTB_directory>/bin:$PATH
```

Other environment variables can be setted such as OTB_GEOID_FILE or ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS.

3. Configure your processing with a configuration file
```
Example into <install_directory>/python_src/ex_config repository
or
Launch python <install_directory>/python_src/utils/generateConfigFile.py that corresponds to a user's Q&A.
```

4. Launch the processing with for example
```
python <install_directory>/python_src/diapOTB.py <configuration_file>
```
