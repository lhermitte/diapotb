/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbSARStreamingDEMClosestHgtFilter.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <complex>
#include <cmath>
#include <ctime>
#include <iosfwd>

#include "otb_tinyxml.h"
#include "itksys/SystemTools.hxx"
// include ossim
#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "ossim/ossimTimeUtilities.h"
#include "ossim/base/ossimKeywordlist.h"
#include "ossim/base/ossimString.h"
#include "ossim/base/ossimDate.h"
#pragma GCC diagnostic pop
#else
#include "ossim/ossimTimeUtilities.h"
#include "ossim/base/ossimKeywordlist.h"
#include "ossim/base/ossimString.h"
#include "ossim/base/ossimDate.h"
#endif



enum
{
  Mode_UserDefined,
  Mode_GCP,
  Mode_Orbits
};



// Time/Date ossim 
typedef ossimplugins::time::ModifiedJulianDate TimeType;
typedef ossimplugins::time::Duration           DurationType;

// Keys for ImageKeywordlist
// Gcp keys
const std::string keyAzimuthTime      = "azimuthTime";
const std::string keySlantRangeTime   = "slant_range_time";
const std::string keyImPtX            = "im_pt.x";
const std::string keyImPtY            = "im_pt.y";
const std::string keyWorldPtLat       = "world_pt.lat";
const std::string keyWorldPtLon       = "world_pt.lon";
const std::string keyWorldPtHgt       = "world_pt.hgt";
const std::string GCP_PREFIX          = "support_data.geom.gcp";
const std::string GCP_NUMBER_KEY      = "support_data.geom.gcp.number";
const std::string SUPPORT_DATA_PREFIX = "support_data.";
// Orbit keys
const std::string ORBIT_PREFIX        = "orbitList.orbit";
const std::string ORBIT_NUMBER_KEY    = "orbitList.nb_orbits";
const std::string keyTime             = "time";
const std::string keyPosX             = "x_pos";
const std::string keyPosY             = "y_pos";
const std::string keyPosZ             = "z_pos";
const std::string keyVelX             = "x_vel";
const std::string keyVelY             = "y_vel";
const std::string keyVelZ             = "z_vel";


///// Functions to handle metadata //////
// Get the four corners and estimate all useful info //
void getCorners(otb::ImageKeywordlist imgKWL, std::vector<double *>* vector_lonlat, 
		std::vector<int *>* vector_colrow, std::vector<int> & vector_ind,
		std::vector<TimeType> &  vector_aziTime,
		std::vector<double> & vector_ranTime)
{
  // Always the same order to fill all vector : ul, ur, lr and center

  // Retrieve lon and lat for the four corners from imgKWL
  double ullat = std::atof(imgKWL.GetMetadataByKey("ul_lat").c_str());
  double ullon = std::atof(imgKWL.GetMetadataByKey("ul_lon").c_str());
  double urlat = std::atof(imgKWL.GetMetadataByKey("ur_lat").c_str());
  double urlon = std::atof(imgKWL.GetMetadataByKey("ur_lon").c_str());
  double lrlat = std::atof(imgKWL.GetMetadataByKey("lr_lat").c_str());
  double lrlon = std::atof(imgKWL.GetMetadataByKey("lr_lon").c_str());
  double lllat = std::atof(imgKWL.GetMetadataByKey("ll_lat").c_str());
  double lllon = std::atof(imgKWL.GetMetadataByKey("ll_lon").c_str());
 
  // Create center lon lat
  double centerlat = 0.25 * (ullat + urlat + lrlat + lllat);
  double centerlon = 0.25 * (ullon + urlon + lrlon + lllon);

  // Fill our vectors
  // Lon lat
  double * ul = new double[2];
  double * ur = new double[2];
  double * lr = new double[2];
  double * ll = new double[2];
  double * center = new double[2];
  ul[0] = ullon;
  ul[1] = ullat;
  ur[0] = urlon;
  ur[1] = urlat;
  ll[0] = lllon;
  ll[1] = lllat;
  lr[0] = lrlon;
  lr[1] = lrlat;
  center[0] = centerlon;
  center[1] = centerlat;
  vector_lonlat->push_back(ul);
  vector_lonlat->push_back(ur);
  vector_lonlat->push_back(ll);
  vector_lonlat->push_back(lr);
  vector_lonlat->push_back(center);

  // idx
  vector_ind.push_back(0);
  vector_ind.push_back(1);
  vector_ind.push_back(2);
  vector_ind.push_back(3);
  vector_ind.push_back(4);

  // col row
  int nbRows = std::atoi(imgKWL.GetMetadataByKey("number_lines").c_str());
  int nbCols = std::atoi(imgKWL.GetMetadataByKey("number_samples").c_str());
  int * ul_cl = new int[2];
  int * ur_cl = new int[2];
  int * ll_cl = new int[2];
  int * lr_cl = new int[2];
  int * center_cl = new int[2];
  ul_cl[0] = 0;
  ul_cl[1] = 0;
  ur_cl[0] = nbCols;
  ur_cl[1] = 0;
  ll_cl[0] = 0;
  ll_cl[1] = nbRows;
  lr_cl[0] = nbCols;
  lr_cl[1] = nbRows;
  center_cl[0] = nbCols/2;
  center_cl[1] = nbRows/2;
  vector_colrow->push_back(ul_cl);
  vector_colrow->push_back(ur_cl);
  vector_colrow->push_back(ll_cl);
  vector_colrow->push_back(lr_cl);
  vector_colrow->push_back(center_cl);

  // azi and ran Time vectors
  char prefixAziTime[1024];
  sprintf(prefixAziTime, "%s%s", SUPPORT_DATA_PREFIX.c_str(), "first_line_time");
  char prefixAziTimeInterval[1024];
  sprintf(prefixAziTimeInterval, "%s%s", SUPPORT_DATA_PREFIX.c_str(), "line_time_interval");
  char prefixRange[1024];
  sprintf(prefixRange, "%s%s", SUPPORT_DATA_PREFIX.c_str(), "slant_range_to_first_pixel");
  char prefixRangeRate[1024];
  sprintf(prefixRangeRate, "%s%s", SUPPORT_DATA_PREFIX.c_str(), "range_sampling_rate");

  std::string aziFirstTime = imgKWL.GetMetadataByKey(prefixAziTime);
  std::string ranFirst = imgKWL.GetMetadataByKey(prefixRange);
  std::string aziTimeInterval = imgKWL.GetMetadataByKey(prefixAziTimeInterval);
  std::string ranRate = imgKWL.GetMetadataByKey(prefixRangeRate);

  TimeType time_first_pixel_Date = ossimplugins::time::toModifiedJulianDate(aziFirstTime);
  // Estimate time_line for center and last : time_line = time_first_line + ind_line*aziTimeInterval
  TimeType center_Azitime = time_first_pixel_Date + 
    ossimplugins::time::seconds(center_cl[1]*std::atof(aziTimeInterval.c_str())); 
  TimeType last_Azitime = time_first_pixel_Date + 
    ossimplugins::time::seconds(lr_cl[1]*std::atof(aziTimeInterval.c_str()));
  
  // Estimate range distance for center and last : ran_dist = ran_first_dist + ind_col/ranRate
  double center_ranDist = std::atof(ranFirst.c_str()) + center_cl[0]/(std::atof(ranRate.c_str())); 
  double last_ranDist = std::atof(ranFirst.c_str()) + lr_cl[0]/(std::atof(ranRate.c_str()));

  vector_aziTime.push_back(time_first_pixel_Date);
  vector_aziTime.push_back(time_first_pixel_Date);
  vector_aziTime.push_back(last_Azitime);
  vector_aziTime.push_back(last_Azitime);
  vector_aziTime.push_back(center_Azitime);

  vector_ranTime.push_back(std::atof(ranFirst.c_str()));
  vector_ranTime.push_back(last_ranDist);
  vector_ranTime.push_back(std::atof(ranFirst.c_str()));
  vector_ranTime.push_back(last_ranDist);
  vector_ranTime.push_back(center_ranDist);
}

// Get GCP //
void getGCP(otb::ImageMetadataInterfaceBase::Pointer metadataInterface, 
	    int gcpcount, otb::ImageKeywordlist kwl, 
	    std::vector<double *>* vector_lonlat, 
	    std::vector<int *>* vector_colrow, std::vector<int> & vector_ind)
{
  // Get the count of GCPs and check if metadataInterface is consistent
  // Sometimes (for S1 IW) metadataInterace is not updated
  int gcpcount_interface = metadataInterface->GetGCPCount();

  // if consistent, use it 
  if (gcpcount == gcpcount_interface)
    {
      // Loop on all GCPs
      for (int gcpIdx = 0; gcpIdx < gcpcount; ++gcpIdx)
	{
	  // Fill our vectors
	  // Lon lat
	  double * gcpLonLat = new double[2];
	  gcpLonLat[0] = static_cast<double>(metadataInterface->GetGCPX(gcpIdx));
	  gcpLonLat[1] = static_cast<double>(metadataInterface->GetGCPY(gcpIdx));
	  vector_lonlat->push_back(gcpLonLat);

	  // idx
	  vector_ind.push_back(std::atoi(metadataInterface->GetGCPId(gcpIdx).c_str()));

	  // col row
	  int * gcpColRow = new int[2];
	  gcpColRow[0] = metadataInterface->GetGCPCol(gcpIdx);
	  gcpColRow[1] = metadataInterface->GetGCPRow(gcpIdx);	     
	  vector_colrow->push_back(gcpColRow);
	}
    }
  // else use the kwl
  else
    {
      // Loop on all GCPs
      for (int gcpIdx = 0; gcpIdx < gcpcount; ++gcpIdx)
	{
	  char prefix[1024];
      
	  std::stringstream stream;

	  // Fill our vectors
	  // Lon lat
	  double * gcpLonLat = new double[2];
	  sprintf(prefix, "%s[%d].%s", GCP_PREFIX.c_str(), gcpIdx, keyWorldPtLon.c_str());
	  gcpLonLat[0] = std::atof(kwl.GetMetadataByKey(prefix).c_str());
	  sprintf(prefix, "%s[%d].%s", GCP_PREFIX.c_str(), gcpIdx, keyWorldPtLat.c_str());
	  gcpLonLat[1] = std::atof(kwl.GetMetadataByKey(prefix).c_str());
	  vector_lonlat->push_back(gcpLonLat);

	  // idx
	  vector_ind.push_back(gcpIdx);

	  // col row
	  int * gcpColRow = new int[2];
	  sprintf(prefix, "%s[%d].%s", GCP_PREFIX.c_str(), gcpIdx, keyImPtX.c_str());
	  gcpColRow[0] = std::atoi(kwl.GetMetadataByKey(prefix).c_str());
	  sprintf(prefix, "%s[%d].%s", GCP_PREFIX.c_str(), gcpIdx, keyImPtY.c_str());
	  gcpColRow[1] = std::atoi(kwl.GetMetadataByKey(prefix).c_str());
	  vector_colrow->push_back(gcpColRow);
	}
    }
}

// Create new GCPs and update outKWL //
int createGCPAndUpdateKWL(std::vector<double *>* vector_lonlat, std::vector<double> * vector_hgt,
			  std::vector<int *>* vector_colrow, std::vector<int> vector_ind,
			  std::vector<TimeType>  vector_aziTime,
			  std::vector<double> vector_ranTime, otb::ImageKeywordlist & outKWL)
{
  // Check size (same size for all vectors)
  unsigned int vecSize = vector_lonlat->size();
  if (vector_colrow->size() != vecSize || vector_ind.size() != vecSize ||
      vector_aziTime.size() != vecSize || vector_ranTime.size() != vecSize)
    {
      // retrun 1 => Pb
      return 1;
    }


  std::string gcpCount = std::to_string(vecSize);
  outKWL.AddKey(GCP_NUMBER_KEY, gcpCount);
  // Create or update each GCP
  for (unsigned int i = 0; i < vecSize; i++) 
    {
      // Add the GCP to kwl
      char prefix[1024];
      
      std::stringstream stream;
      
      sprintf(prefix, "%s[%d].%s", GCP_PREFIX.c_str(), i, keyAzimuthTime.c_str());
      outKWL.AddKey(prefix, to_simple_string(vector_aziTime[i]));
      
      sprintf(prefix, "%s[%d].%s", GCP_PREFIX.c_str(), i, keySlantRangeTime.c_str());
      stream << std::fixed << std::setprecision(11) << vector_ranTime[i];
      outKWL.AddKey(prefix, stream.str());

      sprintf(prefix, "%s[%d].%s", GCP_PREFIX.c_str(), i, keyImPtX.c_str());
      outKWL.AddKey(prefix, std::to_string(vector_colrow->at(i)[0]));
      
      sprintf(prefix, "%s[%d].%s", GCP_PREFIX.c_str(), i, keyImPtY.c_str());
      outKWL.AddKey(prefix, std::to_string(vector_colrow->at(i)[1]));

      sprintf(prefix, "%s[%d].%s", GCP_PREFIX.c_str(), i, keyWorldPtLat.c_str());
      stream.str("");
      stream << std::fixed << std::setprecision(11) << vector_lonlat->at(i)[1];
      outKWL.AddKey(prefix, stream.str());

      sprintf(prefix, "%s[%d].%s", GCP_PREFIX.c_str(), i, keyWorldPtLon.c_str());
      stream.str("");
      stream << std::fixed << std::setprecision(11) << vector_lonlat->at(i)[0];
      outKWL.AddKey(prefix, stream.str());

      // only miss the heigth (form MNT)
      sprintf(prefix, "%s[%d].%s", GCP_PREFIX.c_str(), i, keyWorldPtHgt.c_str());
      stream.str("");
      stream << std::fixed << std::setprecision(11) << vector_hgt->at(i);
      outKWL.AddKey(prefix, stream.str());
    }
  return 0;
}

// Update only GCP height and KWL //
int updateGCPAndKWL(std::vector<double> * vector_hgt, unsigned int gcpcount, otb::ImageKeywordlist & outKWL)
{
  // Check vector size with gcp count
  if (vector_hgt->size() != gcpcount)
    {
      std::cout << "Wrong size, does not correspond to the GCP count" << std::endl;
      return 1;
    }
  
  // Create or update each GCP
  for (unsigned int i = 0; i < vector_hgt->size(); i++) 
    {
      // Add the GCP to kwl
      char prefix[1024];
      
      std::stringstream stream;
      
      // only update the keyWorldPtHgt
      sprintf(prefix, "%s[%d].%s", GCP_PREFIX.c_str(), i, keyWorldPtHgt.c_str());
      stream.str("");
      stream << std::fixed << std::setprecision(11) << vector_hgt->at(i);
      outKWL.AddKey(prefix, stream.str());
    }

  return 0;  
}

// Selection for fine orbits (choose only orbits that fit with the image)
int selectOrbits(std::vector<std::string> * vector_time,
		 const otb::ImageKeywordlist inKWL, int & ind_first, int & ind_last)
{
  // Initialize indexes
  ind_first = 0;
  ind_last = vector_time->size() - 1;

  // Get time for first and last original orbit states
  int orbitCount = std::atoi(inKWL.GetMetadataByKey(ORBIT_NUMBER_KEY).c_str());
  
  char prefix_first[1024];
  sprintf(prefix_first, "%s[%d].%s", ORBIT_PREFIX.c_str(), 0, keyTime.c_str());
  std::string UTC_first = inKWL.GetMetadataByKey(std::string (prefix_first));  
 
  char prefix_last[1024];
  sprintf(prefix_last, "%s[%d].%s", ORBIT_PREFIX.c_str(), (orbitCount-1), keyTime.c_str());
  std::string UTC_last = inKWL.GetMetadataByKey(std::string (prefix_last));
  
  // Transform string to t_time (in seconds)
  std::tm tm1 ={};
  std::tm tm2 ={};
  
  std::istringstream UTC_first_ss (UTC_first);
  UTC_first_ss.imbue(std::locale(setlocale(LC_ALL, nullptr)));
  UTC_first_ss >> std::get_time(&tm1, "%Y-%m-%dT%H:%M:%SZ");
  time_t t_first = mktime(&tm1);

  std::istringstream UTC_last_ss (UTC_last);
  UTC_last_ss.imbue(std::locale(setlocale(LC_ALL, nullptr)));
  UTC_last_ss >> std::get_time(&tm2, "%Y-%m-%dT%H:%M:%SZ");
  time_t t_last = mktime(&tm2);
  

  // Selection of indexes into vector_time
  // ind_first = last elt to be inferior to t_first
  // ind_last = first elt to be superior to t_last
  for (unsigned int i = 0; i < vector_time->size(); i++) 
    {
      std::tm tm;
      std::istringstream UTC_ss (vector_time->at(i));
      UTC_ss >> std::get_time(&tm, "%Y-%m-%dT%H:%M:%SZ");
      time_t t = mktime(&tm);

      if (difftime(t, t_first) > 0 && ind_first == 0)
	{
	  if (i !=0)
	    {
	      ind_first = i - 1; 
	    } 
	}
      if (difftime(t_last, t) < 0)
	{
	  ind_last = i;
	  
	  break;
	}
    }

  return 0;
}


// Create new Orbits and update outKWL //
int createOrbitsAndUpdateKWL(std::vector<double *>* vector_posvel, 
			     std::vector<std::string> * vector_time,
			     int ind_first, int ind_last,
			     otb::ImageKeywordlist & outKWL)
{
  // Check size (same size for all vectors)
  unsigned int vecSize = vector_posvel->size();
  if (vector_time->size() != vecSize)
    {
      // retrun 1 => Pb
      return 1;
    }

  int orbitCount = std::atoi(outKWL.GetMetadataByKey(ORBIT_NUMBER_KEY).c_str());
 
  if (vecSize < static_cast<unsigned int>(orbitCount))
    {
      // Fine orbit are inferior to original => pb
      return 1;
    }

  std::string orbitFineCount = std::to_string(ind_last - ind_first + 1);
  outKWL.AddKey(ORBIT_NUMBER_KEY, orbitFineCount);
  
  // Create or update each Orbit state
  for (unsigned int i = 0; 
       i < static_cast<unsigned int>(std::atoi(orbitFineCount.c_str())); i++) 
    {
      // Add the orbit to kwl
      char prefix[1024];
      
      std::stringstream stream;
 
      // time
      sprintf(prefix, "%s[%d].%s", ORBIT_PREFIX.c_str(), i, keyTime.c_str());
      outKWL.AddKey(prefix, vector_time->at(ind_first + i));
      
      // position
      sprintf(prefix, "%s[%d].%s", ORBIT_PREFIX.c_str(), i, keyPosX.c_str());
      stream.str("");
      stream << std::fixed << std::setprecision(6) << vector_posvel->at(ind_first + i)[0];
      outKWL.AddKey(prefix, stream.str());

      sprintf(prefix, "%s[%d].%s", ORBIT_PREFIX.c_str(), i, keyPosY.c_str());
      stream.str("");
      stream << std::fixed << std::setprecision(6) << vector_posvel->at(ind_first + i)[1];
      outKWL.AddKey(prefix, stream.str());

      sprintf(prefix, "%s[%d].%s", ORBIT_PREFIX.c_str(), i, keyPosZ.c_str());
      stream.str("");
      stream << std::fixed << std::setprecision(6) << vector_posvel->at(ind_first + i)[2];
      outKWL.AddKey(prefix, stream.str());
      
      // velocity
      sprintf(prefix, "%s[%d].%s", ORBIT_PREFIX.c_str(), i, keyVelX.c_str());
      stream.str("");
      stream << std::fixed << std::setprecision(6) << vector_posvel->at(ind_first + i)[3];
      outKWL.AddKey(prefix, stream.str());

      sprintf(prefix, "%s[%d].%s", ORBIT_PREFIX.c_str(), i, keyVelY.c_str());
      stream.str("");
      stream << std::fixed << std::setprecision(6) << vector_posvel->at(ind_first + i)[4];
      outKWL.AddKey(prefix, stream.str());

      sprintf(prefix, "%s[%d].%s", ORBIT_PREFIX.c_str(), i, keyVelZ.c_str());
      stream.str("");
      stream << std::fixed << std::setprecision(6) << vector_posvel->at(ind_first + i)[5];
      outKWL.AddKey(prefix, stream.str());
    }
  return 0;
}

///// OTB Application //////
namespace otb
{
  namespace Wrapper
{

class SARMetadataCorrection : public Application
{
public:
  typedef SARMetadataCorrection Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARMetadataCorrection, otb::Wrapper::Application);


  // Filter
  typedef otb::SARStreamingDEMClosestHgtFilter<FloatImageType> DEMClosestHgtFilterType;

private:
  void DoInit() override
  {
    SetName("SARMetadataCorrection");
    SetDescription("SAR metadata correction.");

    SetDocLongDescription("This application corrects metadata with a focus on GCP.");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "indem",   "Input DEM");
    SetParameterDescription("indem", "Input DEM to extract height data.");

    AddParameter(ParameterType_InputImage,  "insar",   "Input SAR image");
    SetParameterDescription("insar", "SAR Image to extract initial metadata.");

    AddParameter(ParameterType_InputFilename, "infineorbits", "Input file to get precise orbits");
    SetParameterDescription("infineorbits","Input file to get precise orbits.");
    MandatoryOff("infineorbits");

    // mode selection : gcp (auto) or fine orbits
    AddParameter(ParameterType_Choice, "mode", "Parameters estimation modes");
    AddChoice("mode.auto", "User Defined");
    SetParameterDescription("mode.auto", "This mode allows you to fully modify default values : GCP corrections");
    AddChoice("mode.gcp", "GCP corrections");
    SetParameterDescription("mode.gcp",
                            "This mode allows you to automatically correct GCP into the output geom file");
    AddChoice("mode.orbits", "Fine orbits");
    SetParameterDescription("mode.orbits",
                            "This mode allows you to automatically retrieve the fine orbits and to save into a new geom file");
    

    AddParameter(ParameterType_OutputFilename, "outkwl", "Write the OSSIM keywordlist to a geom file");
    SetParameterDescription("outkwl", "This option allows extracting the OSSIM keywordlist of the image into a geom file.");
    MandatoryOff("outkwl");
    
    AddRAMParameter();

    SetDocExampleParameterValue("indem","S21E055.hgt");
    SetDocExampleParameterValue("insar","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff");
  }

  void DoUpdateParameters() override
  {
    // Handle inputs following the mode
    // chose by the user
    switch (GetParameterInt("mode"))
      {
      case Mode_UserDefined:
	{
	  EnableParameter("indem");
	  EnableParameter("outkwl");
	  DisableParameter("infineorbits");

	  MandatoryOn("indem");
	  MandatoryOff("outkwl");
	  MandatoryOff("infineorbits");
	}
      break;
      case Mode_GCP:
	{
	  EnableParameter("indem");
	  EnableParameter("outkwl");
	  DisableParameter("infineorbits");
	  
	  MandatoryOn("indem");
	  MandatoryOff("outkwl");
	  MandatoryOff("infineorbits");
	}
      break;
      case Mode_Orbits:
	{
	  DisableParameter("indem");
	  EnableParameter("outkwl");
	  EnableParameter("infineorbits");

	  MandatoryOff("indem");
	  MandatoryOn("outkwl");
	  MandatoryOn("infineorbits");
	}
      break;
      } // switch (GetParameterInt("mode") )
  }

  void DoExecute() override
  {  
    std::ostringstream ossOutput;

    ////// Get inputs and keywordlist ///// 
    ComplexFloatImageType::Pointer SARPtr = GetParameterComplexFloatImage("insar");
 
    // Read information
    typedef otb::ImageMetadataInterfaceBase ImageMetadataInterfaceType;
    ImageMetadataInterfaceType::Pointer metadataInterface = ImageMetadataInterfaceFactory::CreateIMI(SARPtr->GetMetaDataDictionary());
  
    SARPtr->UpdateOutputInformation();
    
    ImageKeywordlist outputKWL = SARPtr->GetImageKeywordlist();


    // Get mode to adapt estimation according to user choice
    bool doGcpCorrection = false;
    bool doFineOrbits = false;

    switch (GetParameterInt("mode"))
      {
      case Mode_UserDefined:
	{
	  doGcpCorrection = true;
	}
      break;
      case Mode_GCP:
	{
	  doGcpCorrection = true;
	}
      break;
      case Mode_Orbits:
	{
	  doFineOrbits = true;
	}
      break;
      } // switch (GetParameterInt("mode") )
   
    /////// GCP Correction ///////
    if (doGcpCorrection)
      {
	FloatImageType::Pointer inputDEM =  GetParameterFloatImage("indem");
	// Check with metadataInterface and keyword List
	int gcpcount = metadataInterface->GetGCPCount();

	if (gcpcount == 0)
	  {
	    int gcpcount_kwl = std::atoi(outputKWL.GetMetadataByKey(GCP_NUMBER_KEY).c_str());
	    if (gcpcount_kwl > 5)
	      {
		gcpcount = gcpcount_kwl;
	      }
	  }

	std::vector<double *> * vector_lonlat = new std::vector<double *>(); 
	std::vector<int *>   * vector_colrow = new std::vector<int *>(); 
	std::vector<int> vector_ind; 
	std::vector<TimeType>  vector_aziTime;
	std::vector<double>  vector_ranTime;

	// Build the input (vector_lonlat) for GetClosestMNTHeight filter
	if (gcpcount == 0)
	  {
	    // If no GCP => Retrieve Corners instead
	    // Get latitude and longitude for ur, ul, ll lr and center points
	    try
	      {
		getCorners(metadataInterface->GetImageKeywordlist(), vector_lonlat, 
			   vector_colrow, vector_ind, vector_aziTime,
			   vector_ranTime);
	      }
	    catch (itk::ExceptionObject& /*err*/)
	      {
	      }
	  }
	else
	  {	
	    // If GCPs => Retrieve its
	    // Get latitude and longitude for each of its
	    getGCP(metadataInterface, gcpcount, outputKWL, vector_lonlat, vector_colrow, vector_ind);
	  }
    
    
	///// DEMClosestHgt filter to estimate accurate heights /////
	DEMClosestHgtFilterType::Pointer filterDEMClosestHgt = DEMClosestHgtFilterType::New();
	m_Ref.push_back(filterDEMClosestHgt.GetPointer());
   
	// Set inputs : DEM and the vector_LonLat
	filterDEMClosestHgt->SetInput(inputDEM);
	filterDEMClosestHgt->SetVectorLonLat(vector_lonlat);

	// Init a vector for hgt
	std::vector<double> * vector_hgt = new vector<double>();
	vector_hgt->resize(vector_lonlat->size()); // Same size than lon and lat
    
	for (unsigned int k = 0; k < vector_lonlat->size(); k++) 
	  {
	    vector_hgt->at(k) = 0;
	  }
    
	// Launch our Persistent filter
	filterDEMClosestHgt->Update();
	filterDEMClosestHgt->GetDEMHgt(vector_hgt);

	// Add or update GCP into the output KWL (according to the sensor (GCPs already present or not))
	if (gcpcount == 0)
	  {
	    // Create GPCs with the four corners and center
	    createGCPAndUpdateKWL(vector_lonlat, vector_hgt,
				  vector_colrow, vector_ind,
				  vector_aziTime,
				  vector_ranTime, outputKWL);
	  }
	else
	  {
	    // Just update heigths
	    updateGCPAndKWL(vector_hgt, gcpcount, outputKWL);
	  }
    
    
	// Display image information in  the dedicated logger
	otbAppLogINFO(<< ossOutput.str());

	// Free Memory
	for (unsigned int itab = 0; itab < vector_lonlat->size(); itab++) 
	  {
	    delete vector_lonlat->at(itab);
	    vector_lonlat->at(itab) = 0;
	  }
	vector_lonlat->clear();
	delete vector_lonlat;
	vector_lonlat = 0;

	vector_hgt->clear();
	delete vector_hgt;
	vector_hgt = 0;

    
	for (unsigned int itab = 0; itab < vector_colrow->size(); itab++) 
	  {
	    delete vector_colrow->at(itab);
	    vector_colrow->at(itab) = 0;
	  }
	vector_colrow->clear();
	delete vector_colrow;
	vector_colrow = 0;

	vector_ind.clear();
	vector_aziTime.clear();
	vector_ranTime.clear();
      }

    /////// Fine orbits ///////
    if (doFineOrbits)
      {
	// Add into SAR Image keyword list, the precise metadata
	std::string inFile = GetParameterString("infineorbits");
	

	// Check that the right extension is given : expected .EOF */
	std::string extension = itksys::SystemTools::GetFilenameLastExtension(inFile);
	if (itksys::SystemTools::LowerCase(extension) != ".eof")
	  {
	    itkExceptionMacro(<< extension << " is a wrong Extension FileName : Expected .EOF");
	  }

	// Open the xml file
	TiXmlDocument doc(inFile.c_str());
	if (!doc.LoadFile())
	  {
	    itkExceptionMacro(<< "Can't open file " << inFile);
	  }

	TiXmlHandle   hDoc(&doc);

	// Get root node
	TiXmlElement* root = hDoc.FirstChildElement("Earth_Explorer_File").ToElement();

	if (root)
	  {
	    // Init vectors 
	    std::vector<double *> * vector_posvel = new std::vector<double *>(); 
	    std::vector<std::string> * vector_time = new std::vector<std::string>();

	    // Get Data_Block
	    TiXmlElement* dataBlock = root->FirstChildElement("Data_Block");
	    
	    // Retrieve the number of orbits
	    int count = std::atoi(dataBlock->FirstChildElement("List_of_OSVs")->Attribute("count"));

	    int count_loop = 0;
	    
	    // Iterate through the tree to get all the orbits
	    // Fill vectors with its 
	    for (TiXmlElement* currentOrb = dataBlock->FirstChildElement("List_of_OSVs")->FirstChildElement(); 
		 currentOrb != nullptr; currentOrb = currentOrb->NextSiblingElement())
	      {
		double * posvel = new double[6];
		
		std::string UTCTime = std::string(currentOrb->FirstChildElement("UTC")->GetText());
		UTCTime = UTCTime.substr(UTCTime.find("=")+1, UTCTime.length());
		
		posvel[0] = std::atof(currentOrb->FirstChildElement("X")->GetText());
		posvel[1] = std::atof(currentOrb->FirstChildElement("Y")->GetText());
		posvel[2] = std::atof(currentOrb->FirstChildElement("Z")->GetText());
		posvel[3] = std::atof(currentOrb->FirstChildElement("VX")->GetText());
		posvel[4] = std::atof(currentOrb->FirstChildElement("VY")->GetText());
		posvel[5] = std::atof(currentOrb->FirstChildElement("VZ")->GetText());

		vector_posvel->push_back(posvel);
		vector_time->push_back(std::string(UTCTime));

		++count_loop;
	      }

	    int ind_first, ind_last;
	    selectOrbits(vector_time, outputKWL, ind_first, ind_last);

    
	    // Check coherency between the number of orbits
	    if (count != count_loop)
	      {
		itkExceptionMacro(<< "Problem of coherency into EOF file");
	      }


	    int ret = createOrbitsAndUpdateKWL(vector_posvel, vector_time, ind_first,
					       ind_last, outputKWL);

	    if (ret == 1)
	      {
		itkExceptionMacro(<< "Problem during orbits update");
	      }

	    // Free Memory
	    for (unsigned int itab = 0; itab < vector_posvel->size(); itab++) 
	      {
		delete vector_posvel->at(itab);
		vector_posvel->at(itab) = 0;
	      }
	    vector_posvel->clear();
	    delete vector_posvel;
	    vector_posvel = 0;
	    
	    vector_time->clear();
	    delete vector_time;
	    vector_time = 0;
	  }
      }

    /////// Write output keywordlist with correct gcp (correction on heigth) ///////
    if (IsParameterEnabled("outkwl") && HasValue("outkwl"))
      {
	WriteGeometry(outputKWL, GetParameterString("outkwl"));
      }
  }


  // Vector for filters 
  std::vector<itk::ProcessObject::Pointer> m_Ref;

};

}
  
}
OTB_APPLICATION_EXPORT(otb::Wrapper::SARMetadataCorrection)
