/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbSARDerampImageFilter.h"

#include "otbWrapperOutputImageParameter.h"
#include "otbWrapperTypes.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{

  namespace Wrapper
  {

    class SARDeramp : public Application
    {
    public:
      typedef SARDeramp Self;
      typedef itk::SmartPointer<Self> Pointer; 

      itkNewMacro(Self);
      itkTypeMacro(SARDeramp, otb::Wrapper::Application);

      // Filter
      typedef otb::SARDerampImageFilter<ComplexFloatImageType> DerampFilterType;

    private:
      void DoInit() override
      {
	SetName("SARDeramp");
	SetDescription("Deramping/Reramping of S1 IW Burst.");

	SetDocLongDescription("This application does the deramping or reramping of S1 Iw burst.");

	//Optional descriptors
	SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
	SetDocAuthors("OTB-Team");
	SetDocSeeAlso(" ");
	AddDocTag(Tags::SAR);
	AddDocTag("DiapOTB");

	//Parameter declarations
	AddParameter(ParameterType_InputImage,  "in",   "Input burst");
	SetParameterDescription("in", "Input burst (from S1 Iw product).");

	AddParameter(ParameterType_InputImage,  "inslave",   "Input SAR slave image (for interferometry processing");
	SetParameterDescription("inslave", "Slave SAR Image to extract SAR geometry (interferometry).");

	AddParameter(ParameterType_OutputImage, "out", "Output burst");
	SetParameterDescription("out","Output burst (deramped or reramped).");

	AddParameter(ParameterType_InputImage,  "ingrid",   "Input deformation grid");
	SetParameterDescription("ingrid", "Input deformation grid.");

	AddParameter(ParameterType_Int,  "gridsteprange", "Grid Step for range dimension into SLC geometry");
	SetParameterDescription("gridsteprange", "Grid Step for range dimension into  SLC geometry.");
	SetDefaultParameterInt("gridsteprange", 150);
	MandatoryOff("gridsteprange");

	AddParameter(ParameterType_Int,  "gridstepazimut", "Grid Step for azimut dimension into SLC geometry");
	SetParameterDescription("gridstepazimut", "Grid Step for azimut dimension into  SLC geometry.");
	SetDefaultParameterInt("gridstepazimut", 150);
	MandatoryOff("gridstepazimut");

	AddParameter(ParameterType_Bool, "reramp", "Activate Reramping mode");
	SetParameterDescription("reramp", "If true, then reramping else deramping.");

	AddParameter(ParameterType_Bool, "shift", "Activate Shift mode (To reramp/deramp accordingly a deformation grid)");
	SetParameterDescription("shift", "If true, reramp/deramp accordingly a deformation grid (interferometry processing).");

	AddRAMParameter();

	SetDocExampleParameterValue("in","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002.tiff");
	SetDocExampleParameterValue("out","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_Deramp.tiff");
      }


      void DoUpdateParameters() override
      {
	// Handle inputs following the mode chose by the user : shift mode
	if (IsParameterEnabled("shift"))
	  {
	    // If shift mode : grid and slave image mandatory
	    EnableParameter("ingrid");
	    EnableParameter("inslave");

	    MandatoryOn("ingrid");
	    MandatoryOn("inslave");
	  }
	else
	  {
	    // Classic deramp or reramp without deformation grid of slave metadata
	    DisableParameter("ingrid");
	    DisableParameter("inslave");

	    MandatoryOff("ingrid");
	    MandatoryOff("inslave");

	  }
	
      }

      void DoExecute() override
      { 
	// Check if reramp is enabled
	bool derampMode = true;
	if (IsParameterEnabled("reramp"))
	  {
	    derampMode = false;
	  }

	if (derampMode)
	  {
	    otbAppLogINFO(<<"Deramp Mode");
	  }
	else
	  {
	    otbAppLogINFO(<<"Reramp Mode");
	  }

	// Check if shift is enabled
	bool shiftMode = false;
	if (IsParameterEnabled("shift"))
	  {
	    shiftMode = true;
	    
	    // Check if required argument are there
	    if (!GetParameterByKey("ingrid")->HasValue() || !GetParameterByKey("gridsteprange")->HasValue() ||
		!GetParameterByKey("gridstepazimut")->HasValue() || !GetParameterByKey("inslave")->HasValue())
	      {
		// Throw an execption
		throw std::runtime_error("Missing arguments for shift mode. Arguments required are : ingrid, gridsteprange, gridstepazimut, inslave");
	      }
	  }

	if (shiftMode)
	  {
	    otbAppLogINFO(<<"Shift Mode");
	  }

	// Instanciate the Interferogram filter
	DerampFilterType::Pointer filterDeramp = DerampFilterType::New();
	m_Ref.push_back(filterDeramp.GetPointer());
	filterDeramp->SetDerampMode(derampMode);
	filterDeramp->SetShiftMode(shiftMode);
	if (shiftMode)
	  {
		filterDeramp->SetGridInput(GetParameterFloatVectorImage("ingrid"));
		filterDeramp->SetSARImageKeyWorList(GetParameterComplexFloatImage("inslave")->GetImageKeywordlist());

		// Get numerical parameters
	       int gridStepRan = GetParameterInt("gridsteprange");
	       int gridStepAzi = GetParameterInt("gridstepazimut");
	       filterDeramp->SetGridStep(gridStepRan, gridStepAzi);
	  }	

	// Pipeline
	filterDeramp->SetImageInput(GetParameterComplexFloatImage("in"));
	SetParameterOutputImage("out", filterDeramp->GetOutput());

      }
      // Vector for filters 
      std::vector<itk::ProcessObject::Pointer> m_Ref;

    };

  }

}


 
OTB_APPLICATION_EXPORT(otb::Wrapper::SARDeramp)
