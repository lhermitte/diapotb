/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbSARDEMPolygonsAnalysisImageFilter.h"
#include "otbSARAmplitudeEstimationFunctor.h"

#include "otbWrapperOutputImageParameter.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{
namespace Wrapper
{

class SARAmplitudeEstimation : public Application
{
public:
  typedef SARAmplitudeEstimation Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARAmplitudeEstimation, otb::Wrapper::Application);

  // Pixels
  typedef typename FloatVectorImageType::PixelType       ImageInPixelType;
  typedef typename FloatImageType::PixelType             ImageOutPixelType;   

  // Function
typedef otb::Function::SARPolygonsFunctor<ImageInPixelType, ImageOutPixelType> PolygonsFunctorType;
  typedef otb::Function::SARAmplitudeEstimationFunctor<ImageInPixelType, ImageOutPixelType> AmpFunctorType;

  // Filters
  typedef otb::SARDEMPolygonsAnalysisImageFilter < FloatVectorImageType, FloatImageType, FloatImageType, ComplexFloatImageType, AmpFunctorType > FilterType;

private:
  void DoInit() override
  {
    SetName("SARAmplitudeEstimation");
    SetDescription("SAR Amplitude estimation thanks to the associated DEM.");

    SetDocLongDescription("This application estimates a simulated amplitude image thanks to a DEM file.");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "indemproj",   "Input vector of DEM projected into SAR geometry");
    SetParameterDescription("indemproj", "Input vector image for amplitude estimation.");

    AddParameter(ParameterType_InputImage,  "indem",   "Input DEM");
    SetParameterDescription("indem", "DEM to extract DEM geometry.");

    AddParameter(ParameterType_InputImage,  "insar",   "Input SAR image");
    SetParameterDescription("insar", "SAR Image to extract SAR geometry.");

    AddParameter(ParameterType_Float,  "gain",   "Multiplying gain");
    SetParameterDescription("gain", "Multiplying gain to obtain a mean radiometry at 100.");

    AddParameter(ParameterType_Int,  "nodata", "No Data values for the DEM");
    SetParameterDescription("nodata", "No Data values for the DEM.");
    SetDefaultParameterInt("nodata", -32768);
    MandatoryOff("nodata");

    AddParameter(ParameterType_Int,  "directiondemc", "Range direction for DEM scan");
    SetParameterDescription("directiondemc", "Range direction for DEM scan.");
    SetDefaultParameterInt("directiondemc", 1);
    MandatoryOff("directiondemc");

    AddParameter(ParameterType_Int,  "directiondeml", "Azimut direction for DEM scan");
    SetParameterDescription("directiondeml", "Azimut direction for DEM scan.");
    SetDefaultParameterInt("directiondeml", 1);
    MandatoryOff("directiondeml");

    AddParameter(ParameterType_OutputImage, "out", "Output amplitude Image");
    SetParameterDescription("out","Output  amplitude Image.");

    AddRAMParameter();

    SetDocExampleParameterValue("indemproj","CLZY_S21E055.tiff");
    SetDocExampleParameterValue("indem","S21E055.hgt");
    SetDocExampleParameterValue("insar","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff");
    SetDocExampleParameterValue("out","s1a-s4-simu-amplitude.tiff");
  }

  void DoUpdateParameters() override
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute() override
  {  
    // Get numeric parameters : gain and directions for DEM scan
    double gain = GetParameterFloat("gain");
    int noData = GetParameterInt("nodata");
    int DEMScanDirectionC = GetParameterInt("directiondemc");
    int DEMScanDirectionL = GetParameterInt("directiondeml");

    otbAppLogINFO(<<"Gain : "<<gain);
    otbAppLogINFO(<<"Direction (DEM scan) in range : "<<DEMScanDirectionC);
    otbAppLogINFO(<<"Direction (DEM scan) in azimut : "<<DEMScanDirectionL);
    otbAppLogINFO(<<"No Data : "<<noData);
    
    // Start the first pipelines (Read SAR and DEM image metedata)
    ComplexFloatImageType::Pointer SARPtr = GetParameterComplexFloatImage("insar");
    FloatImageType::Pointer DEMPtr = GetParameterFloatImage("indem");


    // Function Type : SARAmplitudeEstimationFunctor
    AmpFunctorType::Pointer functor =  AmpFunctorType::New(gain);
    
    // AmplitudeEstimation Filter (use SARDEMPolygonsAnalysisImageFilter with SARAmplitudeEstimationFunctor
    // to estimate amplitude image)
    FilterType::Pointer filterAmplitudeEstimation = FilterType::New();
    m_Ref.push_back(filterAmplitudeEstimation.GetPointer());
    filterAmplitudeEstimation->SetSARImageKeyWorList(SARPtr->GetImageKeywordlist());
    filterAmplitudeEstimation->SetSARImagePtr(SARPtr);
    filterAmplitudeEstimation->SetDEMImagePtr(DEMPtr);
    filterAmplitudeEstimation->SetDEMInformation(gain, DEMScanDirectionC, DEMScanDirectionL);
    filterAmplitudeEstimation->SetFunctorPtr(functor);
    filterAmplitudeEstimation->initializeMarginAndRSTransform();
    filterAmplitudeEstimation->SetNoData(noData);

    // Define the main pipeline (controlled with extended FileName)
    std::string origin_FileName = GetParameterString("out");

    // Check if FileName is extended (with the ? caracter)
    // If not extended then override the FileName
    if (origin_FileName.find("?") == std::string::npos && !origin_FileName.empty()) 
      {
	std::string extendedFileName = origin_FileName;

	// Get the ram value (in MB)
	int ram = GetParameterInt("ram");
	
	// Define with the ram value, the number of lines for the streaming
	int nbColSAR = SARPtr->GetLargestPossibleRegion().GetSize()[0];
	int nbLinesSAR = SARPtr->GetLargestPossibleRegion().GetSize()[1];
	// To determine the number of lines : 
	// nbColSAR * nbLinesStreaming * sizeof(OutputPixel) = RamValue/2 (/2 to be sure)
	// OutputPixel are float => sizeof(OutputPixel) = 4 (Bytes)
	long int ram_In_KBytes = (ram/2) * 1024;
	long int nbLinesStreaming = (ram_In_KBytes / (nbColSAR)) * (1024/4);
	
	// Check the value of nbLinesStreaming
	int nbLinesStreamingMax = 10000;
	if (nbLinesStreamingMax > nbLinesSAR)
	  {
	    nbLinesStreamingMax = nbLinesSAR;
	  }
	if (nbLinesStreaming <= 0 || nbLinesStreaming >  nbLinesStreamingMax)
	  {
	    nbLinesStreaming =  nbLinesStreamingMax;
	  } 
	
	// Construct the extendedPart
	std::ostringstream os;
	os << "?&streaming:type=stripped&streaming:sizevalue=" << nbLinesStreaming;
	
	// Add the extendedPart
	std::string extendedPart = os.str();
	extendedFileName.append(extendedPart);
	
	// Set the new FileName with extended options
	SetParameterString("out", extendedFileName);
      }

    // Execute the main Pipeline
    filterAmplitudeEstimation->SetInput(GetParameterImage("indemproj"));
    SetParameterOutputImage("out", filterAmplitudeEstimation->GetOutput());
  }
  // Vector for filters 
  std::vector<itk::ProcessObject::Pointer> m_Ref;

};


}

}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARAmplitudeEstimation)
