/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbImageFileReader.h"
#include "otbSarSensorModelAdapter.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <vector>

namespace otb
{
namespace Wrapper
{

class SARAltAmbig : public Application
{
public:
  typedef SARAltAmbig Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARAltAmbig, otb::Wrapper::Application);


private:
  void DoInit() override
  {
    SetName("SARAltAmbig");
    SetDescription("Evaluation of ambiguity height");

    SetDocLongDescription("Calculate the angle alpha between "
			  "ground-master and ground-slave vectors. "
			  "Calculates the local incidence. The ambiguity "
			  "heigth is given by: "
			  "lambda * sin (incidence) / 2 * tg (alpha)");

    //Optional descriptors
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImageList, "inlist", "Input images list (for estimations)");
    SetParameterDescription("inlist", "The list of images to estimate required values (local vecotrs, ambiguity ...).");

    AddParameter(ParameterType_Float, "lat", "Tartget lat (degree)");
    SetParameterDescription("lat", "Target Latitude (degree)");
    SetMinimumParameterFloatValue("lat", -90.);
    SetMaximumParameterFloatValue("lat", 90.);
    
    AddParameter(ParameterType_Float, "lon", "Target long (degree)");
    SetParameterDescription("lon", "Target Longitude (degree)");
    SetMinimumParameterFloatValue("lon", -360.);
    SetMaximumParameterFloatValue("lon", 360.);
    
    AddParameter(ParameterType_Float, "height", "Target height (m)");
    SetParameterDescription("height", "Target Height (m)");
    SetMinimumParameterFloatValue("height", -100.);
    SetMaximumParameterFloatValue("height", 9000.);

    AddParameter(ParameterType_OutputFilename, "outfile", "Output file to store the results");
    SetParameterDescription("outfile","Output file to store the results");

    AddParameter(ParameterType_Bool, "bistatic", "Activate bistatic Mode");
    SetParameterDescription("bistatic", "If true, set the formula in bistatic mode");
    MandatoryOff("bistatic");

    AddRAMParameter();

    SetDocExampleParameterValue("inlist", "s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff s1a_s6_slc_0180416T195057_vv_cap_verde.tif");
    SetDocExampleParameterValue("lat", "10.5");
    SetDocExampleParameterValue("lon", "5.8");
    SetDocExampleParameterValue("height", "45.9");
    SetDocExampleParameterValue("outfile", "alt_ambig.log");
    
  }

  void DoUpdateParameters() override
  {
     // Nothing to do here : all parameters are independent
  }

  void DoExecute() override
  {
    
    //////////// Get lat, lon and height (same for all couples) ////////////
    float lat = GetParameterFloat("lat");
    otbAppLogINFO(<<"Lat : "<<lat);
    
    float lon = GetParameterFloat("lon");
    otbAppLogINFO(<<"Lon : "<<lon);
    
    if (lon < 0)
      lon = 360.0 + lon;

    float height = GetParameterFloat("height");
    otbAppLogINFO(<<"Height : "<<height);
    
    bool bistatic = IsParameterEnabled("bistatic");
    otbAppLogINFO(<<"Bistatic : " << bistatic);

    // Estimation on target
    SarSensorModelAdapter::Point3DType demGeoPoint;
    SarSensorModelAdapter::Point3DType xyzCart;

    demGeoPoint[0] = lon;
    demGeoPoint[1] = lat;
    demGeoPoint[2] = height;
   
    // Cartesian conversion
    SarSensorModelAdapter::WorldToCartesian(demGeoPoint, xyzCart);
    otbAppLogDEBUG(<<"Cartesian coords : " << xyzCart[0] << " " << xyzCart[1] << " " << xyzCart[2]);
    

    //////////// Create couples and global storage ///////////////
    std::vector<double *> altAmbigVector;
    std::map<int, double *> groundMap;
    std::map<int, std::string> namesMap;

    // Get List parameters
    FloatVectorImageListType::Pointer inList = this->GetParameterImageList("inlist");
    std::vector<std::string> inListNames = GetParameterStringList("inlist");

    //////////// For each couple ///////////////
    // Nested lopp to create all couples
    for (unsigned int i = 0; i < (inList->Size() - 1); i++) 
      {
	for (unsigned int j = i+1; j < inList->Size(); j++) 
	  {
	    // Start the target pipeline (Read SAR image metadata)
	    FloatVectorImageType::Pointer SARPtr = inList->GetNthElement(i);
	    SARPtr->UpdateOutputInformation();

	    // Start the target pipeline (Read SAR image metadata)
	    FloatVectorImageType::Pointer SARPtr_slave = inList->GetNthElement(j);
	    SARPtr_slave->UpdateOutputInformation();

	    // Get image keywordlists
	    otb::ImageKeywordlist sarKWL = SARPtr->GetImageKeywordlist();
	    otb::ImageKeywordlist sarKWL_slave = SARPtr_slave->GetImageKeywordlist();

	    // Create and Initilaze the SarSensorModelAdapter
	    SarSensorModelAdapter::Pointer m_SarSensorModelAdapter = SarSensorModelAdapter::New();
	    bool loadOk = m_SarSensorModelAdapter->LoadState(sarKWL);

	    if(!loadOk || !m_SarSensorModelAdapter->IsValidSensorModel())
	      {
		itkExceptionMacro(<<"SAR image does not contain a valid SAR sensor model.");
	      }

	    SarSensorModelAdapter::Point3DType satellitePosition[2];
	    SarSensorModelAdapter::Point3DType satelliteVelocity[2];

	    // Results allocations
	    double * resultsAltAmbig = new double[6];
	    altAmbigVector.push_back(resultsAltAmbig);


	    // Image names 
	    std::string master = getFilename(inListNames[i]);
	    std::string slave = getFilename(inListNames[j]);

	    // Image orbits : manifest_data.abs_orbit or support_data.abs_orbit according to the sensor
	    int master_orbit = 0;
	    int slave_orbit = 0;

	    if (sarKWL.HasKey("support_data.abs_orbit") && sarKWL_slave.HasKey("support_data.abs_orbit"))
	      {
		master_orbit = std::atoi(sarKWL.GetMetadataByKey("support_data.abs_orbit").c_str());
		slave_orbit = std::atoi(sarKWL_slave.GetMetadataByKey("support_data.abs_orbit").c_str());
	      }
	    else if (sarKWL.HasKey("manifest_data.abs_orbit") && sarKWL_slave.HasKey("manifest_data.abs_orbit")) 
	      {
		master_orbit = std::atoi(sarKWL.GetMetadataByKey("manifest_data.abs_orbit").c_str());
		slave_orbit = std::atoi(sarKWL_slave.GetMetadataByKey("manifest_data.abs_orbit").c_str());
	      }
	  else 
	    {
	      // Send an exception
	      itkExceptionMacro(<<"Impossible to get orbit number, please check your input images");
	    }

	    namesMap[master_orbit] = master;
	    namesMap[slave_orbit] = slave;

	    resultsAltAmbig[0] = static_cast<double>(master_orbit);
	    resultsAltAmbig[1] = static_cast<double>(slave_orbit);

	    // Satellite position and velocity
	    // Master
	    otbAppLogDEBUG(<<"Inmaster Image : ");
	    m_SarSensorModelAdapter->WorldToSatPositionAndVelocity(demGeoPoint, satellitePosition[0], satelliteVelocity[0]);

	    otbAppLogDEBUG(<<"Satellite Position : " << satellitePosition[0][0] << " " << satellitePosition[0][1] << " " << satellitePosition[0][2]);
	    otbAppLogDEBUG(<<"Satellite Velocity : " << satelliteVelocity[0][0] << " " << satelliteVelocity[0][1] << " " << satelliteVelocity[0][2]);
    
	    // Slave
	    loadOk = m_SarSensorModelAdapter->LoadState(sarKWL_slave);
	    if(!loadOk || !m_SarSensorModelAdapter->IsValidSensorModel())
	      {
		itkExceptionMacro(<<"SAR image does not contain a valid SAR sensor model.");
	      }

	    otbAppLogDEBUG(<<"Target Image : ");
	    m_SarSensorModelAdapter->WorldToSatPositionAndVelocity(demGeoPoint, satellitePosition[1], satelliteVelocity[1]);
	    otbAppLogDEBUG(<<"Satellite Position : " << satellitePosition[1][0] << " " << satellitePosition[1][1] << " " << satellitePosition[1][2]);
	    otbAppLogDEBUG(<<"Satellite Velocity : " << satelliteVelocity[1][0] << " " << satelliteVelocity[1][1] << " " << satelliteVelocity[1][2]);

	    // Calculate lambda
	    double radarFreq = atof(sarKWL.GetMetadataByKey("support_data.radar_frequency").c_str());
	    const double C = 299792458.;
	    float lambda = C/radarFreq;
	    otbAppLogDEBUG(<<"Lambda : " << lambda);

	    // Estimations : mean incidence, ambig_heigth, radial coord, lateral coord
	    otbAppLogDEBUG(<< "Compute Alt Ambig");
	    computeAltAmbig(xyzCart, satellitePosition, satelliteVelocity, lambda, master, 
			    slave, &resultsAltAmbig[2]);

	    // Local vector estimation (if not already done)
	    if (groundMap.find(master_orbit) == groundMap.end())
	      { 
		double * resultsLocalVector = new double[3];
		computeLocalVector(satellitePosition[0], xyzCart, resultsLocalVector);

		groundMap[master_orbit] = resultsLocalVector;
	      }
    
	    if (groundMap.find(slave_orbit) == groundMap.end())
	      { 
		double * resultsLocalVector = new double[3];
		computeLocalVector(satellitePosition[1], xyzCart, resultsLocalVector);

		groundMap[slave_orbit] = resultsLocalVector;
	      }
    
	  }
      } // End of nested loop


    //////////// Write into output file ////////////////
    // Open the output file
    std::ofstream output(GetParameterString("outfile"), std::ios::out | std::ios::trunc);

    // Write target coordinates
    output << "Latitude of the target  = " << lat << std::endl;
    output << "Longitude of the target = " << lon << std::endl;
    output << "Altitude of the target  = " << height << std::endl;

    output << "GROUND COORDINATES = " << std::fixed << xyzCart[0] << " " << xyzCart[1] << " " << xyzCart[2] << std::endl;

    // Write images and orbits
    output << std::endl << std::left
	   << std::setw(70) << "IMAGE"
	   << std::setw(12) << "ORBIT" << std::endl;

    output << std::left;

    for(std::map<int,std::string>::iterator it = namesMap.begin(); 
	it != namesMap.end(); ++it) 
      {
	output << std::setw(70) << it->second
 	       << std::setw(12) << it->first << std::endl;
      }


    // Write alt ambig array
    output << std::endl << std::left
	   << std::setw(15) << "MASTER ORBIT"
	   << std::setw(15) << "SLAVE ORBIT"
	   << std::setw(15) << "Incidence"
	   << std::setw(15) << "ALT AMBIG"
	   << std::setw(15) << "Radial"
	   << std::setw(15) << "Lateral" << std::endl;

    for(unsigned int i = 0; i < altAmbigVector.size(); i++ ) 
      {
      	output << std::left << std::setprecision(6)
	       << std::setw(15) << static_cast<int>(altAmbigVector[i][0])
	       << std::setw(15) << static_cast<int>(altAmbigVector[i][1])
	       << std::setw(15) << altAmbigVector[i][2]
	       << std::setprecision(3)
	       << std::setw(15) << altAmbigVector[i][3]
	       << std::setw(15) << altAmbigVector[i][4]
	       << std::setw(15) << altAmbigVector[i][5] << std::endl;
      }

    // Write satellite vectors array
    output << std::setprecision(6) << 
      std::endl << "Ground target --> Satellite Vectors : " << std::endl;
    output << std::left
	   << std::setw(15) << "Image Orbit"
	   << std::setw(18) << "North Projection"
	   << std::setw(18) << "East Projection"
	   << std::setw(18) << "Vertical Projection" << std::endl;

    for(std::map<int,double *>::iterator it = groundMap.begin(); 
	it != groundMap.end(); ++it) 
      {
	double * localVector = it->second;

	output << std::left
	       << std::setw(15) << it->first
	       << std::setw(18) << localVector[0]
	       << std::setw(18) << localVector[1]
	       << std::setw(18) << localVector[2] << std::endl;
      }
    

    // Close the output file
    output.close();

    // Free Memory 
    for(unsigned int i = 0; i < altAmbigVector.size(); i++ ) 
      {
	delete altAmbigVector[i];
	altAmbigVector[i] = 0;
      }
    altAmbigVector.clear();

    for(std::map<int,double *>::iterator it = groundMap.begin(); 
	it != groundMap.end(); ++it) 
      {
	delete it->second;
	it->second = 0;
      }
      groundMap.clear();

      namesMap.clear();
  }

  

  /////// getFilename function ////////
  std::string getFilename(std::string filePath)
  {

    // Remove HDF5 datasets into filePath 
    std::size_t HDF5Pos = filePath.rfind("HDF5:");
    std::size_t DatasetPos = filePath.rfind("://");
    
    std::string fname = filePath;

    if(HDF5Pos != std::string::npos)
      {
	if (DatasetPos != std::string::npos)
	  {
	    fname = filePath.substr(HDF5Pos+5, DatasetPos-5);
	  }
	else
	  {
	    fname = filePath.substr(HDF5Pos+5, filePath.length()-5);
	  }
      }
    else
      {
	if (DatasetPos != std::string::npos)
	  {
	    fname = filePath.substr(0, DatasetPos);
	  }
      }

    std::size_t dotPos = fname.rfind(".");
    std::size_t sepPos = fname.rfind("/");
   

    return fname.substr(sepPos + 1, dotPos - sepPos - 1);
  }


  ///////// compute functions /////////
  // Incidence
  double computeIncidence(SarSensorModelAdapter::Point3DType satellitePosition, 
			  SarSensorModelAdapter::Point3DType xyzCart, 
			  SarSensorModelAdapter::Point3DType rm)
  {
    // Incidence
    double normeS = sqrt(satellitePosition[0]*satellitePosition[0] + 
			 satellitePosition[1]*satellitePosition[1] +
			 satellitePosition[2]*satellitePosition[2]);
    double normeCible = sqrt(xyzCart[0] * xyzCart[0] + xyzCart[1] * xyzCart[1] + xyzCart[2] * xyzCart[2]);
    double normeRm = sqrt(rm[0] * rm[0] + rm[1] * rm[1] + rm[2] * rm[2]);
      
    double incidence = acos((normeS * normeS - normeRm * normeRm - normeCible * normeCible) /
			    (2 * normeCible * normeRm) ) * 180. / M_PI;
    return incidence;
  }

  // Local Vector
  void computeLocalVector(SarSensorModelAdapter::Point3DType satellitePosition, 
			  SarSensorModelAdapter::Point3DType xyzCart,
			  double * resultsLocalVector)
  {
    SarSensorModelAdapter::Point3DType rm;
    rm[0] = -(xyzCart[0] - satellitePosition[0]);
    rm[1] = -(xyzCart[1] - satellitePosition[1]);
    rm[2] = -(xyzCart[2] - satellitePosition[2]);

    double normeRm = sqrt(rm[0] * rm[0] + rm[1] * rm[1] + rm[2] * rm[2]);
    
    // Calculate the local vectors
    SarSensorModelAdapter::Point3DType vecti;
    SarSensorModelAdapter::Point3DType vectj;
    SarSensorModelAdapter::Point3DType vectk;
    
    // lon an lat into radians
    double lon = GetParameterFloat("lon")*M_PI/180.;
    double lat = GetParameterFloat("lat")*M_PI/180.;
    
    vecti[0] = -sin(lat)*cos(lon);
    vecti[1] = -sin(lat)*sin(lon);
    vecti[2] = cos(lat);

    vectj[0] = -sin(lon);
    vectj[1] = cos(lon);
    vectj[2] = 0.;

    vectk[0] = cos(lat)*cos(lon);
    vectk[1] = cos(lat)*sin(lon);
    vectk[2] = sin(lat);

    // Rm projection
    // Use -Rm  because the given rm, here, is egal to : -(xyzCart - satellitePosition)
    double northProjection = -(vecti[0]*(-rm[0])+vecti[1]*(-rm[1])+vecti[2]*(-rm[2]))/normeRm;
    double eastProjection = -(vectj[0]*(-rm[0])+vectj[1]*(-rm[1])+vectj[2]*(-rm[2]))/normeRm;
    double verticalProjection = -(vectk[0]*(-rm[0])+vectk[1]*(-rm[1])+vectk[2]*(-rm[2]))/normeRm;
    otbAppLogDEBUG(<< "North Projection : " << northProjection);
    otbAppLogDEBUG(<< "East Projection : " << eastProjection);
    otbAppLogDEBUG(<< "Vertical Projection : " << verticalProjection);

    // Assign output
    resultsLocalVector[0] = northProjection;
    resultsLocalVector[1] = eastProjection;
    resultsLocalVector[2] = verticalProjection;
  }

  // Alt Ambig
  void computeAltAmbig(SarSensorModelAdapter::Point3DType xyzCart, 
		       SarSensorModelAdapter::Point3DType* satellitePosition, 
		       SarSensorModelAdapter::Point3DType* satelliteVelocity, 
		       double lambda, std::string master, std::string slave, 
		       double * results)
  {
    // Adjust the formula in case of non-monostatic mode
    double factorBperp = (!IsParameterEnabled("bistatic"))? 1.0:2.0;
    double factorHamb = (!IsParameterEnabled("bistatic"))? 2.0:1.0;
    
    SarSensorModelAdapter::Point3DType rm;
    rm[0] = -(xyzCart[0] - satellitePosition[0][0]);
    rm[1] = -(xyzCart[1] - satellitePosition[0][1]);
    rm[2] = -(xyzCart[2] - satellitePosition[0][2]);

    SarSensorModelAdapter::Point3DType re;
    re[0] = -(xyzCart[0] - satellitePosition[1][0]);
    re[1] = -(xyzCart[1] - satellitePosition[1][1]);
    re[2] = -(xyzCart[2] - satellitePosition[1][2]);

    double incidenceRm = computeIncidence(satellitePosition[0], xyzCart, rm);
    double incidenceRe = computeIncidence(satellitePosition[1], xyzCart, re);

    // project the slave orbit slant range vector
    // onto the master orbit zero doppler plane
    double vmNorm = sqrt(satelliteVelocity[0][0] * satelliteVelocity[0][0] +
			 satelliteVelocity[0][1] * satelliteVelocity[0][1] +
			 satelliteVelocity[0][2] * satelliteVelocity[0][2]);

    // unitary vector with the same direction as the master velocity
    // this vector is orthogonal to the master zero doppler plane
    SarSensorModelAdapter::Point3DType vmUnit;
    vmUnit[0] = satelliteVelocity[0][0] / vmNorm;
    vmUnit[1] = satelliteVelocity[0][1] / vmNorm;
    vmUnit[2] = satelliteVelocity[0][2] / vmNorm;

    // compute the component along vmUnit of the slave slant-range vector
    // scalar product Re . vmUnit
    double reVmunit = re[0] * vmUnit[0] + re[1] * vmUnit[1] + re[2] * vmUnit[2];

    // remove the along-track component
    // this gives the projection of the slave
    // slant-range vector onto the master zero-doppler plane
    re[0] -= reVmunit * vmUnit[0];
    re[1] -= reVmunit * vmUnit[1];
    re[2] -= reVmunit * vmUnit[2];

    double normeRm = sqrt(rm[0] * rm[0] + rm[1] * rm[1] + rm[2] * rm[2]);
    double normeRe = sqrt(re[0] * re[0] + re[1] * re[1] + re[2] * re[2]);

    // Scalar product RM.RE
    double scalarRmRe = rm[0] * re[0] + rm[1] * re[1] + rm[2] * re[2];
    double theta = acos(scalarRmRe / (normeRm * normeRe));

    double ye = normeRm * tan(theta);
    double xe = normeRe - normeRm / cos(theta);

    if (incidenceRe > incidenceRm){
      ye *= -1.0;
    }

    double alpha = (incidenceRe - incidenceRm) * M_PI / 180.;
    double inc_moy = ((incidenceRe + incidenceRm) / 2) * M_PI / 180.;

    double alt_amb = lambda * sin(inc_moy) / (factorHamb * tan(alpha));
    otbAppLogDEBUG(<< std::left
		  << std::setw(70) << master
		  << std::setw(70) << slave
		  << std::setw(12) << inc_moy * 180. / M_PI
		  << std::setw(12) << alt_amb
		  << std::setw(12) << xe
		  << std::setw(12) << ye / factorBperp);

    // Assign output
    results[0] = inc_moy * 180. / M_PI; // Mean incidence (in degrees)
    results[1] = alt_amb; // Ambiguity height
    results[2] = xe; // Radial coordinate
    results[3] = ye / factorBperp; // Lateral coordinate
  }

};

}

}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARAltAmbig)
