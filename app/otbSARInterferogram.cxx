/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbSARInterferogramImageFilter.h"

#include "otbWrapperOutputImageParameter.h"
#include "otbWrapperTypes.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{

  namespace Wrapper
  {

    class SARInterferogram : public Application
    {
    public:
      typedef SARInterferogram Self;
      typedef itk::SmartPointer<Self> Pointer; 

      itkNewMacro(Self);
      itkTypeMacro(SARInterferogram, otb::Wrapper::Application);

      // Filters
      typedef otb::SARInterferogramImageFilter<ComplexFloatImageType, FloatImageType, FloatVectorImageType, FloatVectorImageType> InterferogramFilterType;

    private:
      void DoInit() override
      {
	SetName("SARInterferogram");
	SetDescription("Interferogram between two SAR images.");

	SetDocLongDescription("This application builds the interferogram between two"
        " SAR images.");

	//Optional descriptors
	SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
	SetDocAuthors("OTB-Team");
	SetDocSeeAlso(" ");
	AddDocTag(Tags::SAR);
	AddDocTag("DiapOTB");

	//Parameter declarations
	AddParameter(ParameterType_InputImage,  "insarslave",   "Input SAR Slave image (Coregistrated image)");
	SetParameterDescription("insarslave", "Input SAR Slave image (Coregistrated image).");

	AddParameter(ParameterType_InputImage,  "insarmaster",   "Input SAR Master image");
	SetParameterDescription("insarmaster", "Input SAR Master image.");

	AddParameter(ParameterType_InputImage,  "topographicphase",   "Input Topographic Phase (estimation with DEM projection)");
	SetParameterDescription("topographicphase", "Input Topographic Phase (estimation with DEM projection).");
	MandatoryOff("topographicphase");

	AddParameter(ParameterType_Int, "mlran", "ML factor on distance");
	SetParameterDescription("mlran","ML factor on distance");
	SetDefaultParameterInt("mlran", 3);
	SetMinimumParameterIntValue("mlran", 1);
	MandatoryOff("mlran");
    
	AddParameter(ParameterType_Int, "mlazi", "ML factor on azimut");
	SetParameterDescription("mlazi","ML factor on azimut");
	SetDefaultParameterInt("mlazi", 3);
	SetMinimumParameterIntValue("mlazi", 1);
	MandatoryOff("mlazi");

	AddParameter(ParameterType_Int, "marginran", "Margin on distance for averaging");
	SetParameterDescription("marginran","Margin on distance for averaging");
	SetDefaultParameterInt("marginran", 1);
	SetMinimumParameterIntValue("marginran", 0);
	MandatoryOff("marginran");
    
	AddParameter(ParameterType_Int, "marginazi", "Margin on azimut for averaging");
	SetParameterDescription("marginazi","Margin on azimut for averaging");
	SetDefaultParameterInt("marginazi", 1);
	SetMinimumParameterIntValue("marginazi", 0);
	MandatoryOff("marginazi");
	
	AddParameter(ParameterType_Float, "gain", "Gain to apply for amplitude estimation");
	SetParameterDescription("gain","Gain to apply for amplitude estimation");
	SetDefaultParameterFloat("gain", 0.1);
	SetMinimumParameterFloatValue("gain", 0);
	MandatoryOff("gain");

	AddParameter(ParameterType_OutputImage, "out", "Interferogram");
	SetParameterDescription("out","Output Vector Image : Interferogram.");

	AddRAMParameter();

	SetDocExampleParameterValue("insarslave","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002.tiff");
	SetDocExampleParameterValue("insarmaster","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff");
	SetDocExampleParameterValue("mlran","3");
	SetDocExampleParameterValue("mlazi","3");
	SetDocExampleParameterValue("gain","0.1");
	SetDocExampleParameterValue("out","s1b-s1a-s4-interferogram.tiff");
      }

      void DoUpdateParameters() override
      {
	// Nothing to do here : all parameters are independent
      }

      void DoExecute() override
      { 
	// Get numeric parameters
	int factor_azi = GetParameterInt("mlazi");
	int factor_ran = GetParameterInt("mlran");
	double factor_gain = GetParameterFloat("gain");
	int margin_Ran = GetParameterInt("marginran");
	int margin_Azi = GetParameterInt("marginazi");

	// Log information
	otbAppLogINFO(<<"Averaging Factor on azimut :"<<factor_azi);
	otbAppLogINFO(<<"Averaging Factor on range : "<<factor_ran);
	otbAppLogINFO(<<"Averaging Margin on azimut :"<<margin_Azi);
	otbAppLogINFO(<<"Averaging Margin on range : "<<margin_Ran);
	otbAppLogINFO(<<"Gain Factor : "<<factor_gain);

	///////////////////////////////////// Interferogram Filter /////////////////////////////////////////////// 
	// Instanciate the Interferogram filter
	InterferogramFilterType::Pointer filterInterferogram = InterferogramFilterType::New();
	m_Ref.push_back(filterInterferogram.GetPointer());
	filterInterferogram->SetMLRan(factor_ran);
	filterInterferogram->SetMLAzi(factor_azi);
	filterInterferogram->SetMarginRan(margin_Ran);
	filterInterferogram->SetMarginAzi(margin_Azi);
	filterInterferogram->SetGain(factor_gain);
	
	// Execute the main Pipeline
	ComplexFloatImageType::Pointer SARMasterPtr;
	SARMasterPtr = GetParameterComplexFloatImage("insarmaster");

	ComplexFloatImageType::Pointer SARSlavePtr;
	SARSlavePtr = GetParameterComplexFloatImage("insarslave");

	// Two Main inputs
	filterInterferogram->SetMasterInput(SARMasterPtr);
	filterInterferogram->SetSlaveInput(SARSlavePtr);

	// One optionnal input
	if (GetParameterByKey("topographicphase")->HasValue())
	  {
	    filterInterferogram->SetTopographicPhaseInput(GetParameterImage("topographicphase"));
	  }
	
	// Main Output
	SetParameterOutputImage("out", filterInterferogram->GetOutput());

      }
      // Vector for filters 
      std::vector<itk::ProcessObject::Pointer> m_Ref;

    };

  }

}


 
OTB_APPLICATION_EXPORT(otb::Wrapper::SARInterferogram)
