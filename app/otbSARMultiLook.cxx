/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "itkComplexToModulusImageFilter.h"

#include "otbSARQuadraticAveragingImageFilter.h"
#include "otbSARGainImageFilter.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{
namespace Wrapper
{

class SARMultiLook : public Application
{
public:
  typedef SARMultiLook Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARMultiLook, otb::Wrapper::Application);

  // Image Output Type
  typedef unsigned char            OutPixelType;
  typedef otb::Image<OutPixelType, 2> OutImageType;
  
  // Filters
  typedef itk::ComplexToModulusImageFilter<ComplexFloatImageType, DoubleImageType> ComplexToRealFilterType; 
  typedef SARQuadraticAveragingImageFilter<DoubleImageType> QuadraticAveragingFilterType;
  typedef SARGainImageFilter< DoubleImageType, OutImageType> GainFilterType;
  

private:
  void DoInit() override
  {
    SetName("SARMultiLook");
    SetDescription("SAR Multi-Look creation.");

    SetDocLongDescription("This application creates the multi-look image of "
      "a SLC product.");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "incomplex",   "Input Complex image");
    SetParameterDescription("incomplex", "Complex Image to perform computation on.");

    AddParameter(ParameterType_InputImage,  "inreal",   "Input image");
    SetParameterDescription("inreal", "Image to perform computation on.");

    AddParameter(ParameterType_Int, "mlran", "Averaging on distance");
    SetParameterDescription("mlran","Averaging on distance");
    SetDefaultParameterInt("mlran", 3);
    SetMinimumParameterIntValue("mlran", 1);
    MandatoryOff("mlran");
    
    AddParameter(ParameterType_Int, "mlazi", "Averaging on azimut");
    SetParameterDescription("mlazi","Averaging on azimut");
    SetDefaultParameterInt("mlazi", 3);
    SetMinimumParameterIntValue("mlazi", 1);
    MandatoryOff("mlazi");

    AddParameter(ParameterType_Float, "mlgain", "Gain to apply on ML image");
    SetParameterDescription("mlgain","Gain to apply on ML image");
    SetDefaultParameterFloat("mlgain", 0.1);
    SetMinimumParameterFloatValue("mlgain", 0);
    MandatoryOff("mlgain");

    AddParameter(ParameterType_OutputImage, "out", "Output ML Image");
    SetParameterDescription("out","Output ML image.");

    AddRAMParameter();

    SetDocExampleParameterValue("incomplex","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff");
    SetDocExampleParameterValue("out","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_ML.tif");
    SetDocExampleParameterValue("mlran", "3");
    SetDocExampleParameterValue("mlazi", "3");
    SetDocExampleParameterValue("mlgain", "0.1");
  }

  void DoUpdateParameters() override
  {
    // Handle the dependency between incomplex and inreal (two kinds of pipelines)
    if (GetParameterByKey("incomplex")->HasValue())
      {
	DisableParameter("inreal");
	MandatoryOff("inreal");

	otbAppLogINFO(<<"Complex input");
      }
    else
      {
	EnableParameter("inreal");
	MandatoryOn("inreal");

	MandatoryOff("incomplex");

	otbAppLogINFO(<<"Real input");
      }
  }

  void DoExecute() override
  {  
    // Get numeric parameters
    int factor_azi = GetParameterInt("mlazi");
    int factor_ran = GetParameterInt("mlran");
    double factor_gain = GetParameterFloat("mlgain");

    // Log information
    otbAppLogINFO(<<"Averaging Factor on azimut :"<<factor_azi);
    otbAppLogINFO(<<"Averaging Factor on range : "<<factor_ran);
    otbAppLogINFO(<<"Gain Factor : "<<factor_gain);

    // itkComplexToModulusImageFilter Filter to convert the SLC image to a real image 
    ComplexToRealFilterType::Pointer complexToRealFilter = ComplexToRealFilterType::New();
    m_Ref.push_back(complexToRealFilter.GetPointer());

    // Quadratic averaging Filter (on range)
    QuadraticAveragingFilterType::Pointer quadAveRanFilter = QuadraticAveragingFilterType::New();
    m_Ref.push_back(quadAveRanFilter.GetPointer());
    quadAveRanFilter->SetDirection(QuadraticAveragingFilterType::DirectionType::RANGE);
    quadAveRanFilter->SetAveragingFactor(factor_ran);
    
    // Quadratic averaging Filter (on azimut)
    QuadraticAveragingFilterType::Pointer quadAveAziFilter = QuadraticAveragingFilterType::New();
    m_Ref.push_back(quadAveAziFilter.GetPointer());
    quadAveAziFilter->SetDirection(QuadraticAveragingFilterType::DirectionType::AZIMUTH);
    quadAveAziFilter->SetAveragingFactor(factor_azi);

    // Gain Filter with conversion into unsigned char for each pixel
    GainFilterType::Pointer gainFilter = GainFilterType::New();
    m_Ref.push_back(gainFilter.GetPointer());
    gainFilter->SetGain((double)factor_gain);


    // Define pipeline (Two kinds of Pipeline : one with complex image and with one with real one)
    if (GetParameterByKey("incomplex")->HasValue())
      {
	complexToRealFilter->SetInput(GetParameterComplexFloatImage("incomplex"));
	quadAveRanFilter->SetInput(complexToRealFilter->GetOutput());
	quadAveAziFilter->SetInput(quadAveRanFilter->GetOutput());
	gainFilter->SetInput(quadAveAziFilter->GetOutput());
	SetParameterOutputImage("out", gainFilter->GetOutput());
      }
    else if (GetParameterByKey("inreal")->HasValue())
      {
	quadAveRanFilter->SetInput(GetParameterDoubleImage("inreal"));
	quadAveAziFilter->SetInput(quadAveRanFilter->GetOutput());
	gainFilter->SetInput(quadAveAziFilter->GetOutput());
	SetParameterOutputImage("out", gainFilter->GetOutput());
      }
    else
      {
	std::cerr << "Pipeline not defined !" << std::endl;
      }

  }

  // Vector for filters
  std::vector<itk::ProcessObject::Pointer> m_Ref;
};


}

}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARMultiLook)
