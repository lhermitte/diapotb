/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbSARDEMGridImageFilter.h"
#include "otbSARDEMProjectionImageFilter.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{
namespace Wrapper
{

class SARDEMGrid : public Application
{
public:
  typedef SARDEMGrid Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARDEMGrid, otb::Wrapper::Application);

  // Filters
  typedef otb::SARDEMGridImageFilter<FloatVectorImageType, ComplexFloatImageType, FloatVectorImageType> DEMGridFilterType;
  typedef otb::SARDEMProjectionImageFilter<FloatImageType, FloatVectorImageType> DEMFilterType;
  

private:
  void DoInit() override
  {
    SetName("SARDEMGrid");
    SetDescription("Computes deformation DEM grid.");

    SetDocLongDescription("This application creates a deformation grid related"
      " to the DEM with projections into Master and Slave SAR geometries.\nThe "
      "output grid is a VectorImage composed of three values : shift in range,"
      " shift in azimut and a number of DEM points for contribution. The inputs"
      " of this application are SAR images. This application can directly take"
      " the projected DEM as inputs.");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "insarmaster",   "Input SAR Master image");
    SetParameterDescription("insarmaster", "Master SAR Image.");

    AddParameter(ParameterType_InputImage,  "insarslave",   "Input SAR Slave image");
    SetParameterDescription("insarslave", "Slave SAR Image.");

    AddParameter(ParameterType_InputImage,  "indem",   "Input DEM");
    SetParameterDescription("indem", "Input DEM.");

    AddParameter(ParameterType_InputImage,  "indemprojmaster",   "Input vector of DEM projected into SAR Master geometry");
    SetParameterDescription("indemprojmaster", "Input vector of DEM projected into SAR Master geometry.");
    MandatoryOff("indemprojmaster");

    AddParameter(ParameterType_InputImage,  "indemprojslave",   "Input vector of DEM projected into SAR Slave geometry");
    SetParameterDescription("indemprojslave", "Input vector of DEM projected into SAR Slave geometry.");
    MandatoryOff("indemprojslave");

    AddParameter(ParameterType_OutputImage, "out", "Output DEM grid (Vector Image)");
    SetParameterDescription("out","Output DEM Grid Vector Image (Shift_ran, Shift_azi, NbDEMPts for contribution).");

    AddParameter(ParameterType_Int, "mlran", "MultiLook factor on distance");
    SetParameterDescription("mlran","MultiLook factor on distance.");
    SetDefaultParameterInt("mlran", 3);
    SetMinimumParameterIntValue("mlran", 1);
    MandatoryOff("mlran");
    
    AddParameter(ParameterType_Int, "mlazi", "MultiLook factor on azimut");
    SetParameterDescription("mlazi","MultiLook factor on azimut.");
    SetDefaultParameterInt("mlazi", 3);
    SetMinimumParameterIntValue("mlazi", 1);
    MandatoryOff("mlazi");
    
    AddParameter(ParameterType_Int, "gridsteprange", "Grid step for range dimension (into SLC/SAR geometry)");
    SetParameterDescription("gridsteprange","Grid step for range dimension (into SLC/SAR geometry).");
    SetDefaultParameterInt("gridsteprange", 150);
    SetMinimumParameterIntValue("gridsteprange", 1);
    MandatoryOff("gridsteprange");

    AddParameter(ParameterType_Int, "gridstepazimut", "Grid step for azimut dimension (into SLC/SAR geometry)");
    SetParameterDescription("gridstepazimut","Grid step for azimut dimension (into SLC/SAR geometry).");
    SetDefaultParameterInt("gridstepazimut", 150);
    SetMinimumParameterIntValue("gridstepazimut", 1);
    MandatoryOff("gridstepazimut");

    AddRAMParameter();

    SetDocExampleParameterValue("indem","./S21E055.hgt");
    SetDocExampleParameterValue("insarmaster","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_ML.tiff");
    SetDocExampleParameterValue("insarslave","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002_ML.tiff");
    SetDocExampleParameterValue("out","demGrid.tif");
  }

  void DoUpdateParameters() override
  {
    // Handle the dependency between indemproj* and indem (two kinds of pipelines)
    if (GetParameterByKey("indemprojmaster")->HasValue() && GetParameterByKey("indemprojslave")->HasValue())
      {
	DisableParameter("indem");
	MandatoryOff("indem");

	otbAppLogINFO(<<"DEM Projection already estimated");
      }
    else
      {
	EnableParameter("indem");
	MandatoryOn("indem");

	MandatoryOff("indemprojmaster");
	MandatoryOff("indemprojslave");

	otbAppLogINFO(<<"DEM Projection must be estimated");
      }
  }

void DoExecute() override
{  
  // Get numeric parameters
  int factorML_azi = GetParameterInt("mlazi");
  int factorML_ran = GetParameterInt("mlran");
  int grid_step_azi = GetParameterInt("gridstepazimut");
  int grid_step_ran = GetParameterInt("gridsteprange");

  // Log information
  otbAppLogINFO(<<"ML Factor on azimut :"<<factorML_azi);
  otbAppLogINFO(<<"ML Factor on range : "<<factorML_ran);
  otbAppLogINFO(<<"Grid Step for range : "<<grid_step_ran);
  otbAppLogINFO(<<"Grid Step for azimut : "<<grid_step_azi);

  // Get master and slave image
  ComplexFloatImageType::Pointer MasterSARPtr = GetParameterComplexFloatImage("insarmaster");
  ComplexFloatImageType::Pointer SlaveSARPtr = GetParameterComplexFloatImage("insarslave");
  
  // CorrelationGrid Filter
  DEMGridFilterType::Pointer filterDEMGrid = DEMGridFilterType::New();
  m_Ref.push_back(filterDEMGrid.GetPointer());

  // Configure DEMGrid Filter
  filterDEMGrid->SetMLran(factorML_ran);
  filterDEMGrid->SetMLazi(factorML_azi);
  filterDEMGrid->SetGridStep(grid_step_ran, grid_step_azi);
  filterDEMGrid->SetSARImagePtr(MasterSARPtr);

  // Two kinds of Pipeline : One with projected DEM alreaded calculated and one with no projeted DEM  
  if (GetParameterByKey("indemprojmaster")->HasValue() && GetParameterByKey("indemprojslave")->HasValue())
    {
      // Get projected DEM
      FloatVectorImageType::Pointer DEMProjOnMasterPtr = GetParameterImage("indemprojmaster");
      FloatVectorImageType::Pointer DEMProjOnSlavePtr = GetParameterImage("indemprojslave");

      // Start Pipeline
      filterDEMGrid->SetDEMProjOnMasterInput(DEMProjOnMasterPtr);
      filterDEMGrid->SetDEMProjOnSlaveInput(DEMProjOnSlavePtr);
      SetParameterOutputImage("out", filterDEMGrid->GetOutput());
    }
  else
    {
      // Get DEM
      FloatImageType::Pointer inputDEM =  GetParameterFloatImage("indem");
      
      // Prepare the projection from DEM to Master/Slave SAR geometry
      DEMFilterType::Pointer filterDEMProjMaster = DEMFilterType::New();
      DEMFilterType::Pointer filterDEMProjSlave = DEMFilterType::New();
      m_Ref.push_back(filterDEMProjMaster.GetPointer());
      m_Ref.push_back(filterDEMProjSlave.GetPointer());
      
      filterDEMProjMaster->SetSARImageKeyWorList(MasterSARPtr->GetImageKeywordlist());
      filterDEMProjSlave->SetSARImageKeyWorList(SlaveSARPtr->GetImageKeywordlist());

      // Start the Pipeline
      filterDEMProjMaster->SetInput(inputDEM);
      filterDEMProjSlave->SetInput(inputDEM);
      filterDEMGrid->SetDEMProjOnMasterInput(filterDEMProjMaster->GetOutput());
      filterDEMGrid->SetDEMProjOnSlaveInput(filterDEMProjSlave->GetOutput());
      SetParameterOutputImage("out", filterDEMGrid->GetOutput());
    }
}
   // Vector for filters 
  std::vector<itk::ProcessObject::Pointer> m_Ref;
};


}

}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARDEMGrid)
