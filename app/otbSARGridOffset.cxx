/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbMultiToMonoChannelExtractROI.h"
#include "otbImageList.h"
#include "otbImageListToVectorImageFilter.h"
#include "itkAddImageFilter.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{
namespace Wrapper
{

class SARGridOffset : public Application
{
public:
  typedef SARGridOffset Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARGridOffset, otb::Wrapper::Application);

  /** Filters typedef */
  typedef otb::ImageList<FloatImageType>  ImageListType;
  typedef otb::ImageListToVectorImageFilter<ImageListType,
                                       FloatVectorImageType >                   ListConcatenerFilterType;
  typedef otb::MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,
                                       FloatImageType::PixelType>               ExtractROIFilterType;
  typedef itk::AddImageFilter <FloatImageType, FloatImageType, FloatImageType>  AddFilterType;

  typedef ObjectList<ExtractROIFilterType>                                      ExtractROIFilterListType;
  typedef ObjectList<AddFilterType>                                      AddFilterListType;
  

private:
  void DoInit() override
  {
    SetName("SARGridOffset");
    SetDescription("Applies offsets on a deformation grid.");

    SetDocLongDescription("This application applies offsets on a deformation grid for each"
      " dimension.");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "ingrid",   "Input grid (Vector Image)");
    SetParameterDescription("ingrid", "Input Grid Vector Image (Shift_ran, Shift_azi).");
   
    AddParameter(ParameterType_Float, "offsetran", "Offset on distance");
    SetParameterDescription("offsetran","Offset on distance");
    SetDefaultParameterFloat("offsetran", 0.);
    MandatoryOff("offsetran");

    AddParameter(ParameterType_Float, "offsetazi", "Offset on azimut");
    SetParameterDescription("offsetazi","Offset on azimut");
    SetDefaultParameterFloat("offsetazi", 0.);
    MandatoryOff("offsetazi");
    
    AddParameter(ParameterType_OutputImage, "out", "Output Grid");
    SetParameterDescription("out","Output Grid.");
 
    AddRAMParameter();

    SetDocExampleParameterValue("ingrid","./grid.tiff");
    SetDocExampleParameterValue("offsetran", "-0.1");
    SetDocExampleParameterValue("offsetazi", "0.1");
    SetDocExampleParameterValue("out","correctionGrid.tif");
  }

  void DoUpdateParameters() override
  {
    // Nothing // TODO: o do here : all parameters are independent
  }

void DoExecute() override
{  
  // Get numeric parameters
  float offset_ran = GetParameterFloat("offsetran");
  float offset_azi = GetParameterFloat("offsetazi");

  // Log information
  otbAppLogINFO(<<"Offset on distance  :"<<offset_ran);
  otbAppLogINFO(<<"Offset on azimut  :"<<offset_azi);
 
  // Instanciate all filters
  ListConcatenerFilterType::Pointer m_Concatener =
    ListConcatenerFilterType::New();
   ExtractROIFilterListType::Pointer m_ExtractorList = 
    ExtractROIFilterListType::New();
  AddFilterListType::Pointer m_AddOffsetList = 
    AddFilterListType::New();
  ImageListType::Pointer m_ImageList =
    ImageListType::New();

  

  // Get input grid
  FloatVectorImageType::Pointer GridPtr = GetParameterFloatVectorImage("ingrid");

  // Check number of components (at least two : range and azimut)
  unsigned int nbComponents = GridPtr->GetNumberOfComponentsPerPixel();
  if (nbComponents < 2)
    {
      otbAppLogFATAL(<< "Input grid has to contain at least two components");
    }  

  // Extract eah band and apply the corresponding offset
  for (unsigned int i = 0; i < nbComponents; ++i)
    {
      // Instanciate filters
      ExtractROIFilterType::Pointer extractor = 
	ExtractROIFilterType::New();
      AddFilterType::Pointer addOffset = 
	AddFilterType::New();

      // Set the channel to extract
      extractor->SetInput(GridPtr);
      extractor->SetChannel(i+1);      
      extractor->UpdateOutputInformation();
      m_ExtractorList->PushBack(extractor);

      addOffset->SetInput(extractor->GetOutput());

      // Adapt offset according to the numnber of band
      if (i == 0)
	{
	  addOffset->SetConstant2(offset_ran);
	}
      else if (i == 1)
	{
	  addOffset->SetConstant2(offset_azi);
	}
      else
	{
	  addOffset->SetConstant2(0);
	}
      
      
      m_AddOffsetList->PushBack(addOffset);
      m_ImageList->PushBack(addOffset->GetOutput() );
      
    }
  
  m_Concatener->SetInput( m_ImageList );

  SetParameterOutputImage("out", m_Concatener->GetOutput());
  RegisterPipeline();
}
   // Vector for filters 
  std::vector<itk::ProcessObject::Pointer> m_Ref;
};


}

}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARGridOffset)
