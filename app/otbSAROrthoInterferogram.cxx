/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbSARCompensatedComplexImageFilter.h"
#include "otbSARGroupedByOrthoImageFilter.h"

#include "otbWrapperOutputImageParameter.h"
#include "otbWrapperTypes.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{

  namespace Wrapper
  {

    class SAROrthoInterferogram : public Application
    {
    public:
      typedef SAROrthoInterferogram Self;
      typedef itk::SmartPointer<Self> Pointer; 

      itkNewMacro(Self);
      itkTypeMacro(SAROrthoInterferogram, otb::Wrapper::Application);

      // Filters
      typedef otb::SARCompensatedComplexImageFilter<ComplexFloatImageType, FloatVectorImageType, FloatVectorImageType> CompensatedComplexFilterType;
      typedef otb::SARGroupedByOrthoImageFilter<FloatVectorImageType, FloatImageType, FloatVectorImageType> GroupedByOrthoFilterType;

    private:
      void DoInit() override
      {
	SetName("SAROrthoInterferogram");
	SetDescription("Interferogram into ground geometry between two SAR images.");

	SetDocLongDescription("This application builds the interferogram into ground geometry between two"
        " SAR images.");

	//Optional descriptors
	SetDocLimitations("Only Sentinel 1 (StripMap mode) and Cosmo products are supported for now.");
	SetDocAuthors("OTB-Team");
	SetDocSeeAlso(" ");
	AddDocTag(Tags::SAR);
	AddDocTag("DiapOTB");

	//Parameter declarations
	AddParameter(ParameterType_InputImage,  "insarslave",   "Input SAR Slave image (Coregistrated image)");
	SetParameterDescription("insarslave", "Input SAR Slave image (Coregistrated image).");

	AddParameter(ParameterType_InputImage,  "insarmaster",   "Input SAR Master image");
	SetParameterDescription("insarmaster", "Input SAR Master image.");

	AddParameter(ParameterType_InputImage,  "topographicphase",   "Input Topographic Phase (estimation with DEM projection)");
	SetParameterDescription("topographicphase", "Input Topographic Phase (estimation with DEM projection).");
	MandatoryOff("topographicphase");
	
        AddParameter(ParameterType_InputImage,  "incartmeanmaster",   "Input Cartesian Mean Master image");
	SetParameterDescription("incartmeanmaster", "Input Cartesian Mean Master image.");

        AddParameter(ParameterType_InputImage,  "indem",   "Input DEM");
        SetParameterDescription("indem", "Get Ground geometry thanks to DEM (only metadata).");
	
	AddParameter(ParameterType_Float, "gain", "Gain to apply for amplitude estimation");
	SetParameterDescription("gain","Gain to apply for amplitude estimation");
	SetDefaultParameterFloat("gain", 0.1);
	SetMinimumParameterFloatValue("gain", 0);
	MandatoryOff("gain");

	AddParameter(ParameterType_Int, "margin", "Margin to enlarge SAR scan for calculation");
	SetParameterDescription("margin","Margin to enlarge SAR scan for calculation");
	SetDefaultParameterInt("margin", 1000);
	SetMinimumParameterIntValue("margin", 0);
	MandatoryOff("margin");
	
	AddParameter(ParameterType_Int, "streamingsizevalue", "Value to specify the streaming size (in MB)");
	SetParameterDescription("streamingsizevalue","Value to specify the streaming size (in MB)");
	SetDefaultParameterInt("streamingsizevalue", 3000); // Default 3GB
	SetMinimumParameterIntValue("streamingsizevalue", 0);
	MandatoryOff("streamingsizevalue");

	AddParameter(ParameterType_OutputImage, "out", "Interferogram in ground geometry");
	SetParameterDescription("out","Output Vector Image : Interferogram in ground geometry.");

	AddRAMParameter();

	SetDocExampleParameterValue("insarslave","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002.tiff");
	SetDocExampleParameterValue("insarmaster","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff");
        SetDocExampleParameterValue("incartmeanmaster","CartesianMaster.tiff");
	SetDocExampleParameterValue("topographicphase","TopographicPhase.tiff");
        SetDocExampleParameterValue("indem","S21E055.hgt");
	SetDocExampleParameterValue("gain","0.1");
	SetDocExampleParameterValue("out","s1b-s1a-s4-Orthointerferogram.tiff");
      }

      void DoUpdateParameters() override
      {
	// Nothing to do here : all parameters are independent
      }

      void DoExecute() override
      { 
	// Get numeric parameters
	double factor_gain = GetParameterFloat("gain");
	int margin = GetParameterInt("margin");
	int streamingSizeValue = GetParameterInt("streamingsizevalue");
	
	otbAppLogINFO(<<"Gain Factor : "<<factor_gain);
	otbAppLogINFO(<<"Margin : "<<margin);
	

	/////////////////////////////////// Compensated Complex Filter ////////////////////////////////////////
	// Instanciate the first filter
	CompensatedComplexFilterType::Pointer filterCompensatedComplex = CompensatedComplexFilterType::New();
	m_Ref.push_back(filterCompensatedComplex.GetPointer());
	
	// Execute the Pipeline
	ComplexFloatImageType::Pointer SARMasterPtr;
	SARMasterPtr = GetParameterComplexFloatImage("insarmaster");

	ComplexFloatImageType::Pointer SARSlavePtr;
	SARSlavePtr = GetParameterComplexFloatImage("insarslave");

	// Two Main inputs
	filterCompensatedComplex->SetMasterInput(SARMasterPtr);
	filterCompensatedComplex->SetSlaveInput(SARSlavePtr);

	// One optionnal input
	if (GetParameterByKey("topographicphase")->HasValue())
	  {
	    filterCompensatedComplex->SetTopographicPhaseInput(GetParameterImage("topographicphase"));
	  }
	

	/////////////////////////////// GroupedBy Filter (for ground geometry) //////////////////////////////////
	// Instanciate the second filter
	FloatImageType::Pointer inputDEM =  GetParameterFloatImage("indem");
	GroupedByOrthoFilterType::Pointer filterGroupedBy = GroupedByOrthoFilterType::New();
	m_Ref.push_back(filterGroupedBy.GetPointer());
	filterGroupedBy->SetSARImageKeyWorList(SARMasterPtr->GetImageKeywordlist());
	filterGroupedBy->SetDEMImagePtr(inputDEM);

	filterGroupedBy->SetMargin(margin);
	
	// Execute the Pipeline
	FloatVectorImageType::Pointer CartMeanPtr;
	CartMeanPtr = GetParameterFloatVectorImage("incartmeanmaster");

	filterGroupedBy->SetCartesianMeanInput(CartMeanPtr);
	filterGroupedBy->SetCompensatedComplexInput(filterCompensatedComplex->GetOutput());


	// Define the main pipeline (controlled with extended FileName in order to obtain better performances)
	Parameter* param = GetParameterByKey("out");
	OutputImageParameter* paramDown = dynamic_cast<OutputImageParameter*>(param);
	std::string origin_FileName = paramDown->GetFileName();

	// Check if FileName is extended (with the ? caracter)
	// If not extended then override the FileName
	if (origin_FileName.find("?") == std::string::npos && !origin_FileName.empty()) 
	  {
	    otbAppLogINFO(<<"Streaming Size Value : "<<streamingSizeValue);

	    std::string extendedFileName = origin_FileName;
	
	    // Construct the extendedPart
	    std::ostringstream os;
	    os << "?&streaming:type=tiled&streaming:sizevalue=" << streamingSizeValue ;
	
	    // Add the extendedPart
	    std::string extendedPart = os.str();
	    extendedFileName.append(extendedPart);
	
	    // Set the new FileName with extended options
	    SetParameterString("out", extendedFileName);
	  }

	// Main Output
	SetParameterOutputImage("out", filterGroupedBy->GetOutput());
      }
      // Vector for filters 
      std::vector<itk::ProcessObject::Pointer> m_Ref;

    };

  }

}


 
OTB_APPLICATION_EXPORT(otb::Wrapper::SAROrthoInterferogram)
