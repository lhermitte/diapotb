/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbSARStreamingGridInformationImageFilter.h"
#include "otbTilesAnalysisImageFilter.h"
#include "otbTilesExampleFunctor.h"
#include "otbSARTilesCoregistrationFunctor.h"

#include "otbWrapperOutputImageParameter.h"
#include "otbWrapperTypes.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{

  namespace Wrapper
  {

    class SARCoRegistration : public Application
    {
    public:
      typedef SARCoRegistration Self;
      typedef itk::SmartPointer<Self> Pointer; 

      itkNewMacro(Self);
      itkTypeMacro(SARCoRegistration, otb::Wrapper::Application);

      // Typedefs
      typedef   ComplexFloatImageType          ComplexCoRegistrationImageType;
     
      // Function
      typedef otb::Function::SARTilesCoregistrationFunctor<ComplexCoRegistrationImageType, ComplexCoRegistrationImageType, FloatVectorImageType> TilesCoRegistrationFunctorType;

      // Filters
      typedef otb::SARStreamingGridInformationImageFilter<FloatVectorImageType> GridInformationFilterType;
      typedef otb::TilesAnalysisImageFilter<ComplexCoRegistrationImageType, ComplexCoRegistrationImageType, TilesCoRegistrationFunctorType> TilesFilterType;

    private:
      void DoInit() override
      {
	SetName("SARCoRegistration");
	SetDescription("CoRegistration between two SAR images.");

	SetDocLongDescription("This application does the coregistration between"
        " two SAR images thanks to a deformation grid.");

	//Optional descriptors
	SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
	SetDocAuthors("OTB-Team");
	SetDocSeeAlso(" ");
	AddDocTag(Tags::SAR);
	AddDocTag("DiapOTB");

	//Parameter declarations
	AddParameter(ParameterType_InputImage,  "ingrid",   "Input deformation grid");
	SetParameterDescription("ingrid", "Input deformation grid.");

	AddParameter(ParameterType_InputImage,  "insarslave",   "Input SAR Slave image");
	SetParameterDescription("insarslave", "Input SAR Slave image.");

	AddParameter(ParameterType_InputImage,  "insarmaster",   "Input SAR Master image (for metadata only)");
	SetParameterDescription("insarmaster", "Input SAR Master image (for metadata only).");

	AddParameter(ParameterType_Int,  "gridsteprange", "Grid Step for range dimension into SLC geometry");
	SetParameterDescription("gridsteprange", "Grid Step for range dimension into  SLC geometry.");
	SetDefaultParameterInt("gridsteprange", 150);
	MandatoryOff("gridsteprange");

	AddParameter(ParameterType_Int,  "gridstepazimut", "Grid Step for azimut dimension into SLC geometry");
	SetParameterDescription("gridstepazimut", "Grid Step for azimut dimension into  SLC geometry.");
	SetDefaultParameterInt("gridstepazimut", 150);
	MandatoryOff("gridstepazimut");
        
	AddParameter(ParameterType_Float,  "doppler0",   "Doppler 0 in azimut for SAR Slave Image");
	SetParameterDescription("doppler0", "Doppler 0 in azimut for SAR Slave Image.");
	SetDefaultParameterFloat("doppler0", 0.);
	MandatoryOff("doppler0");
        
	AddParameter(ParameterType_Int,  "sizetiles", "Size Tiles for output cut");
	SetParameterDescription("sizetiles", "Size Tiles for output cut.");
	SetDefaultParameterInt("sizetiles", 50);
	MandatoryOff("sizetiles");

	AddParameter(ParameterType_Int,  "margin", "Margin for window extractions into Slave Image");
	SetParameterDescription("margin", "Margin for window extractions into Slave Image.");
	SetDefaultParameterInt("margin", 7);
	MandatoryOff("margin");

	AddParameter(ParameterType_Int,  "nbramps", "Number of Ramps for filtering inside coregistration");
	SetParameterDescription("nbramps", "Number of Ramps for filtering inside coregistration.");
	SetDefaultParameterInt("nbramps", 257);
	MandatoryOff("nbramps");

	AddParameter(ParameterType_OutputImage, "out", "Coregistrated slave image into master geometry");
	SetParameterDescription("out","Output Image : Coregistrated slave image into master geometry.");

	//TruncatePrunedTree
	AddParameter(ParameterType_Bool, "cint16", "Set Output conversion with cInt16 to false");
	SetParameterDescription("cint16", "If true, then output conversion to cInt16.");

	AddRAMParameter();

	SetDocExampleParameterValue("insarslave","s1b-s4-slc-vv-20160929t014610-20160929t014634-002277-003d71-002.tiff");
	SetDocExampleParameterValue("insarmaster","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff");
	SetDocExampleParameterValue("ingrid","./FineDeformation.tiff");
	SetDocExampleParameterValue("doppler0","0.011851326955");
	SetDocExampleParameterValue("out","s1b-s4-coregistrated.tiff");
      }

      void DoUpdateParameters() override
      {
	// Nothing to do here : all parameters are independent
      }

      void DoExecute() override
      { 
	// Get numeric parameters : sizeTiles, margin, gridStep, nbRamps and Doppler0
	double doppler0 = GetParameterFloat("doppler0");
	int gridStepRange = GetParameterInt("gridsteprange");
	int gridStepAzimut = GetParameterInt("gridstepazimut");
	int nbRamps = GetParameterInt("nbramps");
	int margin = GetParameterInt("margin");
	int sizeTiles = GetParameterInt("sizetiles");
	    
	otbAppLogINFO(<<"Doppler 0 in azimut : "<<doppler0);
	otbAppLogINFO(<<"Grid Step Range : "<<gridStepRange);
	otbAppLogINFO(<<"Grid Step Azimut : "<<gridStepAzimut);
	otbAppLogINFO(<<"Number of Ramps for filtering : "<<nbRamps);
	otbAppLogINFO(<<"Margin for window extraction into slave input image: "<<margin);
	otbAppLogINFO(<<"Size of tiles for ouput (slave coregistrated image) cutting : "<<sizeTiles);
    
	// Start the first pipeline (Read SAR Master image metedata)
	ComplexCoRegistrationImageType::Pointer SARMasterPtr;
	SARMasterPtr = GetParameterComplexFloatImage("insarmaster");
   
	// Retrive input grid
	FloatVectorImageType::Pointer GridPtr = GetParameterFloatVectorImage("ingrid");

	///////// Persistent filter to get some information about grid (min/max shift inot each dimension ) /////////
	GridInformationFilterType::Pointer filterGridInfo = GridInformationFilterType::New();
	m_Ref.push_back(filterGridInfo.GetPointer());
	filterGridInfo->SetInput(GridPtr);
	// Adapt streaming with ram parameter (default 256 MB)
	filterGridInfo->GetStreamer()->SetAutomaticStrippedStreaming(GetParameterInt("ram"));
	filterGridInfo->SetEstimateMean(false);

	// Retrieve min/max into intput grid for each dimension
	filterGridInfo->Update();
	double minRan, minAzi, maxRan, maxAzi; 
	filterGridInfo->GetMinMax(minRan, minAzi, maxRan, maxAzi);
	double maxAbsRan = std::max(std::abs(minRan), std::abs(maxRan));
	double maxAbsAzi = std::max(std::abs(minAzi), std::abs(maxAzi)); 

	///////////////////////////////////// CoRegistration Filter /////////////////////////////////////////////// 
	// Function Type : TilesCoRegistation
	unsigned int sizeWindow = sizeTiles + 2*margin;
	TilesCoRegistrationFunctorType::Pointer functor =  TilesCoRegistrationFunctorType::New();
	functor->SetSizeTiles(sizeTiles);
	functor->SetMargin(margin);
	functor->SetSizeWindow(sizeWindow);
	functor->SetNbRampes(nbRamps);
	functor->SetDopAzi(doppler0);
	functor->SetGridParameters(GridPtr, gridStepRange, gridStepAzimut);
	if (IsParameterEnabled("cint16"))
	  {
	    functor->SetConvToCInt16(true);
	  }

	// Instanciate the Coregistration filter
	TilesFilterType::Pointer filterCoregistration = TilesFilterType::New();
	m_Ref.push_back(filterCoregistration.GetPointer());
	filterCoregistration->SetSizeTiles(sizeTiles);
	filterCoregistration->SetMargin(margin);
	filterCoregistration->SetConfigForCache(200); // 200 MB available to store tiles
	filterCoregistration->SetGridParameters(maxAbsRan, maxAbsAzi, gridStepRange, gridStepAzimut);
	filterCoregistration->SetFunctorPtr(functor);

	// Master metadata
	filterCoregistration->SetMasterImagePtr(SARMasterPtr);
      
	// Define the main pipeline (controlled with extended FileName in order to obtain better performances)
	//std::string origin_FileName = GetParameterString("out"); // Doesn't work with ComplexOutputImage
	Parameter* param = GetParameterByKey("out");
	OutputImageParameter* paramDown = dynamic_cast<OutputImageParameter*>(param);
	std::string origin_FileName = paramDown->GetFileName();

	// Check if FileName is extended (with the ? caracter)
	// If not extended then override the FileName
	if (origin_FileName.find("?") == std::string::npos && !origin_FileName.empty()) 
	  {
	    std::string extendedFileName = origin_FileName;

	    // Get the ram value (in MB)
	    int ram = GetParameterInt("ram");
	
	    // Define with the ram value, the number of lines for the streaming
	    int nbColSAR = SARMasterPtr->GetLargestPossibleRegion().GetSize()[0];
	    int nbLinesSAR = SARMasterPtr->GetLargestPossibleRegion().GetSize()[1];
	    // To determine the number of lines : 
	    // nbColSAR * nbLinesStreaming * sizeof(OutputPixel) = RamValue/2 (/2 to be sure)
	    // OutputPixel are float => sizeof(OutputPixel) = 4 (Bytes)
	    long int ram_In_KBytes = (ram/2) * 1024;
	    long int nbLinesStreaming = (ram_In_KBytes / (nbColSAR)) * (1024/4);
	
	    // Check the value of nbLinesStreaming
	    int nbLinesStreamingMax = 2000;
	    if (nbLinesStreamingMax > nbLinesSAR)
	      {
		nbLinesStreamingMax = nbLinesSAR;
	      }
	    if (nbLinesStreaming <= 0 || nbLinesStreaming >  nbLinesStreamingMax)
	      {
		nbLinesStreaming =  nbLinesStreamingMax;
	      } 
	
	    // Construct the extendedPart
	    std::ostringstream os;
	    os << "?&streaming:type=stripped&streaming:sizevalue=" << nbLinesStreaming;
	
	    // Add the extendedPart
	    std::string extendedPart = os.str();
	    extendedFileName.append(extendedPart);
	
	    // Set the new FileName with extended options
	    SetParameterString("out", extendedFileName);
	  }

	// Execute the main Pipeline
	ComplexCoRegistrationImageType::Pointer SARSlavePtr;
	SARSlavePtr = GetParameterComplexFloatImage("insarslave");


	filterCoregistration->SetImageInput(SARSlavePtr);
	filterCoregistration->SetGridInput(GridPtr);

	// Output
	SetParameterOutputImage("out", filterCoregistration->GetOutput());
      }
      // Vector for filters 
      std::vector<itk::ProcessObject::Pointer> m_Ref;

    };

  }

}


 
OTB_APPLICATION_EXPORT(otb::Wrapper::SARCoRegistration)
