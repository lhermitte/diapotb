/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbImageFileReader.h"
#include "otbSARDEMProjectionImageFilter.h"
#include "otbSARStreamingDEMInformationFilter.h"
#include "otbSARStreamingDEMCheckFilter.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{
namespace Wrapper
{

class SARDEMProjection : public Application
{
public:
  typedef SARDEMProjection Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARDEMProjection, otb::Wrapper::Application);

  // Filters
  typedef otb::SARDEMProjectionImageFilter<FloatImageType, FloatVectorImageType> DEMFilterType;
  typedef otb::SARStreamingDEMInformationFilter<FloatImageType> DEMInfoFilterType;
  typedef otb::SARStreamingDEMCheckFilter<FloatVectorImageType> DEMCheckFilterType;
  

private:
  void DoInit() override
  {
    SetName("SARDEMProjection");
    SetDescription("Projects a DEM into SAR geometry.");

    SetDocLongDescription("This application puts a DEM file into SAR geometry"
      " and estimates two additional coordinates.\nIn all for each point of "
      "the DEM input four components are calculated : C (colunm into SAR "
      "image), L (line into SAR image), Z and Y.");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "indem",   "Input DEM");
    SetParameterDescription("indem", "DEM to perform projection on.");

    AddParameter(ParameterType_InputImage,  "insar",   "Input SAR image");
    SetParameterDescription("insar", "SAR Image to extract SAR geometry.");

    AddParameter(ParameterType_InputFilename, "infilemetadata", "Input file to get precise metadata");
    SetParameterDescription("infilemetadata","Input file to get precise metadata.");
    MandatoryOff("infilemetadata");

    // Empty parameters to allow more components into the projeted DEM
    AddParameter(ParameterType_Bool, "withh", "Set H components into projection");
    SetParameterDescription("withh", "If true, then H components into projection.");
    
    AddParameter(ParameterType_Bool, "withxyz", "Set XYZ Cartesian components into projection");
    SetParameterDescription("withxyz", "If true, then XYZ Cartesian components into projection.");
    
    AddParameter(ParameterType_Bool, "withsatpos", "Set SatPos components into projection");
    SetParameterDescription("withsatpos", "If true, then SatPos components into projection.");

    AddParameter(ParameterType_Int,  "nodata", "No Data values for the DEM");
    SetParameterDescription("nodata", "No Data values for the DEM.");
    SetDefaultParameterInt("nodata", -32768);
    MandatoryOff("nodata");

    AddParameter(ParameterType_OutputImage, "out", "Output Vector Image");
    SetParameterDescription("out","Output Vector Image (C,L,Z,Y).");

    AddParameter(ParameterType_Float,"gain","gain value (output of this application)");
    SetParameterDescription("gain", "Gain value.");
    SetParameterRole("gain", Role_Output);
    SetDefaultParameterFloat("gain", 0);

    AddParameter(ParameterType_Int,"directiontoscandemc","direction in range into DEM geometry to across SAR geometry from Near range to Far range (output of this applicxation)");
    SetParameterDescription("directiontoscandemc", "Range direction for DEM scan.");
    SetParameterRole("directiontoscandemc", Role_Output);
    SetDefaultParameterInt("directiontoscandemc", 1);

    AddParameter(ParameterType_Int,"directiontoscandeml","direction in range into DEM geometry to across SAR geometry from Near range to Far range (output of this application)");
    SetParameterDescription("directiontoscandeml", "Azimut direction for DEM scan.");
    SetParameterRole("directiontoscandeml", Role_Output);
    SetDefaultParameterInt("directiontoscandeml", 1);

    AddRAMParameter();

    SetDocExampleParameterValue("indem","S21E055.hgt");
    SetDocExampleParameterValue("insar","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff");
    SetDocExampleParameterValue("out","S21E55_proj.tif");
  }

  void DoUpdateParameters() override
  {
// Nothing to do here : all parameters are independent
  }

void DoExecute() override
  {  
    // Get No data value
    int noData = GetParameterInt("nodata");
    otbAppLogINFO(<<"No Data : "<<noData);
        
  // Start the first pipeline (Read SAR image metedata)
  ComplexFloatImageType::Pointer SARPtr = GetParameterComplexFloatImage("insar");

  // Add into SAR Image keyword list, the precise metadata
  std::string inFile = GetParameterString("infilemetadata");

  if (!inFile.empty())
    {
      std::ifstream preciseMetadataFile(inFile);

      if (preciseMetadataFile.is_open())
	{
	  std::string line;
	  std::string delimiter = " : ";
	  std::string token;
	  std::string value;

	  otb::ImageKeywordlist sarKWL = SARPtr->GetImageKeywordlist();
	  
	  while ( std::getline (preciseMetadataFile,line) )
	    {
	      token = line.substr(0, line.find(delimiter));
	      value = line.substr(line.find(delimiter) + delimiter.length(), line.length());

	      if (token == "PRECISE TIME OF FIRST LINE")
		{
		  sarKWL.AddKey("header.first_line_time", value);
		  sarKWL.AddKey("support_data.first_line_time ", value);
		}
	      else if (token == "PRECISE SLANT RANGE")
		{
		  sarKWL.AddKey("support_data.slant_range_to_first_pixel", value);
		}
	      else
		{
		  otbAppLogWARNING(<<"Wrong Metadata into input file ");
		}
	      
	    }
	  
	  preciseMetadataFile.close();

	  // Uodate the SAR KeywordList
	  otbAppLogINFO(<<"SAR KeyWordList is updated with precise metadata");
	  SARPtr->SetImageKeywordList(sarKWL);
	}
    }
  

  // DEMProjection Filter
  DEMFilterType::Pointer filterDEMProjection = DEMFilterType::New();
  m_Ref.push_back(filterDEMProjection.GetPointer());
  filterDEMProjection->SetSARImageKeyWorList(SARPtr->GetImageKeywordlist());
  
  // Streaming Gain Filter
  DEMInfoFilterType::Pointer filterStreamingDEMInfo = DEMInfoFilterType::New();
  m_Ref.push_back(filterStreamingDEMInfo.GetPointer());
  filterStreamingDEMInfo->SetSARImageKeyWorList(SARPtr->GetImageKeywordlist());

  // Streaming Check Filter (To check if DEM cover the SAR Image)
  DEMCheckFilterType::Pointer filterStreamingDEMCheck = DEMCheckFilterType::New();
  m_Ref.push_back(filterStreamingDEMCheck.GetPointer());
  
  int sizeC = SARPtr->GetLargestPossibleRegion().GetSize()[0];
  int sizeL = SARPtr->GetLargestPossibleRegion().GetSize()[1];
  int originC = static_cast<int>(SARPtr->GetOrigin()[0] - 0.5);
  int originL = static_cast<int>(SARPtr->GetOrigin()[1] - 0.5);

  filterStreamingDEMCheck->setSARSizeAndOrigin(sizeC, sizeL, originC, originL);

  // Define the main pipeline 
  FloatImageType::Pointer inputDEM =  GetParameterFloatImage("indem");
  
  filterDEMProjection->SetInput(inputDEM);
  if (IsParameterEnabled("withh"))
    {
      filterDEMProjection->SetwithH(true);
    }
  if (IsParameterEnabled("withxyz"))
    {
      filterDEMProjection->SetwithXYZ(true);
    }
  if (IsParameterEnabled("withsatpos"))
    {
      filterDEMProjection->SetwithSatPos(true);
    }

  filterDEMProjection->SetNoData(noData);


  
  // Define the pipeline to estimate the gain
  filterStreamingDEMInfo->SetInput(inputDEM);
  // Adapt streaming with ram parameter (default 256 MB)
  filterStreamingDEMInfo->GetStreamer()->SetAutomaticStrippedStreaming(GetParameterInt("ram"));
  filterStreamingDEMInfo->Update();
  
  double gain;
  int directionDEM_forlines, directionDEM_forcolunms; 
  filterStreamingDEMInfo->GetDEMInformation(gain, directionDEM_forcolunms, directionDEM_forlines);
  
  
  // Check if DEM cover SAR Image (Counter must be > 0)
  filterStreamingDEMCheck->SetInput(filterDEMProjection->GetOutput());
  // Adapt streaming with ram parameter (default 256 MB)
  filterStreamingDEMCheck->GetStreamer()->SetAutomaticStrippedStreaming(GetParameterInt("ram"));
  filterStreamingDEMCheck->Update();
  long int counterOfDEMPts_intoSARGeo = 0;
  filterStreamingDEMCheck->GetDEMCheck(counterOfDEMPts_intoSARGeo);
  
  if (counterOfDEMPts_intoSARGeo <= 0)
    {
      std::cout << "Input DEM does not cover the SAR geometry. Please check your DEM" << std::endl;
      otbAppLogWARNING(<<"Input DEM does not cover the SAR geometry");
    }

  // Output
  SetParameterOutputImage("out", filterDEMProjection->GetOutput());

  // Assigne Gain and Directions
  SetParameterFloat("gain", gain);
  SetParameterInt("directiontoscandemc",  directionDEM_forcolunms);
  SetParameterInt("directiontoscandeml",  directionDEM_forlines);
}
  // Vector for filters 
  std::vector<itk::ProcessObject::Pointer> m_Ref;

};


}

}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARDEMProjection)
