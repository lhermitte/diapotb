/*
 * Copyright (C) 2005-2018 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbWrapperCompositeApplication.h"
#include "otbWrapperApplicationFactory.h"

#include <iostream>
#include <string>
#include <fstream>

namespace otb
{
namespace Wrapper
{

class SARDEMToAmplitude : public CompositeApplication
{
public:
  typedef SARDEMToAmplitude Self;
  typedef itk::SmartPointer<Self> Pointer; 

  itkNewMacro(Self);
  itkTypeMacro(SARDEMToAmplitude, otb::Wrapper::CompositeApplication);

private:
  void DoInit() override
  {
    // Clear all internal applications
    ClearApplications(); 	
       
    // Add the three internal applications
    AddApplication("SARDEMProjection", "DEMProjApp", "Application for DEM projection");
    AddApplication("SARAmplitudeEstimation", "AmplitudeEstimationApp", "Application for amplitude estimation");
    AddApplication("SARMultiLook", "MultiLookApp", "Application for MultiLook");
     
    // SARDEMToAmplitude description
    SetName("SARDEMToAmplitude");
    SetDescription("Simu_SAR step for Diapason chain.");

    SetDocLongDescription("This application executes the Simu_SAR step with "
      "three internal applications : SARDEMProjection, SARAmplitudeEstimation"
      " and SARMultiLook.");

    //Optional descriptors
    SetDocLimitations("Only Sentinel 1 (IW and StripMap mode) and Cosmo products are supported for now.");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::SAR);
    AddDocTag("DiapOTB");

    //Parameter declarations
    AddParameter(ParameterType_InputImage,  "indem",   "Input DEM");
    SetParameterDescription("indem", "DEM to extract DEM geometry.");

    AddParameter(ParameterType_InputImage,  "insar",   "Input SAR image");
    SetParameterDescription("insar", "SAR Image to extract SAR geometry.");

    AddParameter(ParameterType_OutputImage,  "out",   "Output simulated amplitude Image (this output image is multilooked)");
    SetParameterDescription("out", "Output simulated amplitude Image (this output image is multilooked).");
    
    AddRAMParameter();

    AddParameter(ParameterType_Int,  "nodata", "No Data values for the DEM");
    SetParameterDescription("nodata", "No Data values for the DEM.");
    SetDefaultParameterInt("nodata", -32768);
    MandatoryOff("nodata");

    AddParameter(ParameterType_Int, "mlran", "Averaging on distance");
    SetParameterDescription("mlran","Averaging on distance");
    SetDefaultParameterInt("mlran", 3);
    SetMinimumParameterIntValue("mlran", 1);
    MandatoryOff("mlran");
    
    AddParameter(ParameterType_Int, "mlazi", "Averaging on azimut");
    SetParameterDescription("mlazi","Averaging on azimut");
    SetDefaultParameterInt("mlazi", 3);
    SetMinimumParameterIntValue("mlazi", 1);
    MandatoryOff("mlazi");

    AddParameter(ParameterType_Float, "mlgain", "Gain to apply on ML image");
    SetParameterDescription("mlgain","Gain to apply on ML image");
    SetDefaultParameterFloat("mlgain", 1);
    SetMinimumParameterFloatValue("mlgain", 0);
    MandatoryOff("mlgain");
    
    SetDocExampleParameterValue("indem","S21E055.hgt");
    SetDocExampleParameterValue("insar","s1a-s4-slc-vv-20160818t014650-20160818t014715-012648-013db1-002_SLC.tiff");
    SetDocExampleParameterValue("out","s1a-s4-simu-amplitude.tiff");

    // Connexion between internal application (not image parameters with Connect)
    Connect("DEMProjApp.gain","AmplitudeEstimationApp.gain");
    Connect("DEMProjApp.directiontoscandemc","AmplitudeEstimationApp.directiondemc");
    Connect("DEMProjApp.directiontoscandeml","AmplitudeEstimationApp.directiondeml");
 }

  void DoUpdateParameters() override
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute() override
  {
    // Get No data value
    int noData = GetParameterInt("nodata");
    otbAppLogINFO(<<"No Data : "<<noData);

    // Get Input Images
    FloatImageType::Pointer inputDEM = GetParameterFloatImage("indem");
    ComplexFloatImageType::Pointer inputSAR = GetParameterComplexFloatImage("insar");

    // Get numeric parameters
    int factor_azi = GetParameterInt("mlazi");
    int factor_ran = GetParameterInt("mlran");
    double factor_gain = GetParameterFloat("mlgain");
     
    // Controlled Pipeline with override of out FileName
    std::string origin_FileName = GetParameterString("out");

    // Check if FileName is already extended (with the ? caracter)
    // If not extended then override the FileName
    if (origin_FileName.find("?") == std::string::npos && !origin_FileName.empty()) 
      {
	std::string extendedFileName = origin_FileName;

	// Get the ram value (in MB)
	int ram = GetParameterInt("ram");
	
	// Define with the ram value, the number of lines for the streaming
	int nbColSAR = inputSAR->GetLargestPossibleRegion().GetSize()[0];
	int nbLinesSAR = inputSAR->GetLargestPossibleRegion().GetSize()[1];
	// To determine the number of lines : 
	// nbColSAR * nbLinesStreaming * sizeof(OutputPixel) = RamValue/2 (/2 to be sure)
	// OutputPixel are float => sizeof(OutputPixel) = 4 (Bytes)
	long int ram_In_KBytes = (ram/2) * 1024;
	long int nbLinesStreaming = (ram_In_KBytes / (nbColSAR)) * (1024/4);
	
	// Check the value of nbLinesStreaming
	int nbLinesStreamingMax = 10000;
	if (nbLinesStreamingMax > nbLinesSAR)
	  {
	    nbLinesStreamingMax = nbLinesSAR;
	  }
	if (nbLinesStreaming <= 0 || nbLinesStreaming >  nbLinesStreamingMax)
	  {
	    nbLinesStreaming =  nbLinesStreamingMax;
	  } 
	
	// Construct the extendedPart
	std::ostringstream os;
	os << "?&streaming:type=stripped&streaming:sizevalue=" << nbLinesStreaming;

	// Add the extendedPart
	std::string extendedPart = os.str();
	extendedFileName.append(extendedPart);
	
	// Set the new FileName with extended options
	SetParameterString("out", extendedFileName);
      }   

    // Execute the Pipelone with internal applications
    // First application : SARDEMProjection
    GetInternalApplication("DEMProjApp")->SetParameterInputImage("indem", inputDEM);
    GetInternalApplication("DEMProjApp")->SetParameterInputImage("insar", inputSAR);
    GetInternalApplication("DEMProjApp")->SetParameterInt("nodata", noData);
    ExecuteInternal("DEMProjApp");

    // Second application : SARApplicationEstimation
    GetInternalApplication("AmplitudeEstimationApp")->SetParameterInputImage("indem", inputDEM);
    GetInternalApplication("AmplitudeEstimationApp")->SetParameterInputImage("insar", inputSAR);
    GetInternalApplication("AmplitudeEstimationApp")->SetParameterInt("nodata", noData);
    GetInternalApplication("AmplitudeEstimationApp")->SetParameterInputImage("indemproj", 
									     GetInternalApplication("DEMProjApp")->GetParameterOutputImage("out"));
    ExecuteInternal("AmplitudeEstimationApp");
    
    // Third (and last) application : SARMultiLook   
    GetInternalApplication("MultiLookApp")->SetParameterInputImage("inreal", 
								   GetInternalApplication("AmplitudeEstimationApp")->GetParameterOutputImage("out"));
    GetInternalApplication("MultiLookApp")->SetParameterInt("mlran", factor_ran);
    GetInternalApplication("MultiLookApp")->SetParameterInt("mlazi", factor_azi);
    GetInternalApplication("MultiLookApp")->SetParameterFloat("mlgain", factor_gain);
    ExecuteInternal("MultiLookApp");
    
    // Retrive the output of MultiLookApp
    SetParameterOutputImage("out", GetInternalApplication("MultiLookApp")->GetParameterOutputImage("out"));

  }
  // Vector for filters 
  std::vector<itk::ProcessObject::Pointer> m_Ref;

};


}

}

OTB_APPLICATION_EXPORT(otb::Wrapper::SARDEMToAmplitude)
